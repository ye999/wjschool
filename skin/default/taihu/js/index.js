// JavaScript Document

var BarWidth;
var BarHeight;

var BannerImgWidth = 1920;
var BannerImgHeight = 600;

var IntervalCount = 0;
var onliCount = 0;
//var onnavCount = 0;

var stouchX = 0;

var etouchX = 0;

var xlbnum=1;
var xlbintcount=0;
function next_xlb(){
	"use strict";
	if(xlbnum===$(".x_lb_tt>li").length){xlbnum=0;}
	$(".x_lb_tb>ul>li").removeClass("on_x_lb_tb");
	$(".x_lb_tb>ul>li").eq(xlbnum).addClass("on_x_lb_tb");
	$(".x_lb_tt>li").fadeOut();
	$(".x_lb_tt>li").eq(xlbnum).fadeIn();
	
	$(".x_lb_b>ul>li").fadeOut();
	$(".x_lb_b>ul>li").eq(xlbnum).fadeIn();
	xlbnum++;
}


function runBannerReturn() {
    "use strict";
    $(".banner>ul").find("li:eq(" + onliCount + ")").removeClass("onli");
    $(".banner").find("img:eq(" + onliCount + ")").fadeOut();
    if (onliCount === ($(".banner>img").length - 1)) {
        $(".banner").find("img:eq(0)").fadeIn();
        $(".banner>ul").find("li:eq(0)").addClass("onli");
        onliCount = 0;
    } else {
        $(".banner").find("img:eq(" + (onliCount + 1) + ")").fadeIn();
        $(".banner>ul").find("li:eq(" + (onliCount + 1) + ")").addClass("onli");
        onliCount += 1;
    }
}

function runInterval() {
    "use strict";
    IntervalCount = window.setInterval(function () {
        runBannerReturn();
    }, 2500);
}

function runxlb(){
	"use strict";
	xlbintcount=window.setInterval(function(){
		next_xlb();
	},4500);
}
function setBanner() {
    "use strict";
    BarWidth = $(".dc").width();
    //BarWidth= $(window).width();
    BarHeight = $(window).height();
    //onnavCount=$(".D_head_nav").find(".onnav").index();
    //alert(BarWidth+":"+BarHeight);
    //if($("body").height()<)
    $(".banner").css("height", BannerImgHeight * (BarWidth / BannerImgWidth));
    $(".banner>ul").css("margin-left", -($(".banner>ul").width() / 2));
}

function run_BannerReturn() {
    "use strict";
    $(".banner>ul").find("li:eq(" + onliCount + ")").removeClass("onli");
    $(".banner").find("img:eq(" + onliCount + ")").fadeOut();
    if (onliCount <= 0) {
        $(".banner").find("img:eq(" + ($(".banner>img").length - 1) + ")").fadeIn();
        $(".banner>ul").find("li:eq(" + ($(".banner>img").length - 1) + ")").addClass("onli");
        onliCount = ($(".banner>img").length - 1);
    } else {
        $(".banner").find("img:eq(" + (onliCount - 1) + ")").fadeIn();
        $(".banner>ul").find("li:eq(" + (onliCount - 1) + ")").addClass("onli");
        onliCount -= 1;
    }
}


$(document).ready(function () {
    "use strict";
	
	//	$(".x_lb_tt>")
	$(".x_lb_tb>ul>li").on("click",function(){
		xlbnum=$(this).index();
		next_xlb();
	});
	
	$(".x_lb").on("mouseenter",function(){
		window.clearInterval(xlbintcount);
		xlbintcount=0;
	});
	$(".x_lb").on("mouseleave",function(){
		runxlb();
	});
	
	
	$(".header>ul>li").on("mouseenter",function(){
		$(this).find(">ul").slideDown();
	});
	$(".header>ul>li").on("mouseleave",function(){
		$(this).find(">ul").slideUp();
	});
	$(".nav_btn>img").on("click",function(){
		if($(this).next().is(":animated")){return false;}
		$(this).next().slideToggle();
	});
	$(".nav").on("mouseleave",function(){
		$(this).slideUp();
	});
	$(".nav>li>span").on("click",function(){
		//var $nthis = $(this);
		if($(this).next().html()===undefined){
		}else{
			if($(this).next().css("display")==="block"){	
				$(this).next().slideUp(function(){
					$(this).parent().find("img").prop("src","images/bot_icon.png");
				});
				//$(this).parent().css("border-bottom","1px solid #bcbcbc");
			}else{
				//$(this).parent().siblings().css("border-bottom","1px solid #bcbcbc");
				$(this).parent().siblings().find("img").prop("src","images/bot_icon.png");
				$(this).parent().siblings().find("ul").slideUp();
				
				$(this).next().slideDown(function(){
					$(this).parent().find("img").prop("src","images/top_icon.png");
				});
				//$(this).parent().css("border","0");
			}
		}

	});
	



	$(".xyzx_c_l_title>div").on("click",function(){
		var ti=$(this).index();
			$(this).siblings().removeClass("ontitle");
			$(this).addClass("ontitle");
		if(ti===1){
			$(".ontitle0").hide();
			$(".ontitle1").show();}
		
		if(ti===0){
			$(".ontitle1").hide();
			$(".ontitle0").show();}
	});
	
	
    //banner
    $(".banner").on("touchstart", function (e) {
        window.clearInterval(IntervalCount);
        IntervalCount = 0;
        stouchX = e.originalEvent.targetTouches[0].pageX;
        //touchstart    e.originalEvent.targetTouches[0].pageX
        //touchend     e.originalEvent.changedTouches[0].pageX
        //alert(stouchX);

    });

    $(".banner").on("touchend", function (e) {

        etouchX = e.originalEvent.changedTouches[0].pageX;

        if (etouchX === stouchX) {
            //点击
            return false;
        } else if (etouchX > stouchX) {
            //alert("右滑");
            run_BannerReturn();
        } else {
            //alert("左滑");
            runBannerReturn();
        }
        if (IntervalCount === 0) {
            runInterval();
        }
    });


    $(".banner>ul>li").on("click", function () {
        onliCount = $(this).index();
        if ($(this).parent().find(".onli").index() === onliCount) {
            return false;
        }
        $(".banner").find("img:eq(" + $(this).parent().find(".onli").index() + ")").fadeOut();
        $(this).addClass("onli").siblings().removeClass("onli");
        $(".banner").find("img:eq(" + onliCount + ")").fadeIn();
    });
    $(".banner").on("mouseenter", function () {
        window.clearInterval(IntervalCount);
    });
    $(".banner").on("mouseleave", function () {
        runInterval();
    });
    $(".banner>img").css("position", "absolute");
    $(window).resize(function () {
        setBanner();
    });
    runInterval();
    setBanner();
    //banner
});