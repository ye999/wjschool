﻿// JavaScript Document
var BarWidth; 
var BarHeight;

var BannerImgWidth = 1920;
var BannerImgHeight = 600;

var IntervalCount=0;
var onliCount=0;
var onnavCount = 0;

var stouchX=0;

var etouchX=0;

$(function (){
	"use strict";
	
	
	//$(".D_news_left_img").css({"height":$(".D_news_left_text").height(),
	//							"line-height":$(".D_news_left_text").height()+"px"});
	//$(".D_news_left_img>a").each(function(){
	//	if($.trim($(this).html())===""){
	//		$(this).parent().next().css("width","100%");
	//		$(this).parent().css({"height":0,
	//							"line-height":0});
	//	}
	//});
	$(".D_banner").on("touchstart",function(e){
		
		
		window.clearInterval(IntervalCount);
		IntervalCount=0;
		stouchX=e.originalEvent.targetTouches[0].pageX;
		//touchstart    e.originalEvent.targetTouches[0].pageX
		//touchend     e.originalEvent.changedTouches[0].pageX
		//alert(stouchX);
		
	});

	$(".D_banner").on("touchend",function(e){
		
		etouchX=e.originalEvent.changedTouches[0].pageX;
		if(etouchX>stouchX){
			//alert("右滑");
			run_BannerReturn();
		}else{
			//alert("左滑");
			runBannerReturn();
		}
		if(IntervalCount===0){runInterval();}
	});

	
	$(".D_bottom_links_btn").on("click",function (){
		$(".D_bottom_links>ul").fadeToggle();
	});	
	$(".D_bottom_links").on("mouseleave",function (){
		$(".D_bottom_links>ul").fadeOut();
	});	
	$(".D_banner>ul>li").on("click",function (){
		onliCount=$(this).index();
		if($(this).parent().find(".onli").index()===onliCount){return false;}
		$(".D_banner").find("img:eq("+$(this).parent().find(".onli").index()+")").fadeOut();
		$(this).addClass("onli").siblings().removeClass("onli");
		$(".D_banner").find("img:eq("+onliCount+")").fadeIn();
	});	
	$(".D_head_nav_img").on("click",function(){
		$(this).parent().find("ul").slideToggle();
	});
	$(".D_head_nav>ul>li").on("mouseenter",function (){
		if(onnavCount!==$(this).index()){$(this).addClass("onnav");}
		$(this).find("dl").stop().slideDown();
	});
	$(".D_head_nav>ul>li").on("mouseleave",function (){
		if(onnavCount!==$(this).index()){$(this).removeClass("onnav");}
		$(this).find("dl").stop().slideUp();
	});
	$(".D_banner").on("mouseenter",function (){
		window.clearInterval(IntervalCount);
	});
	$(".D_banner").on("mouseleave",function (){
		runInterval();
	});
	$(window).resize(function() {
		setBanner();
	});
	runInterval();
	setBanner();
});
function runInterval(){
	"use strict";
	IntervalCount=window.setInterval(function(){
		runBannerReturn();
	},5500);
}
function setBanner(){
	"use strict";
	BarWidth= $(".D").width();//$(window).width();
	BarHeight= $(window).height();
	onnavCount=$(".D_head_nav").find(".onnav").index();
	//alert(BarWidth+":"+BarHeight);
	
	$(".D_banner").css("height",BannerImgHeight*(BarWidth/BannerImgWidth));
	$(".D_banner>ul").css("margin-left",-($(".D_banner>ul").width()/2));
}
function runBannerReturn(){
	"use strict";
	$(".D_banner>ul").find("li:eq("+onliCount+")").removeClass("onli");
	$(".D_banner").find("img:eq("+onliCount+")").fadeOut();
	if(onliCount===($(".D_banner>img").length-1)){
		$(".D_banner").find("img:eq(0)").fadeIn();
		$(".D_banner>ul").find("li:eq(0)").addClass("onli");
		onliCount=0;
	}else{
		$(".D_banner").find("img:eq("+(onliCount+1)+")").fadeIn();
		$(".D_banner>ul").find("li:eq("+(onliCount+1)+")").addClass("onli");
		onliCount++;	
	}
}

function run_BannerReturn(){
	"use strict";
	$(".D_banner>ul").find("li:eq("+onliCount+")").removeClass("onli");
	$(".D_banner").find("img:eq("+onliCount+")").fadeOut();
	if(onliCount<=0){
		$(".D_banner").find("img:eq("+($(".D_banner>img").length-1)+")").fadeIn();
		$(".D_banner>ul").find("li:eq("+($(".D_banner>img").length-1)+")").addClass("onli");
		onliCount=($(".D_banner>img").length-1);
	}else{
		$(".D_banner").find("img:eq("+(onliCount-1)+")").fadeIn();
		$(".D_banner>ul").find("li:eq("+(onliCount-1)+")").addClass("onli");
		onliCount--;	
	}
}