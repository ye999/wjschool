// JavaScript Document
var BarWidth;
var BarHeight;

var BannerImgWidth = 1920;
var BannerImgHeight = 600;

var IntervalCount = 0;
var onliCount = 0;
//var onnavCount = 0;

var stouchX = 0;

var etouchX = 0;




function runBannerReturn() {
    "use strict";
    $(".banner>ul").find("li:eq(" + onliCount + ")").removeClass("onli");
    $(".banner").find("img:eq(" + onliCount + ")").fadeOut();
    if (onliCount === ($(".banner>img").length - 1)) {
        $(".banner").find("img:eq(0)").fadeIn();
        $(".banner>ul").find("li:eq(0)").addClass("onli");
        onliCount = 0;
    } else {
        $(".banner").find("img:eq(" + (onliCount + 1) + ")").fadeIn();
        $(".banner>ul").find("li:eq(" + (onliCount + 1) + ")").addClass("onli");
        onliCount += 1;
    }
}

function runInterval() {
    "use strict";
    IntervalCount = window.setInterval(function () {
        runBannerReturn();
    }, 2500);
}

function setBanner() {
    "use strict";
    BarWidth = $(".dc").width();
    //BarWidth= $(window).width();
    BarHeight = $(window).height();
    //onnavCount=$(".D_head_nav").find(".onnav").index();
    //alert(BarWidth+":"+BarHeight);
    //if($("body").height()<)
    $(".banner").css("height", BannerImgHeight * (BarWidth / BannerImgWidth));
    $(".banner>ul").css("margin-left", -($(".banner>ul").width() / 2));
}

function run_BannerReturn() {
    "use strict";
    $(".banner>ul").find("li:eq(" + onliCount + ")").removeClass("onli");
    $(".banner").find("img:eq(" + onliCount + ")").fadeOut();
    if (onliCount <= 0) {
        $(".banner").find("img:eq(" + ($(".banner>img").length - 1) + ")").fadeIn();
        $(".banner>ul").find("li:eq(" + ($(".banner>img").length - 1) + ")").addClass("onli");
        onliCount = ($(".banner>img").length - 1);
    } else {
        $(".banner").find("img:eq(" + (onliCount - 1) + ")").fadeIn();
        $(".banner>ul").find("li:eq(" + (onliCount - 1) + ")").addClass("onli");
        onliCount -= 1;
    }
}


$(document).ready(function () {
    "use strict";
    //banner
    $(".banner").on("touchstart", function (e) {
        window.clearInterval(IntervalCount);
        IntervalCount = 0;
        stouchX = e.originalEvent.targetTouches[0].pageX;
        //touchstart    e.originalEvent.targetTouches[0].pageX
        //touchend     e.originalEvent.changedTouches[0].pageX
        //alert(stouchX);

    });

    $(".banner").on("touchend", function (e) {

        etouchX = e.originalEvent.changedTouches[0].pageX;

        if (etouchX === stouchX) {
            //点击
            return false;
        } else if (etouchX > stouchX) {
            //alert("右滑");
            run_BannerReturn();
        } else {
            //alert("左滑");
            runBannerReturn();
        }
        if (IntervalCount === 0) {
            runInterval();
        }
    });


    $(".banner>ul>li").on("click", function () {
        onliCount = $(this).index();
        if ($(this).parent().find(".onli").index() === onliCount) {
            return false;
        }
        $(".banner").find("img:eq(" + $(this).parent().find(".onli").index() + ")").fadeOut();
        $(this).addClass("onli").siblings().removeClass("onli");
        $(".banner").find("img:eq(" + onliCount + ")").fadeIn();
    });
    $(".banner").on("mouseenter", function () {
        window.clearInterval(IntervalCount);
    });
    $(".banner").on("mouseleave", function () {
        runInterval();
    });
    $(".banner>img").css("position", "absolute");
    $(window).resize(function () {
        setBanner();
    });
    runInterval();
    setBanner();
    //banner
});