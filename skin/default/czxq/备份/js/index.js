var BarWidth; 
var BarHeight;

var BannerImgWidth = 1920;
var BannerImgHeight = 600;

var IntervalCount=0;
var onliCount=0;
//var onnavCount = 0;

var stouchX=0;

var etouchX=0;
$(document).ready(function() {
	"use strict";
	$(".nav_btn>img").on("click",function(){
		if($(this).next().is(":animated")){return false;}
		$(this).next().slideToggle();
	});
	$(".nav").on("mouseleave",function(){
		$(this).slideUp();
	});
	$(".nav>li>span").on("click",function(){
		//var $nthis = $(this);
		if($(this).next().html()===undefined){
		}else{
			if($(this).next().css("display")==="block"){	
				$(this).next().slideUp(function(){
					$(this).parent().find("img").prop("src","/skin/default/czxq/images/bot_icon.png");
				});
				//$(this).parent().css("border-bottom","1px solid #bcbcbc");
			}else{
				//$(this).parent().siblings().css("border-bottom","1px solid #bcbcbc");
				$(this).parent().siblings().find("img").prop("src","/skin/default/czxq/images/bot_icon.png");
				$(this).parent().siblings().find("ul").slideUp();
				
				$(this).next().slideDown(function(){
					$(this).parent().find("img").prop("src","/skin/default/czxq/images/top_icon.png");
				});
				//$(this).parent().css("border","0");
			}
		}

	});
		//banner
	$(".D_banner").on("touchstart",function(e){
		window.clearInterval(IntervalCount);
		IntervalCount=0;
		stouchX=e.originalEvent.targetTouches[0].pageX;
		//touchstart    e.originalEvent.targetTouches[0].pageX
		//touchend     e.originalEvent.changedTouches[0].pageX
		//alert(stouchX);
		
	});

	$(".D_banner").on("touchend",function(e){
		
		etouchX=e.originalEvent.changedTouches[0].pageX;
		if(etouchX>stouchX){
			//alert("右滑");
			run_BannerReturn();
		}else{
			//alert("左滑");
			runBannerReturn();
		}
		if(IntervalCount===0){runInterval();}
	});

	
	$(".D_banner>ul>li").on("click",function (){
		onliCount=$(this).index();
		if($(this).parent().find(".onli").index()===onliCount){return false;}
		$(".D_banner").find("img:eq("+$(this).parent().find(".onli").index()+")").fadeOut();
		$(this).addClass("onli").siblings().removeClass("onli");
		$(".D_banner").find("img:eq("+onliCount+")").fadeIn();
	});	
	$(".D_banner").on("mouseenter",function (){
		window.clearInterval(IntervalCount);
	});
	$(".D_banner").on("mouseleave",function (){
		runInterval();
	});
	$(".D_banner>img").css("position","absolute");
	$(window).resize(function() {
		setBanner();
	});
	runInterval();
	setBanner();
	//banner
});
function runInterval(){
	"use strict";
	IntervalCount=window.setInterval(function(){
		runBannerReturn();
	},2500);
}
function setBanner(){
	"use strict";
	BarWidth= $(".d_c").width();
	//BarWidth= $(window).width();
	BarHeight= $(window).height();
	//onnavCount=$(".D_head_nav").find(".onnav").index();
	//alert(BarWidth+":"+BarHeight);
	//if($("body").height()<)
	$(".D_banner").css("height",BannerImgHeight*(BarWidth/BannerImgWidth));
	$(".D_banner>ul").css("margin-left",-($(".D_banner>ul").width()/2));
}
function runBannerReturn(){
	"use strict";
	$(".D_banner>ul").find("li:eq("+onliCount+")").removeClass("onli");
	$(".D_banner").find("img:eq("+onliCount+")").fadeOut();
	if(onliCount===($(".D_banner>img").length-1)){
		$(".D_banner").find("img:eq(0)").fadeIn();
		$(".D_banner>ul").find("li:eq(0)").addClass("onli");
		onliCount=0;
	}else{
		$(".D_banner").find("img:eq("+(onliCount+1)+")").fadeIn();
		$(".D_banner>ul").find("li:eq("+(onliCount+1)+")").addClass("onli");
		onliCount++;	
	}
}

function run_BannerReturn(){
	"use strict";
	$(".D_banner>ul").find("li:eq("+onliCount+")").removeClass("onli");
	$(".D_banner").find("img:eq("+onliCount+")").fadeOut();
	if(onliCount<=0){
		$(".D_banner").find("img:eq("+($(".D_banner>img").length-1)+")").fadeIn();
		$(".D_banner>ul").find("li:eq("+($(".D_banner>img").length-1)+")").addClass("onli");
		onliCount=($(".D_banner>img").length-1);
	}else{
		$(".D_banner").find("img:eq("+(onliCount-1)+")").fadeIn();
		$(".D_banner>ul").find("li:eq("+(onliCount-1)+")").addClass("onli");
		onliCount--;	
	}
}