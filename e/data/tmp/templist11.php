<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/bjfc.css">
<script src="/skin/default/czxq/js/jq.js"></script>
<script src="/skin/default/czxq/js/index_2.js"></script>
<title>城中分校</title>
</head>

<body>
<div class="dc">
	<div class="header clear">
		<a href="/chengzhong/">
			<img src="/skin/default/czxq/images/logo.png" alt=""/>
		</a>
	</div>
	<div class="banner clear">
		<img src="/skin/default/czxq/images/0001.jpg" alt="" />
		<img src="/skin/default/czxq/images/0002.jpg" alt="" />
		<img src="/skin/default/czxq/images/0003.jpg" alt="" />
		<img src="/skin/default/czxq/images/0004.jpg" alt="" />
		<img src="/skin/default/czxq/images/0005.jpg" alt="" />
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
                        <li></li>
			<li></li>
		</ul>
	</div>
<div class="content  clear">
		<div class="meun">
			<div class="meun_title"><script language="javascript">var txt="[!--bclass.name--]"; if (txt=="") {txt="[!--class.name--]";}; document.write(txt);</script></div>
			<ul><li class="c_info_bg c_info_bg_dtl"></li>
				<? @sys_ShowClassByTemp('selfinfo',15,0,0);?>
			</ul>
		</div>
		<div class="detail_d">
			<div class="c_title detail_d_newstitle clear">
				<span>[!--class.name--]</span>
			</div>
			<div class="c_info detail_d_info">
				<ul>[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]
				</ul>
<div class="pages">[!--show.listpage--]</div>
			</div>
		</div>
	</div><div class="footer">
		<p>地址：江苏省吴江市松陵镇永康路84号  邮编：215200</p>
		<p>Copy(c) 吴江市实验小学2000-2016</p>
		<p>All Rights Reserved 苏ICP备10057875号</p>
	</div>
</div>
</body>
</html>
