<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/bjfc.css">
<script src="/skin/default/czxq/js/jq.js"></script>
<script src="/skin/default/czxq/js/index_2.js"></script>
<title>城中分校</title>
</head>

<body>
<div class="dc">
	<div class="header clear">
		<a href="/chengzhong/">
			<img src="/skin/default/czxq/images/logo.png" alt=""/>
		</a>
	</div>
	<div class="banner clear">
		<img src="/skin/default/czxq/images/0001.jpg" alt="" />
		<img src="/skin/default/czxq/images/0002.jpg" alt="" />
		<img src="/skin/default/czxq/images/0003.jpg" alt="" />
		<img src="/skin/default/czxq/images/0004.jpg" alt="" />
		<img src="/skin/default/czxq/images/0005.jpg" alt="" />
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
                        <li></li>
			<li></li>
		</ul>
	</div>
<div class="content  clear">
		<div class="webnav"><?=$grurl?></div>
		<div class="detail_dd">

						<div class="detail_d_title">
				<h2><?=$ecms_gr[title]?></h2><div class="c_info detail_d_info">
				<ul>
					<li class="c_info_bg c_info_bg_dtl"></li></ul></div>

				<div>
					<span>发布时间:
						<span><?=date('Y-m-d',$ecms_gr[newstime])?></span>
					</span>
					<span>点击量:
						<span><script src=/e/public/ViewClick/?classid=<?=$ecms_gr[classid]?>&id=<?=$ecms_gr[id]?>&addclick=1></script></span>
					</span>
				</div>
			</div>
			<div class="detail_d_p">
				<span><p><?=strstr($ecms_gr[newstext],'[!--empirenews.page--]')?'[!--newstext--]':$ecms_gr[newstext]?></p> </span>
			</div>
		</div>
	</div><div class="footer">
		<p>地址：江苏省吴江市松陵镇永康路84号  邮编：215200</p>
		<p>Copy(c) 吴江市实验小学2000-2016</p>
		<p>All Rights Reserved 苏ICP备10057875号</p>
	</div>
</div>
</body>
</html>
