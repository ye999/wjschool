<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<link rel="stylesheet" type="text/css" href="/skin/default/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/mediacss.css">
<script src="/skin/default/js/jq.js"></script>
<script src="/skin/default/js/id.js"></script>
<title>吴江实验小学</title>
</head>
<body>
<div class="D">
	<!--顶部-->
	<div class="D_bg top">
		<div class="D_top clear">
			<ul>
				<li>
					<a href="/">返回首页</a>
				</li>
				<li>
					<a href="###">速递邮局</a>
				</li>
				<li>
					<a href="###">办公OA</a>
				</li>
			</ul>
			<div>
				<a class="" href="###">常用信息</a>
				<div>
					<input class="D_top_text" type="text" value="" />
					<input class="D_top_btn" type="button" value="" />
				</div>
			</div>
		</div>
	</div>
	<!--logo部分,导航部分-->
	<div class="D_head clear">
		<div class="D_head_logo">
			<a href="index.html">
				<img src="/skin/default/images/logo.jpg" alt="">
			</a>
		</div>
		<div class="D_head_nav">
			<img  class="D_head_nav_img" src="/skin/default/images/nav1.png" alt="">
			<ul class="clear">
				<li  class="onnav">
					<a href="/">首页</a>
				</li>
				<li>
					<a href="javascript:;">关于我们</a>
					<dl><? @sys_ShowClassByTemp(1,13,0,0);?></dl>
				</li>
				<li>
					<a href="/group/">集团组成</a>
					<dl>
						<dd>
							<a href="/chengzhong/" target="_blank">城中校区</a>
						</dd>
						<dd>
							<a href="/aide/" target="_blank">爱德校区</a>
						</dd>
						<dd>
							<a href="/taihu/" target="_blank">太湖校区</a>
						</dd>
						<dd>
							<a href="/szw/" target="_blank">苏州湾校区</a>
						</dd>
						<dd>
							<a href="index.html">幼儿园</a>
						</dd>
						<dd>
							<a href="index.html">德音书院</a>
						</dd>
					</dl>
				</li>
				<li>
					<a href="javascript:;">新闻动态</a>
					<dl><? @sys_ShowClassByTemp(8,13,0,0);?></dl>
				</li>
				<li>
					<a href="javascript:;">教师频道</a>
					<dl>
						<dd>
							<a href="#">学术委员会</a>
						</dd>
						<dd>
							<a href="#">教育工会</a>
						</dd>
						<dd>
							<a href="#">德音讲坛</a>
						</dd>
						<dd>
							<a href="#">名师工作室</a>
						</dd>
						<dd>
							<a href="#">青年联盟</a>
						</dd>
<dd>
							<a href="#">党建天地</a>
						</dd>

					</dl>
				</li>
				<li>
					<a href="javascript:;">管理平台</a>
					<dl>
						<dd>
							<a href="#">文件阅办系统</a>
						</dd>
						<dd>
							<a href="#">校务管理系统</a>
						</dd>
						<dd>
							<a href="#">学生管理系统</a>
						</dd>
						<dd>
							<a href="#">课题管理系统</a>
						</dd>
					</dl>
				</li>
				<li>
					<a href="javascript:;">资源中心</a>
					<dl>
						<dd>
							<a href="#">网上图书馆</a>
						</dd>
						<dd>
							<a href="#">图视中心</a>
						</dd>
						<dd>
							<a href="#">求真半月刊</a>
						</dd>
						<dd>
							<a href="#">统计年鉴</a>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
<div class="D_banner clear">
		<img src="/skin/default/images/0001.jpg"  alt=""/>
		<img src="/skin/default/images/0002.jpg"  alt=""/>
		<img src="/skin/default/images/0003.jpg"  alt=""/>
		<img src="/skin/default/images/0004.jpg"  alt=""/>
		<img src="/skin/default/images/0005.jpg"  alt=""/>
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div><script type="text/javascript" src="http://api.map.baidu.com/api?key=&amp;v=1.1&amp;services=true"></script>
<div class="D_adcontent">
		<ul class="clear">
			<? @sys_ShowClassByTemp(1,1,0,0);?>
		</ul>
	</div>
	<div class="webnav">
		<?=$grurl?>
	</div>
	<div class="adbx">
		<div id="dituContent"></div>
	</div><div class="D_bg about">
		<div class="D_about">
			<ul class="clear">
				<li>
					<dl>
						<dt>关于我们</dt>
						<? @sys_ShowClassByTemp(1,13,0,0);?>
					</dl>
				</li>
				<li>
					<dl>
						<dt>集团组成</dt>
						<dd>
							<a href="/chengzhong/">城中校区</a>
						</dd>
						<dd>
							<a href="/aide/">爱德校区</a>
						</dd>
						<dd>
							<a href="/taihu/">太湖校区</a>
						</dd>
						<dd>
							<a href="index.html">苏州湾校区</a>
						</dd>
						<dd>
							<a href="index.html">幼儿园</a>
						</dd>
						<dd>
							<a href="index.html">德音书院</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>新闻动态</dt>
						<? @sys_ShowClassByTemp(8,13,0,0);?>					</dl>
				</li>
				<li>
					<dl>
						<dt>教师频道</dt>
						<dd>
							<a  href="index.html">学术委员会</a>
						</dd>
						<dd>
							<a href="index.html">教育工会</a>
						</dd>
						<dd>
							<a href="index.html">德音讲坛</a>
						</dd>
						<dd>
							<a href="index.html">名师工作室</a>
						</dd>
						<dd>
							<a href="index.html">青年联盟</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>管理平台</dt>
						<dd>
							<a href="index.html">文件阅办系统</a>
						</dd>
						<dd>
							<a href="index.html">校务管理系统</a>
						</dd>
						<dd>
							<a href="index.html">学生管理系统</a>
						</dd>
						<dd>
							<a href="index.html">课题管理系统</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>资源中心</dt>
						<dd>
							<a href="index.html">网上图书馆</a>
						</dd>
						<dd>
							<a href="index.html">图视中心</a>
						</dd>
						<dd>
							<a href="index.html">求真半月刊</a>
						</dd>
						<dd>
							<a href="index.html">统计年鉴</a>
						</dd>
					</dl>
				</li>
				<li class="li_dis">
					<dl>
						<dt>联系我们</dt>
						<dd>
							<h2>0512-63970028</h2>
						</dd>
						<dd>
							<h2>0512-63970028</h2>
						</dd>
						<dd>
							<span>E-mail：edu@wjsx.com</span>
						</dd>
						<dd>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
		<div class="D_bg bottom">
		<div class="D_bottom">
			<span>Copyright 2015 吴江实验小学 保留所有版权 苏ICP备10057875号</span>
			<div class="D_bottom_links">
				<span class="D_bottom_links_btn">相关连接</span>
				<ul><? @sys_GetEcmsInfo(13,10,32,0,0,3,0);?></ul>
			</div>
		</div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
    //创建和初始化地图函数：
    function initMap(){
        createMap();//创建地图
        setMapEvent();//设置地图事件
        addMapControl();//向地图添加控件
        addMarker();//向地图中添加marker
    }
    
    //创建地图函数：
    function createMap(){
        var map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图
        var point = new BMap.Point(120.654933,31.157809);//定义一个中心点坐标
        map.centerAndZoom(point,13);//设定地图的中心点和坐标并将地图显示在地图容器中
        window.map = map;//将map变量存储在全局
    }
    
    //地图事件设置函数：
    function setMapEvent(){
        map.enableDragging();//启用地图拖拽事件，默认启用(可不写)
		
		//map.disableDragging();     //禁止拖拽
        map.enableScrollWheelZoom();//启用地图滚轮放大缩小
        map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)
        map.enableKeyboard();//启用键盘上下左右键移动地图
    }
    
    //地图控件添加函数：
    function addMapControl(){
        //向地图中添加缩放控件
	var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
	map.addControl(ctrl_nav);
        //向地图中添加缩略图控件
	var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
	map.addControl(ctrl_ove);
        //向地图中添加比例尺控件
	var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
	map.addControl(ctrl_sca);
    }
    
    //标注点数组
    var markerArr = [{title:"太湖校区",content:"联系方式：63452079",point:"120.634488|31.145772",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}
		 ,{title:"爱德校区",content:"联系方式：",point:"120.640524|31.155314",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}
		 ,{title:"城中校区",content:"联系方式：",point:"120.647333|31.168872",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}
		 ];
    //创建marker
    function addMarker(){
        for(var i=0;i<markerArr.length;i++){
            var json = markerArr[i];
            var p0 = json.point.split("|")[0];
            var p1 = json.point.split("|")[1];
            var point = new BMap.Point(p0,p1);
			var iconImg = createIcon(json.icon);
            var marker = new BMap.Marker(point,{icon:iconImg});
			var iw = createInfoWindow(i);
			var label = new BMap.Label(json.title,{"offset":new BMap.Size(json.icon.lb-json.icon.x+10,-20)});
			marker.setLabel(label);
            map.addOverlay(marker);
            label.setStyle({
                        borderColor:"#808080",
                        color:"#333",
                        cursor:"pointer"
            });
			
			(function(){
				var index = i;
				var _iw = createInfoWindow(i);
				var _marker = marker;
				_marker.addEventListener("click",function(){
				    this.openInfoWindow(_iw);
			    });
			    _iw.addEventListener("open",function(){
				    _marker.getLabel().hide();
			    })
			    _iw.addEventListener("close",function(){
				    _marker.getLabel().show();
			    })
				label.addEventListener("click",function(){
				    _marker.openInfoWindow(_iw);
			    })
				if(!!json.isOpen){
					label.hide();
					_marker.openInfoWindow(_iw);
				}
			})()
        }
    }
    //创建InfoWindow
    function createInfoWindow(i){
        var json = markerArr[i];
        var iw = new BMap.InfoWindow("<b class='iw_poi_title' title='" + json.title + "'>" + json.title + "</b><div class='iw_poi_content'>"+json.content+"</div>");
        return iw;
    }
    //创建一个Icon
    function createIcon(json){
        var icon = new BMap.Icon("http://app.baidu.com/map/images/us_mk_icon.png", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowOffset:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})
        return icon;
    }
    
    initMap();//创建和初始化地图
</script>