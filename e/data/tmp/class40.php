<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<title>太湖校区</title>
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/style_mld.css">
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/mediacss.css">
<script src="/skin/default/taihu/js/jq.js"></script>
<script src="/skin/default/taihu/js/index.js"></script>
</head>
<body>
<div class="dc">
	<div class="header_bg">
		<div class="header clear">
			<a href="index.html">
			<img src="/skin/default/taihu/images/logo.png" alt=""/>
			</a>
			<ul class="clear">
				<li>
					<a href="/taihu/">首 页</a>
				</li>
				<li>
					<a href="/taihu/about/">关于我们</a>
				</li>
				<li>
					<a href="/taihu/kcdiy/">德育天地</a>
				</li>
				<li>
					<a href="/taihu/jspost/">教育科研</a>
				</li>
				<li>
					<a href="/taihu/news/">时时关注</a><ul><? @sys_ShowClassByTemp(41,1,0,0);?></ul>
				</li>
				<li>
					<a href="/taihu/bj/">教学之窗</a>
				</li>
				<li>
					<a href="/taihu/xlspa/">精致校园</a>
				</li>
			</ul>
			<div class="nav_btn">
				<img src="/skin/default/taihu/images/nav_btn.png" alt=""/>
				<ul class="nav">
					<li class="">
						<span>
						<a href="/taihu/">首页</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/about/">关于我们</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/kcdiy/">德育天地</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/jspost/">教育科研</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/news/">时时关注</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/bj/">教学之窗</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/xlspa/">精致校园</a>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="banner clear">
		<img src="/skin/default/taihu/images/0002.jpg" alt="" />
		<img src="/skin/default/taihu/images/0001.jpg" alt="" />
		<img src="/skin/default/taihu/images/0003.jpg" alt="" />
		<img src="/skin/default/taihu/images/0004.jpg" alt="" />
		<img src="/skin/default/taihu/images/0005.jpg" alt="" />
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
<div class="content">
		<div class="xyfw">
			<div class="xyfw_title c_title clear">
				<div>
					<span>校园服务</span>
				</div>
				
				</a>
				</span>
			</div>
			<div class="xyfw_c clear">
				<ul class="clear">
					
<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq('select * from phome_enewsclass where classid in (46,47,48,49) order by classid',20,24,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?><li><div class="xyfw_c_b">
							<a href="/<?=$bqr[classpath]?>"><img src="<?=$bqr[classimg]?>" alt=""></a>
							<a href="/<?=$bqr[classpath]?>" class="pic"><?=$bqr[classname]?></a>
						</div></li><?php
}
}
?> 
				</ul>
			</div>
		</div>
		<div class="xyzx">
			<div class="xyzx_title c_title clear">
				<div>
					<span>新闻资讯</span>
				</div>
				</div>
			<div class="xyzx_c clear">
				<div class="xyzx_c_l">
					<div class="xyzx_c_l_title xyzx_c_title clear">
						<div class="ontitle">
							<span>动感校园</span>
						</div>
						<div>
							<span>媒体报道</span>
						</div>
						<span>
						<a href="/taihu/news/">MORE <span>+</span>
						</a>
						</span>
					</div>
					<div class="xyzx_c_l_c ontitle0 clear">
						<div><div class="x_lb">
	<div class="x_lb_t">
		<ul class="x_lb_tt">
			<? @sys_GetEcmsInfo(42,4,32,0,0,21,0,'isgood=1');?>		</ul>
		<div class="x_lb_tb">
			<ul class="clear">
				<? @sys_GetEcmsInfo(42,4,32,0,0,22,0,'isgood=1');?>			</ul>
		</div>
	</div>
	<div class="x_lb_b">
		<ul><? @sys_GetEcmsInfo(42,4,32,0,0,23,0,'isgood=1');?>		</ul>
	</div>
</div>
</div>
						<ul><? @sys_GetEcmsInfo(42,6,32,0,0,4,0,'isgood<>1');?>
						</ul>
					</div>
					<div class="xyzx_c_l_c ontitle1 clear">
						<div><? @sys_FlashPixpic(43,5,316,238,1,46,0,0,'isgood=1');?></div>
						<ul><? @sys_GetEcmsInfo(43,6,32,0,0,4,0,'isgood<>1');?>
						</ul>
					</div>
				</div>
				<div class="xyzx_c_r">
					<div class="xyzx_c_r_title xyzx_c_title clear">
						<div class="ontitle">
							<span>通知公告 </span>
						</div>
						<span>
						<a href="/taihu/news/notice/">MORE <span>+</span>
						</a>
						</span>
					</div>
					<ul class="xyzx_c_r_c">
						<? @sys_GetEcmsInfo(44,2,32,0,0,12,0);?>
					</ul>
					
				</div>
			</div>
		</div>
	</div>
<div class="footer_bg">
		<div class="footer">
			<span>学院信箱：service@wjsx.com</span>
			<span>校长信箱：wjsx@wjsx.com</span>
			<span>联系电话：0512-6345079</span>
			<span>地址：江苏省苏州市吴江区开平路1666号</span>
			<span>邮编：215200</span>
			<span>Copy(c)吴江市实验小学2015-2016</span>
			<span>All Rights Reserved 苏ICP备10057875号</span>
		</div>
	</div>
</div>
</body>
</html>