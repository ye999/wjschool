<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/bjfc.css">
<script src="/skin/default/czxq/js/jq.js"></script>
<script src="/skin/default/czxq/js/index_2.js"></script>
<title>城中分校</title>
</head>

<body>
<div class="dc">
	<div class="header clear">
		<a href="/chengzhong/">
			<img src="/skin/default/czxq/images/logo.png" alt=""/>
		</a>
	</div>
	<div class="banner clear">
		<img src="/skin/default/czxq/images/0001.jpg" alt="" />
		<img src="/skin/default/czxq/images/0002.jpg" alt="" />
		<img src="/skin/default/czxq/images/0003.jpg" alt="" />
		<img src="/skin/default/czxq/images/0004.jpg" alt="" />
		<img src="/skin/default/czxq/images/0005.jpg" alt="" />
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
                        <li></li>
			<li></li>
		</ul>
	</div>    
<div class="bjfc">
<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq("select a.classid,a.classname,b.create_at from {$dbtbpre}enewsclass as a left join {$dbtbpre}enewsclassadd as b on a.classid=b.classid where a.bclassid=".$GLOBALS[navclassid]." group by b.create_at order by b.create_at desc,a.classid ASC",0,24,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
<?php $grade = date('Y',time())-(int)$bqr['create_at']+1; 
    $grade_name = '';
    switch ($grade) {
        case 1:
            $grade_name = '一年级';
            break;
        case 2:
            $grade_name = '二年级';
            break;
        case 3:
            $grade_name = '三年级';
            break;
        case 4:
            $grade_name = '四年级';
            break;
        case 5:
            $grade_name = '五年级';
            break;
        case 6:
            $grade_name = '六年级';
            break;
        default:
            $grade_name = $bqr['create_at'].'届';
            break;
    }
    
    if($grade<=6) {
    if($bqno==1) {
        $total_num = $empire->num("select a.classid from {$dbtbpre}enewsclass as a left join {$dbtbpre}enewsclassadd as b on a.classid=b.classid where a.bclassid=".$GLOBALS[navclassid]." group by b.create_at");
        echo '<ul><li class="bjfc_t"><span>班级风采</span></li>';
    } 
?>
    <li class="bjfc_s clear"><span><?=$grade_name?></span>
        <ul class="clear"> 
        <?php
            //取得栏目下的子栏目       
            $newsclass=$empire->query("select a.classid,a.classname from ".$dbtbpre."enewsclass a left join ".$dbtbpre."enewsclassadd b on a.classid=b.classid where b.create_at =".$bqr['create_at']." order by b.create_at desc,a.classid asc limit 6");
            while($classr=$empire->fetch($newsclass))
            {
                $classurl=sys_ReturnBqClassname($classr,9);//取得栏目地址
                $class_name = str_replace($grade_name, '', $classr[classname]);
        ?>
            <li><a href="<?=$classurl?>"><?=$class_name?></a></li>
        <?php } ?>
        </ul>
    </li>
<?php 
    if($bqno==6) echo '</ul>';
}else{
if($bqno==7) echo '<ul><li class="bjfc_t"><span>班级历史</span></li>';  
?>
    <li class="bjfc_s clear"><span><?=$grade_name?></span>
        <ul class="clear"> 
        <?php
            //取得栏目下的子栏目       
            $newsclass=$empire->query("select a.classid,a.classname from ".$dbtbpre."enewsclass a left join ".$dbtbpre."enewsclassadd b on a.classid=b.classid where b.create_at =".$bqr['create_at']." order by b.create_at desc,a.classid asc limit 6");
            while($classr=$empire->fetch($newsclass))
            {
                $classurl=sys_ReturnBqClassname($classr,9);//取得栏目地址
                $class_name = str_replace($grade_name, '', $classr[classname]);
        ?>
            <li><a href="<?=$classurl?>"><?=$class_name?></a></li>
        <?php } ?>
        </ul>
    </li>
</ul>
<?php 
    if($bqno==$total_num) echo '</ul>';
} ?>
<?php
}
}
?>
</div>
<div class="footer">
		<p>地址：江苏省吴江市松陵镇永康路84号  邮编：215200</p>
		<p>Copy(c) 吴江市实验小学2000-2016</p>
		<p>All Rights Reserved 苏ICP备10057875号</p>
	</div>
</div>
</body>
</html>
