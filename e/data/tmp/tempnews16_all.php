<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<title>太湖校区</title>
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/style_mld.css">
<link rel="stylesheet" type="text/css" href="/skin/default/taihu/css/mediacss.css">
<script src="/skin/default/taihu/js/jq.js"></script>
<script src="/skin/default/taihu/js/index.js"></script>
</head>
<body>
<div class="dc">
	<div class="header_bg">
		<div class="header clear">
			<a href="index.html">
			<img src="/skin/default/taihu/images/logo.png" alt=""/>
			</a>
			<ul class="clear">
				<li>
					<a href="/taihu/">首 页</a>
				</li>
				<li>
					<a href="/taihu/about/">关于我们</a>
				</li>
				<li>
					<a href="/taihu/kcdiy/">德育天地</a>
				</li>
				<li>
					<a href="/taihu/jspost/">教育科研</a>
				</li>
				<li>
					<a href="/taihu/news/">时时关注</a><ul><? @sys_ShowClassByTemp(41,1,0,0);?></ul>
				</li>
				<li>
					<a href="/taihu/bj/">教学之窗</a>
				</li>
				<li>
					<a href="/taihu/xlspa/">精致校园</a>
				</li>
			</ul>
			<div class="nav_btn">
				<img src="/skin/default/taihu/images/nav_btn.png" alt=""/>
				<ul class="nav">
					<li class="">
						<span>
						<a href="/taihu/">首页</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/about/">关于我们</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/kcdiy/">德育天地</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/jspost/">教育科研</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/news/">时时关注</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/bj/">教学之窗</a>
						</span>
					</li>
					<li class="">
						<span>
						<a href="/taihu/xlspa/">精致校园</a>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="banner clear">
		<img src="/skin/default/taihu/images/0002.jpg" alt="" />
		<img src="/skin/default/taihu/images/0001.jpg" alt="" />
		<img src="/skin/default/taihu/images/0003.jpg" alt="" />
		<img src="/skin/default/taihu/images/0004.jpg" alt="" />
		<img src="/skin/default/taihu/images/0005.jpg" alt="" />
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
<div class="meun_bg">
		<div class="meun">
			<ul class="clear" id="leftNav">
				<? @sys_ShowClassByTemp('selfinfo',1,0,0);?>
			</ul><script language="javascript">
$(document).ready(
function() {
$("#leftNav li").each(
  function() {
if($(this).attr("classid")=="<?=$ecms_gr[classid]?>") { $(this).addClass("onmeun");}
}
);
}
);
</script>
		</div>
	</div>
<div class="content">
		<div class="webnav">
			<?=$grurl?>
		</div>
		<div class="detail_d">
			<div class="detail_d_title">
				<h2><?=$ecms_gr[title]?></h2>
				<div>
					<span>发布时间:
						<span><?=date('Y-m-d',$ecms_gr[newstime])?></span>
					</span>
					<span>点击量:
						<span><script src=/e/public/ViewClick/?classid=<?=$ecms_gr[classid]?>&id=<?=$ecms_gr[id]?>&addclick=1></script></span>
					</span>
				</div>
			</div>
			<div class="detail_d_p">
				<span><?=strstr($ecms_gr[newstext],'[!--empirenews.page--]')?'[!--newstext--]':$ecms_gr[newstext]?></span>
			</div>
		</div>
	</div>
<div class="footer_bg">
		<div class="footer">
			<span>学院信箱：service@wjsx.com</span>
			<span>校长信箱：wjsx@wjsx.com</span>
			<span>联系电话：0512-6345079</span>
			<span>地址：江苏省苏州市吴江区开平路1666号</span>
			<span>邮编：215200</span>
			<span>Copy(c)吴江市实验小学2015-2016</span>
			<span>All Rights Reserved 苏ICP备10057875号</span>
		</div>
	</div>
</div>
</body>
</html>