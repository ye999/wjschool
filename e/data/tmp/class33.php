<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<link rel="stylesheet" type="text/css" href="/skin/default/aide/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/aide/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/aide/css/mediacss.css">
<script src="/skin/default/aide/js/jq.js"></script>
<script src="/skin/default/aide/js/index.js"></script>
<title>爱德校区</title>
</head>

<body>
<div class="d_c">
	<div class="header_bg">
		<div class="header clear">
			<a href="index.html">
				<img src="/skin/default/aide/images/logo.png" alt=""/>
			</a>
			<ul class="clear">
				<li >
					<a href="/aide/">首页</a>
				</li>
				<li>
					<a href="/aide/ssgz/">时时关注</a><ul><? @sys_ShowClassByTemp(37,1,0,0);?></ul>

				</li>
				<li>
					<a href="/aide/kcdiy/">课程DIY</a><ul><? @sys_ShowClassByTemp(35,1,0,0);?></ul>
				</li>
				<li>
					<a href="/aide/jspost/">教师POST</a><ul><? @sys_ShowClassByTemp(36,1,0,0);?></ul>
				</li>
				
				<li>
					<a href="/aide/bj/">班级GRARDEN</a><ul><? @sys_ShowClassByTemp(38,1,0,0);?></ul>
				</li>
				<li>
					<a href="/aide/xlspa/">心理SPA</a><ul><? @sys_ShowClassByTemp(39,1,0,0);?></ul>
				</li>
<li>
					<a href="/aide/channel/">家长CHANNEL</a><ul><? @sys_ShowClassByTemp(70,1,0,0);?></ul>
				</li>
			</ul>
			<div class="nav_btn">
				<img src="/skin/default/aide/images/nav_btn.png" alt=""/>
				<ul class="nav">
					<li class="icon_index">
						<span>
							<a href="/aide/">首页</a>
						</span>
					</li>
					<li class="icon_kj">
						<span>
							<a href="/aide/dlad/">点亮爱德</a>
						</span>
					</li>
					<li class="icon_dt">
						<span>
							<a href="/aide/kcdiy/">课程DIY</a>
						</span>
					</li>
					<li class="icon_wh">
						<span>
							<a href="/aide/jspost/">教师POST</a>
						</span>
					</li>
					<li class="icon_ms">
						<span>
							<a href="/aide/ssgz/">时时关注</a>
						</span>
					</li>
					<li class="icon_ms">
						<span>
							<a href="/aide/bj/">班级GARDEND</a>
						</span>
					</li>
					<li class="icon_ms">
						<span>
							<a href="/aide/xlspa/">心理SPA</a>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<!--banner-->
	<div class="D_banner clear">
		<img src="/skin/default/aide/images/0001.jpg"  alt=""/>
		<img src="/skin/default/aide/images/0002.jpg"  alt=""/>
		<img src="/skin/default/aide/images/0003.jpg"  alt=""/>
		<img src="/skin/default/aide/images/0004.jpg"  alt=""/>
		<img src="/skin/default/aide/images/0005.jpg"  alt=""/>
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
<div class="content">
		<ul class="clear">
			<li class="c_ssgz">
				<div class="c_ssgz_title clear">
					<span>新闻快递</span>
					<a href="/aide/ssgz/xwkd/">更多&gt;&gt;</a>
				</div>
				<div class="c_ssgz_info clear">
					<? @sys_GetEcmsInfo(74,1,32,0,0,18,0,'isgood=1');?>
					<ul>
						<? @sys_GetEcmsInfo(74,8,32,0,0,17,0,'isgood<>1');?>
					</ul>
				</div>
			</li>
			<li class="c_dlad">
				<div class="c_dlad_title clear">
					<span>通知公告</span>
					<a href="/aide/ssgz/tz/">更多&gt;&gt;</a>
				</div>
				<ul>
					<? @sys_GetEcmsInfo(75,2,32,0,0,20,0);?>
				</ul>
			</li>
			<li class="c_diy c_ulli clear">
				<div class="clear">
					<span><a href="/aide/kcdiy/">课程DIY</a></span>
				</div>
<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq('select * from phome_enewsclass where classid in (35) order by classid',20,24,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?><a href="/<?=$bqr[classpath]?>"><img src="<?=$bqr[classimg]?>" alt=""></a>
<p><?=$bqr[intro]?></p>
<?php
}
}
?>
			</li>
			<li class="c_post c_ulli clear">
				<div class="clear">
					<span><a href="/aide/jspost/">教师POST</a></span>
				</div>
				<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq('select * from phome_enewsclass where classid in (36) order by classid',20,24,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?><a href="/<?=$bqr[classpath]?>"><img src="<?=$bqr[classimg]?>" alt=""></a>
<p><?=$bqr[intro]?></p>
<?php
}
}
?>			</li>
			<li class="c_gardend c_ulli clear">
				<div class="clear">
					<span><a href="/aide/bj/">班级GARDEN</a></span>
				</div>
				<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq('select * from phome_enewsclass where classid in (38) order by classid',20,24,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?><a href="/<?=$bqr[classpath]?>"><img src="<?=$bqr[classimg]?>" alt=""></a>
<p><?=$bqr[intro]?></p>
<?php
}
}
?>			</li>
			<li class="c_spa c_ulli clear">
				<div class="clear">
					<span><a href="/aide/xlspa/">心理SPA</a></span>
				</div>
				<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq('select * from phome_enewsclass where classid in (39) order by classid',20,24,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?><a href="/<?=$bqr[classpath]?>"><img src="<?=$bqr[classimg]?>" alt=""></a>
<p><?=$bqr[intro]?></p>
<?php
}
}
?>			</li>
		</ul>
	</div>
<div class="footer_bg">
		<div class="footer">
			<span>学校信箱：service@wjsx.com</span>
			<span>校长信箱：wjsx@wjsx.com</span>
			<span>联系电话：0512-63970028</span>
			<span>地址：江苏省吴江区太湖新城吴模路1915号</span>
			<span>邮编：215200</span><br>
			<span>Copy(c)吴江市实验小学2015-2016</span>
			<span>All Rights Reserved 苏ICP备10057875号</span>
		</div>
	</div>
</div>
</body>
</html>