<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<link rel="stylesheet" type="text/css" href="/skin/default/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/mediacss.css">
<script src="/skin/default/js/jq.js"></script>
<script src="/skin/default/js/id.js"></script>
<title>吴江实验小学</title>
</head>
<body>
<div class="D">
	<!--顶部-->
	<div class="D_bg top">
		<div class="D_top clear">
			<ul>
				<li>
					<a href="/">返回首页</a>
				</li>
				<li>
					<a href="###">速递邮局</a>
				</li>
				<li>
					<a href="###">办公OA</a>
				</li>
			</ul>
			<div>
				<a class="" href="###">常用信息</a>
				<div>
					<input class="D_top_text" type="text" value="" />
					<input class="D_top_btn" type="button" value="" />
				</div>
			</div>
		</div>
	</div>
	<!--logo部分,导航部分-->
	<div class="D_head clear">
		<div class="D_head_logo">
			<a href="index.html">
				<img src="/skin/default/images/logo.jpg" alt="">
			</a>
		</div>
		<div class="D_head_nav">
			<img  class="D_head_nav_img" src="/skin/default/images/nav1.png" alt="">
			<ul class="clear">
				<li  class="onnav">
					<a href="/">首页</a>
				</li>
				<li>
					<a href="javascript:;">关于我们</a>
					<dl><? @sys_ShowClassByTemp(1,13,0,0);?></dl>
				</li>
				<li>
					<a href="/group/">集团组成</a>
					<dl>
						<dd>
							<a href="/chengzhong/" target="_blank">城中校区</a>
						</dd>
						<dd>
							<a href="/aide/" target="_blank">爱德校区</a>
						</dd>
						<dd>
							<a href="/taihu/" target="_blank">太湖校区</a>
						</dd>
						<dd>
							<a href="/szw/" target="_blank">苏州湾校区</a>
						</dd>
						<dd>
							<a href="index.html">幼儿园</a>
						</dd>
						<dd>
							<a href="index.html">德音书院</a>
						</dd>
					</dl>
				</li>
				<li>
					<a href="javascript:;">新闻动态</a>
					<dl><? @sys_ShowClassByTemp(8,13,0,0);?></dl>
				</li>
				<li>
					<a href="javascript:;">教师频道</a>
					<dl>
						<dd>
							<a href="#">学术委员会</a>
						</dd>
						<dd>
							<a href="#">教育工会</a>
						</dd>
						<dd>
							<a href="#">德音讲坛</a>
						</dd>
						<dd>
							<a href="#">名师工作室</a>
						</dd>
						<dd>
							<a href="#">青年联盟</a>
						</dd>
<dd>
							<a href="#">党建天地</a>
						</dd>

					</dl>
				</li>
				<li>
					<a href="javascript:;">管理平台</a>
					<dl>
						<dd>
							<a href="#">文件阅办系统</a>
						</dd>
						<dd>
							<a href="#">校务管理系统</a>
						</dd>
						<dd>
							<a href="#">学生管理系统</a>
						</dd>
						<dd>
							<a href="#">课题管理系统</a>
						</dd>
					</dl>
				</li>
				<li>
					<a href="javascript:;">资源中心</a>
					<dl>
						<dd>
							<a href="#">网上图书馆</a>
						</dd>
						<dd>
							<a href="#">图视中心</a>
						</dd>
						<dd>
							<a href="#">求真半月刊</a>
						</dd>
						<dd>
							<a href="#">统计年鉴</a>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
<div class="D_banner clear">
		<img src="/skin/default/images/0001.jpg"  alt=""/>
		<img src="/skin/default/images/0002.jpg"  alt=""/>
		<img src="/skin/default/images/0003.jpg"  alt=""/>
		<img src="/skin/default/images/0004.jpg"  alt=""/>
		<img src="/skin/default/images/0005.jpg"  alt=""/>
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
<div class="D_adcontent D_adcontent_4">
		<ul class="clear">
			<? @sys_ShowClassByTemp('selfinfo',1,0,0);?>		</ul>
	</div>
	<div class="webnav">
		<?=$grurl?>	</div>
	<!--新闻news-->
	<div class="D_news_d clear">
		<div class="D_news_d_title">
			<h2><?=$ecms_gr[title]?></h2>
			<div class="time">
				<span>发布时间：<span><?=date('Y-m-d H:i:s',$ecms_gr[newstime])?></span></span>
				<span>点击量：<span><script src=/e/public/ViewClick/?classid=<?=$ecms_gr[classid]?>&id=<?=$ecms_gr[id]?>&addclick=1></script>次</span></span>
			</div>
		</div>
		<p><?=strstr($ecms_gr[newstext],'[!--empirenews.page--]')?'[!--newstext--]':$ecms_gr[newstext]?>
		</p><div class="sx">上一篇：<?php
	$next_r=$empire->fetch1("select isurl,titleurl,classid,id,title from {$dbtbpre}ecms_".$class_r[$ecms_gr[classid]][tbname]." where id<$ecms_gr[id] and classid='$ecms_gr[classid]' order by id desc limit 1");
	if(empty($next_r[id]))
	{$infonext="<a href='".$grclassurl."'>返回列表</a>";}
	else
	{
		$nexttitleurl=sys_ReturnBqTitleLink($next_r);
		$infonext="<a href='".$nexttitleurl."'>".$next_r[title]."</a>";
	}
	echo $infonext;
	?><br>
下一篇：<?php
	$next_r=$empire->fetch1("select isurl,titleurl,classid,id,title from {$dbtbpre}ecms_".$class_r[$ecms_gr[classid]][tbname]." where id>$ecms_gr[id] and classid='$ecms_gr[classid]' order by id limit 1");
	if(empty($next_r[id]))
	{$infonext="<a href='".$grclassurl."'>返回列表</a>";}
	else
	{
		$nexttitleurl=sys_ReturnBqTitleLink($next_r);
		$infonext="<a href='".$nexttitleurl."'>".$next_r[title]."</a>";
	}
	echo $infonext;
	?></div>
	</div><div class="D_bg about">
		<div class="D_about">
			<ul class="clear">
				<li>
					<dl>
						<dt>关于我们</dt>
						<? @sys_ShowClassByTemp(1,13,0,0);?>
					</dl>
				</li>
				<li>
					<dl>
						<dt>集团组成</dt>
						<dd>
							<a href="/chengzhong/">城中校区</a>
						</dd>
						<dd>
							<a href="/aide/">爱德校区</a>
						</dd>
						<dd>
							<a href="/taihu/">太湖校区</a>
						</dd>
						<dd>
							<a href="index.html">苏州湾校区</a>
						</dd>
						<dd>
							<a href="index.html">幼儿园</a>
						</dd>
						<dd>
							<a href="index.html">德音书院</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>新闻动态</dt>
						<? @sys_ShowClassByTemp(8,13,0,0);?>					</dl>
				</li>
				<li>
					<dl>
						<dt>教师频道</dt>
						<dd>
							<a  href="index.html">学术委员会</a>
						</dd>
						<dd>
							<a href="index.html">教育工会</a>
						</dd>
						<dd>
							<a href="index.html">德音讲坛</a>
						</dd>
						<dd>
							<a href="index.html">名师工作室</a>
						</dd>
						<dd>
							<a href="index.html">青年联盟</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>管理平台</dt>
						<dd>
							<a href="index.html">文件阅办系统</a>
						</dd>
						<dd>
							<a href="index.html">校务管理系统</a>
						</dd>
						<dd>
							<a href="index.html">学生管理系统</a>
						</dd>
						<dd>
							<a href="index.html">课题管理系统</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>资源中心</dt>
						<dd>
							<a href="index.html">网上图书馆</a>
						</dd>
						<dd>
							<a href="index.html">图视中心</a>
						</dd>
						<dd>
							<a href="index.html">求真半月刊</a>
						</dd>
						<dd>
							<a href="index.html">统计年鉴</a>
						</dd>
					</dl>
				</li>
				<li class="li_dis">
					<dl>
						<dt>联系我们</dt>
						<dd>
							<h2>0512-63970028</h2>
						</dd>
						<dd>
							<h2>0512-63970028</h2>
						</dd>
						<dd>
							<span>E-mail：edu@wjsx.com</span>
						</dd>
						<dd>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</div>
		<div class="D_bg bottom">
		<div class="D_bottom">
			<span>Copyright 2015 吴江实验小学 保留所有版权 苏ICP备10057875号</span>
			<div class="D_bottom_links">
				<span class="D_bottom_links_btn">相关连接</span>
				<ul><? @sys_GetEcmsInfo(13,10,32,0,0,3,0);?></ul>
			</div>
		</div>
	</div>
</div>
</body>
</html>