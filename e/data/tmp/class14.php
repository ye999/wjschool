<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/reset.css">
<link rel="stylesheet" type="text/css" href="/skin/default/czxq/css/style.css">
<link rel="stylesheet" type="text/css" href="/skin/default/css/bjfc.css">
<script src="/skin/default/czxq/js/jq.js"></script>
<script src="/skin/default/czxq/js/index_2.js"></script>
<title>城中分校</title>
</head>

<body>
<div class="dc">
	<div class="header clear">
		<a href="/chengzhong/">
			<img src="/skin/default/czxq/images/logo.png" alt=""/>
		</a>
	</div>
	<div class="banner clear">
		<img src="/skin/default/czxq/images/0001.jpg" alt="" />
		<img src="/skin/default/czxq/images/0002.jpg" alt="" />
		<img src="/skin/default/czxq/images/0003.jpg" alt="" />
		<img src="/skin/default/czxq/images/0004.jpg" alt="" />
		<img src="/skin/default/czxq/images/0005.jpg" alt="" />
		<ul class="clear">
			<li class="onli"></li>
			<li></li>
			<li></li>
                        <li></li>
			<li></li>
		</ul>
	</div>	
	<div class="news_2 clear">
		<div>
			<div class="biaoti">
				校园新闻<span><a href="/chengzhong/news/xnews/">More>></a></span>
			</div>
			<ul>
				<? @sys_GetEcmsInfo(16,6,48,0,0,4,0);?>
			</ul>
		</div>
		<div>
			<div class="biaoti">
				公告栏<span><a href="/chengzhong/news/notice/">More>></a></span>
			</div>
			<ul>
				<? @sys_GetEcmsInfo(17,6,48,0,0,4,0);?>
			</ul>
		</div>
	</div>
	<div class="nav_2">
		<div class="div1">
			<div>
				<span>阅读:</span>
			</div>
			<ul><? @sys_ShowClassByTemp(18,16,0,0);?></ul>
		</div>
		<div class="div2">
			<div>
				<span>班级:</span>
			</div>
			<ul><? @sys_ShowClassByTemp(22,16,0,0);?></ul>
		</div>
		<div class="div3">
			<div>
				<span>科技:</span>
			</div>
			<ul><? @sys_ShowClassByTemp(29,16,0,0);?></ul>
		</div>
		<div class="div4">
			<div>
				<span>教师:</span>
			</div>
			<ul><? @sys_ShowClassByTemp(25,16,0,0);?></ul>
		</div>
	</div>
<div class="footer">
		<p>地址：江苏省吴江市松陵镇永康路84号  邮编：215200</p>
		<p>Copy(c) 吴江市实验小学2000-2016</p>
		<p>All Rights Reserved 苏ICP备10057875号</p>
	</div>
</div>
</body>
</html>
