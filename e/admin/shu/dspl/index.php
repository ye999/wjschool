<?php
define('EmpireCMSAdmin','1');
require("../../../class/connect.php");
require("../../../class/db_sql.php");
require("../../../class/functions.php");
require "../../".LoadLang("pub/fun.php");
$link=db_connect();
$empire=new mysqlquery();
$editor=1;
//验证用户
$lur=is_login();
$logininid=$lur['userid'];
$loginin=$lur['username'];
$loginrnd=$lur['rnd'];
$loginlevel=$lur['groupid'];
$loginadminstyleid=$lur['adminstyleid'];
//ehash
$ecms_hashur=hReturnEcmsHashStrAll();
//验证权限
CheckLevel($logininid,$loginin,$classid,"pl");

require(ECMS_PATH.'e/extend/SyncDuoshuo/pldata.php');
$plArr = unserialize($pldata);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../../adminstyle/<?=$loginadminstyleid?>/adminstyle.css" rel="stylesheet" type="text/css">
<title>管理多说评论</title>
<script>
function eCopyAdStr(adid,isjs){
	var str='';
	if(isjs==1)
	{
		str='<scri'+'pt src="<?=$public_r[newsurl]?>d/js/acmsd/<?=$public_r[adfile]?>'+adid+'.js"></scri'+'pt>';
	}
	else
	{
		str='[phomead]'+adid+'[/phomead]';
	}
	window.clipboardData.setData('Text',str);
}
</script>
</head>

<body>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
  <tr> 
    <td width="20%" height="25">位置：<a href="index.php<?=$ecms_hashur['whehref']?>">管理多说评论</a></td>
    <td width="80%"><div align="right" class="emenubutton">
        <input type="button" name="Submit522" value="更新多说评论" onclick="self.location.href='../../../extend/SyncDuoshuo/index.php<?=$ecms_hashur['whehref']?>&enews=updatedsinfo';">
      </div></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" class="tableborder">
  <tr class="header"> 
    <td width="8%" height="25"><div align="center">用户</div></td>
    <td width="8%"><div align="center">头像</div></td>
    <td width="11%"><div align="center">评论信息</div></td>
    <td width="7%"><div align="center">评论时间</div></td>
    <td width="66%"><div align="center">评论内容</div></td>
  </tr>
  <?
  foreach($plArr as $k=>$v)
  {
  ?>
  <tr bgcolor="#FFFFFF" onmouseout="this.style.backgroundColor='#ffffff'" onmouseover="this.style.backgroundColor='#C3EFFF'"> 
    <td height="25"> <div align="center"> <?=$v[username]?></div></td>
    <td> <div align="center"><img src="<?=$v[userpic]?>" width="40" height="40"/></div></td>
    <td> <div align="left"><a href="<?=$v[titleurl]?>" target="_blank"><?=$v[title]?></a></div></td>
    <td> <div align="center"> <?=date('Y-m-d H:i:s',$v[saytime])?></div></td>
    <td> <div align="left"><?=$v[saytext]?></div></td>
  </tr>
  <?
  }
  ?>
  <tr bgcolor="#FFFFFF"> 
    <td height="25" colspan="5"> 
      共：<?=count($plArr)?> 条评论
    </td>
  </tr>
</table>
</body>
</html>
<?
db_close();
$empire=null;
?>
