<?php
//---------------------------用户自定义标签函数文件
//add by gary 2016/10/16 返回所有校区
function getCampus()
{
    $arr = [
        0 => '所有校区',
        1 => '城中分校',
        2 => '爱德分校',
        3 => '太湖分校',
        4 => '苏州湾校区',

    ];
    return $arr;
}

function getAuthMap($key){
    $arr = [
        0 => 0,
        1 => 14,
        2 => 33,
        3 => 40,
        4 => 50,
    ];
    return $arr[$key];
}
//获取多说评论
function Sync_GetPl($num_items=10){
    require(ECMS_PATH.'e/extend/SyncDuoshuo/pldata.php');
    $plArr = unserialize($pldata);
    if(!$plArr) return '';
    $plnum = count($plArr);
    if($plnum<$num_items) $num_items = $plnum;
    for($i=0;$i<$num_items;$i++)
    {
        $nplArr[$i]=$plArr[$i];
    }
    return $nplArr;
}
?>