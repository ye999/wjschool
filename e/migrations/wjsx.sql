/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.7.9 : Database - wjsx
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wjsx` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `wjsx`;

/*Table structure for table `phome_ecms_article` */

DROP TABLE IF EXISTS `phome_ecms_article`;

CREATE TABLE `phome_ecms_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` char(20) NOT NULL DEFAULT '',
  `filename` char(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` char(14) NOT NULL DEFAULT '',
  `titleurl` char(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` char(80) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` char(120) NOT NULL DEFAULT '',
  `ftitle` char(120) NOT NULL DEFAULT '',
  `smalltext` char(255) NOT NULL DEFAULT '',
  `writer` char(30) NOT NULL DEFAULT '',
  `befrom` char(60) NOT NULL DEFAULT '',
  `newstext` char(50) NOT NULL DEFAULT '',
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article` */

/*Table structure for table `phome_ecms_article_check` */

DROP TABLE IF EXISTS `phome_ecms_article_check`;

CREATE TABLE `phome_ecms_article_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` char(20) NOT NULL DEFAULT '',
  `filename` char(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` char(14) NOT NULL DEFAULT '',
  `titleurl` char(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` char(80) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` char(120) NOT NULL DEFAULT '',
  `ftitle` char(120) NOT NULL DEFAULT '',
  `smalltext` char(255) NOT NULL DEFAULT '',
  `writer` char(30) NOT NULL DEFAULT '',
  `befrom` char(60) NOT NULL DEFAULT '',
  `newstext` char(50) NOT NULL DEFAULT '',
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article_check` */

/*Table structure for table `phome_ecms_article_check_data` */

DROP TABLE IF EXISTS `phome_ecms_article_check_data`;

CREATE TABLE `phome_ecms_article_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article_check_data` */

/*Table structure for table `phome_ecms_article_data_1` */

DROP TABLE IF EXISTS `phome_ecms_article_data_1`;

CREATE TABLE `phome_ecms_article_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article_data_1` */

/*Table structure for table `phome_ecms_article_doc` */

DROP TABLE IF EXISTS `phome_ecms_article_doc`;

CREATE TABLE `phome_ecms_article_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` char(20) NOT NULL DEFAULT '',
  `filename` char(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` char(14) NOT NULL DEFAULT '',
  `titleurl` char(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` char(80) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` char(120) NOT NULL DEFAULT '',
  `ftitle` char(120) NOT NULL DEFAULT '',
  `smalltext` char(255) NOT NULL DEFAULT '',
  `writer` char(30) NOT NULL DEFAULT '',
  `befrom` char(60) NOT NULL DEFAULT '',
  `newstext` char(50) NOT NULL DEFAULT '',
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article_doc` */

/*Table structure for table `phome_ecms_article_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_article_doc_data`;

CREATE TABLE `phome_ecms_article_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article_doc_data` */

/*Table structure for table `phome_ecms_article_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_article_doc_index`;

CREATE TABLE `phome_ecms_article_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article_doc_index` */

/*Table structure for table `phome_ecms_article_index` */

DROP TABLE IF EXISTS `phome_ecms_article_index`;

CREATE TABLE `phome_ecms_article_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_article_index` */

/*Table structure for table `phome_ecms_download` */

DROP TABLE IF EXISTS `phome_ecms_download`;

CREATE TABLE `phome_ecms_download` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `softfj` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(16) NOT NULL DEFAULT '',
  `softtype` varchar(16) NOT NULL DEFAULT '',
  `softsq` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `softsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download` */

/*Table structure for table `phome_ecms_download_check` */

DROP TABLE IF EXISTS `phome_ecms_download_check`;

CREATE TABLE `phome_ecms_download_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `softfj` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(16) NOT NULL DEFAULT '',
  `softtype` varchar(16) NOT NULL DEFAULT '',
  `softsq` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `softsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download_check` */

/*Table structure for table `phome_ecms_download_check_data` */

DROP TABLE IF EXISTS `phome_ecms_download_check_data`;

CREATE TABLE `phome_ecms_download_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `softwriter` varchar(30) NOT NULL DEFAULT '',
  `homepage` varchar(80) NOT NULL DEFAULT '',
  `demo` varchar(120) NOT NULL DEFAULT '',
  `downpath` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download_check_data` */

/*Table structure for table `phome_ecms_download_data_1` */

DROP TABLE IF EXISTS `phome_ecms_download_data_1`;

CREATE TABLE `phome_ecms_download_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `softwriter` varchar(30) NOT NULL DEFAULT '',
  `homepage` varchar(80) NOT NULL DEFAULT '',
  `demo` varchar(120) NOT NULL DEFAULT '',
  `downpath` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download_data_1` */

/*Table structure for table `phome_ecms_download_doc` */

DROP TABLE IF EXISTS `phome_ecms_download_doc`;

CREATE TABLE `phome_ecms_download_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `softfj` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(16) NOT NULL DEFAULT '',
  `softtype` varchar(16) NOT NULL DEFAULT '',
  `softsq` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `softsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download_doc` */

/*Table structure for table `phome_ecms_download_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_download_doc_data`;

CREATE TABLE `phome_ecms_download_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `softwriter` varchar(30) NOT NULL DEFAULT '',
  `homepage` varchar(80) NOT NULL DEFAULT '',
  `demo` varchar(120) NOT NULL DEFAULT '',
  `downpath` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download_doc_data` */

/*Table structure for table `phome_ecms_download_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_download_doc_index`;

CREATE TABLE `phome_ecms_download_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download_doc_index` */

/*Table structure for table `phome_ecms_download_index` */

DROP TABLE IF EXISTS `phome_ecms_download_index`;

CREATE TABLE `phome_ecms_download_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_download_index` */

/*Table structure for table `phome_ecms_flash` */

DROP TABLE IF EXISTS `phome_ecms_flash`;

CREATE TABLE `phome_ecms_flash` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `flashwriter` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `flashurl` varchar(255) NOT NULL DEFAULT '',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  `flashsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash` */

/*Table structure for table `phome_ecms_flash_check` */

DROP TABLE IF EXISTS `phome_ecms_flash_check`;

CREATE TABLE `phome_ecms_flash_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `flashwriter` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `flashurl` varchar(255) NOT NULL DEFAULT '',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  `flashsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash_check` */

/*Table structure for table `phome_ecms_flash_check_data` */

DROP TABLE IF EXISTS `phome_ecms_flash_check_data`;

CREATE TABLE `phome_ecms_flash_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash_check_data` */

/*Table structure for table `phome_ecms_flash_data_1` */

DROP TABLE IF EXISTS `phome_ecms_flash_data_1`;

CREATE TABLE `phome_ecms_flash_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash_data_1` */

/*Table structure for table `phome_ecms_flash_doc` */

DROP TABLE IF EXISTS `phome_ecms_flash_doc`;

CREATE TABLE `phome_ecms_flash_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `flashwriter` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `flashurl` varchar(255) NOT NULL DEFAULT '',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  `flashsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash_doc` */

/*Table structure for table `phome_ecms_flash_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_flash_doc_data`;

CREATE TABLE `phome_ecms_flash_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash_doc_data` */

/*Table structure for table `phome_ecms_flash_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_flash_doc_index`;

CREATE TABLE `phome_ecms_flash_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash_doc_index` */

/*Table structure for table `phome_ecms_flash_index` */

DROP TABLE IF EXISTS `phome_ecms_flash_index`;

CREATE TABLE `phome_ecms_flash_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_flash_index` */

/*Table structure for table `phome_ecms_info` */

DROP TABLE IF EXISTS `phome_ecms_info`;

CREATE TABLE `phome_ecms_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `smalltext` text NOT NULL,
  `myarea` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info` */

/*Table structure for table `phome_ecms_info_check` */

DROP TABLE IF EXISTS `phome_ecms_info_check`;

CREATE TABLE `phome_ecms_info_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `smalltext` text NOT NULL,
  `myarea` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info_check` */

/*Table structure for table `phome_ecms_info_check_data` */

DROP TABLE IF EXISTS `phome_ecms_info_check_data`;

CREATE TABLE `phome_ecms_info_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `mycontact` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info_check_data` */

/*Table structure for table `phome_ecms_info_data_1` */

DROP TABLE IF EXISTS `phome_ecms_info_data_1`;

CREATE TABLE `phome_ecms_info_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `mycontact` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info_data_1` */

/*Table structure for table `phome_ecms_info_doc` */

DROP TABLE IF EXISTS `phome_ecms_info_doc`;

CREATE TABLE `phome_ecms_info_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `smalltext` text NOT NULL,
  `myarea` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info_doc` */

/*Table structure for table `phome_ecms_info_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_info_doc_data`;

CREATE TABLE `phome_ecms_info_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `mycontact` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info_doc_data` */

/*Table structure for table `phome_ecms_info_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_info_doc_index`;

CREATE TABLE `phome_ecms_info_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info_doc_index` */

/*Table structure for table `phome_ecms_info_index` */

DROP TABLE IF EXISTS `phome_ecms_info_index`;

CREATE TABLE `phome_ecms_info_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_info_index` */

/*Table structure for table `phome_ecms_infoclass_article` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_article`;

CREATE TABLE `phome_ecms_infoclass_article` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_ftitle` text NOT NULL,
  `z_ftitle` varchar(255) NOT NULL DEFAULT '',
  `qz_ftitle` varchar(255) NOT NULL DEFAULT '',
  `save_ftitle` varchar(10) NOT NULL DEFAULT '',
  `zz_smalltext` text NOT NULL,
  `z_smalltext` varchar(255) NOT NULL DEFAULT '',
  `qz_smalltext` varchar(255) NOT NULL DEFAULT '',
  `save_smalltext` varchar(10) NOT NULL DEFAULT '',
  `zz_writer` text NOT NULL,
  `z_writer` varchar(255) NOT NULL DEFAULT '',
  `qz_writer` varchar(255) NOT NULL DEFAULT '',
  `save_writer` varchar(10) NOT NULL DEFAULT '',
  `zz_befrom` text NOT NULL,
  `z_befrom` varchar(255) NOT NULL DEFAULT '',
  `qz_befrom` varchar(255) NOT NULL DEFAULT '',
  `save_befrom` varchar(10) NOT NULL DEFAULT '',
  `zz_newstext` text NOT NULL,
  `z_newstext` varchar(255) NOT NULL DEFAULT '',
  `qz_newstext` varchar(255) NOT NULL DEFAULT '',
  `save_newstext` varchar(10) NOT NULL DEFAULT '',
  `zz_diggtop` text NOT NULL,
  `z_diggtop` varchar(255) NOT NULL DEFAULT '',
  `qz_diggtop` varchar(255) NOT NULL DEFAULT '',
  `save_diggtop` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_article` */

/*Table structure for table `phome_ecms_infoclass_download` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_download`;

CREATE TABLE `phome_ecms_infoclass_download` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_softwriter` text NOT NULL,
  `z_softwriter` varchar(255) NOT NULL DEFAULT '',
  `qz_softwriter` varchar(255) NOT NULL DEFAULT '',
  `save_softwriter` varchar(10) NOT NULL DEFAULT '',
  `zz_homepage` text NOT NULL,
  `z_homepage` varchar(255) NOT NULL DEFAULT '',
  `qz_homepage` varchar(255) NOT NULL DEFAULT '',
  `save_homepage` varchar(10) NOT NULL DEFAULT '',
  `zz_demo` text NOT NULL,
  `z_demo` varchar(255) NOT NULL DEFAULT '',
  `qz_demo` varchar(255) NOT NULL DEFAULT '',
  `save_demo` varchar(10) NOT NULL DEFAULT '',
  `zz_softfj` text NOT NULL,
  `z_softfj` varchar(255) NOT NULL DEFAULT '',
  `qz_softfj` varchar(255) NOT NULL DEFAULT '',
  `save_softfj` varchar(10) NOT NULL DEFAULT '',
  `zz_language` text NOT NULL,
  `z_language` varchar(255) NOT NULL DEFAULT '',
  `qz_language` varchar(255) NOT NULL DEFAULT '',
  `save_language` varchar(10) NOT NULL DEFAULT '',
  `zz_softtype` text NOT NULL,
  `z_softtype` varchar(255) NOT NULL DEFAULT '',
  `qz_softtype` varchar(255) NOT NULL DEFAULT '',
  `save_softtype` varchar(10) NOT NULL DEFAULT '',
  `zz_softsq` text NOT NULL,
  `z_softsq` varchar(255) NOT NULL DEFAULT '',
  `qz_softsq` varchar(255) NOT NULL DEFAULT '',
  `save_softsq` varchar(10) NOT NULL DEFAULT '',
  `zz_star` text NOT NULL,
  `z_star` varchar(255) NOT NULL DEFAULT '',
  `qz_star` varchar(255) NOT NULL DEFAULT '',
  `save_star` varchar(10) NOT NULL DEFAULT '',
  `zz_filetype` text NOT NULL,
  `z_filetype` varchar(255) NOT NULL DEFAULT '',
  `qz_filetype` varchar(255) NOT NULL DEFAULT '',
  `save_filetype` varchar(10) NOT NULL DEFAULT '',
  `zz_filesize` text NOT NULL,
  `z_filesize` varchar(255) NOT NULL DEFAULT '',
  `qz_filesize` varchar(255) NOT NULL DEFAULT '',
  `save_filesize` varchar(10) NOT NULL DEFAULT '',
  `zz_downpath` text NOT NULL,
  `z_downpath` varchar(255) NOT NULL DEFAULT '',
  `qz_downpath` varchar(255) NOT NULL DEFAULT '',
  `save_downpath` varchar(10) NOT NULL DEFAULT '',
  `zz_softsay` text NOT NULL,
  `z_softsay` varchar(255) NOT NULL DEFAULT '',
  `qz_softsay` varchar(255) NOT NULL DEFAULT '',
  `save_softsay` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_download` */

/*Table structure for table `phome_ecms_infoclass_flash` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_flash`;

CREATE TABLE `phome_ecms_infoclass_flash` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_flashwriter` text NOT NULL,
  `z_flashwriter` varchar(255) NOT NULL DEFAULT '',
  `qz_flashwriter` varchar(255) NOT NULL DEFAULT '',
  `save_flashwriter` varchar(10) NOT NULL DEFAULT '',
  `zz_email` text NOT NULL,
  `z_email` varchar(255) NOT NULL DEFAULT '',
  `qz_email` varchar(255) NOT NULL DEFAULT '',
  `save_email` varchar(10) NOT NULL DEFAULT '',
  `zz_star` text NOT NULL,
  `z_star` varchar(255) NOT NULL DEFAULT '',
  `qz_star` varchar(255) NOT NULL DEFAULT '',
  `save_star` varchar(10) NOT NULL DEFAULT '',
  `zz_filesize` text NOT NULL,
  `z_filesize` varchar(255) NOT NULL DEFAULT '',
  `qz_filesize` varchar(255) NOT NULL DEFAULT '',
  `save_filesize` varchar(10) NOT NULL DEFAULT '',
  `zz_flashurl` text NOT NULL,
  `z_flashurl` varchar(255) NOT NULL DEFAULT '',
  `qz_flashurl` varchar(255) NOT NULL DEFAULT '',
  `save_flashurl` varchar(10) NOT NULL DEFAULT '',
  `zz_width` text NOT NULL,
  `z_width` varchar(255) NOT NULL DEFAULT '',
  `qz_width` varchar(255) NOT NULL DEFAULT '',
  `save_width` varchar(10) NOT NULL DEFAULT '',
  `zz_height` text NOT NULL,
  `z_height` varchar(255) NOT NULL DEFAULT '',
  `qz_height` varchar(255) NOT NULL DEFAULT '',
  `save_height` varchar(10) NOT NULL DEFAULT '',
  `zz_flashsay` text NOT NULL,
  `z_flashsay` varchar(255) NOT NULL DEFAULT '',
  `qz_flashsay` varchar(255) NOT NULL DEFAULT '',
  `save_flashsay` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_flash` */

/*Table structure for table `phome_ecms_infoclass_info` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_info`;

CREATE TABLE `phome_ecms_infoclass_info` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_smalltext` text NOT NULL,
  `z_smalltext` varchar(255) NOT NULL DEFAULT '',
  `qz_smalltext` varchar(255) NOT NULL DEFAULT '',
  `save_smalltext` varchar(10) NOT NULL DEFAULT '',
  `zz_myarea` text NOT NULL,
  `z_myarea` varchar(255) NOT NULL DEFAULT '',
  `qz_myarea` varchar(255) NOT NULL DEFAULT '',
  `save_myarea` varchar(10) NOT NULL DEFAULT '',
  `zz_email` text NOT NULL,
  `z_email` varchar(255) NOT NULL DEFAULT '',
  `qz_email` varchar(255) NOT NULL DEFAULT '',
  `save_email` varchar(10) NOT NULL DEFAULT '',
  `zz_mycontact` text NOT NULL,
  `z_mycontact` varchar(255) NOT NULL DEFAULT '',
  `qz_mycontact` varchar(255) NOT NULL DEFAULT '',
  `save_mycontact` varchar(10) NOT NULL DEFAULT '',
  `zz_address` text NOT NULL,
  `z_address` varchar(255) NOT NULL DEFAULT '',
  `qz_address` varchar(255) NOT NULL DEFAULT '',
  `save_address` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_info` */

/*Table structure for table `phome_ecms_infoclass_movie` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_movie`;

CREATE TABLE `phome_ecms_infoclass_movie` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_movietype` text NOT NULL,
  `z_movietype` varchar(255) NOT NULL DEFAULT '',
  `qz_movietype` varchar(255) NOT NULL DEFAULT '',
  `save_movietype` varchar(10) NOT NULL DEFAULT '',
  `zz_company` text NOT NULL,
  `z_company` varchar(255) NOT NULL DEFAULT '',
  `qz_company` varchar(255) NOT NULL DEFAULT '',
  `save_company` varchar(10) NOT NULL DEFAULT '',
  `zz_movietime` text NOT NULL,
  `z_movietime` varchar(255) NOT NULL DEFAULT '',
  `qz_movietime` varchar(255) NOT NULL DEFAULT '',
  `save_movietime` varchar(10) NOT NULL DEFAULT '',
  `zz_player` text NOT NULL,
  `z_player` varchar(255) NOT NULL DEFAULT '',
  `qz_player` varchar(255) NOT NULL DEFAULT '',
  `save_player` varchar(10) NOT NULL DEFAULT '',
  `zz_playadmin` text NOT NULL,
  `z_playadmin` varchar(255) NOT NULL DEFAULT '',
  `qz_playadmin` varchar(255) NOT NULL DEFAULT '',
  `save_playadmin` varchar(10) NOT NULL DEFAULT '',
  `zz_filetype` text NOT NULL,
  `z_filetype` varchar(255) NOT NULL DEFAULT '',
  `qz_filetype` varchar(255) NOT NULL DEFAULT '',
  `save_filetype` varchar(10) NOT NULL DEFAULT '',
  `zz_filesize` text NOT NULL,
  `z_filesize` varchar(255) NOT NULL DEFAULT '',
  `qz_filesize` varchar(255) NOT NULL DEFAULT '',
  `save_filesize` varchar(10) NOT NULL DEFAULT '',
  `zz_star` text NOT NULL,
  `z_star` varchar(255) NOT NULL DEFAULT '',
  `qz_star` varchar(255) NOT NULL DEFAULT '',
  `save_star` varchar(10) NOT NULL DEFAULT '',
  `zz_playdk` text NOT NULL,
  `z_playdk` varchar(255) NOT NULL DEFAULT '',
  `qz_playdk` varchar(255) NOT NULL DEFAULT '',
  `save_playdk` varchar(10) NOT NULL DEFAULT '',
  `zz_playtime` text NOT NULL,
  `z_playtime` varchar(255) NOT NULL DEFAULT '',
  `qz_playtime` varchar(255) NOT NULL DEFAULT '',
  `save_playtime` varchar(10) NOT NULL DEFAULT '',
  `zz_moviefen` text NOT NULL,
  `z_moviefen` varchar(255) NOT NULL DEFAULT '',
  `qz_moviefen` varchar(255) NOT NULL DEFAULT '',
  `save_moviefen` varchar(10) NOT NULL DEFAULT '',
  `zz_downpath` text NOT NULL,
  `z_downpath` varchar(255) NOT NULL DEFAULT '',
  `qz_downpath` varchar(255) NOT NULL DEFAULT '',
  `save_downpath` varchar(10) NOT NULL DEFAULT '',
  `zz_playerid` text NOT NULL,
  `z_playerid` varchar(255) NOT NULL DEFAULT '',
  `qz_playerid` varchar(255) NOT NULL DEFAULT '',
  `save_playerid` varchar(10) NOT NULL DEFAULT '',
  `zz_onlinepath` text NOT NULL,
  `z_onlinepath` varchar(255) NOT NULL DEFAULT '',
  `qz_onlinepath` varchar(255) NOT NULL DEFAULT '',
  `save_onlinepath` varchar(10) NOT NULL DEFAULT '',
  `zz_moviesay` text NOT NULL,
  `z_moviesay` varchar(255) NOT NULL DEFAULT '',
  `qz_moviesay` varchar(255) NOT NULL DEFAULT '',
  `save_moviesay` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_movie` */

/*Table structure for table `phome_ecms_infoclass_news` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_news`;

CREATE TABLE `phome_ecms_infoclass_news` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_ftitle` text NOT NULL,
  `z_ftitle` varchar(255) NOT NULL DEFAULT '',
  `qz_ftitle` varchar(255) NOT NULL DEFAULT '',
  `save_ftitle` varchar(10) NOT NULL DEFAULT '',
  `zz_smalltext` text NOT NULL,
  `z_smalltext` varchar(255) NOT NULL DEFAULT '',
  `qz_smalltext` varchar(255) NOT NULL DEFAULT '',
  `save_smalltext` varchar(10) NOT NULL DEFAULT '',
  `zz_writer` text NOT NULL,
  `z_writer` varchar(255) NOT NULL DEFAULT '',
  `qz_writer` varchar(255) NOT NULL DEFAULT '',
  `save_writer` varchar(10) NOT NULL DEFAULT '',
  `zz_befrom` text NOT NULL,
  `z_befrom` varchar(255) NOT NULL DEFAULT '',
  `qz_befrom` varchar(255) NOT NULL DEFAULT '',
  `save_befrom` varchar(10) NOT NULL DEFAULT '',
  `zz_newstext` text NOT NULL,
  `z_newstext` varchar(255) NOT NULL DEFAULT '',
  `qz_newstext` varchar(255) NOT NULL DEFAULT '',
  `save_newstext` varchar(10) NOT NULL DEFAULT '',
  `zz_diggtop` text NOT NULL,
  `z_diggtop` varchar(255) NOT NULL DEFAULT '',
  `qz_diggtop` varchar(255) NOT NULL DEFAULT '',
  `save_diggtop` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_news` */

/*Table structure for table `phome_ecms_infoclass_photo` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_photo`;

CREATE TABLE `phome_ecms_infoclass_photo` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_filesize` text NOT NULL,
  `z_filesize` varchar(255) NOT NULL DEFAULT '',
  `qz_filesize` varchar(255) NOT NULL DEFAULT '',
  `save_filesize` varchar(10) NOT NULL DEFAULT '',
  `zz_picsize` text NOT NULL,
  `z_picsize` varchar(255) NOT NULL DEFAULT '',
  `qz_picsize` varchar(255) NOT NULL DEFAULT '',
  `save_picsize` varchar(10) NOT NULL DEFAULT '',
  `zz_picfbl` text NOT NULL,
  `z_picfbl` varchar(255) NOT NULL DEFAULT '',
  `qz_picfbl` varchar(255) NOT NULL DEFAULT '',
  `save_picfbl` varchar(10) NOT NULL DEFAULT '',
  `zz_picfrom` text NOT NULL,
  `z_picfrom` varchar(255) NOT NULL DEFAULT '',
  `qz_picfrom` varchar(255) NOT NULL DEFAULT '',
  `save_picfrom` varchar(10) NOT NULL DEFAULT '',
  `zz_picurl` text NOT NULL,
  `z_picurl` varchar(255) NOT NULL DEFAULT '',
  `qz_picurl` varchar(255) NOT NULL DEFAULT '',
  `save_picurl` varchar(10) NOT NULL DEFAULT '',
  `zz_morepic` text NOT NULL,
  `z_morepic` varchar(255) NOT NULL DEFAULT '',
  `qz_morepic` varchar(255) NOT NULL DEFAULT '',
  `save_morepic` varchar(10) NOT NULL DEFAULT '',
  `zz_num` text NOT NULL,
  `z_num` varchar(255) NOT NULL DEFAULT '',
  `qz_num` varchar(255) NOT NULL DEFAULT '',
  `save_num` varchar(10) NOT NULL DEFAULT '',
  `zz_width` text NOT NULL,
  `z_width` varchar(255) NOT NULL DEFAULT '',
  `qz_width` varchar(255) NOT NULL DEFAULT '',
  `save_width` varchar(10) NOT NULL DEFAULT '',
  `zz_height` text NOT NULL,
  `z_height` varchar(255) NOT NULL DEFAULT '',
  `qz_height` varchar(255) NOT NULL DEFAULT '',
  `save_height` varchar(10) NOT NULL DEFAULT '',
  `zz_picsay` text NOT NULL,
  `z_picsay` varchar(255) NOT NULL DEFAULT '',
  `qz_picsay` varchar(255) NOT NULL DEFAULT '',
  `save_picsay` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_photo` */

/*Table structure for table `phome_ecms_infoclass_shop` */

DROP TABLE IF EXISTS `phome_ecms_infoclass_shop`;

CREATE TABLE `phome_ecms_infoclass_shop` (
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `zz_title` text NOT NULL,
  `z_title` varchar(255) NOT NULL DEFAULT '',
  `qz_title` varchar(255) NOT NULL DEFAULT '',
  `save_title` varchar(10) NOT NULL DEFAULT '',
  `zz_titlepic` text NOT NULL,
  `z_titlepic` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepic` varchar(255) NOT NULL DEFAULT '',
  `save_titlepic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstime` text NOT NULL,
  `z_newstime` varchar(255) NOT NULL DEFAULT '',
  `qz_newstime` varchar(255) NOT NULL DEFAULT '',
  `save_newstime` varchar(10) NOT NULL DEFAULT '',
  `zz_productno` text NOT NULL,
  `z_productno` varchar(255) NOT NULL DEFAULT '',
  `qz_productno` varchar(255) NOT NULL DEFAULT '',
  `save_productno` varchar(10) NOT NULL DEFAULT '',
  `zz_pbrand` text NOT NULL,
  `z_pbrand` varchar(255) NOT NULL DEFAULT '',
  `qz_pbrand` varchar(255) NOT NULL DEFAULT '',
  `save_pbrand` varchar(10) NOT NULL DEFAULT '',
  `zz_intro` text NOT NULL,
  `z_intro` varchar(255) NOT NULL DEFAULT '',
  `qz_intro` varchar(255) NOT NULL DEFAULT '',
  `save_intro` varchar(10) NOT NULL DEFAULT '',
  `zz_unit` text NOT NULL,
  `z_unit` varchar(255) NOT NULL DEFAULT '',
  `qz_unit` varchar(255) NOT NULL DEFAULT '',
  `save_unit` varchar(10) NOT NULL DEFAULT '',
  `zz_weight` text NOT NULL,
  `z_weight` varchar(255) NOT NULL DEFAULT '',
  `qz_weight` varchar(255) NOT NULL DEFAULT '',
  `save_weight` varchar(10) NOT NULL DEFAULT '',
  `zz_tprice` text NOT NULL,
  `z_tprice` varchar(255) NOT NULL DEFAULT '',
  `qz_tprice` varchar(255) NOT NULL DEFAULT '',
  `save_tprice` varchar(10) NOT NULL DEFAULT '',
  `zz_price` text NOT NULL,
  `z_price` varchar(255) NOT NULL DEFAULT '',
  `qz_price` varchar(255) NOT NULL DEFAULT '',
  `save_price` varchar(10) NOT NULL DEFAULT '',
  `zz_buyfen` text NOT NULL,
  `z_buyfen` varchar(255) NOT NULL DEFAULT '',
  `qz_buyfen` varchar(255) NOT NULL DEFAULT '',
  `save_buyfen` varchar(10) NOT NULL DEFAULT '',
  `zz_pmaxnum` text NOT NULL,
  `z_pmaxnum` varchar(255) NOT NULL DEFAULT '',
  `qz_pmaxnum` varchar(255) NOT NULL DEFAULT '',
  `save_pmaxnum` varchar(10) NOT NULL DEFAULT '',
  `zz_productpic` text NOT NULL,
  `z_productpic` varchar(255) NOT NULL DEFAULT '',
  `qz_productpic` varchar(255) NOT NULL DEFAULT '',
  `save_productpic` varchar(10) NOT NULL DEFAULT '',
  `zz_newstext` text NOT NULL,
  `z_newstext` varchar(255) NOT NULL DEFAULT '',
  `qz_newstext` varchar(255) NOT NULL DEFAULT '',
  `save_newstext` varchar(10) NOT NULL DEFAULT '',
  `zz_psalenum` text NOT NULL,
  `z_psalenum` varchar(255) NOT NULL DEFAULT '',
  `qz_psalenum` varchar(255) NOT NULL DEFAULT '',
  `save_psalenum` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infoclass_shop` */

/*Table structure for table `phome_ecms_infotmp_article` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_article`;

CREATE TABLE `phome_ecms_infotmp_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `ftitle` varchar(120) NOT NULL DEFAULT '',
  `smalltext` varchar(255) NOT NULL DEFAULT '',
  `writer` varchar(30) NOT NULL DEFAULT '',
  `befrom` varchar(60) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_article` */

/*Table structure for table `phome_ecms_infotmp_download` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_download`;

CREATE TABLE `phome_ecms_infotmp_download` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `softwriter` varchar(30) NOT NULL DEFAULT '',
  `homepage` varchar(80) NOT NULL DEFAULT '',
  `demo` varchar(120) NOT NULL DEFAULT '',
  `softfj` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(16) NOT NULL DEFAULT '',
  `softtype` varchar(16) NOT NULL DEFAULT '',
  `softsq` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `downpath` mediumtext NOT NULL,
  `softsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_download` */

/*Table structure for table `phome_ecms_infotmp_flash` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_flash`;

CREATE TABLE `phome_ecms_infotmp_flash` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `flashwriter` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `flashurl` varchar(255) NOT NULL DEFAULT '',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  `flashsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_flash` */

/*Table structure for table `phome_ecms_infotmp_info` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_info`;

CREATE TABLE `phome_ecms_infotmp_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `smalltext` text NOT NULL,
  `myarea` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `mycontact` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_info` */

/*Table structure for table `phome_ecms_infotmp_movie` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_movie`;

CREATE TABLE `phome_ecms_infotmp_movie` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `movietype` varchar(16) NOT NULL DEFAULT '',
  `company` varchar(200) NOT NULL DEFAULT '',
  `movietime` varchar(20) NOT NULL DEFAULT '',
  `player` varchar(255) NOT NULL DEFAULT '',
  `playadmin` varchar(255) NOT NULL DEFAULT '',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `playdk` varchar(30) NOT NULL DEFAULT '',
  `playtime` varchar(20) NOT NULL DEFAULT '',
  `moviefen` int(11) NOT NULL DEFAULT '0',
  `downpath` mediumtext NOT NULL,
  `playerid` smallint(6) NOT NULL DEFAULT '0',
  `onlinepath` mediumtext NOT NULL,
  `moviesay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_movie` */

/*Table structure for table `phome_ecms_infotmp_news` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_news`;

CREATE TABLE `phome_ecms_infotmp_news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `ftitle` varchar(120) NOT NULL DEFAULT '',
  `smalltext` varchar(255) NOT NULL DEFAULT '',
  `writer` varchar(30) NOT NULL DEFAULT '',
  `befrom` varchar(60) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_news` */

/*Table structure for table `phome_ecms_infotmp_photo` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_photo`;

CREATE TABLE `phome_ecms_infotmp_photo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `filesize` varchar(10) NOT NULL DEFAULT '',
  `picsize` varchar(20) NOT NULL DEFAULT '',
  `picfbl` varchar(20) NOT NULL DEFAULT '',
  `picfrom` varchar(120) NOT NULL DEFAULT '',
  `picurl` varchar(200) NOT NULL DEFAULT '',
  `morepic` mediumtext NOT NULL,
  `num` tinyint(4) NOT NULL DEFAULT '0',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  `picsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_photo` */

/*Table structure for table `phome_ecms_infotmp_shop` */

DROP TABLE IF EXISTS `phome_ecms_infotmp_shop`;

CREATE TABLE `phome_ecms_infotmp_shop` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `classid` int(10) unsigned NOT NULL DEFAULT '0',
  `oldurl` varchar(200) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `tmptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `keyboard` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `productno` varchar(30) NOT NULL DEFAULT '',
  `pbrand` varchar(30) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `unit` varchar(16) NOT NULL DEFAULT '',
  `weight` varchar(20) NOT NULL DEFAULT '',
  `tprice` float(11,2) NOT NULL DEFAULT '0.00',
  `price` float(11,2) NOT NULL DEFAULT '0.00',
  `buyfen` int(11) NOT NULL DEFAULT '0',
  `pmaxnum` int(11) NOT NULL DEFAULT '0',
  `productpic` varchar(255) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  `psalenum` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_infotmp_shop` */

/*Table structure for table `phome_ecms_movie` */

DROP TABLE IF EXISTS `phome_ecms_movie`;

CREATE TABLE `phome_ecms_movie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `movietype` varchar(16) NOT NULL DEFAULT '',
  `company` varchar(200) NOT NULL DEFAULT '',
  `movietime` varchar(20) NOT NULL DEFAULT '',
  `player` varchar(255) NOT NULL DEFAULT '',
  `playadmin` varchar(255) NOT NULL DEFAULT '',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `moviefen` int(11) NOT NULL DEFAULT '0',
  `moviesay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie` */

/*Table structure for table `phome_ecms_movie_check` */

DROP TABLE IF EXISTS `phome_ecms_movie_check`;

CREATE TABLE `phome_ecms_movie_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `movietype` varchar(16) NOT NULL DEFAULT '',
  `company` varchar(200) NOT NULL DEFAULT '',
  `movietime` varchar(20) NOT NULL DEFAULT '',
  `player` varchar(255) NOT NULL DEFAULT '',
  `playadmin` varchar(255) NOT NULL DEFAULT '',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `moviefen` int(11) NOT NULL DEFAULT '0',
  `moviesay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie_check` */

/*Table structure for table `phome_ecms_movie_check_data` */

DROP TABLE IF EXISTS `phome_ecms_movie_check_data`;

CREATE TABLE `phome_ecms_movie_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `playdk` varchar(30) NOT NULL DEFAULT '',
  `playtime` varchar(20) NOT NULL DEFAULT '',
  `downpath` mediumtext NOT NULL,
  `playerid` smallint(6) NOT NULL DEFAULT '0',
  `onlinepath` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie_check_data` */

/*Table structure for table `phome_ecms_movie_data_1` */

DROP TABLE IF EXISTS `phome_ecms_movie_data_1`;

CREATE TABLE `phome_ecms_movie_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `playdk` varchar(30) NOT NULL DEFAULT '',
  `playtime` varchar(20) NOT NULL DEFAULT '',
  `downpath` mediumtext NOT NULL,
  `playerid` smallint(6) NOT NULL DEFAULT '0',
  `onlinepath` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie_data_1` */

/*Table structure for table `phome_ecms_movie_doc` */

DROP TABLE IF EXISTS `phome_ecms_movie_doc`;

CREATE TABLE `phome_ecms_movie_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `movietype` varchar(16) NOT NULL DEFAULT '',
  `company` varchar(200) NOT NULL DEFAULT '',
  `movietime` varchar(20) NOT NULL DEFAULT '',
  `player` varchar(255) NOT NULL DEFAULT '',
  `playadmin` varchar(255) NOT NULL DEFAULT '',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `filesize` varchar(16) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `moviefen` int(11) NOT NULL DEFAULT '0',
  `moviesay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie_doc` */

/*Table structure for table `phome_ecms_movie_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_movie_doc_data`;

CREATE TABLE `phome_ecms_movie_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `playdk` varchar(30) NOT NULL DEFAULT '',
  `playtime` varchar(20) NOT NULL DEFAULT '',
  `downpath` mediumtext NOT NULL,
  `playerid` smallint(6) NOT NULL DEFAULT '0',
  `onlinepath` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie_doc_data` */

/*Table structure for table `phome_ecms_movie_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_movie_doc_index`;

CREATE TABLE `phome_ecms_movie_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie_doc_index` */

/*Table structure for table `phome_ecms_movie_index` */

DROP TABLE IF EXISTS `phome_ecms_movie_index`;

CREATE TABLE `phome_ecms_movie_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_movie_index` */

/*Table structure for table `phome_ecms_news` */

DROP TABLE IF EXISTS `phome_ecms_news`;

CREATE TABLE `phome_ecms_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` char(20) NOT NULL DEFAULT '',
  `filename` char(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` char(14) NOT NULL DEFAULT '',
  `titleurl` char(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` char(80) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` char(120) NOT NULL DEFAULT '',
  `ftitle` char(120) NOT NULL DEFAULT '',
  `smalltext` char(255) NOT NULL DEFAULT '',
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news` */

insert  into `phome_ecms_news`(`id`,`classid`,`ttid`,`onclick`,`plnum`,`totaldown`,`newspath`,`filename`,`userid`,`username`,`firsttitle`,`isgood`,`ispic`,`istop`,`isqf`,`ismember`,`isurl`,`truetime`,`lastdotime`,`havehtml`,`groupid`,`userfen`,`titlefont`,`titleurl`,`stb`,`fstb`,`restb`,`keyboard`,`title`,`newstime`,`titlepic`,`ftitle`,`smalltext`,`diggtop`) values (1,2,0,0,0,0,'','index',1,'manage',0,0,0,0,0,0,0,1471845887,1473823439,1,0,0,'','/about/group/index.html',1,1,1,'','承“爱德、求真”校训  办“生动、大气”教育',1471845827,'','','集团概况',0),(2,3,0,0,0,0,'','index',1,'manage',0,0,0,0,0,0,0,1471845911,1474964362,1,0,0,'','/about/xzjy/index.html',1,1,1,'','总校长寄语',1471845894,'','','总校长寄语',0),(3,4,0,0,0,0,'','index',1,'manage',0,0,0,0,0,0,0,1471845931,1473825177,1,0,0,'','/about/jtzl/index.html',1,1,1,'','集团战略',1471845916,'','','集团战略测试',0),(4,6,0,0,0,0,'','index',1,'manage',0,0,0,0,0,0,0,1471846051,1471846172,1,0,0,'','/about/dlwz/index.html',1,1,1,'','地理位置',1471845938,'','','     //创建和初始化地图函数：    function initMap(){        createMap();//创建地图        setMapEvent();//设置地图事件        addMapControl();//向地图添加控件',0),(5,7,0,0,0,0,'','index',1,'manage',0,0,0,0,0,0,0,1471846092,1475056313,1,0,0,'','/about/contact/index.html',1,1,1,'','联系我们',1471846055,'','','																	本部校区					地址：江苏省苏州市吴江区开平路1666号						联系电话：0512-88888888						网址：www.wjsx.com																								本部校区					地址：江苏省',0),(40,10,0,100,0,0,'2016-09-21','40',7,'管文锦',0,0,0,0,0,0,0,1474433165,1476235525,1,0,0,'','/news/jtgs/2016-09-21/40.html',1,1,1,'','信纸上的旧时光（ 邱丽勤）',1474433133,'','','    六年级前，学校安排我担任一年级的班主任兼教研组长，任务之重让我倍感压力，但是我咬咬牙，扛了下来。当看到那一群活泼开朗的一年级新生，听着一声声稚嫩的&ldquo;老师&rdquo;，我',0),(37,10,0,88,0,0,'2016-09-21','37',7,'管文锦',0,0,0,0,0,0,0,1474432981,1476235568,1,0,0,'','/news/jtgs/2016-09-21/37.html',1,1,1,'','有一种女子，温暖而嫣然（史春晓）',1474432862,'','','              有一种女子，她是警察，穿梭在各类&ldquo;案发现场&rdquo;，凭借着自身的高智商和低姿态破解各桩案件；有一种女子，她是主持人，不着华丽的服装，却驰骋于各种场合，丰富多彩',0),(35,9,0,179,0,0,'2016-09-14','35',1,'manage',0,0,1,0,0,0,0,1473790122,1473790748,1,0,0,'','/news/hnews/2016-09-14/35.html',1,1,1,'','丰厚素养，点亮未来',1473789853,'/d/file/news/hnews/2016-09-14/6d51673a7e5ae22b33e6c121d240d3a3.jpg','','丹桂飘香、金风送爽，在第32个教师节来临之际，吴江实验小学教育集团的青年教师们在太湖校区总部大楼一楼南会议室，欢聚一堂，畅谈交流、展示风采、重温美好，展望未来。',0),(34,9,0,140,0,0,'2016-09-14','34',1,'manage',0,0,1,0,0,0,0,1473789054,1473789721,1,0,0,'','/news/hnews/2016-09-14/34.html',1,1,1,'','为教育现代化树立新标杆',1473788744,'/d/file/news/hnews/2016-09-14/693bee5d4ef2e6b48618e4b0ddca89a5.jpg','','9月9日上午，我校教育集团苏州湾实验小学新建落成启用仪式举行，区委书记梁一波，区委副书记、区长沈国芳，区人大常委会主任沈金明等出席仪式并为学校落成启用揭牌。副区长王悦',0),(41,9,0,146,0,0,'2016-10-01','41',1,'manage',0,0,1,0,0,0,0,1475253866,1475254410,1,0,0,'','/news/hnews/2016-10-01/41.html',1,1,1,'','播下英语素养的种子，让英语教学走得更稳更远',1475253703,'/d/file/news/hnews/2016-10-01/456347dee59c33ef0814e342b5d4acb7.jpg','','&mdash;&mdash;记苏州市小学《英语》二上教材培训活动 　　9月30日,苏州市小学《英语》二上教材培训活动在吴江实验小学太湖校区举行。此次活动将有助于全市小学英语教师更',0),(42,54,0,12,0,0,'2016-10-08','42',5,'苏州湾',0,1,1,0,0,0,0,1475891744,1475892663,1,0,0,'','/szw/ssgz/2016-10-08/42.html',1,1,1,'','播下英语素养的种子，让英语教学走得更稳更远',1475891598,'/d/file/szw/ssgz/2016-10-08/042ae70e0a812083b6cea2342255cee0.jpg','','&mdash;&mdash;记苏州市小学《英语》二上教材培训活动　　9月30日,苏州市小学《英语》二上教材培训活动在吴江实验小学太湖校区举行。此次活动将有助于全市小学英语教师更好',0),(39,10,0,60,0,0,'2016-09-21','39',7,'管文锦',0,0,0,0,0,0,0,1474433127,1476235541,1,0,0,'','/news/jtgs/2016-09-21/39.html',1,1,1,'','蜕变（杨艳）',1474433101,'','','2011年，学校开展首届&ldquo;一师一卷&rdquo;答辩大赛，在五语组老师的推选下，杨艳成为了参赛选手。真是压力山大，连夜里做梦的背景都清一色幻化为&ldquo;恐怖&rdquo;的答辩现场。',0),(38,10,0,63,0,0,'2016-09-21','38',7,'管文锦',0,0,0,0,0,0,0,1474433059,1476235556,1,0,0,'','/news/jtgs/2016-09-21/38.html',1,1,1,'','交友护照，亲历美好（管文锦）',1474433021,'','','今天中午，经过二年级走廊，几个孩子飞奔而来。&ldquo;老师，给我签个字吧！&rdquo;一本本绿色小册子递到了我眼前。哦，这是学校的&ldquo;学生交友护照&rdquo;，&ldquo;分享友情卡，心连',0),(43,35,0,14,0,0,'2016-10-08','43',4,'爱德',0,1,1,0,0,0,0,1475914318,1475915968,1,0,0,'','/aide/kcdiy/2016-10-08/43.html',1,1,1,'','爷爷奶奶课堂开讲啦',1475914002,'/d/file/aide/kcdiy/2016-10-08/b6a259f66a677948b08aa6839eebf5eb.jpg','——家长学校之特色课程','﻿   9月30日，吴江实验小学爱德校区邀请吴江&ldquo;鲈乡父母大讲堂&rdquo;家教专家何冬怡老师来给一年级爷爷奶奶做了《隔代教育》讲座。 \r\n   何老师从斯坦福大学一个调查说起，',0),(29,34,0,45,0,0,'2016-09-12','29',4,'爱德',0,0,0,0,0,0,0,1473637320,1473637423,1,0,0,'','/aide/dlad/2016-09-12/29.html',1,1,1,'','“学奥运精神 做爱德好少年”——爱德校区开学典礼篇',1473637224,'','“学奥运精神 做爱德好少年”——爱德校区开学典礼篇','     告别悠长的假期，迎着灿烂的阳光，爱德校区全体师生汇聚操场，隆重举行&ldquo;学奥运精神，做爱德好少年&rdquo;新学期开学典礼。\r\n     校歌声中，高年级大哥哥大姐姐牵着一年级',0);

/*Table structure for table `phome_ecms_news_check` */

DROP TABLE IF EXISTS `phome_ecms_news_check`;

CREATE TABLE `phome_ecms_news_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` char(20) NOT NULL DEFAULT '',
  `filename` char(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` char(14) NOT NULL DEFAULT '',
  `titleurl` char(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` char(80) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` char(120) NOT NULL DEFAULT '',
  `ftitle` char(120) NOT NULL DEFAULT '',
  `smalltext` char(255) NOT NULL DEFAULT '',
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news_check` */

insert  into `phome_ecms_news_check`(`id`,`classid`,`ttid`,`onclick`,`plnum`,`totaldown`,`newspath`,`filename`,`userid`,`username`,`firsttitle`,`isgood`,`ispic`,`istop`,`isqf`,`ismember`,`isurl`,`truetime`,`lastdotime`,`havehtml`,`groupid`,`userfen`,`titlefont`,`titleurl`,`stb`,`fstb`,`restb`,`keyboard`,`title`,`newstime`,`titlepic`,`ftitle`,`smalltext`,`diggtop`) values (31,51,0,0,0,0,'2016-09-13','31',5,'苏州湾',0,0,0,0,0,0,0,1473729120,1473729120,0,0,0,'','/szw/dlad/2016-09-13/31.html',1,1,1,'','2222',1473729116,'','',' 2222',0),(44,16,0,0,0,0,'2016-10-11','44',6,'城中',0,0,0,0,0,0,0,1476147877,1476147877,0,0,0,'','/chengzhong/news/xnews/2016-10-11/44.html',1,1,1,'','九九重阳节 浓浓温暖情',1476147569,'','——实小城中开展重阳节老少同乐送温暖活动','      岁岁重阳今又重阳，为了切实关心离退休教职员工的生活，为了帮助学生认识重阳节的传统习俗，培养少年儿童敬老、爱老的民族传统美德，使他们从小懂得关爱老人，10月9日，吴江实验',0),(45,16,0,0,0,0,'2016-10-11','45',6,'城中',0,0,0,0,0,0,0,1476169980,1476169980,0,0,0,'','/chengzhong/news/xnews/2016-10-11/45.html',1,1,1,'','给我一个舞台，还你一份精彩',1476169647,'','——城中校区大队委竞选','     &ldquo;给我一个舞台，还你一份精彩&rdquo;城中校区大队委竞选正在火热进行中，9月30日下午，在队员们整齐的口号中，进入校大队委选举环节，以上的候选人可都是经过层层选拔，从小',0);

/*Table structure for table `phome_ecms_news_check_data` */

DROP TABLE IF EXISTS `phome_ecms_news_check_data`;

CREATE TABLE `phome_ecms_news_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `writer` varchar(30) NOT NULL DEFAULT '',
  `befrom` varchar(60) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news_check_data` */

insert  into `phome_ecms_news_check_data`(`id`,`classid`,`keyid`,`dokey`,`newstempid`,`closepl`,`haveaddfen`,`infotags`,`writer`,`befrom`,`newstext`) values (31,51,'',1,0,0,0,'','','','<p>&nbsp;2222</p>'),(44,16,'',1,0,0,0,'','','','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 岁岁重阳今又重阳，为了切实关心离退休教职员工的生活，为了帮助学生认识重阳节的传统习俗，培养少年儿童敬老、爱老的民族传统美德，使他们从小懂得关爱老人，10月9日，吴江实验小学城中校区五年级229名少先队员和近五十位退休教职员工欢聚一堂，开展了一次充满温情而又轻松愉悦的重阳节老少同乐活动。<br />\r\n&nbsp;&nbsp;&nbsp; 活动分品菊花茶、文艺汇演、尝重阳糕、做登高玩偶四个环节。各班准备了丰富多彩的文艺游戏活动，展示了孩子们的风采。诗朗诵《园丁颂》表达了孩子们对爷爷奶奶们把最美好的年华献给实小学子的感谢；小品《蜡笔小新》和相声《好学生坏学生》两个节目让全场笑翻了天；手语歌《感恩的心》和小合唱《孝亲敬老歌》则让大家沐浴在&ldquo;会学习、会感恩、会生活&rdquo;的弄清之中。活动中，孩子们代表学校向离退休教师们为学校的发展所做出的无私奉献表示了诚挚的敬意。<br />\r\n&nbsp;&nbsp;&nbsp; 老教师们也带来的精彩的越剧演唱和大合唱。他们中有学校的老领导，也有普通教师，大家相聚在城中校区，品菊花茶，吃重阳糕，聊校园情，故友重逢，喝茶言欢，闲情逸致，怎个乐字了得。他们表示，感受到学校新的变化，感慨学校近几年所取得的成就， 对学校对老职工的关爱和温暖表示感谢。<br />\r\n最后，食堂阿姨为孩子们现场演示了重阳糕的做法。香喷喷的糕点，灵巧的制作，让孩子们对民族传统手艺充满了好奇，也为自己民族的智慧感到自豪。整场活动在人人动手捏登高玩偶中兴致盎然地落下了帷幕。<br />\r\n&nbsp;&nbsp;&nbsp; 本次老少同乐活动的开展，让孩子们感受到了中华民族&ldquo;尊老、敬老&rdquo;的优秀民族传统，培养了学生的动手实践能力，增强感恩意识，在活动中真切地体验了一把老少同乐。<br />\r\n&nbsp;</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/9fb186e3aedfc603991d0ce0ca7183a6.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/9fb186e3aedfc603991d0ce0ca7183a6.jpg\\\" /></a></p>\r\n<p>[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/f976f88bac648e816910a92b29700837.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/f976f88bac648e816910a92b29700837.jpg\\\" /></a></p>\r\n<p>[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/ab59e31db5a7e3c5f2ac3b82f49f9f1e.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/ab59e31db5a7e3c5f2ac3b82f49f9f1e.jpg\\\" /></a></p>\r\n<p>[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/b1b38e840df2d9ed9641de990345522d.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/b1b38e840df2d9ed9641de990345522d.jpg\\\" /></a></p>\r\n<p>[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/b8ac0f1846c5edcde88fe02b640291b8.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/b8ac0f1846c5edcde88fe02b640291b8.jpg\\\" /></a></p>\r\n<p>[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/1b9332bc7b936e5a3a0f056f301afc50.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/1b9332bc7b936e5a3a0f056f301afc50.jpg\\\" /></a></p>\r\n<p>[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/7794076b8544cb6c0b43fffd4026ec3e.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/7794076b8544cb6c0b43fffd4026ec3e.jpg\\\" /></a></p>\r\n<p>[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/f412b295f7afff71e063beac04084a66.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/f412b295f7afff71e063beac04084a66.jpg\\\" /></a></p>\r\n<p align=\\\"center\\\">&nbsp;</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/7648c0a93ba4b8886580872745f0baac.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/7648c0a93ba4b8886580872745f0baac.jpg\\\" /></a></p>\r\n<p align=\\\"center\\\">[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/e7b7d666d6b4b4af005edbe8ee831312.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/e7b7d666d6b4b4af005edbe8ee831312.jpg\\\" /></a></p>\r\n<p align=\\\"center\\\">[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/60a05ad25a1b4da0ab709781c760ad24.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/60a05ad25a1b4da0ab709781c760ad24.jpg\\\" /></a></p>\r\n<p align=\\\"center\\\">[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/ab23ff20cd3b9ce74db99e9a8e0739ce.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/ab23ff20cd3b9ce74db99e9a8e0739ce.jpg\\\" /></a></p>\r\n<p align=\\\"center\\\">[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/9637c452650ad4fb29edb515d9c15ad8.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/9637c452650ad4fb29edb515d9c15ad8.jpg\\\" /></a></p>\r\n<p align=\\\"center\\\">[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/3f8405610a96db396a49f4cafb22fd83.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/3f8405610a96db396a49f4cafb22fd83.jpg\\\" /></a></p>\r\n<p align=\\\"center\\\">[!--empirenews.page--]</p>\r\n<p align=\\\"center\\\"><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/c633b058a515e05d22c7692252814316.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/c633b058a515e05d22c7692252814316.jpg\\\" /></a></p>'),(45,16,'',1,0,0,0,'','','','<p>&nbsp;</p>\r\n<p style=\\\"text-align: left\\\">&nbsp;&nbsp;&nbsp; &ldquo;给我一个舞台，还你一份精彩&rdquo;城中校区大队委竞选正在火热进行中，9月30日下午，在队员们整齐的口号中，进入校大队委选举环节，以上的候选人可都是经过层层选拔，从小组，到班级，进行评分投票选举出的优秀队员，大队长候选人周恬伊说：&ldquo;我要争做一名优秀的少先队员，除了要做到学识渊博，还要有优秀的品德。&rdquo;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 演讲完毕，上一任六年级大队委们对候选人们进行现场提问，&ldquo;如何尽快让同学了解你？&rdquo;&ldquo;你上任后，该如何面对那边并不信服你的人呢？&rdquo;一个个问题问得有水平深度，竞选者经过短暂思考，都能说出自己的看法，有的落落大方地坦言了自己团队的优点和缺点，他们多样的回答即彰显了他们鲜明的个性，又显示出他们为同学们服务的心，无一例外，每个候选人的眼中都跳动着喜悦的光芒。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 演讲结束后由现场投票，原六年级大队委员现场计票，经过统计，选举结果终于产生。&nbsp;&nbsp; 李思慧同学稳居榜首，荣登大队长之座。&ldquo;给我一个舞台，还你一份精彩&rdquo;，选举出二十七名大队委候选人也用出色的表现为自己的竞选历程交上了一份满意的答卷，当上大队委，这是一份荣耀，但更是一份责任。相信新一届大队委们一定会时时刻刻记住自己肩上的责任，谱写出更璀璨的诗篇。<br />\r\n&nbsp;</p>\r\n<p><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/4eba747e15d3deb88f5e28232b22a86b.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/4eba747e15d3deb88f5e28232b22a86b.jpg\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p><a target=\\\"_blank\\\" href=\\\"/d/file/chengzhong/news/xnews/2016-10-11/673da3f4b5937722d74f30b4bc7b29e5.jpg\\\"><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"/d/file/chengzhong/news/xnews/2016-10-11/673da3f4b5937722d74f30b4bc7b29e5.jpg\\\" /></a></p>\r\n<p>&nbsp;</p>');

/*Table structure for table `phome_ecms_news_data_1` */

DROP TABLE IF EXISTS `phome_ecms_news_data_1`;

CREATE TABLE `phome_ecms_news_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `writer` varchar(30) NOT NULL DEFAULT '',
  `befrom` varchar(60) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news_data_1` */

insert  into `phome_ecms_news_data_1`(`id`,`classid`,`keyid`,`dokey`,`newstempid`,`closepl`,`haveaddfen`,`infotags`,`writer`,`befrom`,`newstext`) values (1,2,'',1,0,0,0,'','','','<h1 style=\\\"text-align: center;\\\">承&ldquo;爱德、求真&rdquo;校训  办&ldquo;生动、大气&rdquo;教育  &mdash;&mdash;吴江实验小学教育集团简介</h1>\r\n<p>&nbsp;</p>\r\n<p style=\\\"line-height:150%\\\">　　江苏省吴江实验小学是一所百年老校，创建于1904年（清光绪三十年），原名&ldquo;私立女子爱德小学&rdquo;，1981年经江苏省教育厅批准成为江苏省首批实验小学。2016年9月，经吴江区委区政府批准，正式成立教育集团，更名为&ldquo;吴江实验小学教育集团&rdquo;，推进办学的创新实践。</p>\r\n<p>&nbsp;</p>\r\n<p style=\\\"line-height:150%\\\">　　吴江实验小学教育集团现由4所小学（城中校区、爱德校区、太湖校区、苏州湾校区）、4所幼儿园（城中园区、爱德园区、太湖园区、苏州湾园区）组成。其中学生7010名，教师401名。学校秉承&ldquo;爱德、求真&rdquo;校训，崇尚&ldquo;为每个孩子的未来发展丰厚素养&rdquo;的办学理念，积极践行教育的&ldquo;儿童立场&rdquo;，坚守&ldquo;素养至上&rdquo;的质量使命，努力办出&ldquo;生动、大气&rdquo;的教育。</p>\r\n<p>&nbsp;</p>\r\n<p style=\\\"line-height:150%\\\">　　在上级和社会各界的关心下，历年来集团取得了显著的办学成果，荣获&ldquo;江苏省文明单位&rdquo;、&ldquo;江苏省模范学校&rdquo;、&ldquo;江苏省文明学校&rdquo; 、&ldquo;全国科普创新示范学校&rdquo;、&ldquo;江苏省青少年科技教育特色学校&quot;等荣誉称号。</p>\r\n<p style=\\\"line-height:150%\\\"><img src=\\\"/d/file/about/group/2016-09-14/d06b7f8ba6da6424cfa67fca6e85b60c.jpg\\\" alt=\\\"xiaoguo1220.jpg\\\" width=\\\"1220\\\" height=\\\"666\\\" /></p>'),(2,3,'',1,0,0,0,'','','','<p style=\\\"text-align: center;\\\"><img src=\\\"/d/file/about/xzjy/2016-09-27/1591537029002c13f2f5fa030c5f297b.jpg\\\" alt=\\\"校长寄语.jpg\\\" width=\\\"1000\\\" height=\\\"526\\\" /></p>'),(3,4,'',1,0,0,0,'','','','<p align=\\\"center\\\"><a href=\\\"/d/file/about/jtzl/2016-09-14/f505deb6c969ed012576c491cbcc8dd8.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/about/jtzl/2016-09-14/f505deb6c969ed012576c491cbcc8dd8.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/about/jtzl/2016-09-14/ad4f7b746ee346e7425b08b909c93df0.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/about/jtzl/2016-09-14/ad4f7b746ee346e7425b08b909c93df0.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/about/jtzl/2016-09-14/119b02753d7837b048d50b5451f51fcb.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/about/jtzl/2016-09-14/119b02753d7837b048d50b5451f51fcb.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/about/jtzl/2016-09-14/3f12d9e90680c1ea585871ccdcbd10a1.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/about/jtzl/2016-09-14/3f12d9e90680c1ea585871ccdcbd10a1.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/about/jtzl/2016-09-14/caa2cadc9687640f3869898794f4fcc1.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/about/jtzl/2016-09-14/caa2cadc9687640f3869898794f4fcc1.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/about/jtzl/2016-09-14/a87dc260796cc86c4b20a4ee85ed7333.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/about/jtzl/2016-09-14/a87dc260796cc86c4b20a4ee85ed7333.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/about/jtzl/2016-09-14/467e57094c885a0fb297ec1058faab9f.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/about/jtzl/2016-09-14/467e57094c885a0fb297ec1058faab9f.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>'),(4,6,'',1,0,0,0,'','','','<div id=\\\"dituContent\\\"></div>\r\n<script type=\\\"text/javascript\\\">\r\n    //创建和初始化地图函数：\r\n    function initMap(){\r\n        createMap();//创建地图\r\n        setMapEvent();//设置地图事件\r\n        addMapControl();//向地图添加控件\r\n        addMarker();//向地图中添加marker\r\n    }\r\n    \r\n    //创建地图函数：\r\n    function createMap(){\r\n        var map = new BMap.Map(\\\"dituContent\\\");//在百度地图容器中创建一个地图\r\n        var point = new BMap.Point(120.654933,31.157809);//定义一个中心点坐标\r\n        map.centerAndZoom(point,13);//设定地图的中心点和坐标并将地图显示在地图容器中\r\n        window.map = map;//将map变量存储在全局\r\n    }\r\n    \r\n    //地图事件设置函数：\r\n    function setMapEvent(){\r\n        map.enableDragging();//启用地图拖拽事件，默认启用(可不写)\r\n		\r\n		//map.disableDragging();     //禁止拖拽\r\n        map.enableScrollWheelZoom();//启用地图滚轮放大缩小\r\n        map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)\r\n        map.enableKeyboard();//启用键盘上下左右键移动地图\r\n    }\r\n    \r\n    //地图控件添加函数：\r\n    function addMapControl(){\r\n        //向地图中添加缩放控件\r\n	var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});\r\n	map.addControl(ctrl_nav);\r\n        //向地图中添加缩略图控件\r\n	var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});\r\n	map.addControl(ctrl_ove);\r\n        //向地图中添加比例尺控件\r\n	var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});\r\n	map.addControl(ctrl_sca);\r\n    }\r\n    \r\n    //标注点数组\r\n    var markerArr = [{title:\\\"太湖校区\\\",content:\\\"联系方式：63452079\\\",point:\\\"120.634488|31.145772\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"爱德校区\\\",content:\\\"联系方式：\\\",point:\\\"120.640524|31.155314\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"城中校区\\\",content:\\\"联系方式：\\\",point:\\\"120.647333|31.168872\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ];\r\n    //创建marker\r\n    function addMarker(){\r\n        for(var i=0;i<markerArr.length;i++){\r\n            var json = markerArr[i];\r\n            var p0 = json.point.split(\\\"|\\\")[0];\r\n            var p1 = json.point.split(\\\"|\\\")[1];\r\n            var point = new BMap.Point(p0,p1);\r\n			var iconImg = createIcon(json.icon);\r\n            var marker = new BMap.Marker(point,{icon:iconImg});\r\n			var iw = createInfoWindow(i);\r\n			var label = new BMap.Label(json.title,{\\\"offset\\\":new BMap.Size(json.icon.lb-json.icon.x+10,-20)});\r\n			marker.setLabel(label);\r\n            map.addOverlay(marker);\r\n            label.setStyle({\r\n                        borderColor:\\\"#808080\\\",\r\n                        color:\\\"#333\\\",\r\n                        cursor:\\\"pointer\\\"\r\n            });\r\n			\r\n			(function(){\r\n				var index = i;\r\n				var _iw = createInfoWindow(i);\r\n				var _marker = marker;\r\n				_marker.addEventListener(\\\"click\\\",function(){\r\n				    this.openInfoWindow(_iw);\r\n			    });\r\n			    _iw.addEventListener(\\\"open\\\",function(){\r\n				    _marker.getLabel().hide();\r\n			    })\r\n			    _iw.addEventListener(\\\"close\\\",function(){\r\n				    _marker.getLabel().show();\r\n			    })\r\n				label.addEventListener(\\\"click\\\",function(){\r\n				    _marker.openInfoWindow(_iw);\r\n			    })\r\n				if(!!json.isOpen){\r\n					label.hide();\r\n					_marker.openInfoWindow(_iw);\r\n				}\r\n			})()\r\n        }\r\n    }\r\n    //创建InfoWindow\r\n    function createInfoWindow(i){\r\n        var json = markerArr[i];\r\n        var iw = new BMap.InfoWindow(\\\"<b class=\\\'iw_poi_title\\\' title=\\\'\\\" + json.title + \\\"\\\'>\\\" + json.title + \\\"</b><div class=\\\'iw_poi_content\\\'>\\\"+json.content+\\\"</div>\\\");\r\n        return iw;\r\n    }\r\n    //创建一个Icon\r\n    function createIcon(json){\r\n        var icon = new BMap.Icon(\\\"http://app.baidu.com/map/images/us_mk_icon.png\\\", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowOffset:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})\r\n        return icon;\r\n    }\r\n    \r\n    initMap();//创建和初始化地图\r\n</script>'),(5,7,'',1,0,0,0,'','','','<ul class=\\\"clear\\\">\r\n    <li>\r\n    <div class=\\\"clear\\\"><img src=\\\"/skin/default/images/ad.jpg\\\" width=\\\"210\\\" height=\\\"167\\\" alt=\\\"\\\" /> <span>太湖校区</span>\r\n    <p>地址：江苏省苏州市吴江区开平路1666号<br />\r\n    联系电话：0512-63970028<br />\r\n    &nbsp;</p>\r\n    </div>\r\n    </li>\r\n    <li>\r\n    <div class=\\\"clear\\\"><img src=\\\"/skin/default/images/ad1.jpg\\\" width=\\\"210\\\" height=\\\"167\\\" alt=\\\"\\\" /> <span>爱德校区</span>\r\n    <p>地址：江苏省吴江区吴模路1915号<br />\r\n    联系电话：0512-63970028<br />\r\n    &nbsp;</p>\r\n    </div>\r\n    </li>\r\n    <li>\r\n    <div class=\\\"clear\\\"><img src=\\\"/skin/default/images/cz1.jpg\\\" width=\\\"210\\\" height=\\\"167\\\" alt=\\\"\\\" /> <span>城中校区</span>\r\n    <p>地址：江苏省苏州市吴江区永康路84号<br />\r\n    联系电话：0512-63452079<br />\r\n    &nbsp;</p>\r\n    </div>\r\n    </li>\r\n    <li>\r\n    <div class=\\\"clear\\\"><img src=\\\"/skin/default/images/bb1.jpg\\\" width=\\\"210\\\" height=\\\"167\\\" alt=\\\"\\\" /><span>苏州湾校区</span>\r\n    <p>地址：江苏省苏州市吴江区开平路2800号<br />\r\n    联系电话：0512-62312600<br />\r\n    &nbsp;</p>\r\n    </div>\r\n    </li>\r\n</ul>'),(34,9,'',1,0,0,0,'','','','<p>　　9月9日上午，我校教育集团苏州湾实验小学新建落成启用仪式举行，区委书记梁一波，区委副书记、区长沈国芳，区人大常委会主任沈金明等出席仪式并为学校落成启用揭牌。副区长王悦主持仪式。</p>\r\n<p>　　苏州湾实验小学2014年10月开始筹建，于2016年8月交付使用，占地面积115亩，建筑面积69974平方米，总投资3.96亿元。其中，小学部分设计规模为10轨60个班，可容纳2700名学生；幼儿园部分设计规模为6轨18个班，可容纳720名幼儿。</p>\r\n<p>　　梁一波在仪式上致辞，代表区四套班子对学校落成启用表示祝贺。他说，作为一项重要的民生工程，苏州湾实验小学的落成，将有力提升太湖新城的基础配套功能，增强城市的竞争力和吸引力，同时也为我区义务教育现代化建设树立新的标杆，为更好地实现公平教育提供优质的资源。</p>\r\n<p>　　梁一波强调，苏州湾实验小学作为吴江实验小学教育集团的新建校区，要秉承&ldquo;爱德、求真&rdquo;的校训，办生动大气的教育，努力为每个孩子的未来发展丰厚素养，用科学的办学理念、精致的教育管理、丰厚的课程内涵、一流的服务品质、优异的教学质量，把学校建成全面实施素质教育的品牌学校，为推进全区教育事业全面向优向上发展注入新的活力。 　&nbsp;</p>\r\n<p>　　9月9日是一个喜庆欢腾和值得纪念的日子。在随后的教师节座谈会上，沈国芳区长为我校&ldquo;吴江实验小学教育集团&rdquo;、&ldquo;管建刚作文教学研究室&rdquo;揭牌，这都标志着我校的学校发展走向了崭新的更高平台。</p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/bbc717aa4b24fd8bef728e32530f4ad3.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/bbc717aa4b24fd8bef728e32530f4ad3.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/2b0d0ec9e015443f648b696ad453a32b.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/2b0d0ec9e015443f648b696ad453a32b.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/c747455f457bc720b6f4812c7c786a47.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/c747455f457bc720b6f4812c7c786a47.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/04f4e0e5ca4ba1a27fc6436f2e0acfc5.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/04f4e0e5ca4ba1a27fc6436f2e0acfc5.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/214cb180d9b6f1446ce1ee2c73b8b6e8.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/214cb180d9b6f1446ce1ee2c73b8b6e8.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/5890fce77307149539dd1ffb681d691d.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/5890fce77307149539dd1ffb681d691d.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/e7e58c2ef9bc06818f1aaeaef381cdc2.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/e7e58c2ef9bc06818f1aaeaef381cdc2.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/929b3311f7b56bac496e119a22fa89ed.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/929b3311f7b56bac496e119a22fa89ed.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/c5d9cc94b7131e576b60d894e47fa69d.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/c5d9cc94b7131e576b60d894e47fa69d.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/90343954646ffa33b1dacd97c03e6d62.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/90343954646ffa33b1dacd97c03e6d62.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/56443aded981c4110dffdee9302238fa.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/56443aded981c4110dffdee9302238fa.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/802596b4d08cba7f9edb6278b921a142.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/802596b4d08cba7f9edb6278b921a142.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/8f6d50d082e3106b5b539b0c27ab4e7e.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/8f6d50d082e3106b5b539b0c27ab4e7e.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/7b057592005ea638a631c74fcac75971.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/7b057592005ea638a631c74fcac75971.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/3317d311b5b2990415906df258e371f7.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/3317d311b5b2990415906df258e371f7.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>'),(42,54,'',1,0,0,0,'','','','<div style=\\\"text-align: center;\\\">&mdash;&mdash;记苏州市小学《英语》二上教材培训活动</div>\r\n<div>　　9月30日,苏州市小学《英语》二上教材培训活动在吴江实验小学太湖校区举行。此次活动将有助于全市小学英语教师更好地使用新修订的义务教育教科书小学《英语》（二年级上册）&mdash;&mdash;契合教学实践，引导英语教师们基于儿童的启蒙英语素养出发，解读教学点 的重要准则，设计教学活动，真正经历学习的历程。</div>\r\n<div>此次活动共有各区县小学英语教研员及英语教师400名参加。</div>\r\n<div>　　上午，首先由吴江长安花苑小学的老师陈华英执教二年级英语《Unit4 Autumn Story time》，她精心设计教案，层层深入，让人耳目一新，深受学生喜爱。接下来由吴江实验小学的徐进老师执教《Unit4 Autumn Fun time&amp;Song time》。她巧妙引领学生在游戏、歌曲和儿歌中学会并区别了&ldquo;these&rdquo;和&ldquo;those&rdquo;。</div>\r\n<div>　　下午，张家港市江帆小学的陈晓雯老师执教二年级英语《Project1 An animal book Period1》，把一到四单元的句型很巧妙的结合在一起，通过形式各样的活动，孩子们的学习兴趣被很好的激发了出来；接下来由常熟市义庄小学的林小芳老师执教《Project1 An animal book Period2》，让学生通过玩、演、视、听、感知、体验、实践、参与及合作等活动方式，营造宽松、和谐、民主的英语教学氛围。两位老师的课堂各具特色，但却不约而同地让我们看到了课堂的魅力。 &nbsp; &nbsp;</div>\r\n<div>&nbsp; &nbsp; 在四节精彩的英语课之后，苏州市教研员袁峥老师为全体老师们做英语培训讲座《重整语音板块，优化单词教学》，她举例讲述了深入学生内心的备教材，如何活化教材灵动课堂。通过合理重整语音板块，从而优化单词教学的策略与方法。</div>\r\n<div>&nbsp; &nbsp; 整整一天的培训，让教师们收获巨大，在英语教学文化分享的盛会中感受教学思维巨大风暴的洗礼，在对英语课堂教学的追寻中，学习如何摆正教育的立场，站在儿童的视角，播下英语素养的种子，让英语教学走得更稳更远。</div>\r\n<div>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/szw/dlad/2016-10-08/b5ca78301030051eaf7271183c875794.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/szw/dlad/2016-10-08/b5ca78301030051eaf7271183c875794.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<br />\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/szw/dlad/2016-10-08/c3ac1e931357a60b5cd580f623ce61fd.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/szw/dlad/2016-10-08/c3ac1e931357a60b5cd580f623ce61fd.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<br />\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/szw/dlad/2016-10-08/f35a16fe2e46e2c6bbe68fcf7cfa674f.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/szw/dlad/2016-10-08/f35a16fe2e46e2c6bbe68fcf7cfa674f.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n</div>'),(29,34,'',1,0,0,0,'','','','<p>&nbsp;&nbsp;&nbsp;&nbsp; 告别悠长的假期，迎着灿烂的阳光，爱德校区全体师生汇聚操场，隆重举行&ldquo;学奥运精神，做爱德好少年&rdquo;新学期开学典礼。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp; 校歌声中，高年级大哥哥大姐姐牵着一年级小朋友的手，陆续通过红地毯，走向操场中央，全校师生以热烈的掌声表示欢迎。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp; 同学们回顾了丰富多彩的暑期生活，他们和父母有约、和朋友有约、和自己有约、和家庭有约。学生代表花予琦给大家带来的奥运小故事，讲述了不畏强敌、顽强拼搏、永不言弃的女排精神。最后，吴校长做了热情洋溢的新学期致辞。&nbsp; <br />\r\n&nbsp;&nbsp;&nbsp;&nbsp; 新学期，新的开始， 同学们纷纷表示女排精神是奥运精神的最好体现，他们会将奥运精神运用在新学期的学习和生活中，人人争做爱德好少年。</p>\r\n<p><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"http://www.wjsx.com/uploadfile/jpg/2016-9/201691152143331.jpg\\\" /></p>\r\n<p><img border=\\\"0\\\" alt=\\\"\\\" src=\\\"http://www.wjsx.com/uploadfile/jpg/2016-9/20169115264339.jpg\\\" /></p>'),(35,9,'',1,0,0,0,'','申晓峰','','<p>&nbsp;　　丹桂飘香、金风送爽，在第32个教师节来临之际，吴江实验小学教育集团的青年教师们在太湖校区总部大楼一楼南会议室，欢聚一堂，畅谈交流、展示风采、重温美好，展望未来。</p>\r\n<p>&nbsp;　　姚虎雄校长热情洋溢地为青年教师致辞，鼓励老师们修炼&ldquo;爱心&rdquo;和&ldquo;积极的生活态度&rdquo;，制作成长卡、填写需求单，在教育集团成立之际，站在更高更好的平台上，实现超越，不断发展自己，完善自己，刷出存在感。</p>\r\n<p>&nbsp;　　之后，李莺副校长宣布学校&ldquo;德音&rdquo;青年教师专业发展成长奖励方案。学校将根据理念与知识、教学（教育）与研究、总结与提炼灯三项内容考核分值累计评分，评出一二三等奖若干并由此设立&ldquo;助推青年教师成长专项奖&rdquo;，对不同等级奖的教师进行奖励。</p>\r\n<p>&nbsp;　　青年教师是学校教育力量的美好未来，为实小注入着青春与活力。</p>\r\n<p>&nbsp;　　在这喜悦的节日里，青年教师代表庞尔江、曾娴秋、钱亚闻、钟大海、蒋慕尧为大家带来了精彩的表演，有体育团队破冰热身、诗朗诵、舞蹈等等，优秀青年教师代表史春晓发言，讲诉自己对教育的感想与体会&hellip;&hellip;欢乐的气息起伏荡漾，青春的活力在这里氤氲歌唱。</p>\r\n<p>&nbsp;　　接下来收获荣誉的颁奖仪式将会场气氛推向了高潮。</p>\r\n<p>&nbsp;　　&ldquo;首届德音讲坛优秀成果奖&rdquo;、&ldquo;首届德音讲坛优秀个人奖&rdquo;、&ldquo;首届德音讲坛优秀导师奖&rdquo;隆重揭晓，潘国平副校长、肖菊红园长、吴韦副萍校长、杨春喜副校长分别为优秀老师们颁奖，爱德求真，生动大气的实小文化绵延不绝、生生不息，&ldquo;向着明亮那方&rdquo;生长！</p>\r\n<p>&nbsp;　　会末，老师合影留念，定格下最美的时光，最美的瞬间。</p>\r\n<p>&nbsp;　　教师的工作是平凡的，但平凡中自有它的伟大；</p>\r\n<p>&nbsp;　　教师的工作是艰苦的，但苦中有乐，其乐无穷。</p>\r\n<p>&nbsp;　　祝福老师们节日快乐!</p>\r\n<p>&nbsp;　　祝福实小教育集团越办越好，为师生的未来发展丰厚素养，点亮未来！</p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/c815a429e383a8f8fd4795c0bcf68b86.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/c815a429e383a8f8fd4795c0bcf68b86.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/ac84af6d6eb834eb93476e2a035dfea6.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/ac84af6d6eb834eb93476e2a035dfea6.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/4a5a8828b46b7452f5bcd77a5b4bb04c.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/4a5a8828b46b7452f5bcd77a5b4bb04c.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/640de84c738875af8fedf47f3c06b556.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/640de84c738875af8fedf47f3c06b556.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/1b0eb07a89012c644c23fd9529790708.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/1b0eb07a89012c644c23fd9529790708.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/516c152612a72d253fba91bb9d21f4b8.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/516c152612a72d253fba91bb9d21f4b8.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/59ce417733867756beeab90d376124d9.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/59ce417733867756beeab90d376124d9.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/2069411c1bcde8c9c0b492ede5041c3e.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/2069411c1bcde8c9c0b492ede5041c3e.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/f95e17da0f80f2bcdaa9d3e4431a494b.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/f95e17da0f80f2bcdaa9d3e4431a494b.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/58d5434c324f719270c8f46c911bfdaa.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/58d5434c324f719270c8f46c911bfdaa.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/893c23a7b07bbe55432a5594fccf3e8e.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/893c23a7b07bbe55432a5594fccf3e8e.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/42bf061e24443b03bf569cf47a57380f.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/42bf061e24443b03bf569cf47a57380f.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/0a2141b75ee243ac1b869d47396959b5.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/0a2141b75ee243ac1b869d47396959b5.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/139d0894326fc3a2ae40880db62ba6e0.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/139d0894326fc3a2ae40880db62ba6e0.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/607d532fb9392a0acd51fb727ee8ac05.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/607d532fb9392a0acd51fb727ee8ac05.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/8b15afc8dc0c9cf4e25d91a3a7a198d7.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/8b15afc8dc0c9cf4e25d91a3a7a198d7.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-09-14/8668927b4a702a7df259f59857dba2cd.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-09-14/8668927b4a702a7df259f59857dba2cd.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p style=\\\"text-align: center;\\\">　　</p>\r\n<p style=\\\"text-align: center;\\\">&nbsp;</p>'),(41,9,'',1,0,0,0,'','','','<p style=\\\"text-align: center;\\\">&mdash;&mdash;记苏州市小学《英语》二上教材培训活动</p>\r\n<p>&nbsp;</p>\r\n<p>　　9月30日,苏州市小学《英语》二上教材培训活动在吴江实验小学太湖校区举行。此次活动将有助于全市小学英语教师更好地使用新修订的义务教育教科书小学《英语》（二年级上册）&mdash;&mdash;契合教学实践，引导英语教师们基于儿童的启蒙英语素养出发，解读教学点的重要准则，设计教学活动，真正经历学习的历程。</p>\r\n<p>　　此次活动共有各区县小学英语教研员及英语教师400名参加。</p>\r\n<p>　　上午，首先由吴江长安花苑小学的老师陈华英执教二年级英语《Unit4 Autumn Story time》，她精心设计教案，层层深入，让人耳目一新，深受学生喜爱。接下来由吴江实验小学的徐进老师执教《Unit4 Autumn Fun time&amp;Song time》。她巧妙引领学生在游戏、歌曲和儿歌中学会并区别了&ldquo;these&rdquo;和&ldquo;those&rdquo;。</p>\r\n<p>　　下午，张家港市江帆小学的陈晓雯老师执教二年级英语《Project1 An animal book Period1》，把一到四单元的句型很巧妙的结合在一起，通过形式各样的活动，孩子们的学习兴趣被很好的激发了出来；接下来由常熟市义庄小学的林小芳老师执教《Project1 An animal book Period2》，让学生通过玩、演、视、听、感知、体验、实践、参与及合作等活动方式，营造宽松、和谐、民主的英语教学氛围。两位老师的课堂各具特色，但却不约而同地让我们看到了课堂的魅力。 &nbsp; &nbsp;</p>\r\n<p>　　在四节精彩的英语课之后，苏州市教研员袁峥老师为全体老师们做英语培训讲座《重整语音板块，优化单词教学》，她举例讲述了深入学生内心的备教材，如何活化教材灵动课堂。通过合理重整语音板块，从而优化单词教学的策略与方法。</p>\r\n<p>　　整整一天的培训，让教师们收获巨大，在英语教学文化分享的盛会中感受教学思维巨大风暴的洗礼，在对英语课堂教学的追寻中，学习如何摆正教育的立场，站在儿童的视角，播下英语素养的种子，让英语教学走得更稳更远。</p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-10-01/10edea159fd32aa74c11767dbe9347b8.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-10-01/10edea159fd32aa74c11767dbe9347b8.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-10-01/c68e40be4480de95f8020c080be8aa88.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-10-01/c68e40be4480de95f8020c080be8aa88.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-10-01/669bab8ade3df8fc2a5d33a2824d6f08.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-10-01/669bab8ade3df8fc2a5d33a2824d6f08.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-10-01/7c8aa96995d690149aee25401333a1e9.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-10-01/7c8aa96995d690149aee25401333a1e9.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-10-01/050d9652e77441bb6aabbff44d411a19.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-10-01/050d9652e77441bb6aabbff44d411a19.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-10-01/e4a20058594c31d7ddeb6cd6543ce0e4.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-10-01/e4a20058594c31d7ddeb6cd6543ce0e4.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p align=\\\"center\\\"><a href=\\\"/d/file/news/hnews/2016-10-01/7665ca18d6732041e3393442f3408ee7.jpg\\\" target=\\\"_blank\\\"><img src=\\\"/d/file/news/hnews/2016-10-01/7665ca18d6732041e3393442f3408ee7.jpg\\\" border=\\\"0\\\" alt=\\\"\\\" /></a></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>'),(37,10,'',1,0,0,0,'','史春晓','','<p align=\\\"center\\\" style=\\\"text-align: center; line-height: 150%; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-family: 楷体; background: #fcfcfc; mso-bidi-font-family: 楷体; mso-bidi-font-size: 12.0pt\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></font><span style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\"><font size=\\\"3\\\">有一种女子，她是警察，穿梭在各类&ldquo;案发现场&rdquo;，凭借着自身的高智商和低姿态破解各桩案件；<span lang=\\\"EN-US\\\"><o:p></o:p></span></font></span></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan\\\"><span style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\"><font size=\\\"3\\\">有一种女子，她是主持人，不着华丽的服装，却驰骋于各种场合，丰富多彩的家长活动、各式各样的班级活动是她们的主阵地；<span lang=\\\"EN-US\\\"><o:p></o:p></span></font></span></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;&nbsp;&nbsp; </span></span><span style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\">有一种女子，她是导演，每天拼尽自己的十八般武艺，不求功名利禄，只为每个孩子能找到属于自己的舞台，在属于自己的人生剧本里成为主角，发光、发亮；<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></font></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan\\\"><span style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\"><font size=\\\"3\\\">她是警察、是主持人、是导演、是舞蹈家、是装潢师、是裁判、是&hellip;&hellip;<span lang=\\\"EN-US\\\"><o:p></o:p></span></font></span></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\">实际上，她只是一名普通的人民教师。春夏秋冬，她坚守在那温暖如春的教室里，为了那份责任，为了那份执着，为了心中</span><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\">的梦想，书写一页页篇章，点亮一个个小人儿的童话世界；年月日时，她捍卫在那亲如一家人的实幼，不为活的多么光鲜亮丽，只为能让每一位孩子沐浴在爱与希望的阳光中。</span><span lang=\\\"EN-US\\\" style=\\\"font-family: 宋体\\\"><o:p></o:p></span></font></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan\\\"><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\"><font size=\\\"3\\\">这样的女子，不是一个，而是许多&hellip;&hellip;</font></span></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan\\\"><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\"><font size=\\\"3\\\">为了集体，她们放弃了太多个人的时间。</font></span></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\">瞧，上级来文中又出现了</span><span lang=\\\"EN-US\\\"><font face=\\\"Calibri\\\">XX</font></span><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\">参观团来访，准备全园接待的通知。我们也只是一群女人，自然在每次接到这样的通知后会不自觉的嘀咕几句：好忙，好累！但也是这群女人，在稍作嘀咕后又一次次团聚在一起，整装待发，想着如何能在有限的时间里将环境打造的更漂亮，如何能让孩子们在亲手布置的环境中快乐的学，开心的玩。加班加点可是常有的事，忙里忙外却也各个精神抖擞，一个灿烂的微笑，一个理解的拥抱，总能唤回对工作的热情和投入，总能将满足感化作正能量，温暖彼此，不断的前进。这一群女人看似柔弱，实则刚毅无比。</span></font></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan; mso-char-indent-count: 2.0\\\"><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\"><font size=\\\"3\\\">为了同事，她们也能倾尽所有。</font></span></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan; mso-char-indent-count: 2.0\\\"><font size=\\\"3\\\"><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\">听，</span><span lang=\\\"EN-US\\\"><font face=\\\"Calibri\\\">2015</font></span><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\">年苏州市幼儿园青年教师基本功大赛战鼓擂响啦，本人有幸能被选中成为全市参赛选手中的一员。只有亲身经历这样的大场面，感受才会这般深刻。比赛项目一出，全园骨干教师自发集中，从理论辅导到技能指导，大到比赛各项目的准备策略，小到一篇篇教案的修改，一个字一个字地斟酌，一句话一句话地揣摩，骨干教师们总是在我困惑的时候帮助我拨开迷雾，找到成功的捷径。每每深表歉意，一个关切的眼神，一句&ldquo;你的比赛就是大家的比赛、实幼的比赛！&rdquo;诸如此类的话语，装满着感动，这样的感动让我明白，为什么实幼教师总是如此出色，为什么每位实幼人面对各种活动时总是自信满满，因为我们深信着这不是一个人在战斗，是团队，是实小给予的强大力量！我为我们的教师喝彩，也为自己能成为实小的一员而喝彩！这一群女人，就这样手拉手，构成了一个大大的圆。</span></font></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\"><font size=\\\"3\\\">为了孩子，她们更是赴汤蹈火，在所不辞。<span lang=\\\"EN-US\\\"><o:p></o:p></span></font></span></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan; mso-char-indent-count: 2.0\\\"><font size=\\\"3\\\"><span style=\\\"font-family: 宋体; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\">再去看看</span><span style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\">小班教师们如何对待新生入园的孩子你就会明白，她们是一群何等强大的女子。我们往往会对新入园的孩子多加关注，却忽视了教师们同样面对着一群陌生的面孔，这群陌生的面孔在同时寻求&ldquo;妈妈&rdquo;独有的爱，你的爱偏袒一丝一毫都不可以。第一个星期，老师从家长手上接过哇哇大哭的孩子，不仅要忍受他们的高分贝，还需绞尽脑汁将这些&ldquo;小王子&rdquo;、&ldquo;小公主&rdquo;逗乐。这群女子呢？没有时间喝水，她的手里可能还抱着一个小朋友；没有时间上厕所，深怕一转眼的时间，一个小孩就溜到教室门外；她们甚至没有时间感受自己的劳累，自己的无奈。每每互相交流，总是会说：&rdquo;一天比一天好，孩子们在努力，我们可不能倒下！&rdquo;这一群女人，就是这样挺过一个个开学季。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></font></p>\r\n<p align=\\\"left\\\" style=\\\"text-align: left; line-height: 150%; text-indent: 24pt; mso-pagination: widow-orphan; mso-char-indent-count: 2.0\\\"><span style=\\\"font-family: 宋体; background: #fcfcfc; mso-ascii-font-family: Calibri; mso-hansi-font-family: Calibri\\\"><font size=\\\"3\\\">有一种女子，不似百合般清丽可人，不似牡丹般雍容华贵，却似向日葵般温暖而嫣然，永远迎着阳光的方向抬头、微笑。或许在通往教育事业前进的道路上会遇到艰难和困惑，但只需要相信实小，相信这朵大大的向日葵。是那光，是那爱如火把般点燃了实小人的心灯，使前方不再黑暗，使我们有勇气驶向远方，使我们温暖，使我们感动，使我们能为继续成为一名实小人而骄傲！来吧，实小的女人们，让我们一起欢呼，永远散发正能量；让我们一起拥抱，迎接最美好的未来！</font></span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-family: 宋体; background: #fcfcfc; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt\\\"><o:p></o:p></span></p>\r\n<p>&nbsp;</p>'),(38,10,'',1,0,0,0,'','管文锦','','<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\">今天中午，经过二年级走廊，几个孩子飞奔而来。&ldquo;老师，给我签个字吧！&rdquo;一本本绿色小册子递到了我眼前。哦，这是学校的&ldquo;学生交友护照&rdquo;，&ldquo;分享友情卡，心连你我他&rdquo;。打开册子，发现上面已经有无数老师和同学热情洋溢的笔迹了。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\">我饶有兴趣地问他们，&ldquo;是不是我签了名，我们就是朋友了？&rdquo;<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 18pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 1.5\\\"><span style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\">&ldquo;对的，对的！&rdquo;<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 18pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 1.5\\\"><span style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\">&ldquo;老师，你快签吧！&rdquo;<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\">看得出，我的&ldquo;磨蹭&rdquo;让孩子们非常焦急。我索性合上了册子，&ldquo;我都不知道你叫名字，你有什么特点，我不了解你啊，我们怎么是朋友呢？&rdquo;这一下，孩子们面面相觑了。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 18pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 1.5\\\"><span style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\">&ldquo;我先自我介绍，然后你也自我介绍，然后，我们才可以做朋友！&rdquo;于是，我先介绍自己的名字，任教班级，年龄爱好&hellip;&hellip;在我的&ldquo;身先士卒&rdquo;下，一个小男孩搔搔后脑勺，很腼腆地介绍了自己，于是我欣然在他的册子上写下了自己的名字和祝愿语。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\">铃声不合时宜地响了，孩子们飞奔回教室了。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-family: 仿宋_GB2312; font-size: 12pt\\\"><o:p>&nbsp;</o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">记得美国现代成人教育之父卡耐基说过：一个人要成功，</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">15%</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">靠知识，而</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">85%</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">则要靠人际关系和与人交往的能力。我校为了增强学生们的交往能力，引领他们学会懂礼貌，与人分享，关爱他人，从细微处入手，营造&ldquo;交往氛围&rdquo;。&ldquo;学生交友护照&rdquo;就是一枚珍贵的友谊&ldquo;通行证&rdquo;，鼓励孩子们自信地、勇敢地和&ldquo;他人&rdquo;说话，和&ldquo;他人&rdquo;交往。孩子们对&ldquo;交友护照&rdquo;充满新奇，并因这份新奇而充满蓬勃热情。但&ldquo;护照&rdquo;的意义并不仅仅止于&ldquo;通行&rdquo;，并不在于签了多少名，而在于之后的共识、共享、共进，共同追求&ldquo;朋友&rdquo;间的认知交流，那是一种积极向上的生命状态，一种发自内心的生命需求，对同伴学习生活充满了向往的激情&mdash;&mdash;</span><span style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\"> </font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">&ldquo;交友&rdquo;将儿童的眼睛一点一点地醒来，洞见、发现、觉察、醒目那般纯洁的真情。</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">所以，我们需要告诉孩子们，不要满足于&ldquo;签名&rdquo;，而要努力栽培、悉心浇灌&ldquo;签名&rdquo;后的友情萌芽，要懂得倾听、珍惜&ldquo;签名&rdquo;之后的友善与温暖。我们签下的一个个名字并非可供炫耀的&ldquo;友谊的橱窗&rdquo;，而是内心最真挚情感的呈现！</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">所以，我们需要告诉孩子们，平凡的生活中有着最真实的感动和最朴素的美。生活中的细枝末节里无不流动着点点滴滴的爱与暖。我们要用心发现与品味，让生命与生命相遇，让感动的心情美丽一生</span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-bidi-font-family: Arial\\\">。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">此外，我们和孩子们交友的过程中也同时净化着自己的心灵，让自己飞翔的心时时享受纯真的牵引。</span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt\\\">当我们共同懂得 &ldquo;交友护照&rdquo;的意义和价值时，&ldquo;朋友&rdquo;的理想境界便不期而至了！<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-bidi-font-family: Arial; mso-font-kerning: 0pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial\\\">&hellip;&hellip;</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">孩子的成长不仅是知识的积累，还是各种情感体验成长的历程；我们的学校不仅仅是一个学习的地方，更应该成为一个关注和丰富儿童内心情感世界，焕发生命智慧光彩的地方，真正成为一方精神的家园。在我们实小充盈着幸福感的&ldquo;和&middot;融&rdquo;家园中，每一位学生都应该成为充满感性、知性和灵性的完整意义的人。</span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-bidi-font-family: Arial; mso-font-kerning: 0pt; mso-bidi-font-weight: bold\\\">美中有真，美中有善，只有美好的才能长久地吸引孩子，才能让整个校园充满经久不衰的人性之光！<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt\\\">我相信&ldquo;友情护照&rdquo;将会成为我们师生生命河床中坚实的底部，成为灵魂珍贵的收藏，成为心灵不菲的财富，在漫漫一生中带来永远的温暖与幸福。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 15.05pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 1.0\\\"><b style=\\\"mso-bidi-font-weight: normal\\\"><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 15pt\\\"><o:p><font face=\\\"Times New Roman\\\">&nbsp;</font></o:p></span></b></p>\r\n<p>&nbsp;</p>'),(40,10,'',1,0,0,0,'',' 邱丽勤','','<p style=\\\"line-height: 21pt; margin-bottom: 15.75pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"font-family: 宋体; background: white; color: black; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt; mso-bidi-font-weight: bold\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;&nbsp;&nbsp; 六年级前，学校安排我担任一年级的班主任兼教研组长，任务之重让我倍感压力，但是我咬咬牙，扛了下来。当看到那一群活泼开朗的一年级新生，听着一声声稚嫩的&ldquo;老师&rdquo;，我暗暗下定决心，一定要做个深受孩子喜欢的好老师。六年光阴匆匆而过，转眼，孩子们小学毕业了，即将踏入初中的学习。在这离别之际，孩子们写下了一封封给我的信。读着信纸上那饱含深情的话语，我感到万分的不舍与欣慰，回想和孩子们相处的时光，那一幕幕仿佛就在眼前。</span></span></font></p>\r\n<p style=\\\"line-height: 21pt; margin-bottom: 15.75pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"font-family: 宋体; background: white; color: black; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt; mso-bidi-font-weight: bold\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;&nbsp;&nbsp; 王同学在信中这样说：&ldquo;邱老师，谢谢你！我会永远珍藏那一双袜子！&rdquo;记得那是一个寒冷的冬天，班上的一个小男生，在晨练时瑟瑟发抖，我看到后，便问他怎么回事，孩子支支吾吾不肯说，边上的同学忍不住插嘴说，他没穿袜子。我俯下身，拉起那男孩的裤脚一看，果然，他赤脚穿了球鞋，腿脖子冻得发紫。看到这样的情景，我忙想帮孩子打电话，让家长送袜子来。没想到孩子抽抽噎噎地说，奶奶生病了，爸爸妈妈都去了老家探病，把他寄养在表姑家，因为天气不好，带去的袜子洗了没干，他没好意思跟表姑说，就不穿袜子来上学了。听了孩子的话，我心疼极了，像这样冷的天气，不穿袜子，就穿一双毫无保暖性的球鞋，那滋味可想而知。等晨练结束，我马上把孩子带到办公室，给他泡了一杯暖暖的红糖水喝，喝完让他先去上课，自己则飞奔学校对面的小超市，挑了一双看上去最厚的袜子，买来给孩子穿上。小男孩也很懂事，下课后到办公室问我，这双袜子多少钱，我明天还给你。我摸摸他的头，微笑着说：&ldquo;好孩子，这双袜子是老师奖励你的，因为你很坚强！&rdquo;几天后，我接到孩子妈妈的电话，说自己刚从老家回来，孩子就迫不及待地把这件事告诉了妈妈。孩子妈妈在电话里说不完的感激，我却平静地对她说，这是我们老师应该做的事。要让学生喜欢自己，首先自己要发自内心地爱孩子。我在与学生的相处中，总时不时地把他们当成自己的孩子，像妈妈一样关心他们。我一直认为：教育如果没有爱，就像春天没有了阳光，树苗没有了水，就像生物离开了氧气，所以&ldquo;爱心&rdquo;是作为一名优秀教师最重要的条件。在我眼里，每个孩子都是天使，他们不论胖瘦、美丑、外向或内向，都拥有一颗纯洁的心灵，他们天真，活泼，可爱。所以，我爱每一个孩子，尊重每一个孩子，如此，我得到了孩子们的喜爱和尊重。</span></span></font></p>\r\n<p style=\\\"line-height: 21pt; margin-bottom: 15.75pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"font-family: 宋体; background: white; color: black; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt; mso-bidi-font-weight: bold\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;&nbsp;&nbsp; 平时和孩子们相处时，我总是时刻关注孩子的变化，有一次，我发现一个女孩上课时蔫蔫的，没有精神。便询问她怎么了，原来是女孩早上起晚了，来不及吃早餐。上第二节语文课，已经饿得没力气了。我忙叫她到办公室，拿出饼干，并给女孩泡了一杯水，嘱咐她吃掉，并告诉她，以后不要让爸爸妈妈叫自己起床，可以买一个小闹钟，自己设定好起床的时间。女孩连连点头称是。果然，从此以后，女孩再也没有迟到过。时隔六年，这样的一件小事，想不到女孩还记在心上，她在信中说：&ldquo;敬爱的邱老师，我永远不会忘记你给我吃的饼干，那是我吃过的最好吃的饼干，以后我也要当老师，像你一样关心自己的学生......&rdquo;真没想到，多年以前的一件小事，孩子还记得那么清晰。只要真心爱孩子，孩子一定会感受到老师的真诚。我坚信，用真诚的爱心与不泯的童心去与一颗颗稚嫩的童心对话，这份尊重如同一把金钥匙，可以破译任何心锁的密码，让孩子充分感受家庭以外的陌生世界的安全与友善，自然地表现真实的自我，快乐自由地成长。</span></span></font></p>\r\n<p style=\\\"line-height: 21pt; margin-bottom: 15.75pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"font-family: 宋体; background: white; color: black; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt; mso-bidi-font-weight: bold\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;&nbsp;&nbsp; 我深刻理解师德内涵，师以德为本,育人品当先,率先垂范，时时、处处严格要求自己，做师生的榜样。二年级开始，虽然我不再当班主任，却总是很早就进教室，督促孩子们静心阅读。地下有一张纸屑，我会弯腰把它捡起；看见扫帚落在教室门口，我准会把它收拾好；学生生病不能来上课，我带着几位学生，带着水果上门看望&hellip;&hellip;这一点一滴都从自己做起。我总是对孩子们说，做人要有责任心。潜移默化中，孩子们也会学着老师的样子，不但做好自己的份内事，还关心集体，关心他人。下雨天，有同学专门整理同学的雨伞，理得井井有条。班上有同学长期生病在家，孩子们就轮流为他抄写回家作业的题目，还在抄题本上写下一句句鼓励生病同学的话，那个生病的孩子虽然人在家中，却能感受到集体的温暖。学校的团体活动，大家都信心满满，力争上游。有孩子在信中这样说：&ldquo;邱老师，你不但教我们语文知识，你还教会了我们怎样做人，谢谢你！&rdquo;</span></span></font></p>\r\n<p style=\\\"line-height: 21pt; margin-bottom: 15.75pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"font-family: 宋体; background: white; color: black; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt; mso-bidi-font-weight: bold\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;&nbsp;&nbsp;&nbsp; 做教师有苦有甜，让我如此竭尽所能、无怨无悔地奋斗在这个平凡岗位上的动力，便是教师工作的巨大魅力&mdash;&mdash;辛苦付出后的精神享受。这一封封感人的书信，这信纸上的旧时光，令我难以忘怀。在接下来的日子里，我将一如既往的用真心对待孩子，让每一个孩子沐浴在爱与希望的阳光中。 </span></span></font></p>\r\n<p style=\\\"line-height: 21pt; margin-bottom: 15.75pt; mso-pagination: widow-orphan\\\"><font size=\\\"3\\\"><span lang=\\\"EN-US\\\" style=\\\"font-family: 宋体; background: white; color: black; mso-bidi-font-family: 宋体; mso-bidi-font-size: 12.0pt; mso-bidi-font-weight: bold\\\"><span style=\\\"mso-spacerun: yes\\\">&nbsp;</span></span></font></p>\r\n<p style=\\\"line-height: 21pt; margin-bottom: 15.75pt; mso-pagination: widow-orphan\\\">&nbsp;</p>'),(39,10,'',1,0,0,0,'','杨艳','','<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">2011</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">年，学校开展首届&ldquo;一师一卷&rdquo;答辩大赛，在五语组老师的推选下，杨艳成为了参赛选手。真是压力山大，连夜里做梦的背景都清一色幻化为&ldquo;恐怖&rdquo;的答辩现场。如何体现整本书的重难点，如何辨析试卷的难易程度，如何契合孩子的关键素养生长点，等等都成为了她日思夜想的重要问题，需要思考的实在太多了，需要把握的实在太沉了，她都快要哭了。吃饭的时候，想这张卷子；走路的时候，想这张卷子；睡觉的时候，还在想这张卷子，它分分秒秒都在折磨着杨艳，逼着杨艳思考再思考，精细再精细，从关注学生的素养入手，绝不出对他们</span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt\\\">终身发展没用的题</span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">！</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">从网上下载试卷，从书店买试卷，杨艳告诉自己不能辜负大伙对杨艳的期待，不能给咱五语组丢脸。然而，第一份融着多少心血、汗水及期待的卷子却遭遇了惨痛的&ldquo;滑铁卢&rdquo;，许多意想不到的问题，比如数字的规范性、田字格的大小、分值的比例、试题的实用性等都凸显出来了&mdash;&mdash;不服输的劲头猛地上来了，杨艳眼里噙着泪，重新开始，一题题研，一道道改，一行行琢磨&hellip;&hellip;&ldquo;关键素养&rdquo;恰如蒙尘的明珠，璀璨光芒渐露，她终于慢慢找到了如何发现它打磨它的路途！在艰难的探索中，明晰了每一道题为什么考，针对孩子哪个知识点&mdash;&mdash;从初稿到定稿，不记得自己修改了多少次，也不记得多少不眠之夜伴着这些思考而辗转反侧，把天熬亮！</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">最初考察&ldquo;查字典&rdquo;时，杨艳设计的是写读音、音序，再组词，后发现和查字典似乎关联不大；再次苦思冥想，另辟蹊径，设计为：写音序，再排列其在字典中的先后顺序&mdash;&mdash;如此这般，既考查了认读字的情况，又检测了</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">26</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">个音序字母排列的掌握情况；再如，起初设计的&ldquo;看图写话&rdquo;用了网上的现成图片，后改为班级亲子活动&ldquo;包饺子&rdquo;图片，孩子们考试后开心地告诉她，这是他们最喜欢的题目，杨艳感动极了，觉得自己所有的付出都是值得的！</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">一师一卷，爱你好痛苦，爱你又好幸福。小到一个标点符号，大到整题的删减、替换都让杨艳经历一次又一次的蜕变，张校长那精益求精的态度，更是有力地给予了杨艳成长的勇气和力量。难忘那天晚上</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">10</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">点</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">30</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">分了，她把文件用</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">QQ</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">传给她并留言&ldquo;明天帮我再看下试卷&rdquo;。没想到，当晚，张校就发来了修改意见。深夜，</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">11</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">点</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><font face=\\\"Times New Roman\\\">30</font></span><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">分，&ldquo;小企鹅&rdquo;仍在跳动闪烁中传递着交流的信息，心与心的呼应与共鸣，争论与辨析：要顺应童心，要直面关键素养，要培育灵性儿童&hellip;&hellip;</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">当数日后，在首届&ldquo;一师一卷&rdquo;的比赛中荣获一等奖的喜讯传来，杨艳不禁热泪盈眶。</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-ascii-font-family: \\\'Times New Roman\\\'; mso-hansi-font-family: \\\'Times New Roman\\\'\\\">蜕变，从刚开始的迷茫到最终的收获。</span><span lang=\\\"EN-US\\\" style=\\\"line-height: 150%; font-size: 12pt\\\"><o:p></o:p></span></p>\r\n<p class=\\\"MsoNormal\\\" style=\\\"line-height: 150%; text-indent: 24pt; margin: 0cm 24pt 0pt 0cm; mso-char-indent-count: 2.0\\\"><span style=\\\"line-height: 150%; font-family: 宋体; font-size: 12pt; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\\\">沐浴月光，啜饮清露，羽化成蝶。<span lang=\\\"EN-US\\\"><o:p></o:p></span></span></p>\r\n<p>&nbsp;</p>'),(43,35,'',1,0,0,0,'','','','<p><span style=\\\"font-size: medium\\\">﻿&nbsp;&nbsp; 　9月30日，吴江实验小学爱德校区邀请吴江&ldquo;鲈乡父母大讲堂&rdquo;家教专家何冬怡老师来给一年级爷爷奶奶做了《隔代教育》讲座。&nbsp;<br />\r\n&nbsp;&nbsp; 　何老师从斯坦福大学一个调查说起，让爷爷奶奶区别&ldquo;帮助孩子做，陪着孩子一起去做，看着孩子做，让孩子独立去做&rdquo;四种不同的教育方式和行为，懂得教育孩子需要&ldquo;爱与规则&rdquo;，家庭和谐，形成合力的重要。<br />\r\n&nbsp;吴伟红副校长简要介绍了学校开展&ldquo;60天好习惯养成&rdquo;活动，请爷爷奶奶着力支持配合的几个细节。爷爷奶奶认真聆听，表达要改进自己的一些教育方式，助力孩子养成十个学习和生活好习惯。<br />\r\n</span>&nbsp;</p>\r\n<p><img alt=\\\"4.jpg\\\" width=\\\"900\\\" height=\\\"675\\\" src=\\\"/d/file/aide/kcdiy/2016-10-08/b6a259f66a677948b08aa6839eebf5eb.jpg\\\" /></p>\r\n<p style=\\\"text-align: center\\\">&nbsp;</p>');

/*Table structure for table `phome_ecms_news_doc` */

DROP TABLE IF EXISTS `phome_ecms_news_doc`;

CREATE TABLE `phome_ecms_news_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` char(20) NOT NULL DEFAULT '',
  `filename` char(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` char(14) NOT NULL DEFAULT '',
  `titleurl` char(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` char(80) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` char(120) NOT NULL DEFAULT '',
  `ftitle` char(120) NOT NULL DEFAULT '',
  `smalltext` char(255) NOT NULL DEFAULT '',
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news_doc` */

/*Table structure for table `phome_ecms_news_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_news_doc_data`;

CREATE TABLE `phome_ecms_news_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `writer` varchar(30) NOT NULL DEFAULT '',
  `befrom` varchar(60) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news_doc_data` */

/*Table structure for table `phome_ecms_news_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_news_doc_index`;

CREATE TABLE `phome_ecms_news_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news_doc_index` */

/*Table structure for table `phome_ecms_news_index` */

DROP TABLE IF EXISTS `phome_ecms_news_index`;

CREATE TABLE `phome_ecms_news_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_news_index` */

insert  into `phome_ecms_news_index`(`id`,`classid`,`checked`,`newstime`,`truetime`,`lastdotime`,`havehtml`) values (1,2,1,1471845827,1471845887,1473823439,1),(2,3,1,1471845894,1471845911,1474964362,1),(3,4,1,1471845916,1471845931,1473825177,1),(4,6,1,1471845938,1471846051,1471846172,1),(5,7,1,1471846055,1471846092,1475056313,1),(40,10,1,1474433133,1474433165,1476235525,1),(37,10,1,1474432862,1474432981,1476235568,1),(35,9,1,1473789853,1473790122,1473790748,1),(34,9,1,1473788744,1473789054,1473789721,1),(41,9,1,1475253703,1475253866,1475254410,1),(31,51,0,1473729116,1473729120,1473729120,0),(45,16,0,1476169647,1476169980,1476169980,0),(44,16,0,1476147569,1476147877,1476147877,0),(39,10,1,1474433101,1474433127,1476235541,1),(38,10,1,1474433021,1474433059,1476235556,1),(43,35,1,1475914002,1475914318,1475915968,1),(29,34,1,1473637224,1473637320,1473637423,1),(42,54,1,1475891598,1475891744,1475892663,1);

/*Table structure for table `phome_ecms_photo` */

DROP TABLE IF EXISTS `phome_ecms_photo`;

CREATE TABLE `phome_ecms_photo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `picurl` varchar(200) NOT NULL DEFAULT '',
  `picsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo` */

/*Table structure for table `phome_ecms_photo_check` */

DROP TABLE IF EXISTS `phome_ecms_photo_check`;

CREATE TABLE `phome_ecms_photo_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `picurl` varchar(200) NOT NULL DEFAULT '',
  `picsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo_check` */

/*Table structure for table `phome_ecms_photo_check_data` */

DROP TABLE IF EXISTS `phome_ecms_photo_check_data`;

CREATE TABLE `phome_ecms_photo_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `filesize` varchar(10) NOT NULL DEFAULT '',
  `picsize` varchar(20) NOT NULL DEFAULT '',
  `picfbl` varchar(20) NOT NULL DEFAULT '',
  `picfrom` varchar(120) NOT NULL DEFAULT '',
  `morepic` mediumtext NOT NULL,
  `num` tinyint(4) NOT NULL DEFAULT '0',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo_check_data` */

/*Table structure for table `phome_ecms_photo_data_1` */

DROP TABLE IF EXISTS `phome_ecms_photo_data_1`;

CREATE TABLE `phome_ecms_photo_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `filesize` varchar(10) NOT NULL DEFAULT '',
  `picsize` varchar(20) NOT NULL DEFAULT '',
  `picfbl` varchar(20) NOT NULL DEFAULT '',
  `picfrom` varchar(120) NOT NULL DEFAULT '',
  `morepic` mediumtext NOT NULL,
  `num` tinyint(4) NOT NULL DEFAULT '0',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo_data_1` */

/*Table structure for table `phome_ecms_photo_doc` */

DROP TABLE IF EXISTS `phome_ecms_photo_doc`;

CREATE TABLE `phome_ecms_photo_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `picurl` varchar(200) NOT NULL DEFAULT '',
  `picsay` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo_doc` */

/*Table structure for table `phome_ecms_photo_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_photo_doc_data`;

CREATE TABLE `phome_ecms_photo_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` varchar(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` varchar(80) NOT NULL DEFAULT '',
  `filesize` varchar(10) NOT NULL DEFAULT '',
  `picsize` varchar(20) NOT NULL DEFAULT '',
  `picfbl` varchar(20) NOT NULL DEFAULT '',
  `picfrom` varchar(120) NOT NULL DEFAULT '',
  `morepic` mediumtext NOT NULL,
  `num` tinyint(4) NOT NULL DEFAULT '0',
  `width` varchar(12) NOT NULL DEFAULT '',
  `height` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo_doc_data` */

/*Table structure for table `phome_ecms_photo_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_photo_doc_index`;

CREATE TABLE `phome_ecms_photo_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo_doc_index` */

/*Table structure for table `phome_ecms_photo_index` */

DROP TABLE IF EXISTS `phome_ecms_photo_index`;

CREATE TABLE `phome_ecms_photo_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_photo_index` */

/*Table structure for table `phome_ecms_shop` */

DROP TABLE IF EXISTS `phome_ecms_shop`;

CREATE TABLE `phome_ecms_shop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `productno` varchar(30) NOT NULL DEFAULT '',
  `pbrand` varchar(30) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `unit` varchar(16) NOT NULL DEFAULT '',
  `weight` varchar(20) NOT NULL DEFAULT '',
  `tprice` float(11,2) NOT NULL DEFAULT '0.00',
  `price` float(11,2) NOT NULL DEFAULT '0.00',
  `buyfen` int(11) NOT NULL DEFAULT '0',
  `pmaxnum` int(11) NOT NULL DEFAULT '0',
  `productpic` varchar(255) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  `psalenum` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop` */

/*Table structure for table `phome_ecms_shop_check` */

DROP TABLE IF EXISTS `phome_ecms_shop_check`;

CREATE TABLE `phome_ecms_shop_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `productno` varchar(30) NOT NULL DEFAULT '',
  `pbrand` varchar(30) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `unit` varchar(16) NOT NULL DEFAULT '',
  `weight` varchar(20) NOT NULL DEFAULT '',
  `tprice` float(11,2) NOT NULL DEFAULT '0.00',
  `price` float(11,2) NOT NULL DEFAULT '0.00',
  `buyfen` int(11) NOT NULL DEFAULT '0',
  `pmaxnum` int(11) NOT NULL DEFAULT '0',
  `productpic` varchar(255) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  `psalenum` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop_check` */

/*Table structure for table `phome_ecms_shop_check_data` */

DROP TABLE IF EXISTS `phome_ecms_shop_check_data`;

CREATE TABLE `phome_ecms_shop_check_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop_check_data` */

/*Table structure for table `phome_ecms_shop_data_1` */

DROP TABLE IF EXISTS `phome_ecms_shop_data_1`;

CREATE TABLE `phome_ecms_shop_data_1` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop_data_1` */

/*Table structure for table `phome_ecms_shop_doc` */

DROP TABLE IF EXISTS `phome_ecms_shop_doc`;

CREATE TABLE `phome_ecms_shop_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` varchar(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` varchar(120) NOT NULL DEFAULT '',
  `productno` varchar(30) NOT NULL DEFAULT '',
  `pbrand` varchar(30) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `unit` varchar(16) NOT NULL DEFAULT '',
  `weight` varchar(20) NOT NULL DEFAULT '',
  `tprice` float(11,2) NOT NULL DEFAULT '0.00',
  `price` float(11,2) NOT NULL DEFAULT '0.00',
  `buyfen` int(11) NOT NULL DEFAULT '0',
  `pmaxnum` int(11) NOT NULL DEFAULT '0',
  `productpic` varchar(255) NOT NULL DEFAULT '',
  `newstext` mediumtext NOT NULL,
  `psalenum` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop_doc` */

/*Table structure for table `phome_ecms_shop_doc_data` */

DROP TABLE IF EXISTS `phome_ecms_shop_doc_data`;

CREATE TABLE `phome_ecms_shop_doc_data` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` char(255) NOT NULL DEFAULT '',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `newstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `haveaddfen` tinyint(1) NOT NULL DEFAULT '0',
  `infotags` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop_doc_data` */

/*Table structure for table `phome_ecms_shop_doc_index` */

DROP TABLE IF EXISTS `phome_ecms_shop_doc_index`;

CREATE TABLE `phome_ecms_shop_doc_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop_doc_index` */

/*Table structure for table `phome_ecms_shop_index` */

DROP TABLE IF EXISTS `phome_ecms_shop_index`;

CREATE TABLE `phome_ecms_shop_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`),
  KEY `newstime` (`newstime`),
  KEY `truetime` (`truetime`,`id`),
  KEY `havehtml` (`classid`,`truetime`,`havehtml`,`checked`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_ecms_shop_index` */

/*Table structure for table `phome_enewsad` */

DROP TABLE IF EXISTS `phome_enewsad`;

CREATE TABLE `phome_enewsad` (
  `adid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picurl` varchar(255) NOT NULL DEFAULT '',
  `url` text NOT NULL,
  `pic_width` int(10) unsigned NOT NULL DEFAULT '0',
  `pic_height` int(10) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `adtype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `target` varchar(10) NOT NULL DEFAULT '',
  `alt` varchar(120) NOT NULL DEFAULT '',
  `starttime` date NOT NULL DEFAULT '0000-00-00',
  `endtime` date NOT NULL DEFAULT '0000-00-00',
  `adsay` varchar(255) NOT NULL DEFAULT '',
  `titlefont` varchar(14) NOT NULL DEFAULT '',
  `titlecolor` varchar(10) NOT NULL DEFAULT '',
  `htmlcode` text NOT NULL,
  `t` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ylink` tinyint(1) NOT NULL DEFAULT '0',
  `reptext` text NOT NULL,
  PRIMARY KEY (`adid`),
  KEY `classid` (`classid`),
  KEY `t` (`t`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsad` */

/*Table structure for table `phome_enewsadclass` */

DROP TABLE IF EXISTS `phome_enewsadclass`;

CREATE TABLE `phome_enewsadclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsadclass` */

insert  into `phome_enewsadclass`(`classid`,`classname`) values (1,'默认广告分类');

/*Table structure for table `phome_enewsadminstyle` */

DROP TABLE IF EXISTS `phome_enewsadminstyle`;

CREATE TABLE `phome_enewsadminstyle` (
  `styleid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `stylename` char(30) NOT NULL DEFAULT '',
  `path` smallint(5) unsigned NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`styleid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsadminstyle` */

insert  into `phome_enewsadminstyle`(`styleid`,`stylename`,`path`,`isdefault`) values (1,'管理员后台界面',1,1),(2,'编辑后台界面',2,0);

/*Table structure for table `phome_enewsbefrom` */

DROP TABLE IF EXISTS `phome_enewsbefrom`;

CREATE TABLE `phome_enewsbefrom` (
  `befromid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `sitename` char(60) NOT NULL DEFAULT '',
  `siteurl` char(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`befromid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsbefrom` */

/*Table structure for table `phome_enewsbq` */

DROP TABLE IF EXISTS `phome_enewsbq`;

CREATE TABLE `phome_enewsbq` (
  `bqid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `bqname` varchar(60) NOT NULL DEFAULT '',
  `bqsay` text NOT NULL,
  `funname` varchar(60) NOT NULL DEFAULT '',
  `bq` varchar(60) NOT NULL DEFAULT '',
  `issys` tinyint(1) NOT NULL DEFAULT '0',
  `bqgs` text NOT NULL,
  `isclose` tinyint(1) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bqid`),
  KEY `classid` (`classid`),
  KEY `isclose` (`isclose`),
  KEY `myorder` (`myorder`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsbq` */

insert  into `phome_enewsbq`(`bqid`,`bqname`,`bqsay`,`funname`,`bq`,`issys`,`bqgs`,`isclose`,`classid`,`myorder`) values (1,'文字调用标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">栏目ID/专题ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看栏目ID点<a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\"><strong><u>这里</u></strong></a>，查看专题ID点<a onclick=\\\"window.open(\\\'../special/ListZt.php\\\');\\\"><strong><u>这里</u></strong></a>,当前ID=\\\'selfinfo\\\'<br />\r\n            多个栏目ID与专题ID可用,号格开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示条数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示前几条记录</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示时间</div>\r\n            </td>\r\n            <td height=\\\"25\\\">是否在标题后显示时间，0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">操作类型</div>\r\n            </td>\r\n            <td height=\\\"25\\\">具体看操作类型说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示栏目名</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">时间格式化</div>\r\n            </td>\r\n            <td height=\\\"25\\\">形式：Y-m-d H:i:s．默认为：\\\'(m-d)\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">附加SQL条件</div>\r\n            </td>\r\n            <td height=\\\"25\\\">附加调用条件，如：&quot;title=\\\'\\\'&quot;</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示排序</div>\r\n            </td>\r\n            <td height=\\\"25\\\">可指定按相应的字段排序，如：&quot;id desc&quot;</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td colspan=\\\"2\\\">\r\n            <div align=\\\"center\\\">（注：在栏目ID中写大栏目，系统会自己搜索子栏目的信息）</div>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_GetClassNews','phomenews',1,'[phomenews]栏目ID/专题ID,显示条数,标题截取数,是否显示时间,操作类型,是否显示栏目名,\\\'时间格式化\\\',附加SQL条件,显示排序[/phomenews]',0,1,9),(22,'留言板调用','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\\\"40%\\\">\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td width=\\\"60%\\\">参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示信息数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示记录数</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"left\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></div>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">&nbsp;</div>\r\n            </td>\r\n            <td height=\\\"25\\\">模板标签变量说明：<br />\r\n            留言ID：[!--lyid--]，留言内容：[!--lytext--]<br />\r\n            回复内容：[!--retext--]，留言时间：[!--lytime--]<br />\r\n            留言者：[!--name--]，留言者邮箱：[!--email--]<br />\r\n            序号：[!--no--]</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">留言分类ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"left\\\">点击<strong><a onclick=\\\"window.open(\\\'../tool/GbookClass.php\\\');\\\">这里</a></strong>查看分类ID，0为不限制</div>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_ShowLyInfo','gbookinfo',1,'[gbookinfo]显示信息数,标签模板ID,留言分类ID[/gbookinfo]',0,3,5),(23,'专题调用标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">&nbsp;</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <p>模板标签变量说明：(list.var)<br />\r\n            [!--classurl--]：专题链接，[!--classname--]：专题名称<br />\r\n            [!--classid--]：专题id，[!--classimg--]：专题图片<br />\r\n            [!--intro--]：专题简介,[!--no--]：序号</p>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">专题类别ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"left\\\">点击<strong><a onclick=\\\"window.open(\\\'../ListZtClass.php\\\');\\\">这里</a></strong>查看分类ID，0为不限制，多个分类ID用逗号隔开，如\\\'1,2\\\'</div>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示专题数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不限制</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">所属栏目ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">点击<strong><a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\">这里</a></strong>查看栏目ID，0为不限制，多个栏目ID用逗号隔开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_ShowZtData','eshowzt',1,'[eshowzt]标签模板ID,专题类别ID,显示专题数,所属栏目ID[/eshowzt]',0,2,6),(2,'图文信息调用(标题图片的信息)','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">栏目ID/专题ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看栏目ID点<a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\"><strong><u>这里</u></strong></a>，查看专题ID点<a onclick=\\\"window.open(\\\'../special/ListZt.php\\\');\\\"><strong><u>这里</u></strong></a>,当前ID=\\\'selfinfo\\\'<br />\r\n            多个栏目ID与专题ID可用,号格开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">每行显示条数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">每行显示几个图片</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示总信息数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示信息总数</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">图片宽度，图片高度</div>\r\n            </td>\r\n            <td height=\\\"25\\\">图文信息图片大小</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示标题</div>\r\n            </td>\r\n            <td height=\\\"25\\\">是否在图片下显示标题，0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td bgcolor=\\\"#ffffff\\\" height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td bgcolor=\\\"#ffffff\\\" height=\\\"25\\\">\r\n            <div align=\\\"center\\\">操作类型说明</div>\r\n            </td>\r\n            <td height=\\\"25\\\">具体看操作类型说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">附加SQL条件</div>\r\n            </td>\r\n            <td height=\\\"25\\\">附加调用条件，如：&quot;title=\\\'\\\'&quot;</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示排序</div>\r\n            </td>\r\n            <td height=\\\"25\\\">可指定按相应的字段排序，如：&quot;id desc&quot;</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_GetClassNewsPic','phomenewspic',1,'[phomenewspic]栏目ID/专题ID,每行显示条数,显示总信息数,图片宽度,图片高度,是否显示标题,标题截取数,操作类型,附加SQL条件,显示排序[/phomenewspic]',0,1,9),(5,'广告标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">广告ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看广告ID点<a onclick=\\\"window.open(\\\'../tool/ListAd.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_GetAd','phomead',1,'[phomead]广告ID[/phomead]',0,3,5),(6,'投票标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">投票ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看投票ID点<a onclick=\\\"window.open(\\\'../tool/ListVote.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_GetVote','phomevote',1,'[phomevote]投票ID[/phomevote]',0,3,5),(11,'带模板的信息调用标签[万能标签]','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">栏目ID/专题ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看栏目ID点<a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\"><strong><u>这里</u></strong></a>，查看专题ID点<a onclick=\\\"window.open(\\\'../special/ListZt.php\\\');\\\"><strong><u>这里</u></strong></a>,当前ID=\\\'selfinfo\\\'<br />\r\n            多个栏目ID与专题ID可用,号格开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示条数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示前几条记录</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示栏目名</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">操作类型</div>\r\n            </td>\r\n            <td height=\\\"25\\\">具体看操作类型说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">只显示有标题图片</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不限制，1为只显示有标题图片的信息</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">附加SQL条件</div>\r\n            </td>\r\n            <td height=\\\"25\\\">附加调用条件，如：&quot;title=\\\'\\\'&quot;</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示排序</div>\r\n            </td>\r\n            <td height=\\\"25\\\">可指定按相应的字段排序，如：&quot;id desc&quot;</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_GetEcmsInfo','ecmsinfo',1,'[ecmsinfo]栏目ID/专题ID,显示条数,标题截取数,是否显示栏目名,操作类型,模板ID,只显示有标题图片,附加SQL条件,显示排序[/ecmsinfo]',0,1,10),(12,'友情链接标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\\\"40%\\\">\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td width=\\\"60%\\\">参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">每行显示记录数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">每行显示记录数</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示总记录数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"left\\\">总记录数</div>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">操作类型</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"left\\\">0为所有记录，1为图片链接，2为文字链接</div>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">分类ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"left\\\">点击<strong><a onclick=\\\"window.open(\\\'../tool/LinkClass.php\\\');\\\">这里</a></strong>查看分类ID，0为不限制</div>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示原链接</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"left\\\">0为统计点击链接，1为显示原链接</div>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_GetSitelink','phomelink',1,'[phomelink]每行显示数,显示总数,操作类型,分类id,是否显示原链接[/phomelink]',0,3,5),(15,'引用文件标签','<p>[includefile]\\\'文件地址\\\'[/includefile]<br />\r\n文件地址需要为相对地址,并且从后台目录算起：&quot;../../header.html&quot;（这个地址的header.html是放在ecms根目录）</p>','sys_IncludeFile','includefile',1,'[includefile]\\\'文件地址\\\'[/includefile]',1,4,4),(16,'读取远程页面','<p>[readhttp]\\\'页面地址\\\'[/readhttp]</p>','sys_ReadFile','readhttp',1,'[readhttp]\\\'页面地址\\\'[/readhttp]',1,4,4),(17,'网站信息统计','<p>操作类型说明：\r\n<table border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\">\r\n    <tbody>\r\n        <tr bgcolor=\\\"#dbeaf5\\\">\r\n            <td width=\\\"25%\\\">\r\n            <div align=\\\"center\\\">操作类型</div>\r\n            </td>\r\n            <td width=\\\"75%\\\">内容</td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">0</div>\r\n            </td>\r\n            <td>统计栏目数据</td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">1</div>\r\n            </td>\r\n            <td>统计标题分类</td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">2</div>\r\n            </td>\r\n            <td>统计数据表</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<br />\r\n时间范围：0为不限；1为今日；2为本月；3为本年。<br />\r\n统计类型：0为统计信息数；1为统计评论数；2为统计点击数；3为统计下载数。<br />\r\n如果操作类型是&ldquo;统计数据表&rdquo;，栏目ID=\\\'数据表名\\\'</p>','sys_TotalData','totaldata',1,'[totaldata]栏目ID,操作类型,时间范围,统计类型[/totaldata]',0,1,7),(18,'FLASH幻灯信息调用','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">\r\n            <div align=\\\"center\\\">栏目ID/专题ID</div>\r\n            </div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看栏目ID点<a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\"><strong><u>这里</u></strong></a>，查看专题ID点<a onclick=\\\"window.open(\\\'../special/ListZt.php\\\');\\\"><strong><u>这里</u></strong></a>,当前ID=\\\'selfinfo\\\'<br />\r\n            多个栏目ID与专题ID可用,号格开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示总数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示信息总数</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">图片宽度，图片高度</div>\r\n            </td>\r\n            <td height=\\\"25\\\">图文信息图片大小</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示标题</div>\r\n            </td>\r\n            <td height=\\\"25\\\">是否在图片下显示标题，0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td bgcolor=\\\"#ffffff\\\" height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td bgcolor=\\\"#ffffff\\\" height=\\\"25\\\">\r\n            <div align=\\\"center\\\">操作类型说明</div>\r\n            </td>\r\n            <td height=\\\"25\\\">具体看操作类型说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">附加SQL条件</div>\r\n            </td>\r\n            <td height=\\\"25\\\">附加调用条件，如：&quot;title=\\\'\\\'&quot;</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示排序</div>\r\n            </td>\r\n            <td height=\\\"25\\\">可指定按相应的字段排序，如：&quot;id desc&quot;</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_FlashPixpic','phomeflashpic',1,'[phomeflashpic]栏目ID/专题ID,显示总数,图片宽度,图片高度,是否显示标题,标题截取数,操作类型,停顿秒数,附加SQL条件,显示排序[/phomeflashpic]',0,1,9),(19,'搜索关键字调用标签','<p>栏目id为0，则显示所有栏目的关键字</p>\r\n<p>操作类型：0为搜索热门排行，1为最新搜索排行</p>','sys_ShowSearchKey','showsearch',1,'[showsearch]每行显示条数,总条数,栏目id,操作类型[/showsearch]',0,1,7),(20,'循环子栏目数据标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">栏目ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看栏目ID点<a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\"><strong><u>这里</u></strong></a>，单个为父栏目ID，多栏目可用&quot;,&quot;格开<br />\r\n            当前栏目ID用：\\\'selfinfo\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示条数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示前几条记录</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示栏目名</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">操作类型</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0：栏目最新<br />\r\n            1：栏目热门<br />\r\n            2：栏目推荐<br />\r\n            3：栏目评论排行<br />\r\n            4：栏目头条<br />\r\n            5：栏目下载排行</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">只显示有标题图片</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不限制，1为只显示有标题图片的信息</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示栏目数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不限制</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示头条操作类型</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0：不显示栏目头条<br />\r\n            1：栏目内容简介<br />\r\n            2：栏目推荐信息<br />\r\n            3：栏目头条信息<br />\r\n            4：栏目最新信息</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">头条标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">头条简介截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">头条只显示有标题图片</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不限制，1为只显示有标题图片的信息</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">附加SQL条件</div>\r\n            </td>\r\n            <td height=\\\"25\\\">附加调用条件，如：&quot;title=\\\'\\\'&quot;</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示排序</div>\r\n            </td>\r\n            <td height=\\\"25\\\">可指定按相应的字段排序，如：&quot;id desc&quot;</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">头条的模板变量<br />\r\n            (标签模板的页面模板内容中使用)</div>\r\n            </td>\r\n            <td height=\\\"25\\\">[!--sonclass.id--]：信息ID<br />\r\n            [!--sonclass.title--]：信息标题<br />\r\n            [!--sonclass.oldtitle--]：信息标题(不截取字数)<br />\r\n            [!--sonclass.titlepic--]：标题图片<br />\r\n            [!--sonclass.titleurl--]：信息链接<br />\r\n            [!--sonclass.text--]：信息简介</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_ForSonclassData','listsonclass',1,'[listsonclass]栏目ID,显示条数,标题截取数,是否显示栏目名,操作类型,模板ID,只显示有标题图片,显示栏目数,显示头条操作类型,头条标题截取数,头条简介截取数,头条只显示有标题图片,附加SQL条件,显示排序[/listsonclass]',0,1,9),(21,'带模板的栏目导航标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">父栏目ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看栏目ID点<a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\"><strong><u>这里</u></strong></a><br />\r\n            \\\'0\\\'为显示所有一级栏目<br />\r\n            \\\'selfinfo\\\'显示本栏目下级栏目</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">&nbsp;</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <p>模板标签变量说明：[!--bclassname--]：父栏目名,[!--bclassurl--]：父栏目链接,[!--bclassid--]：父栏目id</p>\r\n            <p>list.var模板标签：<br />\r\n            [!--classurl--]：栏目链接,[!--classname--]：栏目名称,[!--classid--]：栏目id,[!--classimg--]：栏目图片,[!--intro--]：栏目简介,[!--num--]：信息数,[!--no--]：序号</p>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示栏目信息数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示栏目数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不限制</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_ShowClassByTemp','showclasstemp',1,'[showclasstemp]父栏目ID,标签模板ID,是否显示栏目信息数,显示栏目数[/showclasstemp]',0,2,6),(24,'图库模型分页标签','<p>\r\n<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">&nbsp;</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <p><strong>模板标签变量说明：</strong><br />\r\n            图片集JS数组：[!--photor--]，缩略图导航：[!--smalldh--]<br />\r\n            分页导航：[!--select--]，标题分页导航：[!--titleselect--]，分页列表式导航：[!--listpage--]</p>\r\n            <p><strong>list.var模板标签：</strong><br />\r\n            图片名称：[!--picname--]，图片地址：[!--picurl--]</p>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">导航图片宽度<br />\r\n            导航图片高度</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为按原图大小</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</p>','sys_PhotoMorepage','eshowphoto',1,'[eshowphoto]标签模板ID,导航图片宽度,导航图片高度[/eshowphoto]',0,1,7),(25,'复选项输出函数','<p>如：[echocheckbox]\\\'title\\\',\\\'&lt;br&gt;\\\'[/echocheckbox]：表示按回车分隔输出title字段的项</p>','sys_EchoCheckboxFValue','echocheckbox',1,'[echocheckbox]\\\'字段\\\',\\\'分隔符\\\'[/echocheckbox]',0,3,5),(26,'相关链接标签','<p><strong>标签模板ID</strong>：查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a><br />\r\n<strong>操作类型</strong>：0为默认；1为按表；2为按栏目；3为按专题<br />\r\n<strong>操作对象</strong>：对应操作类型的表/栏目/专题。空则为默认。<br />\r\n<strong>是否显示栏目名</strong>：0为不显示；1为显示<br />\r\n<strong>只显示标题图片的信息</strong>：0为不限；1为只显示标题图片的信息</p>','sys_GetOtherLinkInfo','otherlink',1,'[otherlink]标签模板ID,操作对象,调用条数,标题截取字数,是否显示栏目名,操作类型,只显示标题图片的信息[/otherlink]',0,1,9),(27,'评论调用标签','<p><strong>标签模板ID</strong>：查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a><br />\r\n<strong>栏目ID</strong>：0为不限调用栏目，父栏目会应用于子栏目<br />\r\n<strong>信息ID</strong>：0为不限<br />\r\n<strong>显示推荐评论</strong>：0为不限；1为只显示推荐评论<br />\r\n<strong>操作类型</strong>：0为按发布时间；1为按支持数；2为按反对数<br />\r\n<strong>标签模板说明</strong>：[!--title--]:信息标题；[!--titleurl--]:信息链接；[!--id--]:信息ID<br />\r\n[!--classid--]:栏目ID；[!--plid--]:评论ID；[!--username--]:发表者；[!--no--]:编号<br />\r\n[!--pltext--]:评论内容；[!--pltime--]:评论时间；[!--zcnum--]:支持数；[!--fdnum--]:反对数</p>','sys_ShowPlInfo','showplinfo',1,'[showplinfo]调用条数,标签模板ID,栏目ID,信息ID,显示推荐评论,操作类型[/showplinfo]',0,3,5),(28,'循环栏目导航标签','<p>\r\n<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">父栏目ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看栏目ID点<a onclick=\\\"window.open(\\\'../ListClass.php\\\');\\\"><strong><u>这里</u></strong></a><br />\r\n            \\\'0\\\'为显示所有一级栏目<br />\r\n            \\\'selfinfo\\\'显示本栏目下级栏目<br />\r\n            多栏目固定调用可用&quot;,&quot;格开</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">&nbsp;</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\r\n            <p>模板标签变量说明：[!--bclassname--]：父栏目名,[!--bclassurl--]：父栏目链接,[!--bclassid--]：父栏目id，[!--bclassimg--]：父栏目图片,[!--bintro--]：父栏目简介,[!--bnum--]：父栏目信息数,[!--bno--]：父栏目序号</p>\r\n            <p>list.var模板标签：<br />\r\n            [!--classurl--]：栏目链接,[!--classname--]：栏目名称,[!--classid--]：栏目id,[!--classimg--]：栏目图片,[!--intro--]：栏目简介,[!--num--]：信息数,[!--no--]：序号</p>\r\n            </td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示栏目信息数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不显示，1为显示</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示栏目数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">0为不限制</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</p>','sys_ForShowSonClass','listshowclass',1,'[listshowclass]父栏目ID,标签模板ID,是否显示栏目信息数,显示栏目数[/listshowclass]',0,2,6),(29,'调用TAGS的信息标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">TAGS的ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看TAGS的ID点<a onclick=\\\"window.open(\\\'../tags/ListTags.php\\\');\\\"><strong><u>这里</u></strong></a><br />\r\n            多个ID可以用,号格开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示条数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示前几条记录</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标签模板ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看标签模板ID点<a onclick=\\\"window.open(\\\'ListBqtemp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">栏目ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">限制只调用某一个或多个栏目的信息<br />\r\n            多个ID可以用,号格开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">系统模型ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">限制只调用某一个或多个系统模型的信息<br />\r\n            多个ID可以用,号格开，如\\\'1,2\\\'</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_eShowTagsInfo','tagsinfo',0,'[tagsinfo]TAGS的ID,显示条数,标题截取数,标签模板ID,栏目ID,系统模型ID[/tagsinfo]',0,0,9),(30,'调用碎片的信息标签','<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">碎片变量名</div>\r\n            </td>\r\n            <td height=\\\"25\\\">查看碎片变量名点<a onclick=\\\"window.open(\\\'../sp/ListSp.php\\\');\\\"><strong><u>这里</u></strong></a></td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示条数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示前几条记录</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">标题截取数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">截取几个字符</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<br>\r\n<table cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\" border=\\\"0\\\">\r\n  <tbody>\r\n    <tr> \r\n      <td colspan=\\\"2\\\"> <div align=\\\"center\\\">碎片标签模板变量说明</div></td>\r\n    </tr>\r\n    <tr bgcolor=\\\"#ffffff\\\"> \r\n      <td width=\\\"34%\\\" height=\\\"25\\\"> \r\n        <div align=\\\"center\\\">静态碎片</div></td>\r\n      <td width=\\\"66%\\\" height=\\\"25\\\">模板标签变量说明：[!--the.spname--]：碎片名称,[!--the.spid--]：碎片ID,[!--the.sppic--]：碎片效果图,[!--the.spsay--]：碎片描述 \r\n        <p>list.var模板标签：<br>\r\n          [!--title--]：标题,[!--oldtitle--]：标题ALT,[!--newstime--]：发布时间,[!--id--]：碎片信息ID,[!--titleurl--]：标题链接,[!--titlepic--]：标题缩图,[!--bigpic--]：标题大图,[!--titlepre--]：标题左边,[!--titlenext--]：标题右边,[!--smalltext--]：内容简介,[!--no.num--]：编号</p></td>\r\n    </tr>\r\n    <tr bgcolor=\\\"#ffffff\\\"> \r\n      <td height=\\\"25\\\"> <div align=\\\"center\\\">动态碎片</div></td>\r\n      <td height=\\\"25\\\">模板标签变量说明：[!--the.spname--]：碎片名称,[!--the.spid--]：碎片ID,[!--the.sppic--]：碎片效果图,[!--the.spsay--]：碎片描述 \r\n        <p>list.var模板标签：<br>\r\n          支持变量同模型信息调用</p></td>\r\n    </tr>\r\n    <tr bgcolor=\\\"#ffffff\\\"> \r\n      <td height=\\\"25\\\"> <div align=\\\"center\\\">代码碎片</div></td>\r\n      <td height=\\\"25\\\">无需标签模板，直接显示代码内容</td>\r\n    </tr>\r\n  </tbody>\r\n</table>\r\n','sys_eShowSpInfo','spinfo',0,'[spinfo]碎片变量名,显示条数,标题截取数[/spinfo]',0,0,9),(31,'调用TAGS标签','<table border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" width=\\\"100%\\\" bgcolor=\\\"#dbeaf5\\\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <div align=\\\"center\\\">参数</div>\r\n            </td>\r\n            <td>参数说明</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">分类ID</div>\r\n            </td>\r\n            <td height=\\\"25\\\">\\\'\\\'空为不限制，查看TAGS分类ID点<a onclick=\\\"window.open(\\\'../tags/TagsClass.php\\\');\\\"><strong><u>这里</u></strong></a><br />\r\n            多个可以用,号格开，如\\\'1,2\\\'<br />\r\n            内容页显示当前TAGS可以用\\\'selfinfo\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示数量</div>\r\n            </td>\r\n            <td height=\\\"25\\\">显示前几条记录，0为显示所有（\\\'selfinfo\\\'本设置无效）</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">每行显示数量</div>\r\n            </td>\r\n            <td height=\\\"25\\\">一行显示多少个TAGS，0为不换行</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示排序</div>\r\n            </td>\r\n            <td height=\\\"25\\\">使用默认设置\\\'\\\'空就可以，默认是\\\'tagid desc\\\'（\\\'selfinfo\\\'本设置无效）</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">只显示推荐</div>\r\n            </td>\r\n            <td height=\\\"25\\\">只显示推荐的TAGS，0为不限制，1为限制（\\\'selfinfo\\\'本设置无效）</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">推荐TAGS属性</div>\r\n            </td>\r\n            <td height=\\\"25\\\">如果是推荐的TAGS，内容是否要加粗或加红（\\\'selfinfo\\\'本设置无效）<br />\r\n            设置\\\'s\\\'表示加粗、\\\'r\\\'表示加红、同时加粗加红用\\\'s,r\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">显示间隔符</div>\r\n            </td>\r\n            <td height=\\\"25\\\">TAGS之间显示间隔符，默认是\\\' &amp;nbsp; \\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">是否显示信息数量</div>\r\n            </td>\r\n            <td height=\\\"25\\\">是否在TAGS后显示信息数量，0为不显示，1为显示（\\\'selfinfo\\\'本设置无效）</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">链接附加参数</div>\r\n            </td>\r\n            <td height=\\\"25\\\">可指定在TAGS链接后面增加参数，比如：\\\'&amp;tempid=模板ID\\\'</td>\r\n        </tr>\r\n        <tr bgcolor=\\\"#ffffff\\\">\r\n            <td height=\\\"25\\\">\r\n            <div align=\\\"center\\\">链接变量名</div>\r\n            </td>\r\n            <td height=\\\"25\\\">可指定在链接使用的变量名(需加引号)：tagname或tagid，默认为tagname，比如：\\\'tagname\\\'</td>\r\n        </tr>\r\n    </tbody>\r\n</table>','sys_eShowTags','showtags',0,'[showtags]分类ID,显示数量,每行显示数量,显示排序,只显示推荐,推荐TAGS属性,显示间隔符,是否显示信息数,链接附加参数,链接变量名[/showtags]',0,0,9);

/*Table structure for table `phome_enewsbqclass` */

DROP TABLE IF EXISTS `phome_enewsbqclass`;

CREATE TABLE `phome_enewsbqclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsbqclass` */

insert  into `phome_enewsbqclass`(`classid`,`classname`) values (1,'信息调用'),(2,'栏目调用'),(3,'非信息调用'),(4,'其它标签');

/*Table structure for table `phome_enewsbqtemp` */

DROP TABLE IF EXISTS `phome_enewsbqtemp`;

CREATE TABLE `phome_enewsbqtemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `modid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `temptext` text NOT NULL,
  `showdate` varchar(50) NOT NULL DEFAULT '',
  `listvar` text NOT NULL,
  `subnews` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rownum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `docode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsbqtemp` */

insert  into `phome_enewsbqtemp`(`tempid`,`tempname`,`modid`,`temptext`,`showdate`,`listvar`,`subnews`,`rownum`,`classid`,`docode`) values (1,'子栏目导航标签模板',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<li classid=\\\"[!--classid--]\\\"><a href=\\\"[!--classurl--]\\\">[!--classname--]</a></li>',0,1,0,0),(2,'首页新闻调用',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"[!--titleurl--]\\\">\r\n						<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">[!--smalltext--]...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>',0,1,0,0),(3,'友情链接',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','m-d','<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\" target=\\\"_blank\\\">[!--title--]</a></li>',60,1,0,0),(4,'标题+时间',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d','<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',0,1,0,0),(5,'图片+标题+简介',1,'[!--empirenews.listtemp--]\r\n<!--list.var1-->\r\n[!--empirenews.listtemp--]','Y-m-d H:i:s','<table width=\\\"94%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"0\\\" cellspacing=\\\"6\\\" class=\\\"picText\\\">\r\n<tr valign=\\\"top\\\">\r\n<td><a href=\\\"[!--titleurl--]\\\" target=\\\"_blank\\\"><img width=\\\"70\\\" height=\\\"78\\\" src=\\\"[!--titlepic--]\\\" alt=\\\"[!--oldtitle--]\\\" /></a></td>\r\n<td><strong><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a></strong>[!--smalltext--]</td>\r\n</tr>\r\n</table>',56,1,0,0),(7,'循环子栏目标签',1,'<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"title\">\r\n<tr>\r\n<td><strong>[!--the.classname--]</strong></td>\r\n<td align=\"right\"><a href=\"[!--the.classurl--]\">更多&gt;&gt;</a></td>\r\n</tr>\r\n</table>\r\n<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"box\">\r\n<tr>\r\n<td><ul>\r\n       [!--empirenews.listtemp--]\r\n       <!--list.var1-->\r\n       [!--empirenews.listtemp--]\r\n</ul>\r\n</td>\r\n</tr>\r\n</table>','Y-m-d','<li><a href=\"[!--titleurl--]\" title=\"[!--oldtitle--]\">[!--title--]</a> <span>[!--newstime--]</span></li>',0,1,0,0),(8,'头条标题',1,'[!--empirenews.listtemp--]\r\n<!--list.var1-->\r\n[!--empirenews.listtemp--]','Y-m-d H:i:s','<strong><a href=\"[!--titleurl--]\">[!--title--]</a></strong>\r\n<p>　　[!--smalltext--]</p>',150,1,0,0),(10,'排行列表',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<li class=\"no[!--no.num--]\"><a href=\"[!--titleurl--]\" title=\"[!--oldtitle--]\">[!--title--]</a></li>',0,1,0,0),(11,'太湖分校首页头条',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a>\r\n<div>[!--smalltext--]</div>',0,1,0,0),(12,'太湖分校首页公告',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','$listtemp=\\\'<li>\r\n							<div>\r\n								<span>\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n								<span>\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n							</div>\r\n							<a href=\\\"[!--titleurl--]\\\" class=\\\"xyzx_c_r_c_fa\\\">[!--title--]</a>\r\n							<p>[!--smalltext--]</p>\r\n							<a href=\\\"[!--titleurl--]\\\">详细内容&gt;&gt;</a>\r\n						</li>\\\';',0,1,0,0),(13,'导航二级',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<dd><a href=\\\"[!--classurl--]\\\">[!--classname--]</a></dd>',0,1,0,0),(14,'首页集团故事',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<div class=\\\"D_news_right_body clear\\\">\r\n				<span class=\\\"span1\\\">\r\n					<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				</span>\r\n				<span class=\\\"span2\\\">[!--smalltext--]</span>\r\n			</div>\r\n',0,1,0,0),(15,'城中分校左侧分类',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<li><a href=\\\"[!--classurl--]\\\"><span>[!--classname--]</span></a></li>\r\n',0,1,0,0),(16,'城中分校首页类别调用',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d H:i:s','<li><a href=\\\"[!--classurl--]\\\" target=\\\"_blank\\\">[!--classname--]</a></li>\r\n',0,1,0,0),(17,'爱德实时关注',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d','<li class=\\\"clear\\\"><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',0,1,0,0),(18,'爱德实时关注推荐',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d','<div><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" width=\\\"310\\\" height=\\\"145\\\"></a>\r\n						<div>\r\n							<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n							<span>[!--smalltext--]...</span>\r\n						</div>\r\n					</div>',0,1,0,0),(19,'爱德首页图片+简介',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d','<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n<p>[!--smalltext--]</p>',0,1,0,0),(20,'爱德首页点亮德育',1,'[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]','Y-m-d','<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--smalltext--]</span></li>',0,1,0,0);

/*Table structure for table `phome_enewsbqtempclass` */

DROP TABLE IF EXISTS `phome_enewsbqtempclass`;

CREATE TABLE `phome_enewsbqtempclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsbqtempclass` */

/*Table structure for table `phome_enewsbuybak` */

DROP TABLE IF EXISTS `phome_enewsbuybak`;

CREATE TABLE `phome_enewsbuybak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL DEFAULT '',
  `card_no` char(120) NOT NULL DEFAULT '',
  `buytime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cardfen` int(10) unsigned NOT NULL DEFAULT '0',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userdate` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsbuybak` */

/*Table structure for table `phome_enewsbuygroup` */

DROP TABLE IF EXISTS `phome_enewsbuygroup`;

CREATE TABLE `phome_enewsbuygroup` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `gname` varchar(255) NOT NULL DEFAULT '',
  `gmoney` int(10) unsigned NOT NULL DEFAULT '0',
  `gfen` int(10) unsigned NOT NULL DEFAULT '0',
  `gdate` int(10) unsigned NOT NULL DEFAULT '0',
  `ggroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gzgroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `buygroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gsay` text NOT NULL,
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsbuygroup` */

/*Table structure for table `phome_enewscard` */

DROP TABLE IF EXISTS `phome_enewscard`;

CREATE TABLE `phome_enewscard` (
  `cardid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_no` char(30) NOT NULL DEFAULT '',
  `password` char(20) NOT NULL DEFAULT '',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  `cardfen` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` date NOT NULL DEFAULT '0000-00-00',
  `cardtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `carddate` int(10) unsigned NOT NULL DEFAULT '0',
  `cdgroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cdzgroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cardid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewscard` */

/*Table structure for table `phome_enewsclass` */

DROP TABLE IF EXISTS `phome_enewsclass`;

CREATE TABLE `phome_enewsclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `bclassid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classname` varchar(50) NOT NULL DEFAULT '',
  `sonclass` text NOT NULL,
  `is_zt` tinyint(1) NOT NULL DEFAULT '0',
  `lencord` smallint(6) NOT NULL DEFAULT '0',
  `link_num` tinyint(4) NOT NULL DEFAULT '0',
  `newstempid` smallint(6) NOT NULL DEFAULT '0',
  `onclick` int(11) NOT NULL DEFAULT '0',
  `listtempid` smallint(6) NOT NULL DEFAULT '0',
  `featherclass` text NOT NULL,
  `islast` tinyint(1) NOT NULL DEFAULT '0',
  `classpath` text NOT NULL,
  `classtype` varchar(10) NOT NULL DEFAULT '',
  `newspath` varchar(20) NOT NULL DEFAULT '',
  `filename` tinyint(1) NOT NULL DEFAULT '0',
  `filetype` varchar(10) NOT NULL DEFAULT '',
  `openpl` tinyint(1) NOT NULL DEFAULT '0',
  `openadd` tinyint(1) NOT NULL DEFAULT '0',
  `newline` int(11) NOT NULL DEFAULT '0',
  `hotline` int(11) NOT NULL DEFAULT '0',
  `goodline` int(11) NOT NULL DEFAULT '0',
  `classurl` varchar(200) NOT NULL DEFAULT '',
  `groupid` smallint(6) NOT NULL DEFAULT '0',
  `myorder` smallint(6) NOT NULL DEFAULT '0',
  `filename_qz` varchar(20) NOT NULL DEFAULT '',
  `hotplline` tinyint(4) NOT NULL DEFAULT '0',
  `modid` smallint(6) NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `firstline` tinyint(4) NOT NULL DEFAULT '0',
  `bname` varchar(50) NOT NULL DEFAULT '',
  `islist` tinyint(1) NOT NULL DEFAULT '0',
  `searchtempid` smallint(6) NOT NULL DEFAULT '0',
  `tid` smallint(6) NOT NULL DEFAULT '0',
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `maxnum` int(11) NOT NULL DEFAULT '0',
  `checkpl` tinyint(1) NOT NULL DEFAULT '0',
  `down_num` tinyint(4) NOT NULL DEFAULT '0',
  `online_num` tinyint(4) NOT NULL DEFAULT '0',
  `listorder` varchar(50) NOT NULL DEFAULT '',
  `reorder` varchar(50) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `classimg` varchar(255) NOT NULL DEFAULT '',
  `jstempid` smallint(6) NOT NULL DEFAULT '0',
  `addinfofen` int(11) NOT NULL DEFAULT '0',
  `listdt` tinyint(1) NOT NULL DEFAULT '0',
  `showclass` tinyint(1) NOT NULL DEFAULT '0',
  `showdt` tinyint(1) NOT NULL DEFAULT '0',
  `checkqadd` tinyint(1) NOT NULL DEFAULT '0',
  `qaddlist` tinyint(1) NOT NULL DEFAULT '0',
  `qaddgroupid` text NOT NULL,
  `qaddshowkey` tinyint(1) NOT NULL DEFAULT '0',
  `adminqinfo` tinyint(1) NOT NULL DEFAULT '0',
  `doctime` smallint(6) NOT NULL DEFAULT '0',
  `classpagekey` varchar(255) NOT NULL DEFAULT '',
  `dtlisttempid` smallint(6) NOT NULL DEFAULT '0',
  `classtempid` smallint(6) NOT NULL DEFAULT '0',
  `nreclass` tinyint(1) NOT NULL DEFAULT '0',
  `nreinfo` tinyint(1) NOT NULL DEFAULT '0',
  `nrejs` tinyint(1) NOT NULL DEFAULT '0',
  `nottobq` tinyint(1) NOT NULL DEFAULT '0',
  `ipath` varchar(255) NOT NULL DEFAULT '',
  `addreinfo` tinyint(1) NOT NULL DEFAULT '0',
  `haddlist` tinyint(4) NOT NULL DEFAULT '0',
  `sametitle` tinyint(1) NOT NULL DEFAULT '0',
  `definfovoteid` smallint(6) NOT NULL DEFAULT '0',
  `wburl` varchar(255) NOT NULL DEFAULT '',
  `qeditchecked` tinyint(1) NOT NULL DEFAULT '0',
  `wapstyleid` smallint(6) NOT NULL DEFAULT '0',
  `repreinfo` tinyint(1) NOT NULL DEFAULT '0',
  `pltempid` smallint(6) NOT NULL DEFAULT '0',
  `cgroupid` text NOT NULL,
  `yhid` smallint(6) NOT NULL DEFAULT '0',
  `wfid` smallint(6) NOT NULL DEFAULT '0',
  `cgtoinfo` tinyint(1) NOT NULL DEFAULT '0',
  `bdinfoid` varchar(25) NOT NULL DEFAULT '',
  `repagenum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keycid` smallint(6) NOT NULL DEFAULT '0',
  `allinfos` int(10) unsigned NOT NULL DEFAULT '0',
  `infos` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`classid`),
  KEY `bclassid` (`bclassid`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclass` */

insert  into `phome_enewsclass`(`classid`,`bclassid`,`classname`,`sonclass`,`is_zt`,`lencord`,`link_num`,`newstempid`,`onclick`,`listtempid`,`featherclass`,`islast`,`classpath`,`classtype`,`newspath`,`filename`,`filetype`,`openpl`,`openadd`,`newline`,`hotline`,`goodline`,`classurl`,`groupid`,`myorder`,`filename_qz`,`hotplline`,`modid`,`checked`,`firstline`,`bname`,`islist`,`searchtempid`,`tid`,`tbname`,`maxnum`,`checkpl`,`down_num`,`online_num`,`listorder`,`reorder`,`intro`,`classimg`,`jstempid`,`addinfofen`,`listdt`,`showclass`,`showdt`,`checkqadd`,`qaddlist`,`qaddgroupid`,`qaddshowkey`,`adminqinfo`,`doctime`,`classpagekey`,`dtlisttempid`,`classtempid`,`nreclass`,`nreinfo`,`nrejs`,`nottobq`,`ipath`,`addreinfo`,`haddlist`,`sametitle`,`definfovoteid`,`wburl`,`qeditchecked`,`wapstyleid`,`repreinfo`,`pltempid`,`cgroupid`,`yhid`,`wfid`,`cgtoinfo`,`bdinfoid`,`repagenum`,`keycid`,`allinfos`,`infos`,`addtime`) values (1,0,'关于我们','|2|3|4|5|6|7|',0,25,10,0,0,0,'',0,'about','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'关于我们',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1471845499),(2,1,'集团概况','',0,25,10,1,0,1,'|1|',1,'about/group','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'集团概况',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,1,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1471845523),(3,1,'总校长寄语','',0,25,10,1,0,1,'|1|',1,'about/xzjy','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'总校长寄语',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,1,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1471845545),(4,1,'集团战略','',0,25,10,1,0,1,'|1|',1,'about/jtzl','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'集团战略',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,1,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1471845590),(5,1,'集团荣誉','',0,25,10,10,0,10,'|1|',1,'about/honor','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'集团荣誉',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1471845613),(6,1,'地理位置','',0,25,10,11,0,1,'|1|',1,'about/dlwz','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'地理位置',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,1,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1471845652),(7,1,'联系我们','',0,25,10,1,0,1,'|1|',1,'about/contact','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'联系我们',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,1,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1471845676),(8,0,'新闻动态','|9|10|11|12|',0,25,10,0,0,1,'',0,'news','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'新闻动态',1,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1471846605),(9,8,'新闻资讯','',0,25,10,10,0,1,'|8|',1,'news/hnews','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'新闻资讯',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,3,3,1471846635),(10,8,'集团故事','',0,25,10,10,0,1,'|8|',1,'news/jtgs','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'集团故事',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,4,4,1471846664),(11,8,'图片掠影','',0,25,10,10,0,9,'|8|',1,'news/pic','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'图片掠影',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1471846690),(12,8,'视频关注','',0,25,10,10,0,9,'|8|',1,'news/video','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'视频关注',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1471846868),(13,0,'相关链接','',0,25,10,10,0,1,'',1,'links','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'相关链接',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1471848667),(14,0,'城中分校','|16|17|19|20|21|23|24|26|27|28|30|31|32|',0,25,10,0,0,0,'',0,'chengzhong','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'城中分校',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472694729),(15,14,'资讯中心','|16|17|',0,25,10,0,0,11,'|14|',0,'chengzhong/news','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'资讯中心',1,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472695043),(16,15,'校园新闻','',0,25,10,12,0,11,'|14|15|',1,'chengzhong/news/xnews','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'校园新闻',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,2,0,1472697762),(17,15,'校园公告','',0,25,10,12,0,11,'|14|15|',1,'chengzhong/news/notice','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'校园公告',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697785),(18,14,'阅读中心','|19|20|21|',0,25,10,0,0,11,'|14|',0,'chengzhong/read','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'阅读中心',1,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697809),(19,18,'经典导读','',0,25,10,12,0,11,'|14|18|',1,'chengzhong/read/jddd','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'经典导读',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697834),(20,18,'挑战作家','',0,25,10,12,0,11,'|14|18|',1,'chengzhong/read/tzzj','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'挑战作家',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697851),(21,18,'我的一本课外书','',0,25,10,12,0,11,'|14|18|',1,'chengzhong/read/kws','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'我的一本课外书',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697879),(22,14,'班级','|23|24|',0,25,10,0,0,11,'|14|',0,'chengzhong/bj','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'班级',1,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697901),(23,22,'英才少年','',0,25,10,12,0,11,'|14|22|',1,'chengzhong/bj/ycsn','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'英才少年',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697924),(24,22,'班级风采','',0,25,10,12,0,11,'|14|22|',1,'chengzhong/bj/bjfc','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'班级风采',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697946),(25,14,'教师','|26|27|28|',0,25,10,0,0,11,'|14|',0,'chengzhong/techer','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'教师',1,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472697976),(26,25,'专业阅读','',0,25,10,12,0,11,'|14|25|',1,'chengzhong/techer/zyyd','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'专业阅读',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472698000),(27,25,'田野笔记','',0,25,10,12,0,11,'|14|25|',1,'chengzhong/techer/tybj','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'田野笔记',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472698023),(28,25,'求真论坛','',0,25,10,12,0,11,'|14|25|',1,'chengzhong/techer/qzlt','.html','Y-m-d',0,'.html',0,0,10,10,10,'http://bbs.wjsx.com',0,0,'',10,1,1,10,'求真论坛',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472698052),(29,14,'科技','|30|31|32|',0,25,10,0,0,11,'|14|',0,'chengzhong/keji','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'科技',1,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472698070),(30,29,'科普中国','',0,25,10,12,0,11,'|14|29|',1,'chengzhong/keji/kpzg','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'科普中国',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472698096),(31,29,'挑战科学','',0,25,10,12,0,11,'|14|29|',1,'chengzhong/keji/tzkx','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'挑战科学',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472698120),(32,29,'我的一个小研究','',0,25,10,12,0,11,'|14|29|',1,'chengzhong/keji/xyj','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'我的一个小研究',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472698145),(33,0,'爱德分校','|34|35|36|37|38|39|',0,25,10,0,0,0,'',0,'aide','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'爱德分校',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,2,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472781307),(34,33,'点亮爱德','',0,25,10,13,0,12,'|33|',1,'aide/dlad','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'点亮爱德',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1472782266),(35,33,'课程DIY','',0,25,10,13,0,13,'|33|',1,'aide/kcdiy','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'课程DIY',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1472782304),(36,33,'教师POST','',0,25,10,13,0,13,'|33|',1,'aide/jspost','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'教师POST',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472782332),(37,33,'时时关注','',0,25,10,13,0,12,'|33|',1,'aide/ssgz','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'时时关注',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472782390),(38,33,'班级GARDEN','',0,25,10,13,0,13,'|33|',1,'aide/bj','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'班级GARDEN',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472782417),(39,33,'心理SPA','',0,25,10,13,0,13,'|33|',1,'aide/xlspa','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'心理SPA',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1472782462),(40,0,'太湖分校','|42|43|44|45|46|47|48|49|',0,25,10,0,0,0,'',0,'taihu','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'太湖分校',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,3,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473127461),(41,40,'实时关注','|42|43|44|',0,25,10,0,0,14,'|40|',0,'taihu/news','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'实时关注',1,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473128315),(42,41,'动感校园','',0,25,10,14,0,14,'|40|41|',1,'taihu/news/hnews','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'动感校园',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473128394),(43,41,'媒体报道','',0,25,10,14,0,14,'|40|41|',1,'taihu/news/mtbd','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'新闻动态',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473128977),(44,41,'通知公告','',0,25,10,14,0,14,'|40|41|',1,'taihu/news/notice','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'通知公告',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473129005),(45,40,'关于我们','',0,25,10,14,0,14,'|40|',1,'taihu/about','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'关于我们',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473129045),(46,40,'德育天地','',0,25,10,14,0,15,'|40|',1,'taihu/kcdiy','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'德育天地',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','/skin/default/taihu/images/xyfw_info_1.jpg',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473129068),(47,40,'教育科研','',0,25,10,14,0,15,'|40|',1,'taihu/jspost','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'教育科研',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473129091),(48,40,'教学之窗','',0,25,10,14,0,15,'|40|',1,'taihu/bj','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'教学之窗',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473129150),(49,40,'精致校园','',0,25,10,14,0,15,'|40|',1,'taihu/xlspa','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'精致校园',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473129166),(50,0,'苏州湾校区','|51|52|53|54|55|56|',0,25,10,0,0,0,'',0,'szw','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'苏州湾校区',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,4,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473380699),(51,50,'点亮苏州湾','',0,25,10,15,0,16,'|50|',1,'szw/dlad','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'点亮苏州湾',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,0,1473380742),(52,50,'课程DIY','',0,25,10,15,0,17,'|50|',1,'szw/kcdiy','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'课程DIY',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473380927),(53,50,'教师POST','',0,25,10,15,0,17,'|50|',1,'szw/jspost','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'教师POST',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473380962),(54,50,'时时关注','',0,25,10,15,0,16,'|50|',1,'szw/ssgz','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'时时关注',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,1,1,1473380986),(55,50,'班级GARDEND','',0,25,10,15,0,17,'|50|',1,'szw/bj','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'班级GARDEND',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473381009),(56,50,'心理SPA','',0,25,10,15,0,17,'|50|',1,'szw/xlspa','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'心理SPA',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,1,0,0,0,0,'',1,6,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1473381041),(57,0,'集团组成','|',0,25,10,0,0,0,'',0,'group','.html','Y-m-d',0,'.html',0,0,10,10,10,'',0,0,'',10,1,1,10,'集团组成',0,0,1,'news',0,0,2,2,'id DESC','newstime DESC','','',1,0,0,0,0,0,0,'',0,0,0,'',0,5,0,0,0,0,'',1,0,0,0,'',0,0,0,0,'',0,0,0,'',0,0,0,0,1474363829);

/*Table structure for table `phome_enewsclass_stats` */

DROP TABLE IF EXISTS `phome_enewsclass_stats`;

CREATE TABLE `phome_enewsclass_stats` (
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0',
  `pvall` int(10) unsigned NOT NULL DEFAULT '0',
  `pvyear` int(10) unsigned NOT NULL DEFAULT '0',
  `pvhalfyear` int(10) unsigned NOT NULL DEFAULT '0',
  `pvquarter` int(10) unsigned NOT NULL DEFAULT '0',
  `pvmonth` int(10) unsigned NOT NULL DEFAULT '0',
  `pvweek` int(10) unsigned NOT NULL DEFAULT '0',
  `pvday` int(10) unsigned NOT NULL DEFAULT '0',
  `pvyesterday` int(10) unsigned NOT NULL DEFAULT '0',
  `ipall` int(10) unsigned NOT NULL DEFAULT '0',
  `ipyear` int(10) unsigned NOT NULL DEFAULT '0',
  `iphalfyear` int(10) unsigned NOT NULL DEFAULT '0',
  `ipquarter` int(10) unsigned NOT NULL DEFAULT '0',
  `ipmonth` int(10) unsigned NOT NULL DEFAULT '0',
  `ipweek` int(10) unsigned NOT NULL DEFAULT '0',
  `ipday` int(10) unsigned NOT NULL DEFAULT '0',
  `ipyesterday` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclass_stats` */

insert  into `phome_enewsclass_stats`(`classid`,`uptime`,`pvall`,`pvyear`,`pvhalfyear`,`pvquarter`,`pvmonth`,`pvweek`,`pvday`,`pvyesterday`,`ipall`,`ipyear`,`iphalfyear`,`ipquarter`,`ipmonth`,`ipweek`,`ipday`,`ipyesterday`) values (1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(19,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(22,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(23,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(24,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(26,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(27,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(29,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(36,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(38,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(39,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(43,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(44,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(45,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(46,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(49,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(50,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(51,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(55,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(56,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(57,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

/*Table structure for table `phome_enewsclass_stats_ip` */

DROP TABLE IF EXISTS `phome_enewsclass_stats_ip`;

CREATE TABLE `phome_enewsclass_stats_ip` (
  `ip` char(21) NOT NULL DEFAULT '',
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclass_stats_ip` */

/*Table structure for table `phome_enewsclass_stats_set` */

DROP TABLE IF EXISTS `phome_enewsclass_stats_set`;

CREATE TABLE `phome_enewsclass_stats_set` (
  `openstats` tinyint(1) NOT NULL DEFAULT '0',
  `pvtime` int(10) unsigned NOT NULL DEFAULT '0',
  `statsdate` int(10) unsigned NOT NULL DEFAULT '0',
  `changedate` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclass_stats_set` */

insert  into `phome_enewsclass_stats_set`(`openstats`,`pvtime`,`statsdate`,`changedate`) values (1,3600,0,0);

/*Table structure for table `phome_enewsclassadd` */

DROP TABLE IF EXISTS `phome_enewsclassadd`;

CREATE TABLE `phome_enewsclassadd` (
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classtext` mediumtext NOT NULL,
  `ttids` text NOT NULL,
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclassadd` */

insert  into `phome_enewsclassadd`(`classid`,`classtext`,`ttids`) values (1,'',''),(2,'',''),(3,'',''),(4,'',''),(5,'',''),(6,'',''),(7,'',''),(8,'',''),(9,'',''),(10,'',''),(11,'',''),(12,'',''),(13,'',''),(14,'',''),(15,'',''),(16,'',''),(17,'',''),(18,'',''),(19,'',''),(20,'',''),(21,'',''),(22,'',''),(23,'',''),(24,'',''),(25,'',''),(26,'',''),(27,'',''),(28,'',''),(29,'',''),(30,'',''),(31,'',''),(32,'',''),(33,'',''),(34,'',''),(35,'',''),(36,'',''),(37,'',''),(38,'',''),(39,'',''),(40,'',''),(41,'',''),(42,'',''),(43,'',''),(44,'',''),(45,'',''),(46,'',''),(47,'',''),(48,'',''),(49,'',''),(50,'',''),(51,'',''),(52,'',''),(53,'',''),(54,'',''),(55,'',''),(56,'',''),(57,'','');

/*Table structure for table `phome_enewsclassf` */

DROP TABLE IF EXISTS `phome_enewsclassf`;

CREATE TABLE `phome_enewsclassf` (
  `fid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f` varchar(30) NOT NULL DEFAULT '',
  `fname` varchar(30) NOT NULL DEFAULT '',
  `fform` varchar(20) NOT NULL DEFAULT '',
  `fhtml` mediumtext NOT NULL,
  `fzs` varchar(255) NOT NULL DEFAULT '',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ftype` varchar(30) NOT NULL DEFAULT '',
  `flen` varchar(20) NOT NULL DEFAULT '',
  `fvalue` text NOT NULL,
  `fformsize` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclassf` */

/*Table structure for table `phome_enewsclassnavcache` */

DROP TABLE IF EXISTS `phome_enewsclassnavcache`;

CREATE TABLE `phome_enewsclassnavcache` (
  `navtype` char(16) NOT NULL DEFAULT '',
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `modid` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `navtype` (`navtype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclassnavcache` */

insert  into `phome_enewsclassnavcache`(`navtype`,`userid`,`modid`) values ('listclass',0,0),('listenews',0,0),('jsclass',0,0);

/*Table structure for table `phome_enewsclasstemp` */

DROP TABLE IF EXISTS `phome_enewsclasstemp`;

CREATE TABLE `phome_enewsclasstemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(30) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclasstemp` */

insert  into `phome_enewsclasstemp`(`tempid`,`tempname`,`temptext`,`classid`) values (1,'城中分校','[!--temp.czheader--]	\r\n	<div class=\\\"news_2 clear\\\">\r\n		<div>\r\n			<div class=\\\"\\\">\r\n				<span>校园新闻</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]16,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n		<div>\r\n			<div >\r\n				<span>公告栏</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]17,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"nav_2\\\">\r\n		<div class=\\\"div1\\\">\r\n			<div>\r\n				<span>阅读:</span>\r\n			</div>\r\n			<ul>[showclasstemp]18,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div2\\\">\r\n			<div>\r\n				<span>班级:</span>\r\n			</div>\r\n			<ul>[showclasstemp]22,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div3\\\">\r\n			<div>\r\n				<span>科技:</span>\r\n			</div>\r\n			<ul>[showclasstemp]29,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div4\\\">\r\n			<div>\r\n				<span>教师:</span>\r\n			</div>\r\n			<ul>[showclasstemp]25,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n	</div>\r\n[!--temp.czfooter--]',0),(5,'集团组成','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		<style>\r\nul{\r\n	padding: 0;margin:0 ;\r\n}\r\nli{\r\n	list-style:none;\r\n}\r\n.img_box{\r\n	width:1200px;\r\n	margin:0 auto;\r\n}\r\n.img_box>ul{\r\n	width:100%;\r\n	overflow:hidden;\r\n}\r\n.img_box>ul>li{\r\n	float:left;overflow:hidden;\r\n}\r\n.text{\r\n	width:59px;\r\n	background:#ccc;\r\n	height:360px;\r\n	float:left;\r\n	position:relative;\r\n	cursor:pointer;\r\n	border-right:1px #fff solid;\r\n}\r\n.text>div{\r\n	position:absolute;\r\n	bottom:160px;\r\n	left:0;\r\n	right:0;\r\n	width:16px;\r\n	margin:0 auto;\r\n}\r\n.img1{\r\n	width:760px;\r\n	height:360px;\r\n	float:right;\r\n	display:none;\r\n}\r\n.img1>a img{\r\n	width:100%;\r\n	height:100%;\r\n}\r\n</style>\r\n<div class=\\\"img_box\\\">\r\n	<ul>\r\n    	<li><div class=\\\"text\\\"><div>城中校区</div></div><div class=\\\"img1\\\" style=\\\"display:block;\\\"><a href=\\\"/chengzhong/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/d1.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>爱德校区</div></div><div class=\\\"img1\\\"><a href=\\\"/aide/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/2.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>太湖校区</div></div><div class=\\\"img1\\\"><a href=\\\"/taihu/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/3.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>苏州湾校区</div></div><div class=\\\"img1\\\"><a href=\\\"/szw/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/4.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n		<li><div class=\\\"text\\\"><div>幼儿园</div></div><div class=\\\"img1\\\"><a href=\\\"#\\\"><img src=\\\"/skin/default/images/fx/5.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n		<li><div class=\\\"text\\\"><div>德音书院</div></div><div class=\\\"img1\\\"><a href=\\\"#\\\"><img src=\\\"/skin/default/images/fx/6.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n    </ul>\r\n</div>\r\n<script>\r\n$(\\\".text\\\").click(function(){\r\n	$(\\\".img1\\\").hide();\r\n	$(this).next(\\\".img1\\\").show();\r\n})\r\n</script>\r\n	</div>[!--temp.footer--]',0),(2,'爱德分校','[!--temp.adheader--]\r\n<div class=\\\"content\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_ssgz\\\">\r\n				<div class=\\\"c_ssgz_title clear\\\">\r\n					<span>时时关注</span>\r\n					<a href=\\\"/aide/ssgz/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<div class=\\\"c_ssgz_info clear\\\">\r\n					[ecmsinfo]37,1,32,0,0,18,0,\\\'isgood=1\\\'[/ecmsinfo]\r\n					<ul>\r\n						[ecmsinfo]37,7,32,0,0,17,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n					</ul>\r\n				</div>\r\n			</li>\r\n			<li class=\\\"c_dlad\\\">\r\n				<div class=\\\"c_dlad_title clear\\\">\r\n					<span>点亮德育</span>\r\n					<a href=\\\"/aide/dlad/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<ul>\r\n					[ecmsinfo]34,2,32,0,0,20,0[/ecmsinfo]\r\n				</ul>\r\n			</li>\r\n			<li class=\\\"c_diy c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>课程DIY</span>\r\n				</div>\r\n				[ecmsinfo]35,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_post c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>教师POST</span>\r\n				</div>\r\n				[ecmsinfo]36,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_gardend c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>班级GARDEN</span>\r\n				</div>\r\n				[ecmsinfo]38,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_spa c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>心理SPA</span>\r\n				</div>\r\n				[ecmsinfo]39,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n		</ul>\r\n	</div>\r\n[!--temp.adfooter--]',0),(3,'太湖分校','[!--temp.thheader--]\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"xyfw\\\">\r\n			<div class=\\\"xyfw_title c_title clear\\\">\r\n				<div>\r\n					<span>校园服务</span>\r\n				</div>\r\n				<span>\r\n				<a href=\\\"###\\\">MORE <span>+</span>\r\n				</a>\r\n				</span>\r\n			</div>\r\n			<div class=\\\"xyfw_c clear\\\">\r\n				<ul class=\\\"clear\\\">\r\n					\r\n[e:loop={\\\'select * from phome_enewsclass where classid in (46,47,48,49) order by classid\\\',20,24,0}]<li><div class=\\\"xyfw_c_b\\\">\r\n							<img src=\\\"<?=$bqr[classimg]?>\\\" alt=\\\"\\\">\r\n							<a href=\\\"<?=$bqr[classurl]?>\\\"><?=$bqr[classname]?></a>\r\n						</div></li>[/e:loop] \r\n				</ul>\r\n			</div>\r\n		</div>\r\n		<div class=\\\"xyzx\\\">\r\n			<div class=\\\"xyzx_title c_title clear\\\">\r\n				<div>\r\n					<span>新闻资讯</span>\r\n				</div>\r\n				</div>\r\n			<div class=\\\"xyzx_c clear\\\">\r\n				<div class=\\\"xyzx_c_l\\\">\r\n					<div class=\\\"xyzx_c_l_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>动感校园</span>\r\n						</div>\r\n						<div>\r\n							<span>媒体报道</span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle0 clear\\\">\r\n						<div>[ecmsinfo]42,1,32,0,0,11,0,\\\'isgood=1\\\'[/ecmsinfo]</div>\r\n						<ul>[ecmsinfo]42,8,32,0,0,4,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle1 clear\\\">\r\n						<div>[ecmsinfo]43,1,32,0,0,11,0,\\\'isgood=1\\\'[/ecmsinfo]</div>\r\n						<ul>[ecmsinfo]43,8,32,0,0,4,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n				</div>\r\n				<div class=\\\"xyzx_c_r\\\">\r\n					<div class=\\\"xyzx_c_r_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>通知公告 </span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/notice/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<ul class=\\\"xyzx_c_r_c\\\">\r\n						[ecmsinfo]44,4,32,0,0,12,0[/ecmsinfo]\r\n					</ul>\r\n					\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0),(4,'苏州湾校区','[!--temp.szwheader--]\r\n<div class=\\\"content\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_ssgz\\\">\r\n				<div class=\\\"c_ssgz_title clear\\\">\r\n					<span>时时关注</span>\r\n					<a href=\\\"/aide/ssgz/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<div class=\\\"c_ssgz_info clear\\\">\r\n					[ecmsinfo]54,1,32,0,0,18,0,\\\'isgood=1\\\'[/ecmsinfo]\r\n					<ul>\r\n						[ecmsinfo]54,7,32,0,0,17,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n					</ul>\r\n				</div>\r\n			</li>\r\n			<li class=\\\"c_dlad\\\">\r\n				<div class=\\\"c_dlad_title clear\\\">\r\n					<span>点亮苏州湾</span>\r\n					<a href=\\\"/szw/dlad/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<ul>\r\n					[ecmsinfo]51,2,32,0,0,20,0[/ecmsinfo]\r\n				</ul>\r\n			</li>\r\n			<li class=\\\"c_diy c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>课程DIY</span>\r\n				</div>\r\n				[ecmsinfo]52,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_post c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>教师POST</span>\r\n				</div>\r\n				[ecmsinfo]53,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_gardend c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>班级GARDEND</span>\r\n				</div>\r\n				[ecmsinfo]55,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_spa c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>心理SPA</span>\r\n				</div>\r\n				[ecmsinfo]56,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n		</ul>\r\n	</div>\r\n[!--temp.szwfooter--]',0);

/*Table structure for table `phome_enewsclasstempclass` */

DROP TABLE IF EXISTS `phome_enewsclasstempclass`;

CREATE TABLE `phome_enewsclasstempclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsclasstempclass` */

/*Table structure for table `phome_enewsdiggips` */

DROP TABLE IF EXISTS `phome_enewsdiggips`;

CREATE TABLE `phome_enewsdiggips` (
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `ips` mediumtext NOT NULL,
  KEY `classid` (`classid`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsdiggips` */

/*Table structure for table `phome_enewsdo` */

DROP TABLE IF EXISTS `phome_enewsdo`;

CREATE TABLE `phome_enewsdo` (
  `doid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `doname` varchar(60) NOT NULL DEFAULT '',
  `dotime` smallint(6) NOT NULL DEFAULT '0',
  `isopen` tinyint(1) NOT NULL DEFAULT '0',
  `doing` tinyint(4) NOT NULL DEFAULT '0',
  `classid` text NOT NULL,
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`doid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsdo` */

insert  into `phome_enewsdo`(`doid`,`doname`,`dotime`,`isopen`,`doing`,`classid`,`lasttime`) values (1,'自动刷新首页',12,0,0,',',1273215883);

/*Table structure for table `phome_enewsdolog` */

DROP TABLE IF EXISTS `phome_enewsdolog`;

CREATE TABLE `phome_enewsdolog` (
  `logid` bigint(20) NOT NULL AUTO_INCREMENT,
  `logip` varchar(20) NOT NULL DEFAULT '',
  `logtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(30) NOT NULL DEFAULT '',
  `enews` varchar(30) NOT NULL DEFAULT '',
  `doing` varchar(255) NOT NULL DEFAULT '',
  `pubid` bigint(16) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`logid`),
  KEY `pubid` (`pubid`)
) ENGINE=MyISAM AUTO_INCREMENT=734 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsdolog` */

insert  into `phome_enewsdolog`(`logid`,`logip`,`logtime`,`username`,`enews`,`doing`,`pubid`) values (1,'::1','2016-08-22 13:42:43','manage','login','---',0),(2,'::1','2016-08-22 13:45:55','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(3,'::1','2016-08-22 13:47:00','manage','EditTempvar','varid=2<br>var=footer&gid=1',0),(4,'::1','2016-08-22 13:48:15','manage','EditPublicTemp','gid=1',0),(5,'::1','2016-08-22 13:48:19','manage','ReIndex','---',0),(6,'::1','2016-08-22 13:50:56','manage','EditNewstemp','tempid=1<br>tempname=关于内容模板&gid=1',0),(7,'::1','2016-08-22 13:53:23','manage','EditListtemp','tempid=1<br>tempname=默认新闻列表模板&gid=1',0),(8,'::1','2016-08-22 13:54:45','manage','AddListtemp','tempid=9<br>tempname=图片列表模板&gid=1',0),(9,'::1','2016-08-22 13:57:36','manage','AddNewstemp','tempid=10<br>tempname=新闻内容模板&gid=1',0),(10,'::1','2016-08-22 13:58:19','manage','AddClass','classid=1<br>classname=关于我们',0),(11,'::1','2016-08-22 13:58:43','manage','AddClass','classid=2<br>classname=集团概况',0),(12,'::1','2016-08-22 13:59:05','manage','AddClass','classid=3<br>classname=校长寄语',0),(13,'::1','2016-08-22 13:59:23','manage','EditClass','classid=3<br>classname=总校长寄语',0),(14,'::1','2016-08-22 13:59:50','manage','AddClass','classid=4<br>classname=集团战略',0),(15,'::1','2016-08-22 14:00:13','manage','AddClass','classid=5<br>classname=集团荣誉',0),(16,'::1','2016-08-22 14:00:29','manage','EditClass','classid=5<br>classname=集团荣誉',0),(17,'::1','2016-08-22 14:00:52','manage','AddClass','classid=6<br>classname=地理位置',0),(18,'::1','2016-08-22 14:01:16','manage','AddClass','classid=7<br>classname=联系我们',0),(19,'::1','2016-08-22 14:02:16','manage','EditBqtemp','tempid=1<br>tempname=子栏目导航标签模板&gid=1',0),(20,'::1','2016-08-22 14:03:38','manage','EditNewstemp','tempid=1<br>tempname=关于内容模板&gid=1',0),(21,'::1','2016-08-22 14:04:47','manage','AddNews','classid=2<br>id=1<br>title=集团概况',1000010000000001),(22,'::1','2016-08-22 14:05:11','manage','AddNews','classid=3<br>id=2<br>title=总校长寄语',1000010000000002),(23,'::1','2016-08-22 14:05:31','manage','AddNews','classid=4<br>id=3<br>title=集团战略',1000010000000003),(24,'::1','2016-08-22 14:07:31','manage','AddNews','classid=6<br>id=4<br>title=地理位置',1000010000000004),(25,'::1','2016-08-22 14:08:12','manage','AddNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(26,'::1','2016-08-22 14:08:22','manage','ReSingleInfo','classid=7',0),(27,'::1','2016-08-22 14:08:41','manage','ReSingleInfo','classid=6',0),(28,'::1','2016-08-22 14:09:32','manage','EditNews','classid=6<br>id=4<br>title=地理位置',1000010000000004),(29,'::1','2016-08-22 14:09:41','manage','ReSingleInfo','classid=6',0),(30,'::1','2016-08-22 14:10:36','manage','AddNewstemp','tempid=11<br>tempname=地理位置内容模板&gid=1',0),(31,'::1','2016-08-22 14:10:53','manage','EditClass','classid=6<br>classname=地理位置',0),(32,'::1','2016-08-22 14:11:02','manage','ReListHtml','---',0),(33,'::1','2016-08-22 14:11:22','manage','ReSingleInfo','classid=6',0),(34,'::1','2016-08-22 14:13:12','manage','EditNewstemp','tempid=11<br>tempname=地理位置内容模板&gid=1',0),(35,'::1','2016-08-22 14:13:22','manage','ReSingleInfo','classid=6',0),(36,'::1','2016-08-22 14:13:52','manage','EditPublicTemp','gid=1',0),(37,'::1','2016-08-22 14:15:21','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(38,'::1','2016-08-22 14:15:44','manage','AddBqtemp','tempid=13<br>tempname=导航二级&gid=1',0),(39,'::1','2016-08-22 14:16:23','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(40,'::1','2016-08-22 14:16:45','manage','AddClass','classid=8<br>classname=新闻动态',0),(41,'::1','2016-08-22 14:17:15','manage','AddClass','classid=9<br>classname=新闻资讯',0),(42,'::1','2016-08-22 14:17:44','manage','AddClass','classid=10<br>classname=集团故事',0),(43,'::1','2016-08-22 14:18:11','manage','AddClass','classid=11<br>classname=图片掠影',0),(44,'::1','2016-08-22 14:21:08','manage','AddClass','classid=12<br>classname=视频关注',0),(45,'::1','2016-08-22 14:24:12','manage','EditListtemp','tempid=9<br>tempname=图片列表模板&gid=1',0),(46,'::1','2016-08-22 14:27:14','manage','EditListtemp','tempid=1<br>tempname=默认新闻列表模板&gid=1',0),(47,'::1','2016-08-22 14:27:28','manage','EditListtemp','tempid=9<br>tempname=图片列表模板&gid=1',0),(48,'::1','2016-08-22 14:28:11','manage','EditNewstemp','tempid=10<br>tempname=新闻内容模板&gid=1',0),(49,'::1','2016-08-22 14:32:11','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(50,'::1','2016-08-22 14:36:29','manage','EditBqtemp','tempid=2<br>tempname=首页新闻调用&gid=1',0),(51,'::1','2016-08-22 14:38:17','manage','EditPublicTemp','gid=1',0),(52,'::1','2016-08-22 14:39:09','manage','AddBqtemp','tempid=14<br>tempname=首页集团故事&gid=1',0),(53,'::1','2016-08-22 14:39:40','manage','EditPublicTemp','gid=1',0),(54,'::1','2016-08-22 14:40:18','manage','AddNews','classid=9<br>id=6<br>title=建构“和融向上”行政管理团队',1000010000000006),(55,'::1','2016-08-22 14:40:30','manage','CopyNews_all','classid=9<br>id=6<br>title=建构“和融向上”行政管理团队<br>to_classid=9',1000010000000006),(56,'::1','2016-08-22 14:40:40','manage','CopyNews_all','classid=9<br>classname=新闻资讯<br>to_classid=9',0),(57,'::1','2016-08-22 14:41:13','manage','EditNews','classid=9<br>id=9<br>title=建构“和融向上”行政管理团队',1000010000000009),(58,'::1','2016-08-22 14:41:23','manage','CopyNews_all','classid=9<br>classname=新闻资讯<br>to_classid=10',0),(59,'::1','2016-08-22 14:42:11','manage','ReIndex','---',0),(60,'::1','2016-08-22 14:44:00','manage','ReIndex','---',0),(61,'::1','2016-08-22 14:44:31','manage','ReIndex','---',0),(62,'::1','2016-08-22 14:44:40','manage','ReIndex','---',0),(63,'::1','2016-08-22 14:45:31','manage','EditInfoSimple','classid=9<br>id=9<br>title=建构“和融向上”行政管理团队',1000010000000009),(64,'::1','2016-08-22 14:45:38','manage','ReIndex','---',0),(65,'::1','2016-08-22 14:45:54','manage','EditPublicTemp','gid=1',0),(66,'::1','2016-08-22 14:50:33','manage','EditTempvar','varid=2<br>var=footer&gid=1',0),(67,'::1','2016-08-22 14:51:07','manage','AddClass','classid=13<br>classname=相关链接',0),(68,'::1','2016-08-22 14:51:56','manage','EditBqtemp','tempid=3<br>tempname=标题+简介&gid=1',0),(69,'::1','2016-08-22 14:53:03','manage','EditBqtemp','tempid=3<br>tempname=友情链接&gid=1',0),(70,'::1','2016-08-22 14:53:53','manage','EditTempvar','varid=2<br>var=footer&gid=1',0),(71,'::1','2016-08-22 14:54:31','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(72,'::1','2016-08-22 14:54:46','manage','ReIndex','---',0),(73,'::1','2016-08-22 14:54:52','manage','ReListHtml_all','---',0),(74,'::1','2016-08-22 14:58:17','manage','EditNewstemp','tempid=10<br>tempname=新闻内容模板&gid=1',0),(75,'::1','2016-08-22 14:58:52','manage','EditNewstemp','tempid=10<br>tempname=新闻内容模板&gid=1',0),(76,'::1','2016-08-22 15:00:57','manage','AddListtemp','tempid=10<br>tempname=集团荣誉模板&gid=1',0),(77,'::1','2016-08-22 15:01:10','manage','EditClass','classid=5<br>classname=集团荣誉',0),(78,'::1','2016-08-22 15:01:19','manage','ReListHtml','---',0),(79,'::1','2016-08-25 13:08:30','manage','login','---',0),(80,'::1','2016-08-25 13:13:22','manage','BakExe','dbname=wjsx',0),(81,'::1','2016-08-25 15:35:46','manage','login','---',0),(82,'::1','2016-08-25 15:36:05','manage','ReIndex','---',0),(83,'::1','2016-08-25 15:36:09','manage','ChangeEnewsData','---',0),(84,'::1','2016-08-25 15:36:14','manage','ReClassPath','---',0),(85,'::1','2016-08-25 15:36:18','manage','DelFcListClass','---',0),(86,'::1','2016-08-25 15:36:33','manage','UpdateClassInfosAll','---',0),(87,'::1','2016-08-25 15:36:40','manage','ReIndex','---',0),(88,'::1','2016-08-25 15:36:45','manage','ReListHtml_all','---',0),(89,'::1','2016-08-25 15:36:54','manage','login','---',0),(90,'::1','2016-08-25 15:42:56','manage','BakExe','dbname=wjsx',0),(91,'::1','2016-08-25 17:13:39','manage','login','---',0),(92,'::1','2016-08-25 17:13:44','manage','ChangeEnewsData','---',0),(93,'::1','2016-08-25 17:13:46','manage','ReClassPath','---',0),(94,'::1','2016-08-25 17:13:48','manage','DelFcListClass','---',0),(95,'::1','2016-08-25 17:13:48','manage','DelFcListClass','---',0),(96,'::1','2016-08-25 17:13:51','manage','UpdateClassInfosAll','---',0),(97,'::1','2016-08-25 17:13:56','manage','ChangeAllModForm','ChangeClass=',0),(98,'::1','2016-08-25 17:13:59','manage','ReIndex','---',0),(99,'::1','2016-08-25 17:14:01','manage','ReListHtml_all','---',0),(100,'::1','2016-08-25 17:14:43','manage','AddNews','classid=9<br>id=14<br>title=ceshi',1000010000000014),(101,'::1','2016-08-25 17:16:36','manage','login','---',0),(102,'::1','2016-08-25 17:17:01','manage','EditNews','classid=9<br>id=14<br>title=ceshi',1000010000000014),(103,'::1','2016-08-25 17:19:40','manage','SetEnews','---',0),(104,'::1','2016-08-25 17:19:40','manage','SetEnews','---',0),(105,'::1','2016-08-25 17:21:36','manage','login','---',0),(106,'::1','2016-08-25 17:22:29','manage','SetEnews','---',0),(107,'::1','2016-08-25 17:23:05','manage','SetEnews','---',0),(108,'::1','2016-08-25 17:23:05','manage','SetEnews','---',0),(109,'::1','2016-08-25 17:43:42','manage','SetEnews','---',0),(110,'::1','2016-08-26 09:03:14','manage','login','---',0),(111,'::1','2016-08-26 09:23:38','manage','EditNewstemp','tempid=10<br>tempname=新闻内容模板&gid=1',0),(112,'::1','2016-08-26 09:23:56','manage','AddUser','userid=2<br>username=admin',0),(113,'::1','2016-08-26 09:24:06','manage','ReIndex','---',0),(114,'::1','2016-08-26 09:24:08','manage','ReListHtml_all','---',0),(115,'::1','2016-08-26 11:40:36','manage','EditNewstemp','tempid=10<br>tempname=新闻内容模板&gid=1',0),(116,'::1','2016-09-01 08:26:09','manage','login','---',0),(117,'::1','2016-09-01 08:43:23','manage','ReData','dbname=wjsx&path=wjsx_20160901082628',0),(118,'::1','2016-09-01 08:43:32','manage','login','---',0),(119,'::1','2016-09-01 08:43:42','manage','ChangeEnewsData','---',0),(120,'::1','2016-09-01 08:43:49','manage','ReClassPath','---',0),(121,'::1','2016-09-01 08:43:54','manage','DelFcListClass','---',0),(122,'::1','2016-09-01 08:45:46','manage','UpdateClassInfosAll','---',0),(123,'::1','2016-09-01 08:45:54','manage','ReIndex','---',0),(124,'::1','2016-09-01 08:45:59','manage','ReListHtml_all','---',0),(125,'::1','2016-09-01 09:51:39','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(126,'::1','2016-09-01 09:52:09','manage','AddClass','classid=14<br>classname=城中分校',0),(127,'::1','2016-09-01 09:52:24','manage','ReListHtml','---',0),(128,'::1','2016-09-01 09:57:23','manage','AddClass','classid=15<br>classname=资讯中心',0),(129,'::1','2016-09-01 10:35:59','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(130,'::1','2016-09-01 10:36:32','manage','EditTempvar','varid=3<br>var=czheader&gid=1',0),(131,'::1','2016-09-01 10:37:01','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(132,'::1','2016-09-01 10:37:27','manage','EditTempvar','varid=4<br>var=czfooter&gid=1',0),(133,'::1','2016-09-01 10:38:53','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(134,'::1','2016-09-01 10:39:04','manage','ReListHtml','---',0),(135,'::1','2016-09-01 10:40:06','manage','AddListtemp','tempid=11<br>tempname=城中分校新闻列表模板&gid=1',0),(136,'::1','2016-09-01 10:41:25','manage','AddNewstemp','tempid=12<br>tempname=城中分校内容模板&gid=1',0),(137,'::1','2016-09-01 10:42:17','manage','EditClass','classid=15<br>classname=资讯中心',0),(138,'::1','2016-09-01 10:42:42','manage','AddClass','classid=16<br>classname=校园新闻',0),(139,'::1','2016-09-01 10:43:05','manage','AddClass','classid=17<br>classname=校园公告',0),(140,'::1','2016-09-01 10:43:29','manage','AddClass','classid=18<br>classname=阅读中心',0),(141,'::1','2016-09-01 10:43:54','manage','AddClass','classid=19<br>classname=经典导读',0),(142,'::1','2016-09-01 10:44:11','manage','AddClass','classid=20<br>classname=挑战作家',0),(143,'::1','2016-09-01 10:44:39','manage','AddClass','classid=21<br>classname=我的一本课外书',0),(144,'::1','2016-09-01 10:45:01','manage','AddClass','classid=22<br>classname=班级',0),(145,'::1','2016-09-01 10:45:24','manage','AddClass','classid=23<br>classname=英才少年',0),(146,'::1','2016-09-01 10:45:46','manage','AddClass','classid=24<br>classname=班级风采',0),(147,'::1','2016-09-01 10:46:16','manage','AddClass','classid=25<br>classname=教师',0),(148,'::1','2016-09-01 10:46:40','manage','AddClass','classid=26<br>classname=专业阅读',0),(149,'::1','2016-09-01 10:47:03','manage','AddClass','classid=27<br>classname=田野笔记',0),(150,'::1','2016-09-01 10:47:32','manage','AddClass','classid=28<br>classname=求真论坛',0),(151,'::1','2016-09-01 10:47:50','manage','AddClass','classid=29<br>classname=科技',0),(152,'::1','2016-09-01 10:48:16','manage','AddClass','classid=30<br>classname=科普中国',0),(153,'::1','2016-09-01 10:48:40','manage','AddClass','classid=31<br>classname=挑战科学',0),(154,'::1','2016-09-01 10:49:05','manage','AddClass','classid=32<br>classname=我的一个小研究',0),(155,'::1','2016-09-01 10:50:34','manage','AddBqtemp','tempid=15<br>tempname=城中分校左侧分类&gid=1',0),(156,'::1','2016-09-01 11:35:35','manage','EditListtemp','tempid=11<br>tempname=城中分校新闻列表模板&gid=1',0),(157,'::1','2016-09-01 11:57:20','manage','EditListtemp','tempid=11<br>tempname=城中分校新闻列表模板&gid=1',0),(158,'::1','2016-09-01 11:58:49','manage','EditNewstemp','tempid=12<br>tempname=城中分校内容模板&gid=1',0),(159,'::1','2016-09-01 11:59:10','manage','ReListHtml','---',0),(160,'::1','2016-09-01 13:04:29','manage','EditTempvar','varid=3<br>var=czheader&gid=1',0),(161,'::1','2016-09-01 13:04:42','manage','ReListHtml_all','---',0),(162,'::1','2016-09-01 13:08:44','manage','EditListtemp','tempid=11<br>tempname=城中分校新闻列表模板&gid=1',0),(163,'::1','2016-09-01 13:11:09','manage','EditNewstemp','tempid=12<br>tempname=城中分校内容模板&gid=1',0),(164,'::1','2016-09-01 13:11:38','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(165,'::1','2016-09-01 13:11:46','manage','ReIndex','---',0),(166,'::1','2016-09-01 13:13:47','manage','EditBqtemp','tempid=4<br>tempname=标题+时间&gid=1',0),(167,'::1','2016-09-01 13:16:21','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(168,'::1','2016-09-01 13:24:13','manage','AddBqtemp','tempid=16<br>tempname=城中分校首页类别调用&gid=1',0),(169,'::1','2016-09-01 13:26:00','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(170,'::1','2016-09-01 13:26:08','manage','ReListHtml','---',0),(171,'::1','2016-09-01 13:26:25','manage','ReListHtml','---',0),(172,'::1','2016-09-02 09:49:34','manage','login','---',0),(173,'::1','2016-09-02 09:53:44','manage','EditTempvar','varid=5<br>var=adheader&gid=1',0),(174,'::1','2016-09-02 09:54:11','manage','EditTempvar','varid=7<br>var=adfooter&gid=1',0),(175,'::1','2016-09-02 09:54:45','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(176,'::1','2016-09-02 09:55:07','manage','AddClass','classid=33<br>classname=爱德分校',0),(177,'::1','2016-09-02 09:55:20','manage','ReListHtml','---',0),(178,'::1','2016-09-02 09:55:55','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(179,'::1','2016-09-02 09:56:02','manage','ReListHtml','---',0),(180,'::1','2016-09-02 09:57:54','manage','EditTempvar','varid=7<br>var=adfooter&gid=1',0),(181,'::1','2016-09-02 09:58:01','manage','ReListHtml','---',0),(182,'::1','2016-09-02 09:59:13','manage','EditTempvar','varid=5<br>var=adheader&gid=1',0),(183,'::1','2016-09-02 10:05:07','manage','AddListtemp','tempid=12<br>tempname=爱德分校新闻列表模板&gid=1',0),(184,'::1','2016-09-02 10:06:17','manage','EditListtemp','tempid=12<br>tempname=爱德分校新闻列表模板&gid=1',0),(185,'::1','2016-09-02 10:08:18','manage','AddListtemp','tempid=13<br>tempname=爱德分校图片+信息列表模板&gid=1',0),(186,'::1','2016-09-02 10:10:23','manage','AddNewstemp','tempid=13<br>tempname=爱德校区详细页&gid=1',0),(187,'::1','2016-09-02 10:11:06','manage','AddClass','classid=34<br>classname=点亮爱德',0),(188,'::1','2016-09-02 10:11:44','manage','AddClass','classid=35<br>classname=课程DIY',0),(189,'::1','2016-09-02 10:12:12','manage','AddClass','classid=36<br>classname=教师POST',0),(190,'::1','2016-09-02 10:13:10','manage','AddClass','classid=37<br>classname=时时关注',0),(191,'::1','2016-09-02 10:13:37','manage','AddClass','classid=38<br>classname=班级GARDEND',0),(192,'::1','2016-09-02 10:14:22','manage','AddClass','classid=39<br>classname=心理SPA',0),(193,'::1','2016-09-02 10:14:48','manage','AddNews','classid=34<br>id=15<br>title=测试信息',1000010000000015),(194,'::1','2016-09-02 10:15:04','manage','AddNews','classid=36<br>id=16<br>title=测试信息测试信息',1000010000000016),(195,'::1','2016-09-02 10:16:43','manage','AddBqtemp','tempid=17<br>tempname=爱德实时关注&gid=1',0),(196,'::1','2016-09-02 10:18:10','manage','AddBqtemp','tempid=18<br>tempname=爱德实时关注推荐&gid=1',0),(197,'::1','2016-09-02 10:20:35','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(198,'::1','2016-09-02 10:21:58','manage','AddBqtemp','tempid=19<br>tempname=爱德首页图片+简介&gid=1',0),(199,'::1','2016-09-02 10:23:46','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(200,'::1','2016-09-02 10:25:38','manage','EditTempvar','varid=5<br>var=adheader&gid=1',0),(201,'::1','2016-09-02 10:26:20','manage','EditBqtemp','tempid=1<br>tempname=子栏目导航标签模板&gid=1',0),(202,'::1','2016-09-02 10:27:53','manage','EditListtemp','tempid=12<br>tempname=爱德分校新闻列表模板&gid=1',0),(203,'::1','2016-09-02 10:28:15','manage','EditListtemp','tempid=13<br>tempname=爱德分校图片+信息列表模板&gid=1',0),(204,'::1','2016-09-02 10:28:30','manage','EditNewstemp','tempid=13<br>tempname=爱德校区详细页&gid=1',0),(205,'::1','2016-09-02 10:28:45','manage','ReListHtml_all','---',0),(206,'::1','2016-09-02 10:29:24','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(207,'::1','2016-09-02 10:29:28','manage','ReIndex','---',0),(208,'::1','2016-09-02 10:29:37','manage','ReListHtml_all','---',0),(209,'::1','2016-09-02 10:30:28','manage','EditBqtemp','tempid=17<br>tempname=爱德实时关注&gid=1',0),(210,'::1','2016-09-02 10:30:35','manage','ReListHtml','---',0),(211,'::1','2016-09-02 10:31:51','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(212,'::1','2016-09-02 10:32:23','manage','EditListtemp','tempid=12<br>tempname=爱德分校新闻列表模板&gid=1',0),(213,'::1','2016-09-02 10:32:33','manage','ReListHtml_all','---',0),(214,'::1','2016-09-02 10:34:35','manage','AddNews','classid=35<br>id=17<br>title=测试数据测试数据',1000010000000017),(215,'::1','2016-09-02 10:35:15','manage','AddNews','classid=36<br>id=18<br>title=测试数据',1000010000000018),(216,'::1','2016-09-02 10:35:50','manage','AddNews','classid=38<br>id=19<br>title=班级测试',1000010000000019),(217,'::1','2016-09-02 10:36:20','manage','AddNews','classid=39<br>id=20<br>title=心理咨询',1000010000000020),(218,'::1','2016-09-02 10:37:11','manage','AddNews','classid=34<br>id=21<br>title=测试信息2',1000010000000021),(219,'::1','2016-09-02 10:37:25','manage','GoodInfo_all','classid=34<br>classname=点亮爱德<br>id=21&doing=0',1000010000000021),(220,'::1','2016-09-02 10:37:36','manage','ReListHtml_all','---',0),(221,'::1','2016-09-02 10:39:05','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(222,'::1','2016-09-02 10:39:39','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(223,'::1','2016-09-02 10:39:57','manage','ReListHtml','---',0),(224,'::1','2016-09-02 10:41:55','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(225,'::1','2016-09-02 10:42:01','manage','ReListHtml','---',0),(226,'::1','2016-09-02 10:43:27','manage','EditNews','classid=36<br>id=18<br>title=测试数据',1000010000000018),(227,'::1','2016-09-02 10:43:33','manage','ReListHtml','---',0),(228,'::1','2016-09-02 10:43:46','manage','ReListHtml','---',0),(229,'::1','2016-09-02 10:44:08','manage','EditNews','classid=36<br>id=18<br>title=测试数据',1000010000000018),(230,'::1','2016-09-02 10:44:16','manage','ReListHtml','---',0),(231,'::1','2016-09-02 10:45:18','manage','EditNewstemp','tempid=13<br>tempname=爱德校区详细页&gid=1',0),(232,'::1','2016-09-02 10:46:45','manage','EditListtemp','tempid=11<br>tempname=城中分校新闻列表模板&gid=1',0),(233,'::1','2016-09-02 10:46:57','manage','EditListtemp','tempid=12<br>tempname=爱德分校新闻列表模板&gid=1',0),(234,'::1','2016-09-02 10:47:10','manage','EditListtemp','tempid=13<br>tempname=爱德分校图片+信息列表模板&gid=1',0),(235,'::1','2016-09-02 10:47:21','manage','EditListtemp','tempid=10<br>tempname=集团荣誉模板&gid=1',0),(236,'::1','2016-09-02 10:47:32','manage','EditListtemp','tempid=9<br>tempname=图片列表模板&gid=1',0),(237,'::1','2016-09-02 10:47:49','manage','EditListtemp','tempid=1<br>tempname=默认新闻列表模板&gid=1',0),(238,'::1','2016-09-02 10:48:53','manage','CopyNews_all','classid=34<br>id=21<br>title=测试信息2<br>to_classid=37',1000010000000021),(239,'::1','2016-09-02 10:49:01','manage','GoodInfo_all','classid=37<br>classname=时时关注<br>id=22&doing=0',1000010000000022),(240,'::1','2016-09-02 10:49:09','manage','CopyNews_all','classid=35<br>id=17<br>title=测试数据测试数据<br>to_classid=37',1000010000000017),(241,'::1','2016-09-02 10:49:19','manage','ReListHtml_all','---',0),(242,'::1','2016-09-02 10:50:31','manage','EditBqtemp','tempid=17<br>tempname=爱德实时关注&gid=1',0),(243,'::1','2016-09-02 10:51:01','manage','AddBqtemp','tempid=20<br>tempname=爱德首页点亮德育&gid=1',0),(244,'::1','2016-09-02 10:51:30','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(245,'::1','2016-09-02 10:51:53','manage','EditNews','classid=37<br>id=22<br>title=测试信息2',1000010000000022),(246,'::1','2016-09-02 10:52:00','manage','ReListHtml','---',0),(247,'::1','2016-09-02 10:52:32','manage','EditBqtemp','tempid=17<br>tempname=爱德实时关注&gid=1',0),(248,'::1','2016-09-02 10:52:39','manage','ReListHtml','---',0),(249,'::1','2016-09-02 10:54:04','manage','EditBqtemp','tempid=18<br>tempname=爱德实时关注推荐&gid=1',0),(250,'::1','2016-09-02 10:54:13','manage','ReListHtml','---',0),(251,'::1','2016-09-02 10:55:17','manage','EditNews','classid=34<br>id=21<br>title=测试信息2',1000010000000021),(252,'::1','2016-09-02 10:55:33','manage','EditNews','classid=37<br>id=22<br>title=测试信息2',1000010000000022),(253,'::1','2016-09-02 10:55:40','manage','ReListHtml','---',0),(254,'::1','2016-09-02 10:56:27','manage','EditNews','classid=34<br>id=21<br>title=测试信息2',1000010000000021),(255,'::1','2016-09-02 10:56:38','manage','EditNews','classid=34<br>id=15<br>title=测试信息',1000010000000015),(256,'::1','2016-09-02 10:56:46','manage','ReListHtml','---',0),(257,'::1','2016-09-02 11:00:04','manage','EditTempvar','varid=2<br>var=footer&gid=1',0),(258,'::1','2016-09-02 11:00:36','manage','SetMoreClass','---',0),(259,'::1','2016-09-02 11:00:41','manage','ReIndex','---',0),(260,'::1','2016-09-02 11:00:51','manage','ReListHtml_all','---',0),(261,'::1','2016-09-02 11:01:23','manage','AddNews','classid=37<br>id=24<br>title=测试信息3',1000010000000024),(262,'::1','2016-09-02 11:03:29','manage','ChangeEnewsData','---',0),(263,'::1','2016-09-02 11:03:38','manage','ReClassPath','---',0),(264,'::1','2016-09-02 11:03:44','manage','DelFcListClass','---',0),(265,'::1','2016-09-02 11:04:00','manage','UpdateClassInfosAll','---',0),(266,'::1','2016-09-02 11:04:07','manage','ReIndex','---',0),(267,'::1','2016-09-02 11:04:17','manage','ReListHtml_all','---',0),(268,'::1','2016-09-02 11:04:46','manage','DelTable','tid=2<br>tbname=download',0),(269,'::1','2016-09-02 11:04:51','manage','DelTable','tid=3<br>tbname=photo',0),(270,'::1','2016-09-02 11:05:00','manage','DelTable','tid=4<br>tbname=flash',0),(271,'::1','2016-09-02 11:05:06','manage','DelTable','tid=5<br>tbname=movie',0),(272,'::1','2016-09-02 11:05:11','manage','DelTable','tid=6<br>tbname=shop',0),(273,'::1','2016-09-02 11:05:17','manage','DelTable','tid=7<br>tbname=article',0),(274,'::1','2016-09-02 11:05:22','manage','DelTable','tid=8<br>tbname=info',0),(275,'::1','2016-09-02 11:05:43','manage','ChangeEnewsData','---',0),(276,'::1','2016-09-02 11:05:52','manage','ReClassPath','---',0),(277,'::1','2016-09-02 11:05:58','manage','DelFcListClass','---',0),(278,'::1','2016-09-02 11:06:15','manage','UpdateClassInfosAll','---',0),(279,'::1','2016-09-02 11:09:33','manage','BakExe','dbname=wjsx',0),(280,'::1','2016-09-02 11:27:29','manage','DoOpi','dbname=wjsx',0),(281,'::1','2016-09-02 11:30:48','manage','BakExe','dbname=wjsx',0),(282,'::1','2016-09-02 11:44:52','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(283,'::1','2016-09-02 11:45:02','manage','EditTempvar','varid=3<br>var=czheader&gid=1',0),(284,'::1','2016-09-02 11:45:15','manage','EditTempvar','varid=5<br>var=adheader&gid=1',0),(285,'::1','2016-09-02 11:45:22','manage','ReIndex','---',0),(286,'::1','2016-09-02 11:45:32','manage','ReListHtml_all','---',0),(287,'::1','2016-09-02 12:38:26','manage','ReData','dbname=wjsx&path=wjsx_20160902120019',0),(288,'::1','2016-09-02 12:39:00','manage','login','---',0),(289,'::1','2016-09-02 12:39:04','manage','ChangeEnewsData','---',0),(290,'::1','2016-09-02 12:39:07','manage','ReClassPath','---',0),(291,'::1','2016-09-02 12:39:09','manage','DelFcListClass','---',0),(292,'::1','2016-09-02 12:39:16','manage','UpdateClassInfosAll','---',0),(293,'::1','2016-09-02 12:39:22','manage','ReIndex','---',0),(294,'::1','2016-09-02 12:39:24','manage','ReListHtml_all','---',0),(295,'::1','2016-09-02 13:07:51','manage','EditNews','classid=9<br>id=9<br>title=建构“和融向上”行政管理团队',1000010000000009),(296,'::1','2016-09-02 13:08:00','manage','EditNews','classid=9<br>id=7<br>title=建构“和融向上”行政管理团队',1000010000000007),(297,'180.103.188.220','2016-09-02 14:27:11','manage','login','---',0),(298,'180.103.188.220','2016-09-02 14:28:03','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(299,'180.103.188.220','2016-09-02 14:28:12','manage','EditTempvar','varid=3<br>var=czheader&gid=1',0),(300,'180.103.188.220','2016-09-02 14:28:20','manage','EditTempvar','varid=5<br>var=adheader&gid=1',0),(301,'180.103.188.220','2016-09-02 14:28:25','manage','ReIndex','---',0),(302,'180.103.188.220','2016-09-02 14:28:29','manage','ReListHtml_all','---',0),(303,'49.64.72.66','2016-09-02 15:58:36','manage','login','---',0),(304,'::1','2016-09-06 09:56:57','manage','login','---',0),(305,'::1','2016-09-06 09:59:01','manage','AddTempvar','varid=8<br>var=thheader&gid=1',0),(306,'::1','2016-09-06 10:01:48','manage','AddTempvar','varid=9<br>var=thfooter&gid=1',0),(307,'::1','2016-09-06 10:02:36','manage','AddClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(308,'::1','2016-09-06 10:04:21','manage','AddClass','classid=40<br>classname=太湖分校',0),(309,'::1','2016-09-06 10:16:23','manage','ReListHtml','---',0),(310,'::1','2016-09-06 10:18:35','manage','AddClass','classid=41<br>classname=新闻资讯',0),(311,'::1','2016-09-06 10:19:54','manage','AddClass','classid=42<br>classname=新闻动态',0),(312,'::1','2016-09-06 10:29:37','manage','AddClass','classid=43<br>classname=媒体报道',0),(313,'::1','2016-09-06 10:30:05','manage','AddClass','classid=44<br>classname=最新公告',0),(314,'::1','2016-09-06 10:30:45','manage','AddClass','classid=45<br>classname=关于我们',0),(315,'::1','2016-09-06 10:31:08','manage','AddClass','classid=46<br>classname=课程DIY',0),(316,'::1','2016-09-06 10:31:31','manage','AddClass','classid=47<br>classname=教师POST',0),(317,'::1','2016-09-06 10:32:13','manage','EditClass','classid=41<br>classname=实时关注',0),(318,'::1','2016-09-06 10:32:30','manage','AddClass','classid=48<br>classname=班级GARDEND',0),(319,'::1','2016-09-06 10:32:46','manage','AddClass','classid=49<br>classname=心理SPA',0),(320,'::1','2016-09-06 11:34:56','manage','AddListtemp','tempid=14<br>tempname=太湖分校新闻列表模板&gid=1',0),(321,'::1','2016-09-06 11:35:16','manage','EditListtemp','tempid=14<br>tempname=太湖分校新闻列表模板&gid=1',0),(322,'::1','2016-09-06 11:35:43','manage','AddListtemp','tempid=15<br>tempname=太湖分校图片+信息列表模板&gid=1',0),(323,'::1','2016-09-06 11:36:34','manage','AddNewstemp','tempid=14<br>tempname=太湖校区详细页&gid=1',0),(324,'::1','2016-09-06 11:36:52','manage','EditClass','classid=41<br>classname=实时关注',0),(325,'::1','2016-09-06 11:37:48','manage','EditClass','classid=42<br>classname=新闻动态',0),(326,'::1','2016-09-06 11:38:10','manage','EditClass','classid=43<br>classname=媒体报道',0),(327,'::1','2016-09-06 11:38:20','manage','EditClass','classid=44<br>classname=最新公告',0),(328,'::1','2016-09-06 11:38:32','manage','EditClass','classid=45<br>classname=关于我们',0),(329,'::1','2016-09-06 11:38:42','manage','EditClass','classid=46<br>classname=课程DIY',0),(330,'::1','2016-09-06 11:39:38','manage','EditClass','classid=47<br>classname=教师POST',0),(331,'::1','2016-09-06 11:39:50','manage','EditClass','classid=48<br>classname=班级GARDEND',0),(332,'::1','2016-09-06 11:39:58','manage','EditClass','classid=49<br>classname=心理SPA',0),(333,'::1','2016-09-06 11:40:03','manage','ReListHtml','---',0),(334,'::1','2016-09-06 11:40:08','manage','ReListHtml','---',0),(335,'::1','2016-09-06 15:50:16','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(336,'::1','2016-09-06 15:51:50','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(337,'::1','2016-09-06 15:52:26','manage','DelBqtemp','tempid=9<br>tempname=标题+软件简介&gid=1',0),(338,'::1','2016-09-06 15:57:20','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(339,'::1','2016-09-06 16:00:41','manage','EditBqtemp','tempid=12<br>tempname=太湖分校首页公告&gid=1',0),(340,'::1','2016-09-06 16:04:47','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(341,'::1','2016-09-06 16:05:30','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(342,'::1','2016-09-06 16:06:23','manage','EditClass','classid=46<br>classname=课程DIY',0),(343,'::1','2016-09-06 16:06:30','manage','ReListHtml','---',0),(344,'::1','2016-09-06 16:08:08','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(345,'::1','2016-09-06 16:08:17','manage','ReListHtml','---',0),(346,'::1','2016-09-06 16:08:43','manage','EditClass','classid=46<br>classname=课程DIY',0),(347,'::1','2016-09-06 16:08:48','manage','ReListHtml','---',0),(348,'::1','2016-09-06 16:09:42','manage','AddNews','classid=42<br>id=25<br>title=测试数据',1000010000000025),(349,'::1','2016-09-06 16:09:58','manage','AddNews','classid=42<br>id=26<br>title=测试数据2',1000010000000026),(350,'::1','2016-09-06 16:12:48','manage','EditBqtemp','tempid=11<br>tempname=太湖分校首页头条&gid=1',0),(351,'::1','2016-09-06 16:14:02','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(352,'::1','2016-09-06 16:14:23','manage','GoodInfo_all','classid=42<br>classname=新闻动态<br>id=26&doing=0',1000010000000026),(353,'::1','2016-09-06 16:14:44','manage','SetMoreClass','---',0),(354,'::1','2016-09-06 16:14:46','manage','ReIndex','---',0),(355,'::1','2016-09-06 16:14:49','manage','ReListHtml_all','---',0),(356,'::1','2016-09-06 16:15:11','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(357,'::1','2016-09-06 16:15:27','manage','EditTempvar','varid=2<br>var=footer&gid=1',0),(358,'::1','2016-09-06 16:15:34','manage','ReIndex','---',0),(359,'::1','2016-09-06 16:15:38','manage','ReListHtml_all','---',0),(360,'::1','2016-09-06 16:21:04','manage','EditTempvar','varid=8<br>var=thheader&gid=1',0),(361,'::1','2016-09-06 17:23:49','manage','EditTempvar','varid=8<br>var=thheader&gid=1',0),(362,'::1','2016-09-06 17:23:53','manage','ReListHtml_all','---',0),(363,'::1','2016-09-06 17:25:27','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(364,'::1','2016-09-06 17:28:05','manage','EditTempvar','varid=8<br>var=thheader&gid=1',0),(365,'::1','2016-09-06 17:28:08','manage','ReListHtml_all','---',0),(366,'::1','2016-09-06 17:28:13','manage','ReIndex','---',0),(367,'58.210.80.187','2016-09-07 12:59:17','admin','login','---',0),(368,'58.210.80.187','2016-09-07 13:00:21','admin','exit','---',0),(369,'58.210.80.187','2016-09-07 13:00:57','admin','login','---',0),(370,'58.210.80.187','2016-09-07 13:02:36','admin','exit','---',0),(371,'58.210.80.187','2016-09-07 16:17:14','admin','login','---',0),(372,'58.210.80.187','2016-09-08 16:06:45','admin','login','---',0),(373,'58.210.80.187','2016-09-08 16:09:09','admin','AddNews','classid=35<br>id=27<br>title=xiexieixie',1000010000000027),(374,'111.74.215.99','2016-09-08 21:34:39','manage','login','---',0),(375,'111.74.215.79','2016-09-08 21:35:18','manage','login','---',0),(376,'117.80.139.75','2016-09-09 08:17:18','manage','login','---',0),(377,'58.210.80.187','2016-09-09 08:19:26','admin','login','---',0),(378,'117.80.139.75','2016-09-09 08:20:42','manage','AddTempvar','varid=10<br>var=szwheader&gid=1',0),(379,'58.210.80.187','2016-09-09 08:21:41','admin','AddInfoToReHtml','classid=35<br>do=6',0),(380,'117.80.139.75','2016-09-09 08:21:44','manage','EditTempvar','varid=5<br>var=adheader&gid=1',0),(381,'58.210.80.187','2016-09-09 08:22:13','admin','GoodInfo_all','classid=35<br>classname=课程DIY<br>id=27&doing=0',1000010000000027),(382,'117.80.139.75','2016-09-09 08:22:17','manage','AddTempvar','varid=11<br>var=szwfooter&gid=1',0),(383,'117.80.139.75','2016-09-09 08:22:50','manage','AddListtemp','tempid=16<br>tempname=苏州湾校区新闻列表模板&gid=1',0),(384,'117.80.139.75','2016-09-09 08:24:11','manage','AddListtemp','tempid=17<br>tempname=苏州湾校区图片+信息列表模板&gid=1',0),(385,'117.80.139.75','2016-09-09 08:24:34','manage','AddClasstemp','tempid=4&tempname=苏州湾校区&gid=1',0),(386,'117.80.139.75','2016-09-09 08:24:59','manage','AddClass','classid=50<br>classname=苏州湾校区',0),(387,'117.80.139.75','2016-09-09 08:25:42','manage','AddClass','classid=51<br>classname=关于我们',0),(388,'58.210.80.187','2016-09-09 08:25:50','admin','AddInfoToReHtml','classid=35<br>do=6',0),(389,'117.80.139.75','2016-09-09 08:26:43','manage','AddUser','userid=3<br>username=dd',0),(390,'117.80.139.75','2016-09-09 08:26:47','manage','exit','---',0),(391,'117.80.139.75','2016-09-09 08:27:13','dd','login','---',0),(392,'117.80.139.75','2016-09-09 08:27:55','dd','AddNewstemp','tempid=15<br>tempname=苏州湾校区详细页&gid=1',0),(393,'117.80.139.75','2016-09-09 08:28:20','dd','EditClass','classid=51<br>classname=关于我们',0),(394,'117.80.139.75','2016-09-09 08:28:47','dd','AddClass','classid=52<br>classname=课程DIY',0),(395,'117.80.139.75','2016-09-09 08:29:22','dd','AddClass','classid=53<br>classname=教师POST',0),(396,'117.80.139.75','2016-09-09 08:29:46','dd','AddClass','classid=54<br>classname=时时关注',0),(397,'117.80.139.75','2016-09-09 08:30:09','dd','AddClass','classid=55<br>classname=班级GARDEND',0),(398,'117.80.139.75','2016-09-09 08:30:41','dd','AddClass','classid=56<br>classname=心理SPA',0),(399,'117.80.139.75','2016-09-09 08:33:49','dd','EditClasstemp','tempid=4&tempname=苏州湾校区&gid=1',0),(400,'117.80.139.75','2016-09-09 08:34:30','dd','EditTempvar','varid=1<br>var=header&gid=1',0),(401,'117.80.139.75','2016-09-09 08:36:36','dd','ReIndex','---',0),(402,'117.80.139.75','2016-09-09 08:36:40','dd','ReListHtml_all','---',0),(403,'117.80.139.75','2016-09-09 08:43:18','dd','EditTempvar','varid=10<br>var=szwheader&gid=1',0),(404,'117.80.139.75','2016-09-09 08:43:32','dd','EditClass','classid=51<br>classname=点亮苏州湾',0),(405,'117.80.139.75','2016-09-09 08:43:58','dd','EditTempvar','varid=1<br>var=header&gid=1',0),(406,'117.80.139.75','2016-09-09 08:44:00','dd','ReIndex','---',0),(407,'117.80.139.75','2016-09-09 08:44:03','dd','ReListHtml_all','---',0),(408,'117.80.139.75','2016-09-09 08:44:22','dd','EditTempvar','varid=10<br>var=szwheader&gid=1',0),(409,'117.80.139.75','2016-09-09 08:52:24','dd','exit','---',0),(410,'117.80.139.75','2016-09-09 08:52:41','dd','login','---',0),(411,'117.80.139.75','2016-09-09 08:52:44','dd','exit','---',0),(412,'117.80.139.75','2016-09-09 08:56:32','manage','login','---',0),(413,'117.80.139.75','2016-09-09 08:56:37','manage','exit','---',0),(414,'117.80.139.75','2016-09-09 08:57:07','dd','login','---',0),(415,'58.210.80.187','2016-09-09 11:18:48','admin','exit','---',0),(416,'58.210.80.187','2016-09-09 13:26:26','manage','login','---',0),(417,'58.210.80.187','2016-09-09 13:27:33','manage','AddUser','userid=4<br>username=爱德',0),(418,'58.210.80.187','2016-09-09 13:27:56','manage','exit','---',0),(419,'58.210.80.187','2016-09-09 13:28:13','爱德','login','---',0),(420,'58.210.80.187','2016-09-09 16:57:18','爱德','exit','---',0),(421,'58.210.80.187','2016-09-09 16:57:27','爱德','login','---',0),(422,'58.210.80.187','2016-09-09 16:57:45','爱德','AddNews','classid=16<br>id=28<br>title=111111',1000010000000028),(423,'58.210.80.187','2016-09-09 16:58:05','爱德','DelNews','classid=16<br>id=28<br>title=111111',1000010000000028),(424,'221.224.114.98','2016-09-12 07:34:02','爱德','login','---',0),(425,'221.224.114.98','2016-09-12 07:42:00','爱德','AddNews','classid=34<br>id=29<br>title=“学奥运精神 做爱德好少年”——爱德校区开学典礼篇',1000010000000029),(426,'221.224.114.98','2016-09-12 07:43:43','爱德','EditNews','classid=34<br>id=29<br>title=“学奥运精神 做爱德好少年”——爱德校区开学典礼篇',1000010000000029),(427,'221.224.114.98','2016-09-12 07:46:07','爱德','exit','---',0),(428,'221.224.114.98','2016-09-12 07:46:59','爱德','login','---',0),(429,'58.210.80.187','2016-09-12 09:34:31','manage','login','---',0),(430,'58.210.80.187','2016-09-12 09:35:16','manage','EditClass','classid=38<br>classname=班级GARDEN',0),(431,'58.210.80.187','2016-09-12 09:35:27','manage','ReListHtml_all','---',0),(432,'58.210.80.187','2016-09-12 09:36:18','manage','EditClasstemp','tempid=2&tempname=爱德分校&gid=1',0),(433,'58.210.80.187','2016-09-12 09:36:29','manage','ReListHtml_all','---',0),(434,'58.210.80.187','2016-09-12 09:36:37','manage','ReIndex','---',0),(435,'58.210.80.187','2016-09-12 09:37:10','manage','ReUserpageAll','---',0),(436,'58.210.80.187','2016-09-12 09:37:21','manage','ReUserlistAll','---',0),(437,'58.210.80.187','2016-09-12 09:37:26','manage','ReUserjsAll','---',0),(438,'58.210.80.187','2016-09-12 09:37:31','manage','ChangeEnewsData','---',0),(439,'58.210.80.187','2016-09-12 09:38:02','manage','ReListHtmlMore','---',0),(440,'58.210.80.187','2016-09-12 09:38:17','manage','ChangeAllModForm','ChangeClass=1',0),(441,'58.210.80.187','2016-09-12 09:38:21','manage','ReAdJs_all','---',0),(442,'58.210.80.187','2016-09-12 09:38:29','manage','ReTtListHtml_all','---',0),(443,'58.210.80.187','2016-09-12 09:38:34','manage','ReDtPage','---',0),(444,'58.210.80.187','2016-09-12 09:38:53','manage','ReListHtml_all','---',0),(445,'58.210.80.187','2016-09-12 09:40:12','manage','EditTempvar','varid=5<br>var=adheader&gid=1',0),(446,'58.210.80.187','2016-09-12 09:40:21','manage','ReListHtml_all','---',0),(447,'221.224.114.98','2016-09-12 13:17:13','爱德','login','---',0),(448,'221.224.114.98','2016-09-12 13:19:39','爱德','DelNews_all','classid=34<br>id=15&ecms=0<br>title=测试信息',1000010000000015),(449,'58.210.80.187','2016-09-13 08:48:14','manage','login','---',0),(450,'58.210.80.187','2016-09-13 08:50:43','manage','AddUser','userid=5<br>username=苏州湾',0),(451,'58.210.80.187','2016-09-13 08:50:49','manage','exit','---',0),(452,'58.210.80.187','2016-09-13 08:51:02','苏州湾','login','---',0),(453,'58.210.80.187','2016-09-13 08:51:15','苏州湾','DelNews','classid=35<br>id=27<br>title=xiexieixie',1000010000000027),(454,'58.210.80.187','2016-09-13 08:51:30','苏州湾','exit','---',0),(455,'58.210.80.187','2016-09-13 08:51:40','manage','login','---',0),(456,'58.210.80.187','2016-09-13 08:52:38','manage','AddGroup','groupid=2<br>groupname=校区管理员',0),(457,'58.210.80.187','2016-09-13 08:53:17','manage','EditUser','userid=5<br>username=苏州湾',0),(458,'58.210.80.187','2016-09-13 08:53:32','manage','EditUser','userid=4<br>username=爱德',0),(459,'58.210.80.187','2016-09-13 08:53:42','manage','exit','---',0),(460,'58.210.80.187','2016-09-13 08:53:57','苏州湾','login','---',0),(461,'58.210.80.187','2016-09-13 08:54:14','苏州湾','exit','---',0),(462,'58.210.80.187','2016-09-13 08:54:58','manage','login','---',0),(463,'58.210.80.187','2016-09-13 08:55:34','manage','EditUser','userid=5<br>username=苏州湾',0),(464,'58.210.80.187','2016-09-13 08:56:07','manage','EditGroup','groupid=2<br>groupname=校区管理员',0),(465,'58.210.80.187','2016-09-13 08:56:57','manage','exit','---',0),(466,'58.210.80.187','2016-09-13 08:57:06','苏州湾','login','---',0),(467,'58.210.80.187','2016-09-13 08:57:18','苏州湾','AddNews','classid=51<br>id=30<br>title=111',1000010000000030),(468,'58.210.80.187','2016-09-13 08:57:47','苏州湾','CheckNews_all','classid=51<br>id=30<br>title=111',1000010000000030),(469,'58.210.80.187','2016-09-13 08:58:52','苏州湾','AddInfoToReHtml','classid=51<br>do=6',0),(470,'58.210.80.187','2016-09-13 09:10:59','苏州湾','GoodInfo_all','classid=51<br>classname=点亮苏州湾<br>id=30&doing=0',1000010000000030),(471,'58.210.80.187','2016-09-13 09:12:00','苏州湾','AddNews','classid=51<br>id=31<br>title=2222',1000010000000031),(472,'58.210.80.187','2016-09-13 09:12:26','苏州湾','AddNews','classid=51<br>id=32<br>title=22222',1000010000000032),(473,'58.210.80.187','2016-09-13 09:12:38','苏州湾','CheckNews_all','classid=51<br>id=32<br>title=22222',1000010000000032),(474,'58.210.80.187','2016-09-13 09:12:47','苏州湾','AddInfoToReHtml','classid=51<br>do=6',0),(475,'58.210.80.187','2016-09-13 09:13:24','苏州湾','exit','---',0),(476,'58.210.80.187','2016-09-13 09:40:03','manage','login','---',0),(477,'58.210.80.187','2016-09-13 09:40:24','manage','EditUser','userid=4<br>username=爱德',0),(478,'58.210.80.187','2016-09-13 09:40:31','manage','DelUser','userid=3<br>username=dd',0),(479,'58.210.80.187','2016-09-13 12:59:06','manage','login','---',0),(480,'58.210.80.187','2016-09-13 13:00:19','manage','AddUser','userid=6<br>username=城中',0),(481,'58.210.80.187','2016-09-13 13:00:25','manage','exit','---',0),(482,'58.210.80.187','2016-09-13 13:00:48','城中','login','---',0),(483,'58.210.80.187','2016-09-13 13:02:41','城中','AddNews','classid=16<br>id=33<br>title=测试',1000010000000033),(484,'58.210.80.187','2016-09-13 13:02:49','城中','CheckNews_all','classid=16<br>id=33<br>title=测试',1000010000000033),(485,'58.210.80.187','2016-09-13 13:03:17','城中','AddInfoToReHtml','classid=16<br>do=6',0),(486,'218.4.59.173','2016-09-13 16:11:31','城中','login','---',0),(487,'218.4.59.173','2016-09-13 16:13:20','城中','EditPassword','---',0),(488,'218.4.59.173','2016-09-13 16:14:19','城中','login','---',0),(489,'222.93.20.136','2016-09-14 01:40:13','manage','login','---',0),(490,'222.93.20.136','2016-09-14 01:40:35','manage','DelNews_all','classid=9<br>classname=新闻资讯&ecms=0',0),(491,'222.93.20.136','2016-09-14 01:41:00','manage','DelNews_all','classid=10<br>classname=集团故事&ecms=0',0),(492,'222.93.20.136','2016-09-14 01:45:20','manage','login','---',0),(493,'222.93.20.136','2016-09-14 01:50:54','manage','AddNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(494,'222.93.20.136','2016-09-14 01:52:27','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(495,'222.93.20.136','2016-09-14 01:53:27','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(496,'222.93.20.136','2016-09-14 01:53:58','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(497,'222.93.20.136','2016-09-14 01:54:59','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(498,'222.93.20.136','2016-09-14 01:56:19','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(499,'222.93.20.136','2016-09-14 01:57:37','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(500,'222.93.20.136','2016-09-14 01:58:52','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(501,'222.93.20.136','2016-09-14 02:01:27','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(502,'222.93.20.136','2016-09-14 02:02:01','manage','EditNews','classid=9<br>id=34<br>title=为教育现代化树立新标杆',1000010000000034),(503,'222.93.20.136','2016-09-14 02:08:42','manage','AddNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(504,'222.93.20.136','2016-09-14 02:08:59','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(505,'222.93.20.136','2016-09-14 02:10:37','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(506,'222.93.20.136','2016-09-14 02:11:10','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(507,'222.93.20.136','2016-09-14 02:12:00','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(508,'222.93.20.136','2016-09-14 02:12:59','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(509,'222.93.20.136','2016-09-14 02:13:58','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(510,'222.93.20.136','2016-09-14 02:15:15','manage','AddWriter','wid=1<br>writer=申晓峰',0),(511,'222.93.20.136','2016-09-14 02:15:40','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(512,'222.93.20.136','2016-09-14 02:18:36','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(513,'222.93.20.136','2016-09-14 02:19:08','manage','EditNews','classid=9<br>id=35<br>title=丰厚素养，点亮未来',1000010000000035),(514,'58.210.80.187','2016-09-14 11:02:58','manage','login','---',0),(515,'58.210.80.187','2016-09-14 11:05:02','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(516,'58.210.80.187','2016-09-14 11:05:56','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(517,'58.210.80.187','2016-09-14 11:06:09','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(518,'58.210.80.187','2016-09-14 11:14:04','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(519,'58.210.80.187','2016-09-14 11:14:22','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(520,'58.210.80.187','2016-09-14 11:15:04','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(521,'58.210.80.187','2016-09-14 11:15:17','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(522,'58.210.80.187','2016-09-14 11:16:22','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(523,'58.210.80.187','2016-09-14 11:20:18','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(524,'58.210.80.187','2016-09-14 11:21:40','manage','TDelFile_all','---',0),(525,'58.210.80.187','2016-09-14 11:21:53','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(526,'58.210.80.187','2016-09-14 11:23:15','manage','TDelFile_all','---',0),(527,'58.210.80.187','2016-09-14 11:23:25','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(528,'58.210.80.187','2016-09-14 11:23:59','manage','EditNews','classid=2<br>id=1<br>title=承“爱德、求真”校训  办“生动、大气”教育',1000010000000001),(529,'58.210.80.187','2016-09-14 11:52:57','manage','EditNews','classid=4<br>id=3<br>title=集团战略',1000010000000003),(530,'221.224.114.98','2016-09-14 11:57:11','爱德','login','---',0),(531,'58.210.80.187','2016-09-14 12:37:57','manage','EditTempvar','varid=7<br>var=adfooter&gid=1',0),(532,'58.210.80.187','2016-09-14 12:38:31','manage','EditTempvar','varid=7<br>var=adfooter&gid=1',0),(533,'58.210.80.187','2016-09-14 12:38:42','manage','ReListHtml_all','---',0),(534,'58.210.80.187','2016-09-14 12:38:44','manage','ReIndex','---',0),(535,'221.224.114.98','2016-09-14 17:02:17','爱德','login','---',0),(536,'58.210.80.187','2016-09-18 08:25:23','manage','login','---',0),(537,'58.210.80.187','2016-09-18 08:38:44','manage','EditNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(538,'58.210.80.187','2016-09-18 08:40:44','manage','EditNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(539,'58.210.80.187','2016-09-18 08:48:20','manage','EditNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(540,'58.210.80.187','2016-09-18 08:48:38','manage','EditNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(541,'58.210.80.187','2016-09-18 08:50:10','manage','EditNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(542,'58.210.80.187','2016-09-18 10:38:56','manage','EditNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(543,'121.224.137.241','2016-09-19 13:00:31','manage','login','---',0),(544,'121.224.137.241','2016-09-19 13:17:02','manage','EditTempvar','varid=3<br>var=czheader&gid=1',0),(545,'121.224.137.241','2016-09-19 13:17:18','manage','EditTempvar','varid=4<br>var=czfooter&gid=1',0),(546,'121.224.137.241','2016-09-19 13:18:59','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(547,'121.224.137.241','2016-09-19 13:19:10','manage','ReListHtml','---',0),(548,'121.224.137.241','2016-09-19 13:20:03','manage','ReListHtml','---',0),(549,'121.224.137.241','2016-09-19 13:21:00','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(550,'121.224.137.241','2016-09-19 13:21:08','manage','ReListHtml','---',0),(551,'121.224.137.241','2016-09-19 13:22:16','manage','EditBqtemp','tempid=16<br>tempname=城中分校首页类别调用&gid=1',0),(552,'121.224.137.241','2016-09-19 13:22:29','manage','EditClasstemp','tempid=1&tempname=城中分校&gid=1',0),(553,'121.224.137.241','2016-09-19 13:22:33','manage','ReListHtml_all','---',0),(554,'121.224.137.241','2016-09-19 13:23:33','manage','EditNews','classid=16<br>id=33<br>title=测试',1000010000000033),(555,'121.224.137.241','2016-09-19 13:24:27','manage','EditNews','classid=16<br>id=33<br>title=测试',1000010000000033),(556,'121.224.137.241','2016-09-19 13:24:50','manage','EditNewstemp','tempid=12<br>tempname=城中分校内容模板&gid=1',0),(557,'121.224.137.241','2016-09-19 13:25:00','manage','ReListHtml_all','---',0),(558,'121.224.137.241','2016-09-19 14:23:25','manage','login','---',0),(559,'121.224.137.241','2016-09-19 14:24:15','manage','EditTempvar','varid=3<br>var=czheader&gid=1',0),(560,'121.224.137.241','2016-09-19 14:24:37','manage','ReListHtml_all','---',0),(561,'121.224.137.241','2016-09-19 14:24:43','manage','ReIndex','---',0),(562,'121.224.137.241','2016-09-19 14:24:50','manage','ReListHtml_all','---',0),(563,'49.75.189.3','2016-09-20 16:56:26','manage','login','---',0),(564,'49.75.189.3','2016-09-20 16:57:38','manage','AddNews','classid=45<br>id=36<br>title=关于我们',1000010000000036),(565,'49.75.189.3','2016-09-20 16:58:14','manage','EditTempvar','varid=8<br>var=thheader&gid=1',0),(566,'49.75.189.3','2016-09-20 16:58:19','manage','ReListHtml_all','---',0),(567,'49.75.189.3','2016-09-20 17:29:30','manage','AddClasstemp','tempid=5&tempname=集团组成&gid=1',0),(568,'49.75.189.3','2016-09-20 17:30:29','manage','AddClass','classid=57<br>classname=集团组成',0),(569,'49.75.189.3','2016-09-20 17:30:34','manage','ReListHtml','---',0),(570,'49.75.189.3','2016-09-20 17:31:12','manage','EditClasstemp','tempid=5&tempname=集团组成&gid=1',0),(571,'49.75.189.3','2016-09-20 17:31:18','manage','ReListHtml','---',0),(572,'49.75.189.3','2016-09-20 17:31:45','manage','EditTempvar','varid=1<br>var=header&gid=1',0),(573,'49.75.189.3','2016-09-20 17:31:48','manage','ReIndex','---',0),(574,'49.75.189.3','2016-09-20 17:31:51','manage','ReListHtml_all','---',0),(575,'58.210.80.187','2016-09-21 09:06:23','manage','login','---',0),(576,'58.210.80.187','2016-09-21 09:07:33','manage','AddUser','userid=7<br>username=管文锦',0),(577,'58.210.80.187','2016-09-21 09:08:53','manage','EditClass','classid=41<br>classname=动感校园',0),(578,'58.210.80.187','2016-09-21 09:09:48','manage','EditClass','classid=44<br>classname=通知公告',0),(579,'58.210.80.187','2016-09-21 09:10:05','manage','EditClass','classid=41<br>classname=实时关注',0),(580,'58.210.80.187','2016-09-21 09:10:27','manage','EditClass','classid=42<br>classname=动感校园',0),(581,'58.210.80.187','2016-09-21 09:11:04','manage','EditClass','classid=46<br>classname=德育天地',0),(582,'58.210.80.187','2016-09-21 09:11:22','manage','EditClass','classid=47<br>classname=教育科研',0),(583,'58.210.80.187','2016-09-21 09:11:37','manage','EditClass','classid=48<br>classname=教学之窗',0),(584,'58.210.80.187','2016-09-21 09:11:50','manage','EditClass','classid=49<br>classname=精致校园',0),(585,'58.210.80.187','2016-09-21 09:12:08','manage','ReListHtml_all','---',0),(586,'58.210.80.187','2016-09-21 09:12:21','manage','ReListHtml_all','---',0),(587,'58.210.80.187','2016-09-21 09:12:25','manage','ReIndex','---',0),(588,'58.210.80.187','2016-09-21 09:13:34','manage','EditClasstemp','tempid=3&tempname=太湖分校&gid=1',0),(589,'58.210.80.187','2016-09-21 09:13:57','manage','ReIndex','---',0),(590,'58.210.80.187','2016-09-21 09:14:05','manage','ReUserpageAll','---',0),(591,'58.210.80.187','2016-09-21 09:17:17','manage','EditTempvar','varid=8<br>var=thheader&gid=1',0),(592,'58.210.80.187','2016-09-21 09:18:18','manage','EditTempvar','varid=9<br>var=thfooter&gid=1',0),(593,'58.210.80.187','2016-09-21 09:19:03','manage','ReTtListHtml_all','---',0),(594,'58.210.80.187','2016-09-21 09:19:13','manage','ReDtPage','---',0),(595,'58.210.80.187','2016-09-21 09:19:23','manage','ReAllNewsJs','---',0),(596,'58.210.80.187','2016-09-21 09:19:33','manage','ReZtListHtml_all','---',0),(597,'58.210.80.187','2016-09-21 09:19:46','manage','ChangeAllModForm','ChangeClass=1',0),(598,'58.210.80.187','2016-09-21 09:20:10','manage','ReUserpageAll','---',0),(599,'58.210.80.187','2016-09-21 09:20:23','manage','ReUserlistAll','---',0),(600,'58.210.80.187','2016-09-21 09:20:33','manage','ReUserjsAll','---',0),(601,'58.210.80.187','2016-09-21 09:20:49','manage','UpdateClassInfosAll','---',0),(602,'58.210.80.187','2016-09-21 09:21:05','manage','DelFcListClass','---',0),(603,'58.210.80.187','2016-09-21 09:21:24','manage','ChangeEnewsData','---',0),(604,'58.210.80.187','2016-09-21 09:21:56','manage','ReListHtmlMore','---',0),(605,'58.210.80.187','2016-09-21 09:25:14','manage','ReListHtml_all','---',0),(606,'58.210.80.187','2016-09-21 09:26:09','manage','DelNews_all','classid=42<br>classname=动感校园&ecms=0',0),(607,'58.210.80.187','2016-09-21 09:26:21','manage','DelNews_all','classid=45<br>id=36&ecms=0<br>title=关于我们',1000010000000036),(608,'58.210.80.187','2016-09-21 10:02:58','manage','exit','---',0),(609,'58.210.80.187','2016-09-21 10:03:11','管文锦','login','---',0),(610,'58.210.80.187','2016-09-21 12:40:42','管文锦','login','---',0),(611,'58.210.80.187','2016-09-21 12:43:01','管文锦','AddNews','classid=10<br>id=37<br>title=有一种女子，温暖而嫣然',1000010000000037),(612,'58.210.80.187','2016-09-21 12:44:19','管文锦','AddNews','classid=10<br>id=38<br>title=交友护照，亲历美好',1000010000000038),(613,'58.210.80.187','2016-09-21 12:44:56','管文锦','EditNews','classid=10<br>id=37<br>title=有一种女子，温暖而嫣然',1000010000000037),(614,'58.210.80.187','2016-09-21 12:45:27','管文锦','AddNews','classid=10<br>id=39<br>title=蜕变',1000010000000039),(615,'58.210.80.187','2016-09-21 12:46:05','管文锦','AddNews','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(616,'58.210.80.187','2016-09-26 08:44:09','管文锦','login','---',0),(617,'58.210.80.187','2016-09-26 08:44:42','管文锦','CheckNews_all','classid=10<br>classname=集团故事',0),(618,'58.210.80.187','2016-09-26 11:32:27','manage','login','---',0),(619,'58.210.80.187','2016-09-26 11:32:37','manage','ReIndex','---',0),(620,'58.210.80.187','2016-09-27 11:05:35','manage','login','---',0),(621,'58.210.80.187','2016-09-27 16:17:55','manage','login','---',0),(622,'58.210.80.187','2016-09-27 16:19:22','manage','EditNews','classid=3<br>id=2<br>title=总校长寄语',1000010000000002),(623,'58.210.80.187','2016-09-27 16:19:37','manage','AddInfoToReHtml','classid=3<br>do=6',0),(624,'58.208.126.174','2016-09-28 17:51:18','manage','login','---',0),(625,'58.208.126.174','2016-09-28 17:51:53','manage','EditNews','classid=7<br>id=5<br>title=联系我们',1000010000000005),(626,'58.210.80.187','2016-09-30 12:52:09','管文锦','login','---',0),(627,'58.210.80.187','2016-09-30 12:55:51','管文锦','login','---',0),(628,'58.210.80.187','2016-09-30 12:56:48','管文锦','login','---',0),(629,'58.210.80.187','2016-09-30 13:47:09','管文锦','login','---',0),(630,'58.210.80.187','2016-09-30 13:47:47','管文锦','NoCheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(631,'58.210.80.187','2016-09-30 13:48:39','管文锦','EditNews','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(632,'58.210.80.187','2016-09-30 13:48:58','管文锦','CheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(633,'58.210.80.187','2016-09-30 13:49:49','管文锦','NoCheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(634,'58.210.80.187','2016-09-30 13:51:09','管文锦','EditNews','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(635,'58.210.80.187','2016-09-30 13:51:16','管文锦','CheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(636,'58.210.80.187','2016-09-30 13:52:12','管文锦','NoCheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(637,'58.210.80.187','2016-09-30 13:53:27','管文锦','EditNews','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(638,'58.210.80.187','2016-09-30 13:53:33','管文锦','CheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(639,'58.210.80.187','2016-09-30 13:54:18','管文锦','NoCheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(640,'58.210.80.187','2016-09-30 13:55:12','管文锦','EditNews','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(641,'58.210.80.187','2016-09-30 13:55:19','管文锦','CheckNews_all','classid=10<br>id=40<br>title=信纸上的旧时光',1000010000000040),(642,'58.210.80.187','2016-09-30 13:57:35','管文锦','login','---',0),(643,'114.217.76.217','2016-10-01 00:41:12','manage','login','---',0),(644,'114.217.76.217','2016-10-01 00:41:35','manage','EditPassword','---',0),(645,'114.217.76.217','2016-10-01 00:44:27','manage','AddNews','classid=9<br>id=41<br>title=播下英语素养的种子，让英语教学走得更稳更远',1000010000000041),(646,'114.217.76.217','2016-10-01 00:46:19','manage','EditNews','classid=9<br>id=41<br>title=播下英语素养的种子，让英语教学走得更稳更远',1000010000000041),(647,'114.217.76.217','2016-10-01 00:51:09','manage','EditNews','classid=9<br>id=41<br>title=播下英语素养的种子，让英语教学走得更稳更远',1000010000000041),(648,'114.217.76.217','2016-10-01 00:53:07','manage','AddInfoToReHtml','classid=9<br>do=6',0),(649,'114.217.76.217','2016-10-01 00:53:31','manage','EditNews','classid=9<br>id=41<br>title=播下英语素养的种子，让英语教学走得更稳更远',1000010000000041),(650,'114.217.76.217','2016-10-01 00:54:11','manage','DelNews','classid=16<br>id=33<br>title=测试',1000010000000033),(651,'114.217.76.217','2016-10-01 00:54:35','manage','DelNews','classid=34<br>id=21<br>title=测试信息2',1000010000000021),(652,'114.217.76.217','2016-10-01 00:54:45','manage','DelNews','classid=35<br>id=17<br>title=测试数据测试数据',1000010000000017),(653,'114.217.76.217','2016-10-01 00:54:55','manage','DelNews_all','classid=36<br>classname=教师POST&ecms=0',0),(654,'114.217.76.217','2016-10-01 00:55:03','manage','DelNews_all','classid=37<br>classname=时时关注&ecms=0',0),(655,'114.217.76.217','2016-10-01 00:55:12','manage','DelNews_all','classid=38<br>id=19&ecms=0<br>title=班级测试',1000010000000019),(656,'114.217.76.217','2016-10-01 00:55:21','manage','DelNews_all','classid=39<br>id=20&ecms=0<br>title=心理咨询',1000010000000020),(657,'114.217.76.217','2016-10-01 00:55:45','manage','DelNews_all','classid=51<br>classname=点亮苏州湾&ecms=0',0),(658,'114.217.76.217','2016-10-01 00:56:06','manage','ReIndex','---',0),(659,'114.217.76.217','2016-10-01 00:56:13','manage','ReListHtml_all','---',0),(660,'58.210.80.187','2016-10-08 09:52:39','苏州湾','login','---',0),(661,'58.210.80.187','2016-10-08 09:55:44','苏州湾','AddNews','classid=51<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(662,'58.210.80.187','2016-10-08 10:00:00','苏州湾','EditNews','classid=51<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(663,'58.210.80.187','2016-10-08 10:00:26','苏州湾','CheckNews_all','classid=51<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(664,'58.210.80.187','2016-10-08 10:01:04','苏州湾','AddInfoToReHtml','classid=51<br>do=6',0),(665,'58.210.80.187','2016-10-08 10:01:53','苏州湾','NoCheckNews_all','classid=51<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(666,'58.210.80.187','2016-10-08 10:02:22','苏州湾','EditNews','classid=51<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(667,'58.210.80.187','2016-10-08 10:02:28','苏州湾','CheckNews_all','classid=51<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(668,'58.210.80.187','2016-10-08 10:02:35','苏州湾','AddInfoToReHtml','classid=51<br>do=6',0),(669,'58.210.80.187','2016-10-08 10:02:58','苏州湾','NoCheckNews_all','classid=51<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(670,'58.210.80.187','2016-10-08 10:03:26','苏州湾','MoveNews_all','classid=51<br>classname=点亮苏州湾<br>id=42<br>to_classid=54',1000010000000042),(671,'58.210.80.187','2016-10-08 10:03:39','苏州湾','CheckNews_all','classid=54<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(672,'58.210.80.187','2016-10-08 10:03:45','苏州湾','AddInfoToReHtml','classid=54<br>do=6',0),(673,'58.210.80.187','2016-10-08 10:04:59','苏州湾','GoodInfo_all','classid=54<br>classname=时时关注<br>id=42&doing=0',1000010000000042),(674,'58.210.80.187','2016-10-08 10:05:21','苏州湾','AddInfoToReHtml','classid=54<br>do=6',0),(675,'58.210.80.187','2016-10-08 10:09:14','苏州湾','NoCheckNews_all','classid=54<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(676,'58.210.80.187','2016-10-08 10:09:44','苏州湾','EditNews','classid=54<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(677,'58.210.80.187','2016-10-08 10:09:49','苏州湾','CheckNews_all','classid=54<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(678,'58.210.80.187','2016-10-08 10:09:54','苏州湾','AddInfoToReHtml','classid=54<br>do=6',0),(679,'58.210.80.187','2016-10-08 10:10:49','苏州湾','NoCheckNews_all','classid=54<br>id=42<br>title= 播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(680,'58.210.80.187','2016-10-08 10:11:03','苏州湾','EditNews','classid=54<br>id=42<br>title=播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(681,'58.210.80.187','2016-10-08 10:11:08','苏州湾','CheckNews_all','classid=54<br>id=42<br>title=播下英语素养的种子，让英语教学走得更稳更远',1000010000000042),(682,'58.210.80.187','2016-10-08 10:11:13','苏州湾','AddInfoToReHtml','classid=54<br>do=6',0),(683,'221.224.114.98','2016-10-08 15:48:24','爱德','login','---',0),(684,'221.224.114.98','2016-10-08 15:49:13','爱德','exit','---',0),(685,'221.224.114.98','2016-10-08 16:06:07','爱德','login','---',0),(686,'221.224.114.98','2016-10-08 16:11:58','爱德','AddNews','classid=35<br>id=43<br>title=爷爷奶奶课堂开讲啦',1000010000000043),(687,'221.224.114.98','2016-10-08 16:17:19','爱德','login','---',0),(688,'221.224.114.98','2016-10-08 16:20:02','爱德','CheckNews_all','classid=35<br>id=43<br>title=爷爷奶奶课堂开讲啦',1000010000000043),(689,'58.210.80.187','2016-10-08 16:29:27','manage','login','---',0),(690,'221.224.114.98','2016-10-08 16:33:45','爱德','AddInfoToReHtml','classid=34<br>do=6',0),(691,'221.224.114.98','2016-10-08 16:35:33','爱德','login','---',0),(692,'221.224.114.98','2016-10-08 16:38:03','爱德','login','---',0),(693,'221.224.114.98','2016-10-08 16:38:18','爱德','NoCheckNews_all','classid=35<br>id=43<br>title=爷爷奶奶课堂开讲啦',1000010000000043),(694,'221.224.114.98','2016-10-08 16:39:28','爱德','EditNews','classid=35<br>id=43<br>title=爷爷奶奶课堂开讲啦',1000010000000043),(695,'221.224.114.98','2016-10-08 16:39:44','爱德','CheckNews_all','classid=35<br>id=43<br>title=爷爷奶奶课堂开讲啦',1000010000000043),(696,'221.224.114.98','2016-10-08 16:39:51','爱德','AddInfoToReHtml','classid=35<br>do=6',0),(697,'58.211.212.154','2016-10-09 16:43:28','admin','login','---',0),(698,'58.211.212.154','2016-10-09 16:44:25','admin','login','---',0),(699,'58.210.80.187','2016-10-10 10:10:56','manage','login','---',0),(700,'58.210.80.187','2016-10-10 10:32:26','manage','exit','---',0),(701,'218.4.59.173','2016-10-11 08:30:20','城中','login','---',0),(702,'218.4.59.173','2016-10-11 08:58:54','城中','login','---',0),(703,'58.210.80.187','2016-10-11 09:03:00','manage','login','---',0),(704,'58.210.80.187','2016-10-11 09:04:25','manage','EditClass','classid=28<br>classname=求真论坛',0),(705,'58.210.80.187','2016-10-11 09:04:37','manage','ReListHtml_all','---',0),(706,'218.4.59.173','2016-10-11 09:04:37','城中','AddNews','classid=16<br>id=44<br>title=九九重阳节 浓浓温暖情',1000010000000044),(707,'218.4.59.173','2016-10-11 15:05:23','城中','login','---',0),(708,'218.4.59.173','2016-10-11 15:13:00','城中','AddNews','classid=16<br>id=45<br>title=给我一个舞台，还你一份精彩',1000010000000045),(709,'58.210.80.187','2016-10-12 09:01:46','manage','login','---',0),(710,'58.210.80.187','2016-10-12 09:02:16','manage','EditPassword','---',0),(711,'58.210.80.187','2016-10-12 09:02:22','manage','exit','---',0),(712,'58.210.80.187','2016-10-12 09:02:32','manage','login','---',0),(713,'58.210.80.187','2016-10-12 09:02:36','manage','exit','---',0),(714,'58.210.80.187','2016-10-12 09:24:02','管文锦','login','---',0),(715,'58.210.80.187','2016-10-12 09:24:40','管文锦','NoCheckNews_all','classid=10<br>classname=集团故事',0),(716,'58.210.80.187','2016-10-12 09:25:25','管文锦','EditNews','classid=10<br>id=40<br>title=信纸上的旧时光（ 邱丽勤）',1000010000000040),(717,'58.210.80.187','2016-10-12 09:25:41','管文锦','EditNews','classid=10<br>id=39<br>title=蜕变（杨艳）',1000010000000039),(718,'58.210.80.187','2016-10-12 09:25:56','管文锦','EditNews','classid=10<br>id=38<br>title=交友护照，亲历美好（管文锦）',1000010000000038),(719,'58.210.80.187','2016-10-12 09:26:08','管文锦','EditNews','classid=10<br>id=37<br>title=有一种女子，温暖而嫣然（史春晓）',1000010000000037),(720,'58.210.80.187','2016-10-12 09:26:19','管文锦','CheckNews_all','classid=10<br>classname=集团故事',0),(721,'49.75.150.46','2016-10-12 10:08:54','manage','login','---',0),(722,'127.0.0.1','2016-10-12 17:57:05','manage','ReData','dbname=wjsx&path=wjsx_20161012100912',0),(723,'127.0.0.1','2016-10-12 18:13:36','manage','login','---',0),(724,'127.0.0.1','2016-10-12 18:13:41','manage','ChangeEnewsData','---',0),(725,'127.0.0.1','2016-10-12 18:13:44','manage','ReClassPath','---',0),(726,'127.0.0.1','2016-10-12 18:13:46','manage','DelFcListClass','---',0),(727,'127.0.0.1','2016-10-12 18:13:49','manage','UpdateClassInfosAll','---',0),(728,'127.0.0.1','2016-10-12 18:13:54','manage','ReIndex','---',0),(729,'127.0.0.1','2016-10-12 18:13:56','manage','ReListHtml_all','---',0),(730,'127.0.0.1','2016-10-16 20:53:10','manage','login','---',0),(731,'127.0.0.1','2016-10-16 21:15:40','manage','EditGroup','groupid=2<br>groupname=校区管理员',0),(732,'127.0.0.1','2016-10-16 21:16:08','manage','EditGroup','groupid=2<br>groupname=校区管理员',0),(733,'127.0.0.1','2016-10-16 21:19:15','manage','EditGroup','groupid=2<br>groupname=校区管理员',0);

/*Table structure for table `phome_enewsdownerror` */

DROP TABLE IF EXISTS `phome_enewsdownerror`;

CREATE TABLE `phome_enewsdownerror` (
  `errorid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `errortext` varchar(255) NOT NULL DEFAULT '',
  `errortime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `errorip` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`errorid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsdownerror` */

/*Table structure for table `phome_enewsdownrecord` */

DROP TABLE IF EXISTS `phome_enewsdownrecord`;

CREATE TABLE `phome_enewsdownrecord` (
  `id` int(11) NOT NULL DEFAULT '0',
  `pathid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `cardfen` int(11) NOT NULL DEFAULT '0',
  `truetime` int(11) NOT NULL DEFAULT '0',
  `classid` smallint(6) NOT NULL DEFAULT '0',
  `online` tinyint(1) NOT NULL DEFAULT '0',
  KEY `userid` (`userid`),
  KEY `truetime` (`truetime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsdownrecord` */

/*Table structure for table `phome_enewsdownurlqz` */

DROP TABLE IF EXISTS `phome_enewsdownurlqz`;

CREATE TABLE `phome_enewsdownurlqz` (
  `urlid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `urlname` varchar(30) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `downtype` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`urlid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsdownurlqz` */

/*Table structure for table `phome_enewserrorclass` */

DROP TABLE IF EXISTS `phome_enewserrorclass`;

CREATE TABLE `phome_enewserrorclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewserrorclass` */

/*Table structure for table `phome_enewsf` */

DROP TABLE IF EXISTS `phome_enewsf`;

CREATE TABLE `phome_enewsf` (
  `fid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f` varchar(30) NOT NULL DEFAULT '',
  `fname` varchar(30) NOT NULL DEFAULT '',
  `fform` varchar(20) NOT NULL DEFAULT '',
  `fhtml` mediumtext NOT NULL,
  `fzs` varchar(255) NOT NULL DEFAULT '',
  `isadd` tinyint(1) NOT NULL DEFAULT '0',
  `isshow` tinyint(1) NOT NULL DEFAULT '0',
  `iscj` tinyint(1) NOT NULL DEFAULT '0',
  `cjhtml` mediumtext NOT NULL,
  `myorder` smallint(6) NOT NULL DEFAULT '0',
  `ftype` varchar(30) NOT NULL DEFAULT '',
  `flen` varchar(20) NOT NULL DEFAULT '',
  `dotemp` tinyint(1) NOT NULL DEFAULT '0',
  `tid` smallint(6) NOT NULL DEFAULT '0',
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `savetxt` tinyint(1) NOT NULL DEFAULT '0',
  `fvalue` text NOT NULL,
  `iskey` tinyint(1) NOT NULL DEFAULT '0',
  `tobr` tinyint(1) NOT NULL DEFAULT '0',
  `dohtml` tinyint(1) NOT NULL DEFAULT '0',
  `qfhtml` mediumtext NOT NULL,
  `isonly` tinyint(1) NOT NULL DEFAULT '0',
  `linkfieldval` varchar(30) NOT NULL DEFAULT '',
  `samedata` tinyint(1) NOT NULL DEFAULT '0',
  `fformsize` varchar(12) NOT NULL DEFAULT '',
  `tbdataf` tinyint(1) NOT NULL DEFAULT '0',
  `ispage` tinyint(1) NOT NULL DEFAULT '0',
  `adddofun` varchar(255) NOT NULL DEFAULT '',
  `editdofun` varchar(255) NOT NULL DEFAULT '',
  `qadddofun` varchar(255) NOT NULL DEFAULT '',
  `qeditdofun` varchar(255) NOT NULL DEFAULT '',
  `linkfieldtb` varchar(60) NOT NULL DEFAULT '',
  `linkfieldshow` varchar(30) NOT NULL DEFAULT '',
  `editorys` tinyint(1) NOT NULL DEFAULT '0',
  `issmalltext` tinyint(1) NOT NULL DEFAULT '0',
  `fmvnum` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`fid`),
  KEY `tid` (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsf` */

insert  into `phome_enewsf`(`fid`,`f`,`fname`,`fform`,`fhtml`,`fzs`,`isadd`,`isshow`,`iscj`,`cjhtml`,`myorder`,`ftype`,`flen`,`dotemp`,`tid`,`tbname`,`savetxt`,`fvalue`,`iskey`,`tobr`,`dohtml`,`qfhtml`,`isonly`,`linkfieldval`,`samedata`,`fformsize`,`tbdataf`,`ispage`,`adddofun`,`editdofun`,`qadddofun`,`qeditdofun`,`linkfieldtb`,`linkfieldshow`,`editorys`,`issmalltext`,`fmvnum`) values (1,'title','标题','text','<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" bgcolor=\\\"#DBEAF5\\\">\r\n<tr> \r\n  <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\">\r\n	<?=$tts?\\\"<select name=\\\'ttid\\\'><option value=\\\'0\\\'>标题分类</option>$tts</select>\\\":\\\"\\\"?>\r\n	<input type=text name=title value=\\\"<?=ehtmlspecialchars(stripSlashes($r[title]))?>\\\" size=\\\"60\\\"> \r\n	<input type=\\\"button\\\" name=\\\"button\\\" value=\\\"图文\\\" onclick=\\\"document.add.title.value=document.add.title.value+\\\'(图文)\\\';\\\"> \r\n  </td>\r\n</tr>\r\n<tr> \r\n  <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\">属性: \r\n	<input name=\\\"titlefont[b]\\\" type=\\\"checkbox\\\" value=\\\"b\\\"<?=$titlefontb?>>粗体\r\n	<input name=\\\"titlefont[i]\\\" type=\\\"checkbox\\\" value=\\\"i\\\"<?=$titlefonti?>>斜体\r\n	<input name=\\\"titlefont[s]\\\" type=\\\"checkbox\\\" value=\\\"s\\\"<?=$titlefonts?>>删除线\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;颜色: <input name=\\\"titlecolor\\\" type=\\\"text\\\" value=\\\"<?=stripSlashes($r[titlecolor])?>\\\" size=\\\"10\\\"><a onclick=\\\"foreColor();\\\"><img src=\\\"../data/images/color.gif\\\" width=\\\"21\\\" height=\\\"21\\\" align=\\\"absbottom\\\"></a>\r\n  </td>\r\n</tr>\r\n</table>','标题',0,1,1,'  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--title--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_title]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_title]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_title]\" type=\"text\" id=\"add[z_title]\" value=\"<?=stripSlashes($r[z_title])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>',0,'CHAR','100',1,1,'news',0,'',0,0,0,'<input name=\\\"title\\\" type=\\\"text\\\" size=\\\"42\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":DoReqValue($mid,\\\'title\\\',stripSlashes($r[title]))?>\\\">',0,'',0,'60',0,0,'','','','','','',0,0,''),(2,'special.field','特殊属性','','<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" bgcolor=\\\"#DBEAF5\\\">\r\n  <tr>\r\n    <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\">信息属性: \r\n      <input name=\\\"checked\\\" type=\\\"checkbox\\\" value=\\\"1\\\"<?=$r[checked]?\\\' checked\\\':\\\'\\\'?>>\r\n      审核 &nbsp;&nbsp; 推荐 \r\n      <select name=\\\"isgood\\\" id=\\\"isgood\\\">\r\n        <option value=\\\"0\\\">不推荐</option>\r\n	<?=$ftnr[\\\'igname\\\']?>\r\n      </select>\r\n      &nbsp;&nbsp; 头条 \r\n      <select name=\\\"firsttitle\\\" id=\\\"firsttitle\\\">\r\n        <option value=\\\"0\\\">非头条</option>\r\n	<?=$ftnr[\\\'ftname\\\']?>\r\n      </select></td>\r\n  </tr>\r\n  <tr> \r\n    <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\">关键字&nbsp;&nbsp;&nbsp;: \r\n      <input name=\\\"keyboard\\\" type=\\\"text\\\" size=\\\"52\\\" value=\\\"<?=stripSlashes($r[keyboard])?>\\\">\r\n      <font color=\\\"#666666\\\">(多个请用&quot;,&quot;隔开)</font></td>\r\n  </tr>\r\n  <tr> \r\n    <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\">外部链接: \r\n      <input name=\\\"titleurl\\\" type=\\\"text\\\" value=\\\"<?=stripSlashes($r[titleurl])?>\\\" size=\\\"52\\\">\r\n      <font color=\\\"#666666\\\">(填写后信息连接地址将为此链接)</font></td>\r\n  </tr>\r\n</table>','特殊属性',0,1,0,'',1,'','',0,1,'news',0,'',0,0,0,'<input name=\\\"keyboard\\\" type=\\\"text\\\" size=42 value=\\\"<?=stripSlashes($r[keyboard])?>\\\">\r\n<font color=\\\"#666666\\\">(多个请用&quot;,&quot;隔开)</font>',0,'',0,'',0,0,'','','','','','',0,0,''),(3,'titlepic','标题图片','img','<input name=\\\"titlepic\\\" type=\\\"text\\\" id=\\\"titlepic\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":ehtmlspecialchars(stripSlashes($r[titlepic]))?>\\\" size=\\\"45\\\">\r\n<a onclick=\\\"window.open(\\\'ecmseditor/FileMain.php?type=1&classid=<?=$classid?>&infoid=<?=$id?>&filepass=<?=$filepass?>&sinfo=1&doing=1&field=titlepic\\\',\\\'\\\',\\\'width=700,height=550,scrollbars=yes\\\');\\\" title=\\\"选择已上传的图片\\\"><img src=\\\"../data/images/changeimg.gif\\\" border=\\\"0\\\" align=\\\"absbottom\\\"></a>','标题图片',0,1,1,'  <tr bgcolor=\\\"#FFFFFF\\\"> \r\n    <td height=\\\"22\\\" valign=\\\"top\\\"><strong>[!--enews.name--]正则：</strong><br>\r\n      ( \r\n      <input name=\\\"textfield\\\" type=\\\"text\\\" id=\\\"textfield\\\" value=\\\"[!--titlepic--]\\\" size=\\\"20\\\">\r\n      )</td>\r\n    <td><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\">\r\n    <tr>\r\n      <td>附件前缀 \r\n        <input name=\\\"add[qz_titlepic]\\\" type=\\\"text\\\" id=\\\"add[qz_titlepic]\\\" value=\\\"<?=stripSlashes($r[qz_titlepic])?>\\\"> \r\n        <input name=\\\"add[save_titlepic]\\\" type=\\\"checkbox\\\" id=\\\"add[save_titlepic]\\\" value=\\\" checked\\\"<?=$r[save_titlepic]?>>\r\n        远程保存 </td>\r\n    </tr>\r\n    <tr> \r\n      <td><textarea name=\\\"add[zz_titlepic]\\\" cols=\\\"60\\\" rows=\\\"10\\\" id=\\\"add[zz_titlepic]\\\"><?=ehtmlspecialchars(stripSlashes($r[zz_titlepic]))?></textarea></td>\r\n    </tr>\r\n    <tr> \r\n      <td><input name=\\\"add[z_titlepic]\\\" type=\\\"text\\\" id=\\\"titlepic5\\\" value=\\\"<?=stripSlashes($r[z_titlepic])?>\\\">\r\n        (如填写这里，这就是字段的值)</td>\r\n    </tr>\r\n  </table></td>\r\n  </tr>\r\n',2,'CHAR','120',1,1,'news',0,'',0,0,0,'<input type=\\\"file\\\" name=\\\"titlepicfile\\\" size=\\\"45\\\">',0,'',0,'60',0,0,'','','','','','',0,0,''),(4,'newstime','发布时间','text','<input name=\\\"newstime\\\" type=\\\"text\\\" value=\\\"<?=$r[newstime]?>\\\"><input type=button name=button value=\\\"设为当前时间\\\" onclick=\\\"document.add.newstime.value=\\\'<?=$todaytime?>\\\'\\\">','发布时间',0,1,1,'  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--newstime--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_newstime]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_newstime]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_newstime]\" type=\"text\" id=\"add[z_newstime]\" value=\"<?=stripSlashes($r[z_newstime])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>',1,'INT','11',1,1,'news',0,'',1,0,0,'',0,'',0,'',0,0,'','','','','','',0,0,''),(5,'ftitle','副标题','text','<input name=\\\"ftitle\\\" type=\\\"text\\\" size=60 id=\\\"ftitle\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":ehtmlspecialchars(stripSlashes($r[ftitle]))?>\\\">\r\n','副标题',1,1,1,'\r\n  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--ftitle--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_ftitle]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_ftitle]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_ftitle]\" type=\"text\" id=\"add[z_ftitle]\" value=\"<?=stripSlashes($r[z_ftitle])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n',0,'CHAR','120',1,1,'news',0,'',0,0,1,'<input name=\\\"ftitle\\\" type=\\\"text\\\" size=42 id=\\\"ftitle\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":DoReqValue($mid,\\\'ftitle\\\',stripSlashes($r[ftitle]))?>\\\">\r\n',0,'',0,'',0,0,'','','','','','',0,0,''),(6,'smalltext','内容简介','textarea','<textarea name=\\\"smalltext\\\" cols=\\\"80\\\" rows=\\\"10\\\" id=\\\"smalltext\\\"><?=$ecmsfirstpost==1?\\\"\\\":ehtmlspecialchars(stripSlashes($r[smalltext]))?></textarea>\r\n','内容简介',1,1,1,'\r\n  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--smalltext--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_smalltext]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_smalltext]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_smalltext]\" type=\"text\" id=\"add[z_smalltext]\" value=\"<?=stripSlashes($r[z_smalltext])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n',2,'CHAR','255',1,1,'news',0,'',0,1,1,'<textarea name=\\\"smalltext\\\" cols=\\\"60\\\" rows=\\\"10\\\" id=\\\"smalltext\\\"><?=$ecmsfirstpost==1?\\\"\\\":DoReqValue($mid,\\\'smalltext\\\',stripSlashes($r[smalltext]))?></textarea>\r\n',0,'',0,'',0,0,'','','','','','',0,1,''),(7,'writer','作者','text','<?php\r\n$writer_sql=$empire->query(\\\"select writer from {$dbtbpre}enewswriter\\\");\r\nwhile($w_r=$empire->fetch($writer_sql))\r\n{\r\n	$w_class.=\\\"<option value=\\\'\\\".$w_r[writer].\\\"\\\'>\\\".$w_r[writer].\\\"</option>\\\";\r\n}\r\n?>\r\n<input type=text name=writer value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":ehtmlspecialchars(stripSlashes($r[writer]))?>\\\" size=\\\"\\\"> \r\n        <select name=\\\"w_id\\\" id=\\\"select7\\\" onchange=\\\"document.add.writer.value=document.add.w_id.value\\\">\r\n          <option>选择作者</option>\r\n		  <?=$w_class?>\r\n        </select>\r\n<input type=\\\"button\\\" name=\\\"wbutton\\\" value=\\\"增加作者\\\" onclick=\\\"window.open(\\\'NewsSys/writer.php?addwritername=\\\'+document.add.writer.value);\\\">\r\n','作者',1,1,1,'\r\n  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--writer--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_writer]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_writer]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_writer]\" type=\"text\" id=\"add[z_writer]\" value=\"<?=stripSlashes($r[z_writer])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n',2,'CHAR','30',1,1,'news',0,'',0,0,1,'<input name=\\\"writer\\\" type=\\\"text\\\" id=\\\"writer\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":DoReqValue($mid,\\\'writer\\\',stripSlashes($r[writer]))?>\\\" size=\\\"\\\">\r\n',0,'',0,'',1,0,'','','','','','',0,0,''),(8,'befrom','信息来源','text','<?php\r\n$befrom_sql=$empire->query(\\\"select sitename from {$dbtbpre}enewsbefrom\\\");\r\nwhile($b_r=$empire->fetch($befrom_sql))\r\n{\r\n	$b_class.=\\\"<option value=\\\'\\\".$b_r[sitename].\\\"\\\'>\\\".$b_r[sitename].\\\"</option>\\\";\r\n}\r\n?>\r\n<input type=\\\"text\\\" name=\\\"befrom\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":ehtmlspecialchars(stripSlashes($r[befrom]))?>\\\" size=\\\"\\\"> \r\n        <select name=\\\"befrom_id\\\" id=\\\"befromid\\\" onchange=\\\"document.add.befrom.value=document.add.befrom_id.value\\\">\r\n          <option>选择信息来源</option>\r\n          <?=$b_class?>\r\n        </select>\r\n<input type=\\\"button\\\" name=\\\"wbutton\\\" value=\\\"增加来源\\\" onclick=\\\"window.open(\\\'NewsSys/BeFrom.php?addsitename=\\\'+document.add.befrom.value);\\\">\r\n','信息来源',1,1,1,'\r\n  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--befrom--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_befrom]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_befrom]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_befrom]\" type=\"text\" id=\"add[z_befrom]\" value=\"<?=stripSlashes($r[z_befrom])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n',2,'CHAR','60',1,1,'news',0,'',0,0,1,'<input name=\\\"befrom\\\" type=\\\"text\\\" id=\\\"befrom\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":DoReqValue($mid,\\\'befrom\\\',stripSlashes($r[befrom]))?>\\\" size=\\\"\\\">\r\n',0,'',0,'',1,0,'','','','','','',0,0,''),(9,'newstext','新闻正文','editor','<?=ECMS_ShowEditorVar(\\\"newstext\\\",$ecmsfirstpost==1?\\\"\\\":stripSlashes($r[newstext]),\\\"Default\\\",\\\"\\\",\\\"300\\\",\\\"100%\\\")?>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#DBEAF5\\\">\r\n          <tr> \r\n            <td bgcolor=\\\"#FFFFFF\\\"> <input name=\\\"dokey\\\" type=\\\"checkbox\\\" value=\\\"1\\\"<?=$r[dokey]==1?\\\' checked\\\':\\\'\\\'?>>\r\n              关键字替换&nbsp;&nbsp; <input name=\\\"copyimg\\\" type=\\\"checkbox\\\" id=\\\"copyimg\\\" value=\\\"1\\\">\r\n      远程保存图片(\r\n      <input name=\\\"mark\\\" type=\\\"checkbox\\\" id=\\\"mark\\\" value=\\\"1\\\">\r\n      <a href=\\\"SetEnews.php\\\" target=\\\"_blank\\\">加水印</a>)&nbsp;&nbsp; \r\n      <input name=\\\"copyflash\\\" type=\\\"checkbox\\\" id=\\\"copyflash\\\" value=\\\"1\\\">\r\n      远程保存FLASH(地址前缀： \r\n      <input name=\\\"qz_url\\\" type=\\\"text\\\" id=\\\"qz_url\\\" size=\\\"\\\">\r\n              )</td>\r\n          </tr>\r\n          <tr>\r\n            \r\n    <td bgcolor=\\\"#FFFFFF\\\"><input name=\\\"repimgnexturl\\\" type=\\\"checkbox\\\" id=\\\"repimgnexturl\\\" value=\\\"1\\\"> 图片链接转为下一页&nbsp;&nbsp; <input name=\\\"autopage\\\" type=\\\"checkbox\\\" id=\\\"autopage\\\" value=\\\"1\\\">自动分页\r\n      ,每 \r\n      <input name=\\\"autosize\\\" type=\\\"text\\\" id=\\\"autosize\\\" value=\\\"5000\\\" size=\\\"5\\\">\r\n      个字节为一页&nbsp;&nbsp; 取第 \r\n      <input name=\\\"getfirsttitlepic\\\" type=\\\"text\\\" id=\\\"getfirsttitlepic\\\" value=\\\"\\\" size=\\\"1\\\">\r\n      张上传图为标题图片( \r\n      <input name=\\\"getfirsttitlespic\\\" type=\\\"checkbox\\\" id=\\\"getfirsttitlespic\\\" value=\\\"1\\\">\r\n      缩略图: 宽 \r\n      <input name=\\\"getfirsttitlespicw\\\" type=\\\"text\\\" id=\\\"getfirsttitlespicw\\\" size=\\\"3\\\" value=\\\"<?=$public_r[spicwidth]?>\\\">\r\n      *高\r\n      <input name=\\\"getfirsttitlespich\\\" type=\\\"text\\\" id=\\\"getfirsttitlespich\\\" size=\\\"3\\\" value=\\\"<?=$public_r[spicheight]?>\\\">\r\n      )</td>\r\n          </tr>\r\n        </table>\r\n','新闻正文',1,1,1,'\r\n  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--newstext--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_newstext]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_newstext]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_newstext]\" type=\"text\" id=\"add[z_newstext]\" value=\"<?=stripSlashes($r[z_newstext])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n',2,'MEDIUMTEXT','',1,1,'news',0,'',0,1,1,'<?=ECMS_ShowEditorVar(\\\"newstext\\\",$ecmsfirstpost==1?\\\"\\\":DoReqValue($mid,\\\'newstext\\\',stripSlashes($r[newstext])),\\\"Default\\\",\\\"\\\",\\\"300\\\",\\\"100%\\\")?>\r\n',0,'',0,'',1,1,'','','','','','',0,0,''),(10,'diggtop','顶','text','\r\n<input name=\"diggtop\" type=\"text\" id=\"diggtop\" value=\"<?=$ecmsfirstpost==1?\"\":ehtmlspecialchars(stripSlashes($r[diggtop]))?>\" size=\"\">\r\n','',1,1,1,'\r\n  <tr bgcolor=\"#FFFFFF\"> \r\n    <td height=\"22\" valign=\"top\"><strong>[!--enews.name--]正则：</strong><br>\r\n      (<input name=\"textfield\" type=\"text\" id=\"textfield\" value=\"[!--diggtop--]\" size=\"20\">)</td>\r\n    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"3\">\r\n        <tr> \r\n          <td><textarea name=\"add[zz_diggtop]\" cols=\"60\" rows=\"10\" id=\"textarea\"><?=ehtmlspecialchars(stripSlashes($r[zz_diggtop]))?></textarea></td>\r\n        </tr>\r\n        <tr> \r\n          <td><input name=\"add[z_diggtop]\" type=\"text\" id=\"add[z_diggtop]\" value=\"<?=stripSlashes($r[z_diggtop])?>\">\r\n            (如填写这里，将为字段的值)</td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n',3,'INT','',1,1,'news',0,'',0,0,1,'\r\n<input name=\"diggtop\" type=\"text\" id=\"diggtop\" value=\"<?=$ecmsfirstpost==1?\"\":DoReqValue($mid,\'diggtop\',stripSlashes($r[diggtop]))?>\" size=\"\">\r\n',0,'',0,'',0,0,'','','','','','',0,0,'');

/*Table structure for table `phome_enewsfava` */

DROP TABLE IF EXISTS `phome_enewsfava`;

CREATE TABLE `phome_enewsfava` (
  `favaid` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL DEFAULT '0',
  `favatime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` int(11) NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `classid` smallint(6) NOT NULL DEFAULT '0',
  `cid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`favaid`),
  KEY `userid` (`userid`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfava` */

/*Table structure for table `phome_enewsfavaclass` */

DROP TABLE IF EXISTS `phome_enewsfavaclass`;

CREATE TABLE `phome_enewsfavaclass` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(30) NOT NULL DEFAULT '',
  `userid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfavaclass` */

/*Table structure for table `phome_enewsfeedback` */

DROP TABLE IF EXISTS `phome_enewsfeedback`;

CREATE TABLE `phome_enewsfeedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(120) NOT NULL DEFAULT '',
  `saytext` text NOT NULL,
  `name` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `mycall` varchar(30) NOT NULL DEFAULT '',
  `homepage` varchar(160) NOT NULL DEFAULT '',
  `company` varchar(80) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `saytime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `job` varchar(36) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `filepath` varchar(20) NOT NULL DEFAULT '',
  `filename` text NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `haveread` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `bid` (`bid`),
  KEY `haveread` (`haveread`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfeedback` */

/*Table structure for table `phome_enewsfeedbackclass` */

DROP TABLE IF EXISTS `phome_enewsfeedbackclass`;

CREATE TABLE `phome_enewsfeedbackclass` (
  `bid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `bname` varchar(60) NOT NULL DEFAULT '',
  `btemp` mediumtext NOT NULL,
  `bzs` varchar(255) NOT NULL DEFAULT '',
  `enter` text NOT NULL,
  `mustenter` text NOT NULL,
  `filef` varchar(255) NOT NULL DEFAULT '',
  `groupid` smallint(6) NOT NULL DEFAULT '0',
  `checkboxf` text NOT NULL,
  `usernames` text NOT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfeedbackclass` */

insert  into `phome_enewsfeedbackclass`(`bid`,`bname`,`btemp`,`bzs`,`enter`,`mustenter`,`filef`,`groupid`,`checkboxf`,`usernames`) values (1,'默认反馈分类','[!--cp.header--]\r\n<table width=100% align=center cellpadding=3 cellspacing=1 class=\\\"tableborder\\\">\r\n  <form name=\\\'feedback\\\' method=\\\'post\\\' enctype=\\\'multipart/form-data\\\' action=\\\'../../enews/index.php\\\'>\r\n    <input name=\\\'enews\\\' type=\\\'hidden\\\' value=\\\'AddFeedback\\\'>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">您的姓名:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'name\\\' type=\\\'text\\\' size=\\\'42\\\'>\r\n        (*)</td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">职务:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'job\\\' type=\\\'text\\\' size=\\\'42\\\'></td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">公司名称:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'company\\\' type=\\\'text\\\' size=\\\'42\\\'></td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">联系邮箱:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'email\\\' type=\\\'text\\\' size=\\\'42\\\'></td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">联系电话:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'mycall\\\' type=\\\'text\\\' size=\\\'42\\\'>\r\n        (*)</td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">网站:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'homepage\\\' type=\\\'text\\\' size=\\\'42\\\'></td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">联系地址:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'address\\\' type=\\\'text\\\' size=\\\"42\\\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">信息标题:</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input name=\\\'title\\\' type=\\\'text\\\' size=\\\"42\\\"> (*)</td>\r\n    </tr>\r\n    <tr> \r\n      <td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'><div align=\\\"right\\\">信息内容(*):</div></td>\r\n      <td bgcolor=\\\'ffffff\\\'><textarea name=\\\'saytext\\\' cols=\\\'60\\\' rows=\\\'12\\\'></textarea> \r\n      </td>\r\n    </tr>\r\n    <tr>\r\n      <td bgcolor=\\\'ffffff\\\'></td>\r\n      <td bgcolor=\\\'ffffff\\\'><input type=\\\'submit\\\' name=\\\'submit\\\' value=\\\'提交\\\'></td>\r\n    </tr>\r\n  </form>\r\n</table>\r\n[!--cp.footer--]','','您的姓名<!--field--->name<!--record-->职务<!--field--->job<!--record-->公司名称<!--field--->company<!--record-->联系邮箱<!--field--->email<!--record-->联系电话<!--field--->mycall<!--record-->网站<!--field--->homepage<!--record-->联系地址<!--field--->address<!--record-->信息标题<!--field--->title<!--record-->信息内容<!--field--->saytext<!--record-->',',name,mycall,title,saytext,',',',0,'','');

/*Table structure for table `phome_enewsfeedbackf` */

DROP TABLE IF EXISTS `phome_enewsfeedbackf`;

CREATE TABLE `phome_enewsfeedbackf` (
  `fid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f` varchar(30) NOT NULL DEFAULT '',
  `fname` varchar(30) NOT NULL DEFAULT '',
  `fform` varchar(20) NOT NULL DEFAULT '',
  `fzs` varchar(255) NOT NULL DEFAULT '',
  `myorder` smallint(6) NOT NULL DEFAULT '0',
  `ftype` varchar(30) NOT NULL DEFAULT '',
  `flen` varchar(20) NOT NULL DEFAULT '',
  `fformsize` varchar(12) NOT NULL DEFAULT '',
  `fvalue` text NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfeedbackf` */

insert  into `phome_enewsfeedbackf`(`fid`,`f`,`fname`,`fform`,`fzs`,`myorder`,`ftype`,`flen`,`fformsize`,`fvalue`) values (1,'title','标题','text','',7,'VARCHAR','120','',''),(2,'saytext','内容','textarea','',8,'TEXT','','',''),(3,'name','姓名','text','',0,'VARCHAR','30','',''),(4,'email','邮箱','text','',3,'VARCHAR','80','',''),(5,'mycall','电话','text','',4,'VARCHAR','30','',''),(6,'homepage','网站','text','',5,'VARCHAR','160','',''),(7,'company','公司名称','text','',2,'VARCHAR','80','',''),(8,'address','联系地址','text','',6,'VARCHAR','255','',''),(9,'job','职务','text','',1,'VARCHAR','36','','');

/*Table structure for table `phome_enewsfile_1` */

DROP TABLE IF EXISTS `phome_enewsfile_1`;

CREATE TABLE `phome_enewsfile_1` (
  `fileid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pubid` bigint(16) unsigned NOT NULL DEFAULT '0',
  `filename` char(60) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `path` char(20) NOT NULL DEFAULT '',
  `adduser` char(30) NOT NULL DEFAULT '',
  `filetime` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `no` char(60) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `onclick` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `cjid` int(10) unsigned NOT NULL DEFAULT '0',
  `fpath` tinyint(1) NOT NULL DEFAULT '0',
  `modtype` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileid`),
  KEY `id` (`id`),
  KEY `type` (`type`),
  KEY `classid` (`classid`),
  KEY `pubid` (`pubid`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfile_1` */

insert  into `phome_enewsfile_1`(`fileid`,`pubid`,`filename`,`filesize`,`path`,`adduser`,`filetime`,`classid`,`no`,`type`,`onclick`,`id`,`cjid`,`fpath`,`modtype`) values (6,1000010000000034,'05a39e45231e7b9d9b70da1554cbf9d8.jpg',368911,'2016-09-14','manage',1473788858,9,'001.jpg',1,0,34,0,0,0),(5,1000010000000034,'784dbe81acf324896be0843ff7c5dbb0.jpg',368911,'2016-09-14','manage',1473788835,9,'001.jpg',1,0,34,0,0,0),(63,1000010000000042,'b5ca78301030051eaf7271183c875794.jpg',301552,'2016-10-08','苏州湾',1475891973,54,'001.jpg',1,0,42,0,0,0),(7,1000010000000034,'9f7e9bbaf4f1e02bcdabb46837241c19.jpg',451981,'2016-09-14','manage',1473788858,9,'002.jpg',1,0,34,0,0,0),(8,1000010000000034,'bbc717aa4b24fd8bef728e32530f4ad3.jpg',368911,'2016-09-14','manage',1473789007,9,'001.jpg',1,0,34,0,0,0),(9,1000010000000034,'2b0d0ec9e015443f648b696ad453a32b.jpg',451981,'2016-09-14','manage',1473789007,9,'002.jpg',1,0,34,0,0,0),(10,1000010000000034,'c747455f457bc720b6f4812c7c786a47.jpg',467616,'2016-09-14','manage',1473789007,9,'003.jpg',1,0,34,0,0,0),(11,1000010000000034,'04f4e0e5ca4ba1a27fc6436f2e0acfc5.jpg',270089,'2016-09-14','manage',1473789007,9,'004.jpg',1,0,34,0,0,0),(12,1000010000000034,'214cb180d9b6f1446ce1ee2c73b8b6e8.jpg',279013,'2016-09-14','manage',1473789007,9,'005.jpg',1,0,34,0,0,0),(13,1000010000000034,'5890fce77307149539dd1ffb681d691d.jpg',321888,'2016-09-14','manage',1473789007,9,'006.jpg',1,0,34,0,0,0),(14,1000010000000034,'e7e58c2ef9bc06818f1aaeaef381cdc2.jpg',363051,'2016-09-14','manage',1473789007,9,'007.jpg',1,0,34,0,0,0),(15,1000010000000034,'929b3311f7b56bac496e119a22fa89ed.jpg',357282,'2016-09-14','manage',1473789007,9,'008.jpg',1,0,34,0,0,0),(16,1000010000000034,'c5d9cc94b7131e576b60d894e47fa69d.jpg',305858,'2016-09-14','manage',1473789007,9,'009.jpg',1,0,34,0,0,0),(17,1000010000000034,'90343954646ffa33b1dacd97c03e6d62.jpg',304438,'2016-09-14','manage',1473789007,9,'010.jpg',1,0,34,0,0,0),(18,1000010000000034,'56443aded981c4110dffdee9302238fa.jpg',294827,'2016-09-14','manage',1473789007,9,'011.jpg',1,0,34,0,0,0),(19,1000010000000034,'802596b4d08cba7f9edb6278b921a142.jpg',298334,'2016-09-14','manage',1473789007,9,'012.jpg',1,0,34,0,0,0),(20,1000010000000034,'8f6d50d082e3106b5b539b0c27ab4e7e.jpg',295486,'2016-09-14','manage',1473789007,9,'013.jpg',1,0,34,0,0,0),(21,1000010000000034,'7b057592005ea638a631c74fcac75971.jpg',342559,'2016-09-14','manage',1473789007,9,'014.jpg',1,0,34,0,0,0),(22,1000010000000034,'3317d311b5b2990415906df258e371f7.jpg',392164,'2016-09-14','manage',1473789007,9,'015.jpg',1,0,34,0,0,0),(23,1000010000000034,'279b0ff51542c6eb90360e24a8323ed0.jpg',8571,'2016-09-14','manage',1473789571,9,'[CropImg]007.jpg',1,0,34,0,0,0),(24,1000010000000034,'693bee5d4ef2e6b48618e4b0ddca89a5.jpg',28274,'2016-09-14','manage',1473789681,9,'000.jpg',1,0,34,0,0,0),(25,1000010000000035,'c815a429e383a8f8fd4795c0bcf68b86.jpg',254719,'2016-09-14','manage',1473790024,9,'001.jpg',1,0,35,0,0,0),(26,1000010000000035,'ac84af6d6eb834eb93476e2a035dfea6.jpg',263148,'2016-09-14','manage',1473790024,9,'002.jpg',1,0,35,0,0,0),(27,1000010000000035,'4a5a8828b46b7452f5bcd77a5b4bb04c.jpg',238601,'2016-09-14','manage',1473790024,9,'003.jpg',1,0,35,0,0,0),(28,1000010000000035,'640de84c738875af8fedf47f3c06b556.jpg',254386,'2016-09-14','manage',1473790024,9,'004.jpg',1,0,35,0,0,0),(29,1000010000000035,'1b0eb07a89012c644c23fd9529790708.jpg',240628,'2016-09-14','manage',1473790024,9,'005.jpg',1,0,35,0,0,0),(30,1000010000000035,'516c152612a72d253fba91bb9d21f4b8.jpg',156440,'2016-09-14','manage',1473790024,9,'006.jpg',1,0,35,0,0,0),(31,1000010000000035,'59ce417733867756beeab90d376124d9.jpg',245956,'2016-09-14','manage',1473790024,9,'007.jpg',1,0,35,0,0,0),(32,1000010000000035,'2069411c1bcde8c9c0b492ede5041c3e.jpg',226650,'2016-09-14','manage',1473790024,9,'008.jpg',1,0,35,0,0,0),(33,1000010000000035,'f95e17da0f80f2bcdaa9d3e4431a494b.jpg',228823,'2016-09-14','manage',1473790024,9,'009.jpg',1,0,35,0,0,0),(34,1000010000000035,'58d5434c324f719270c8f46c911bfdaa.jpg',230766,'2016-09-14','manage',1473790024,9,'010.jpg',1,0,35,0,0,0),(35,1000010000000035,'893c23a7b07bbe55432a5594fccf3e8e.jpg',207842,'2016-09-14','manage',1473790024,9,'012.jpg',1,0,35,0,0,0),(36,1000010000000035,'42bf061e24443b03bf569cf47a57380f.jpg',262210,'2016-09-14','manage',1473790024,9,'013.jpg',1,0,35,0,0,0),(37,1000010000000035,'0a2141b75ee243ac1b869d47396959b5.jpg',209605,'2016-09-14','manage',1473790024,9,'014.jpg',1,0,35,0,0,0),(38,1000010000000035,'139d0894326fc3a2ae40880db62ba6e0.jpg',275008,'2016-09-14','manage',1473790024,9,'015.jpg',1,0,35,0,0,0),(39,1000010000000035,'607d532fb9392a0acd51fb727ee8ac05.jpg',260261,'2016-09-14','manage',1473790024,9,'016.jpg',1,0,35,0,0,0),(40,1000010000000035,'8b15afc8dc0c9cf4e25d91a3a7a198d7.jpg',259512,'2016-09-14','manage',1473790024,9,'017.jpg',1,0,35,0,0,0),(41,1000010000000035,'8668927b4a702a7df259f59857dba2cd.jpg',235602,'2016-09-14','manage',1473790024,9,'018.jpg',1,0,35,0,0,0),(42,1000010000000035,'6d51673a7e5ae22b33e6c121d240d3a3.jpg',37006,'2016-09-14','manage',1473790024,9,'000.jpg',1,0,35,0,0,0),(45,1000010000000001,'d06b7f8ba6da6424cfa67fca6e85b60c.jpg',575715,'2016-09-14','manage',1473823401,2,'xiaoguo1220.jpg',1,0,1,0,0,0),(46,1000010000000003,'f505deb6c969ed012576c491cbcc8dd8.jpg',103115,'2016-09-14','manage',1473825139,4,'ZL_001.jpg',1,0,3,0,0,0),(47,1000010000000003,'ad4f7b746ee346e7425b08b909c93df0.jpg',399485,'2016-09-14','manage',1473825139,4,'ZL_002.jpg',1,0,3,0,0,0),(48,1000010000000003,'119b02753d7837b048d50b5451f51fcb.jpg',238441,'2016-09-14','manage',1473825139,4,'ZL_003.jpg',1,0,3,0,0,0),(49,1000010000000003,'3f12d9e90680c1ea585871ccdcbd10a1.jpg',500419,'2016-09-14','manage',1473825139,4,'ZL_004.jpg',1,0,3,0,0,0),(50,1000010000000003,'caa2cadc9687640f3869898794f4fcc1.jpg',794625,'2016-09-14','manage',1473825139,4,'ZL_005.jpg',1,0,3,0,0,0),(51,1000010000000003,'a87dc260796cc86c4b20a4ee85ed7333.jpg',706437,'2016-09-14','manage',1473825139,4,'ZL_006.jpg',1,0,3,0,0,0),(52,1000010000000003,'467e57094c885a0fb297ec1058faab9f.jpg',713345,'2016-09-14','manage',1473825139,4,'ZL_007.jpg',1,0,3,0,0,0),(53,1000010000000002,'1591537029002c13f2f5fa030c5f297b.jpg',111000,'2016-09-27','manage',1474964353,3,'校长寄语.jpg',1,0,2,0,0,0),(54,1000010000000041,'10edea159fd32aa74c11767dbe9347b8.jpg',301552,'2016-10-01','manage',1475253789,9,'001.jpg',1,0,41,0,0,0),(55,1000010000000041,'c68e40be4480de95f8020c080be8aa88.jpg',143416,'2016-10-01','manage',1475253789,9,'002.jpg',1,0,41,0,0,0),(56,1000010000000041,'669bab8ade3df8fc2a5d33a2824d6f08.jpg',217243,'2016-10-01','manage',1475253789,9,'003.jpg',1,0,41,0,0,0),(57,1000010000000041,'7c8aa96995d690149aee25401333a1e9.jpg',219777,'2016-10-01','manage',1475253789,9,'004.jpg',1,0,41,0,0,0),(58,1000010000000041,'050d9652e77441bb6aabbff44d411a19.jpg',175939,'2016-10-01','manage',1475253789,9,'005.jpg',1,0,41,0,0,0),(59,1000010000000041,'e4a20058594c31d7ddeb6cd6543ce0e4.jpg',220177,'2016-10-01','manage',1475253789,9,'006.jpg',1,0,41,0,0,0),(60,1000010000000041,'7665ca18d6732041e3393442f3408ee7.jpg',303519,'2016-10-01','manage',1475253789,9,'007.jpg',1,0,41,0,0,0),(61,1000010000000041,'bcb9a7f8c503c348af82ceea5a2fc01a.jpg',2675,'2016-10-01','manage',1475254320,9,'[CropImg]001.jpg',1,0,41,0,0,0),(62,1000010000000041,'456347dee59c33ef0814e342b5d4acb7.jpg',41093,'2016-10-01','manage',1475254382,9,'0001.jpg',1,0,41,0,0,0),(64,1000010000000042,'c3ac1e931357a60b5cd580f623ce61fd.jpg',143416,'2016-10-08','苏州湾',1475891973,54,'002.jpg',1,0,42,0,0,0),(65,1000010000000042,'f35a16fe2e46e2c6bbe68fcf7cfa674f.jpg',217243,'2016-10-08','苏州湾',1475891973,54,'003.jpg',1,0,42,0,0,0),(66,1000010000000042,'042ae70e0a812083b6cea2342255cee0.jpg',41093,'2016-10-08','苏州湾',1475892577,54,'2017.jpg',1,0,42,0,0,0),(67,1000010000000043,'b6a259f66a677948b08aa6839eebf5eb.jpg',147629,'2016-10-08','爱德',1475914208,35,'4.jpg',1,0,43,0,0,0),(68,1000010000000044,'9fb186e3aedfc603991d0ce0ca7183a6.jpg',295744,'2016-10-11','城中',1476147752,16,'000.JPG',1,0,44,0,0,0),(69,1000010000000044,'f976f88bac648e816910a92b29700837.jpg',285730,'2016-10-11','城中',1476147752,16,'001.JPG',1,0,44,0,0,0),(70,1000010000000044,'ab59e31db5a7e3c5f2ac3b82f49f9f1e.jpg',325345,'2016-10-11','城中',1476147752,16,'002.JPG',1,0,44,0,0,0),(71,1000010000000044,'b1b38e840df2d9ed9641de990345522d.jpg',261189,'2016-10-11','城中',1476147752,16,'003.JPG',1,0,44,0,0,0),(72,1000010000000044,'b8ac0f1846c5edcde88fe02b640291b8.jpg',348578,'2016-10-11','城中',1476147752,16,'004.JPG',1,0,44,0,0,0),(73,1000010000000044,'1b9332bc7b936e5a3a0f056f301afc50.jpg',346628,'2016-10-11','城中',1476147752,16,'005.JPG',1,0,44,0,0,0),(74,1000010000000044,'7794076b8544cb6c0b43fffd4026ec3e.jpg',448595,'2016-10-11','城中',1476147752,16,'006.JPG',1,0,44,0,0,0),(75,1000010000000044,'f412b295f7afff71e063beac04084a66.jpg',446784,'2016-10-11','城中',1476147752,16,'007.JPG',1,0,44,0,0,0),(76,1000010000000044,'7648c0a93ba4b8886580872745f0baac.jpg',385930,'2016-10-11','城中',1476147822,16,'008.JPG',1,0,44,0,0,0),(77,1000010000000044,'e7b7d666d6b4b4af005edbe8ee831312.jpg',397302,'2016-10-11','城中',1476147822,16,'009.JPG',1,0,44,0,0,0),(78,1000010000000044,'60a05ad25a1b4da0ab709781c760ad24.jpg',500602,'2016-10-11','城中',1476147822,16,'010.JPG',1,0,44,0,0,0),(79,1000010000000044,'ab23ff20cd3b9ce74db99e9a8e0739ce.jpg',298527,'2016-10-11','城中',1476147822,16,'011.JPG',1,0,44,0,0,0),(80,1000010000000044,'9637c452650ad4fb29edb515d9c15ad8.jpg',400939,'2016-10-11','城中',1476147822,16,'012.JPG',1,0,44,0,0,0),(81,1000010000000044,'3f8405610a96db396a49f4cafb22fd83.jpg',635836,'2016-10-11','城中',1476147822,16,'013.JPG',1,0,44,0,0,0),(82,1000010000000044,'c633b058a515e05d22c7692252814316.jpg',415151,'2016-10-11','城中',1476147822,16,'014.JPG',1,0,44,0,0,0),(83,1000010000000045,'673da3f4b5937722d74f30b4bc7b29e5.jpg',1471382,'2016-10-11','城中',1476169878,16,'IMG_5202.JPG',1,0,45,0,0,0),(84,1000010000000045,'4eba747e15d3deb88f5e28232b22a86b.jpg',1730941,'2016-10-11','城中',1476169878,16,'IMG_5203.JPG',1,0,45,0,0,0);

/*Table structure for table `phome_enewsfile_member` */

DROP TABLE IF EXISTS `phome_enewsfile_member`;

CREATE TABLE `phome_enewsfile_member` (
  `fileid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pubid` tinyint(1) NOT NULL DEFAULT '0',
  `filename` char(60) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `path` char(20) NOT NULL DEFAULT '',
  `adduser` char(30) NOT NULL DEFAULT '',
  `filetime` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` tinyint(1) NOT NULL DEFAULT '0',
  `no` char(60) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `onclick` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `cjid` int(10) unsigned NOT NULL DEFAULT '0',
  `fpath` tinyint(1) NOT NULL DEFAULT '0',
  `modtype` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileid`),
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfile_member` */

/*Table structure for table `phome_enewsfile_other` */

DROP TABLE IF EXISTS `phome_enewsfile_other`;

CREATE TABLE `phome_enewsfile_other` (
  `fileid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pubid` tinyint(1) NOT NULL DEFAULT '0',
  `filename` char(60) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `path` char(20) NOT NULL DEFAULT '',
  `adduser` char(30) NOT NULL DEFAULT '',
  `filetime` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` tinyint(1) NOT NULL DEFAULT '0',
  `no` char(60) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `onclick` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `cjid` int(10) unsigned NOT NULL DEFAULT '0',
  `fpath` tinyint(1) NOT NULL DEFAULT '0',
  `modtype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileid`),
  KEY `id` (`id`),
  KEY `type` (`type`),
  KEY `modtype` (`modtype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfile_other` */

/*Table structure for table `phome_enewsfile_public` */

DROP TABLE IF EXISTS `phome_enewsfile_public`;

CREATE TABLE `phome_enewsfile_public` (
  `fileid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pubid` tinyint(1) NOT NULL DEFAULT '0',
  `filename` char(60) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `path` char(20) NOT NULL DEFAULT '',
  `adduser` char(30) NOT NULL DEFAULT '',
  `filetime` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` tinyint(1) NOT NULL DEFAULT '0',
  `no` char(60) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `onclick` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `cjid` int(10) unsigned NOT NULL DEFAULT '0',
  `fpath` tinyint(1) NOT NULL DEFAULT '0',
  `modtype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileid`),
  KEY `id` (`id`),
  KEY `type` (`type`),
  KEY `modtype` (`modtype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsfile_public` */

/*Table structure for table `phome_enewsgbook` */

DROP TABLE IF EXISTS `phome_enewsgbook`;

CREATE TABLE `phome_enewsgbook` (
  `lyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `mycall` varchar(30) NOT NULL DEFAULT '',
  `lytime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lytext` text NOT NULL,
  `retext` text NOT NULL,
  `bid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`lyid`),
  KEY `bid` (`bid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsgbook` */

/*Table structure for table `phome_enewsgbookclass` */

DROP TABLE IF EXISTS `phome_enewsgbookclass`;

CREATE TABLE `phome_enewsgbookclass` (
  `bid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `bname` varchar(60) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsgbookclass` */

insert  into `phome_enewsgbookclass`(`bid`,`bname`,`checked`,`groupid`) values (1,'默认留言分类',0,0);

/*Table structure for table `phome_enewsgfenip` */

DROP TABLE IF EXISTS `phome_enewsgfenip`;

CREATE TABLE `phome_enewsgfenip` (
  `ip` varchar(20) NOT NULL DEFAULT '',
  `addtime` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsgfenip` */

/*Table structure for table `phome_enewsgroup` */

DROP TABLE IF EXISTS `phome_enewsgroup`;

CREATE TABLE `phome_enewsgroup` (
  `groupid` smallint(6) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(50) NOT NULL DEFAULT '',
  `dopublic` tinyint(1) NOT NULL DEFAULT '0',
  `doclass` tinyint(1) NOT NULL DEFAULT '0',
  `dotemplate` tinyint(1) NOT NULL DEFAULT '0',
  `dopicnews` tinyint(1) NOT NULL DEFAULT '0',
  `dofile` tinyint(1) NOT NULL DEFAULT '0',
  `douser` tinyint(1) NOT NULL DEFAULT '0',
  `dolog` tinyint(1) NOT NULL DEFAULT '0',
  `domember` tinyint(1) NOT NULL DEFAULT '0',
  `dobefrom` tinyint(1) NOT NULL DEFAULT '0',
  `doword` tinyint(1) NOT NULL DEFAULT '0',
  `dokey` tinyint(1) NOT NULL DEFAULT '0',
  `doad` tinyint(1) NOT NULL DEFAULT '0',
  `dovote` tinyint(1) NOT NULL DEFAULT '0',
  `dogroup` tinyint(1) NOT NULL DEFAULT '0',
  `doall` tinyint(1) NOT NULL DEFAULT '0',
  `docj` tinyint(1) NOT NULL DEFAULT '0',
  `dobq` tinyint(1) NOT NULL DEFAULT '0',
  `domovenews` tinyint(1) NOT NULL DEFAULT '0',
  `dopostdata` tinyint(1) NOT NULL DEFAULT '0',
  `dochangedata` tinyint(1) NOT NULL DEFAULT '0',
  `dopl` tinyint(1) NOT NULL DEFAULT '0',
  `dof` tinyint(1) NOT NULL DEFAULT '0',
  `dom` tinyint(1) NOT NULL DEFAULT '0',
  `dodo` tinyint(1) NOT NULL DEFAULT '0',
  `dodbdata` tinyint(1) NOT NULL DEFAULT '0',
  `dorepnewstext` tinyint(1) NOT NULL DEFAULT '0',
  `dotempvar` tinyint(1) NOT NULL DEFAULT '0',
  `dostats` tinyint(1) NOT NULL DEFAULT '0',
  `dowriter` tinyint(1) NOT NULL DEFAULT '0',
  `dototaldata` tinyint(1) NOT NULL DEFAULT '0',
  `dosearchkey` tinyint(1) NOT NULL DEFAULT '0',
  `dozt` tinyint(1) NOT NULL DEFAULT '0',
  `docard` tinyint(1) NOT NULL DEFAULT '0',
  `dolink` tinyint(1) NOT NULL DEFAULT '0',
  `doselfinfo` tinyint(1) NOT NULL DEFAULT '0',
  `doexecsql` tinyint(1) NOT NULL DEFAULT '0',
  `dotable` tinyint(1) NOT NULL DEFAULT '0',
  `dodownurl` tinyint(1) NOT NULL DEFAULT '0',
  `dodeldownrecord` tinyint(1) NOT NULL DEFAULT '0',
  `doshoppayfs` tinyint(1) NOT NULL DEFAULT '0',
  `doshopps` tinyint(1) NOT NULL DEFAULT '0',
  `doshopdd` tinyint(1) NOT NULL DEFAULT '0',
  `dogbook` tinyint(1) NOT NULL DEFAULT '0',
  `dofeedback` tinyint(1) NOT NULL DEFAULT '0',
  `douserpage` tinyint(1) NOT NULL DEFAULT '0',
  `donotcj` tinyint(1) NOT NULL DEFAULT '0',
  `dodownerror` tinyint(1) NOT NULL DEFAULT '0',
  `dodelinfodata` tinyint(1) NOT NULL DEFAULT '0',
  `doaddinfo` tinyint(1) NOT NULL DEFAULT '0',
  `doeditinfo` tinyint(1) NOT NULL DEFAULT '0',
  `dodelinfo` tinyint(1) NOT NULL DEFAULT '0',
  `doadminstyle` tinyint(1) NOT NULL DEFAULT '0',
  `dorepdownpath` tinyint(1) NOT NULL DEFAULT '0',
  `douserjs` tinyint(1) NOT NULL DEFAULT '0',
  `douserlist` tinyint(1) NOT NULL DEFAULT '0',
  `domsg` tinyint(1) NOT NULL DEFAULT '0',
  `dosendemail` tinyint(1) NOT NULL DEFAULT '0',
  `dosetmclass` tinyint(1) NOT NULL DEFAULT '0',
  `doinfodoc` tinyint(1) NOT NULL DEFAULT '0',
  `dotempgroup` tinyint(1) NOT NULL DEFAULT '0',
  `dofeedbackf` tinyint(1) NOT NULL DEFAULT '0',
  `dotask` tinyint(1) NOT NULL DEFAULT '0',
  `domemberf` tinyint(1) NOT NULL DEFAULT '0',
  `dospacestyle` tinyint(1) NOT NULL DEFAULT '0',
  `dospacedata` tinyint(1) NOT NULL DEFAULT '0',
  `dovotemod` tinyint(1) NOT NULL DEFAULT '0',
  `doplayer` tinyint(1) NOT NULL DEFAULT '0',
  `dowap` tinyint(1) NOT NULL DEFAULT '0',
  `dopay` tinyint(1) NOT NULL DEFAULT '0',
  `dobuygroup` tinyint(1) NOT NULL DEFAULT '0',
  `dosearchall` tinyint(1) NOT NULL DEFAULT '0',
  `doinfotype` tinyint(1) NOT NULL DEFAULT '0',
  `doplf` tinyint(1) NOT NULL DEFAULT '0',
  `dopltable` tinyint(1) NOT NULL DEFAULT '0',
  `dochadminstyle` tinyint(1) NOT NULL DEFAULT '0',
  `dotags` tinyint(1) NOT NULL DEFAULT '0',
  `dosp` tinyint(1) NOT NULL DEFAULT '0',
  `doyh` tinyint(1) NOT NULL DEFAULT '0',
  `dofirewall` tinyint(1) NOT NULL DEFAULT '0',
  `dosetsafe` tinyint(1) NOT NULL DEFAULT '0',
  `douserclass` tinyint(1) NOT NULL DEFAULT '0',
  `doworkflow` tinyint(1) NOT NULL DEFAULT '0',
  `domenu` tinyint(1) NOT NULL DEFAULT '0',
  `dopubvar` tinyint(1) NOT NULL DEFAULT '0',
  `doclassf` tinyint(1) NOT NULL DEFAULT '0',
  `doztf` tinyint(1) NOT NULL DEFAULT '0',
  `dofiletable` tinyint(1) NOT NULL DEFAULT '0',
  `docheckinfo` tinyint(1) NOT NULL DEFAULT '0',
  `dogoodinfo` tinyint(1) NOT NULL DEFAULT '0',
  `dodocinfo` tinyint(1) NOT NULL DEFAULT '0',
  `domoveinfo` tinyint(1) NOT NULL DEFAULT '0',
  `dodttemp` tinyint(1) NOT NULL DEFAULT '0',
  `doloadcj` tinyint(1) NOT NULL DEFAULT '0',
  `domustcheck` tinyint(1) NOT NULL DEFAULT '0',
  `docheckedit` tinyint(1) NOT NULL DEFAULT '0',
  `domemberconnect` tinyint(1) NOT NULL DEFAULT '0',
  `doprecode` tinyint(1) NOT NULL DEFAULT '0',
  `ofcampus` tinyint(1) DEFAULT '0' COMMENT '所属校区',
  PRIMARY KEY (`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsgroup` */

insert  into `phome_enewsgroup`(`groupid`,`groupname`,`dopublic`,`doclass`,`dotemplate`,`dopicnews`,`dofile`,`douser`,`dolog`,`domember`,`dobefrom`,`doword`,`dokey`,`doad`,`dovote`,`dogroup`,`doall`,`docj`,`dobq`,`domovenews`,`dopostdata`,`dochangedata`,`dopl`,`dof`,`dom`,`dodo`,`dodbdata`,`dorepnewstext`,`dotempvar`,`dostats`,`dowriter`,`dototaldata`,`dosearchkey`,`dozt`,`docard`,`dolink`,`doselfinfo`,`doexecsql`,`dotable`,`dodownurl`,`dodeldownrecord`,`doshoppayfs`,`doshopps`,`doshopdd`,`dogbook`,`dofeedback`,`douserpage`,`donotcj`,`dodownerror`,`dodelinfodata`,`doaddinfo`,`doeditinfo`,`dodelinfo`,`doadminstyle`,`dorepdownpath`,`douserjs`,`douserlist`,`domsg`,`dosendemail`,`dosetmclass`,`doinfodoc`,`dotempgroup`,`dofeedbackf`,`dotask`,`domemberf`,`dospacestyle`,`dospacedata`,`dovotemod`,`doplayer`,`dowap`,`dopay`,`dobuygroup`,`dosearchall`,`doinfotype`,`doplf`,`dopltable`,`dochadminstyle`,`dotags`,`dosp`,`doyh`,`dofirewall`,`dosetsafe`,`douserclass`,`doworkflow`,`domenu`,`dopubvar`,`doclassf`,`doztf`,`dofiletable`,`docheckinfo`,`dogoodinfo`,`dodocinfo`,`domoveinfo`,`dodttemp`,`doloadcj`,`domustcheck`,`docheckedit`,`domemberconnect`,`doprecode`,`ofcampus`) values (1,'超级管理员',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,1,0),(2,'校区管理员',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,1,1,0,0,1);

/*Table structure for table `phome_enewshmsg` */

DROP TABLE IF EXISTS `phome_enewshmsg`;

CREATE TABLE `phome_enewshmsg` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL DEFAULT '',
  `msgtext` text NOT NULL,
  `haveread` tinyint(1) NOT NULL DEFAULT '0',
  `msgtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to_username` varchar(30) NOT NULL DEFAULT '',
  `from_userid` int(10) unsigned NOT NULL DEFAULT '0',
  `from_username` varchar(30) NOT NULL DEFAULT '',
  `isadmin` tinyint(1) NOT NULL DEFAULT '0',
  `issys` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`),
  KEY `to_username` (`to_username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewshmsg` */

/*Table structure for table `phome_enewshnotice` */

DROP TABLE IF EXISTS `phome_enewshnotice`;

CREATE TABLE `phome_enewshnotice` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL DEFAULT '',
  `msgtext` text NOT NULL,
  `haveread` tinyint(1) NOT NULL DEFAULT '0',
  `msgtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to_username` varchar(30) NOT NULL DEFAULT '',
  `from_userid` int(10) unsigned NOT NULL DEFAULT '0',
  `from_username` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`mid`),
  KEY `to_username` (`to_username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewshnotice` */

/*Table structure for table `phome_enewshy` */

DROP TABLE IF EXISTS `phome_enewshy`;

CREATE TABLE `phome_enewshy` (
  `fid` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(30) NOT NULL DEFAULT '',
  `cid` int(11) NOT NULL DEFAULT '0',
  `fsay` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`fid`),
  KEY `userid` (`userid`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewshy` */

/*Table structure for table `phome_enewshyclass` */

DROP TABLE IF EXISTS `phome_enewshyclass`;

CREATE TABLE `phome_enewshyclass` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(30) NOT NULL DEFAULT '',
  `userid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewshyclass` */

/*Table structure for table `phome_enewsindexpage` */

DROP TABLE IF EXISTS `phome_enewsindexpage`;

CREATE TABLE `phome_enewsindexpage` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(30) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  PRIMARY KEY (`tempid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsindexpage` */

/*Table structure for table `phome_enewsinfoclass` */

DROP TABLE IF EXISTS `phome_enewsinfoclass`;

CREATE TABLE `phome_enewsinfoclass` (
  `classid` int(11) NOT NULL AUTO_INCREMENT,
  `bclassid` int(11) NOT NULL DEFAULT '0',
  `classname` varchar(100) NOT NULL DEFAULT '',
  `infourl` mediumtext NOT NULL,
  `newsclassid` smallint(6) NOT NULL DEFAULT '0',
  `startday` date NOT NULL DEFAULT '0000-00-00',
  `endday` date NOT NULL DEFAULT '0000-00-00',
  `bz` text NOT NULL,
  `num` smallint(6) NOT NULL DEFAULT '0',
  `copyimg` tinyint(1) NOT NULL DEFAULT '0',
  `renum` smallint(6) NOT NULL DEFAULT '0',
  `keyboard` text NOT NULL,
  `oldword` text NOT NULL,
  `newword` text NOT NULL,
  `titlelen` smallint(6) NOT NULL DEFAULT '0',
  `retitlewriter` tinyint(1) NOT NULL DEFAULT '0',
  `smalltextlen` smallint(6) NOT NULL DEFAULT '0',
  `zz_smallurl` text NOT NULL,
  `zz_newsurl` text NOT NULL,
  `httpurl` varchar(255) NOT NULL DEFAULT '',
  `repad` text NOT NULL,
  `imgurl` varchar(255) NOT NULL DEFAULT '',
  `relistnum` smallint(6) NOT NULL DEFAULT '0',
  `zz_titlepicl` text NOT NULL,
  `z_titlepicl` varchar(255) NOT NULL DEFAULT '',
  `qz_titlepicl` varchar(255) NOT NULL DEFAULT '',
  `save_titlepicl` varchar(10) NOT NULL DEFAULT '',
  `keynum` tinyint(4) NOT NULL DEFAULT '0',
  `insertnum` smallint(6) NOT NULL DEFAULT '0',
  `copyflash` tinyint(1) NOT NULL DEFAULT '0',
  `tid` smallint(6) NOT NULL DEFAULT '0',
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `pagetype` tinyint(1) NOT NULL DEFAULT '0',
  `smallpagezz` text NOT NULL,
  `pagezz` text NOT NULL,
  `smallpageallzz` text NOT NULL,
  `pageallzz` text NOT NULL,
  `mark` tinyint(1) NOT NULL DEFAULT '0',
  `enpagecode` tinyint(1) NOT NULL DEFAULT '0',
  `recjtheurl` tinyint(1) NOT NULL DEFAULT '0',
  `hiddenload` tinyint(1) NOT NULL DEFAULT '0',
  `justloadin` tinyint(1) NOT NULL DEFAULT '0',
  `justloadcheck` tinyint(1) NOT NULL DEFAULT '0',
  `delloadinfo` tinyint(1) NOT NULL DEFAULT '0',
  `pagerepad` mediumtext NOT NULL,
  `newsztid` text NOT NULL,
  `getfirstpic` tinyint(4) NOT NULL DEFAULT '0',
  `oldpagerep` text NOT NULL,
  `newpagerep` text NOT NULL,
  `keeptime` smallint(6) NOT NULL DEFAULT '0',
  `lasttime` int(11) NOT NULL DEFAULT '0',
  `newstextisnull` tinyint(1) NOT NULL DEFAULT '0',
  `getfirstspic` tinyint(1) NOT NULL DEFAULT '0',
  `getfirstspicw` smallint(6) NOT NULL DEFAULT '0',
  `getfirstspich` smallint(6) NOT NULL DEFAULT '0',
  `doaddtextpage` tinyint(1) NOT NULL DEFAULT '0',
  `infourlispage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classid`),
  KEY `newsclassid` (`newsclassid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsinfoclass` */

/*Table structure for table `phome_enewsinfotype` */

DROP TABLE IF EXISTS `phome_enewsinfotype`;

CREATE TABLE `phome_enewsinfotype` (
  `typeid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tname` varchar(30) NOT NULL DEFAULT '',
  `mid` smallint(6) NOT NULL DEFAULT '0',
  `myorder` smallint(6) NOT NULL DEFAULT '0',
  `yhid` smallint(6) NOT NULL DEFAULT '0',
  `tnum` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `listtempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tpath` varchar(100) NOT NULL DEFAULT '',
  `ttype` varchar(10) NOT NULL DEFAULT '',
  `maxnum` int(10) unsigned NOT NULL DEFAULT '0',
  `reorder` varchar(50) NOT NULL DEFAULT '',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `timg` varchar(200) NOT NULL DEFAULT '',
  `intro` varchar(255) NOT NULL DEFAULT '',
  `pagekey` varchar(255) NOT NULL DEFAULT '',
  `newline` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hotline` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `goodline` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hotplline` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `firstline` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `jstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `nrejs` tinyint(1) NOT NULL DEFAULT '0',
  `listdt` tinyint(1) NOT NULL DEFAULT '0',
  `repagenum` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`typeid`),
  KEY `mid` (`mid`),
  KEY `myorder` (`myorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsinfotype` */

/*Table structure for table `phome_enewsinfovote` */

DROP TABLE IF EXISTS `phome_enewsinfovote`;

CREATE TABLE `phome_enewsinfovote` (
  `pubid` bigint(16) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(120) NOT NULL DEFAULT '',
  `votenum` int(10) unsigned NOT NULL DEFAULT '0',
  `voteip` mediumtext NOT NULL,
  `votetext` text NOT NULL,
  `voteclass` tinyint(1) NOT NULL DEFAULT '0',
  `doip` tinyint(1) NOT NULL DEFAULT '0',
  `dotime` date NOT NULL DEFAULT '0000-00-00',
  `tempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `diyotherlink` tinyint(1) NOT NULL DEFAULT '0',
  `infouptime` int(10) unsigned NOT NULL DEFAULT '0',
  `infodowntime` int(10) unsigned NOT NULL DEFAULT '0',
  `copyids` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`pubid`),
  KEY `id` (`id`,`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsinfovote` */

/*Table structure for table `phome_enewsjstemp` */

DROP TABLE IF EXISTS `phome_enewsjstemp`;

CREATE TABLE `phome_enewsjstemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(30) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `showdate` varchar(20) NOT NULL DEFAULT '',
  `modid` smallint(6) NOT NULL DEFAULT '0',
  `subnews` smallint(6) NOT NULL DEFAULT '0',
  `subtitle` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsjstemp` */

insert  into `phome_enewsjstemp`(`tempid`,`tempname`,`temptext`,`classid`,`isdefault`,`showdate`,`modid`,`subnews`,`subtitle`) values (1,'默认js模板','[!--empirenews.listtemp--]<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a></li>[!--empirenews.listtemp--]',0,1,'m-d',1,0,32);

/*Table structure for table `phome_enewsjstempclass` */

DROP TABLE IF EXISTS `phome_enewsjstempclass`;

CREATE TABLE `phome_enewsjstempclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsjstempclass` */

/*Table structure for table `phome_enewskey` */

DROP TABLE IF EXISTS `phome_enewskey`;

CREATE TABLE `phome_enewskey` (
  `keyid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `keyname` char(50) NOT NULL DEFAULT '',
  `keyurl` char(200) NOT NULL DEFAULT '',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`keyid`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewskey` */

/*Table structure for table `phome_enewskeyclass` */

DROP TABLE IF EXISTS `phome_enewskeyclass`;

CREATE TABLE `phome_enewskeyclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewskeyclass` */

/*Table structure for table `phome_enewslink` */

DROP TABLE IF EXISTS `phome_enewslink`;

CREATE TABLE `phome_enewslink` (
  `lid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `lname` varchar(100) NOT NULL DEFAULT '',
  `lpic` varchar(255) NOT NULL DEFAULT '',
  `lurl` varchar(255) NOT NULL DEFAULT '',
  `ltime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `onclick` int(11) NOT NULL DEFAULT '0',
  `width` varchar(10) NOT NULL DEFAULT '',
  `height` varchar(10) NOT NULL DEFAULT '',
  `target` varchar(10) NOT NULL DEFAULT '',
  `myorder` tinyint(4) NOT NULL DEFAULT '0',
  `email` varchar(60) NOT NULL DEFAULT '',
  `lsay` text NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `ltype` smallint(6) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`lid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewslink` */

/*Table structure for table `phome_enewslinkclass` */

DROP TABLE IF EXISTS `phome_enewslinkclass`;

CREATE TABLE `phome_enewslinkclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewslinkclass` */

/*Table structure for table `phome_enewslinktmp` */

DROP TABLE IF EXISTS `phome_enewslinktmp`;

CREATE TABLE `phome_enewslinktmp` (
  `newsurl` varchar(255) NOT NULL DEFAULT '',
  `checkrnd` varchar(50) NOT NULL DEFAULT '',
  `linkid` bigint(20) NOT NULL AUTO_INCREMENT,
  `titlepic` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`linkid`),
  KEY `checkrnd` (`checkrnd`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewslinktmp` */

/*Table structure for table `phome_enewslisttemp` */

DROP TABLE IF EXISTS `phome_enewslisttemp`;

CREATE TABLE `phome_enewslisttemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  `subnews` smallint(6) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `listvar` text NOT NULL,
  `rownum` smallint(6) NOT NULL DEFAULT '0',
  `modid` smallint(6) NOT NULL DEFAULT '0',
  `showdate` varchar(50) NOT NULL DEFAULT '',
  `subtitle` smallint(6) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `docode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewslisttemp` */

insert  into `phome_enewslisttemp`(`tempid`,`tempname`,`temptext`,`subnews`,`isdefault`,`listvar`,`rownum`,`modid`,`showdate`,`subtitle`,`classid`,`docode`) values (1,'默认新闻列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left D_news_conter clear\\\">\r\n						[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>[!--temp.footer--]',0,0,'<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"[!--titleurl--]\\\">\r\n						<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">[!--smalltext--]...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>\r\n',1,1,'Y-m-d',0,0,0),(2,'默认下载列表模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td colspan=\\\"3\\\" class=\\\"down_list\\\"><table bgcolor=\\\"#FFFFFF\\\" width=\\\"100%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" style=\\\"table-layout:  fixed;  word-wrap:  break-word\\\">\r\n<tr align=\\\"center\\\">\r\n<td bgcolor=\\\"#D0E7F7\\\">软件名称</td>\r\n<td width=\\\"76\\\"  nowrap=\\\"nowrap\\\" bgcolor=\\\"#D0E7F7\\\">整理日期</td>\r\n<td width=\\\"70\\\" nowrap=\\\"nowrap\\\" bgcolor=\\\"#D0E7F7\\\">软件大小</td>\r\n<td width=\\\"50\\\" nowrap=\\\"nowrap\\\" bgcolor=\\\"#D0E7F7\\\">人气</td>\r\n<td width=\\\"76\\\" nowrap=\\\"nowrap\\\" bgcolor=\\\"#D0E7F7\\\">软件等级</td>\r\n</tr>\r\n[!--empirenews.listtemp--]\r\n<!--list.var1-->\r\n[!--empirenews.listtemp--]\r\n</table></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n<tr>\r\n<td>[!--show.page--]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table></td>\r\n<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>推荐下载</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[ecmsinfo]\\\'selfinfo\\\',10,44,0,2,2,0[/ecmsinfo]\r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>最后更新</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[ecmsinfo]\\\'selfinfo\\\',10,44,0,0,2,0[/ecmsinfo]\r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>热门点击</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ol class=\\\"rank\\\">\r\n[ecmsinfo]\\\'selfinfo\\\',10,40,0,1,10,0[/ecmsinfo]\r\n</ol></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',200,0,'<tr> \r\n    <td width=\\\"290\\\"><img src=\\\"[!--news.url--]e/data/images/list.gif\\\" width=\\\"11\\\" height=\\\"11\\\" border=\\\"0\\\" /> <b>[!--class.name--]<a href=\\\"[!--titleurl--]\\\">[!--title--]</a></b></td>\r\n    <td align=\\\"center\\\" nowrap=\\\"nowrap\\\">[!--newstime--]</td>\r\n    <td align=\\\"center\\\" nowrap=\\\"nowrap\\\">[!--filesize--]</td>\r\n    <td align=\\\"center\\\" nowrap=\\\"nowrap\\\">[!--totaldown--]</td>\r\n    <td align=\\\"center\\\" nowrap=\\\"nowrap\\\"><img src=\\\"[!--news.url--]e/data/images/[!--star--]star.gif\\\" border=\\\"0\\\" /></td>\r\n  </tr>\r\n  <tr valign=\\\"top\\\"> \r\n    <td colspan=\\\"5\\\" class=\\\"softsay\\\">[!--softsay--]</td>\r\n</tr>\r\n  <tr> \r\n    <td colspan=\\\"5\\\"><font color=\\\"green\\\">界面语言：</font>[!--language--]  <font color=\\\"green\\\">软件类型：</font>[!--softtype--]  <font color=\\\"green\\\">授权方式：</font>[!--softsq--]  <font color=\\\"green\\\">运行平台：</font>[!--softfj--]</td>\r\n</tr></td></tr>\r\n  <tr> \r\n    <td colspan=\\\"5\\\"><img src=\\\"[!--news.url--]e/data/images/line.gif\\\" width=\\\"100%\\\" height=\\\"9\\\" /></td>\r\n</tr>',1,2,'Y-m-d',0,0,0),(3,'默认图片列表模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td colspan=\\\"3\\\" class=\\\"photo_list\\\">\r\n[!--empirenews.listtemp--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"line_bottom\\\">\r\n	<tr>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var1--></td>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var2--></td>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var3--></td>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var4--></td>\r\n	</tr>\r\n</table>\r\n[!--empirenews.listtemp--]\r\n</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n<tr>\r\n<td>[!--show.page--]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table></td>\r\n<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>图片推荐</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>最后更新</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[ecmsinfo]\\\'selfinfo\\\',10,44,0,0,2,0[/ecmsinfo]\r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>热门点击</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ol class=\\\"rank\\\">\r\n[ecmsinfo]\\\'selfinfo\\\',10,40,0,1,10,0[/ecmsinfo]\r\n</ol></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',0,0,'<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"4\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td align=\\\"center\\\" class=\\\"titlepic\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"[!--oldtitle--]\\\" width=\\\"128\\\" height=\\\"90\\\" border=\\\"0\\\" /></a></td>\r\n	</tr>\r\n	<tr>\r\n		<td align=\\\"center\\\"><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a></td>\r\n	</tr>\r\n</table>',4,3,'Y-m-d H:i:s',22,0,0),(4,'默认FLASH列表模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td colspan=\\\"3\\\" class=\\\"flash_list\\\">\r\n[!--empirenews.listtemp--]\r\n<!--list.var1-->\r\n[!--empirenews.listtemp--]\r\n</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n<tr>\r\n<td>[!--show.page--]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table></td>\r\n<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>推荐下载</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>最后更新</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[ecmsinfo]\\\'selfinfo\\\',10,44,0,0,2,0[/ecmsinfo]\r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>热门点击</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ol class=\\\"rank\\\">\r\n[ecmsinfo]\\\'selfinfo\\\',10,40,0,1,10,0[/ecmsinfo]\r\n</ol></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',200,0,'<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"8\\\" class=\\\"line_bottom\\\">\r\n<tr valign=\\\"top\\\">\r\n<td width=\\\"200\\\" align=\\\"center\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" width=\\\"200\\\" height=\\\"150\\\" border=\\\"0\\\" /></a></td>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td width=\\\"70%\\\" height=\\\"26\\\" style=\\\"font-size:14px\\\"><strong><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></strong></td>\r\n<td width=\\\"30%\\\"><div align=\\\"right\\\">[!--newstime--]</div></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\\\"2\\\"class=\\\"flashsay\\\">简介：[!--flashsay--]</td>\r\n</tr>\r\n<tr>\r\n<td height=\\\"26\\\" colspan=\\\"2\\\">作者：[!--flashwriter--]　文件大小：[!--filesize--]　作品人气：[!--onclick--] </td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>',1,4,'Y-m-d',0,0,0),(5,'默认电影列表模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td colspan=\\\"3\\\" class=\\\"movie_list\\\">\r\n[!--empirenews.listtemp--]\r\n<!--list.var1-->\r\n[!--empirenews.listtemp--]\r\n</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n<tr>\r\n<td>[!--show.page--]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table></td>\r\n<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>影视推荐</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>最后更新</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[ecmsinfo]\\\'selfinfo\\\',10,44,0,0,2,0[/ecmsinfo]\r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>热门点击</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ol class=\\\"rank\\\">\r\n[ecmsinfo]\\\'selfinfo\\\',10,40,0,1,10,0[/ecmsinfo]\r\n</ol></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',100,0,'<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"8\\\" class=\\\"line_bottom\\\">\r\n<tr valign=\\\"top\\\">\r\n    <td width=\\\"27%\\\" align=\\\"center\\\" class=\\\"titlepic\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"[!--oldtitle--]\\\" width=\\\"120\\\" height=\\\"150\\\" border=\\\"0\\\" /></a></td>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n          <td width=\\\"80%\\\" height=\\\"26\\\" style=\\\"font-size:14px\\\"><strong><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></strong> \r\n            <img src=\\\"[!--news.url--]e/data/images/[!--star--]star.gif\\\" alt=\\\"推荐指数[!--star--]颗星\\\" border=\\\"0\\\" align=\\\"absmiddle\\\" /></td>\r\n          <td width=\\\"20%\\\"> \r\n            <div align=\\\"right\\\">[!--newstime--]</div></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\\\"2\\\"><strong>年代：</strong>2008　　<strong>出品公司：</strong>[!--company--]</td>\r\n</tr>\r\n<tr>\r\n	<td height=\\\"26\\\" colspan=\\\"2\\\"><strong>导演：</strong>[!--playadmin--]</td>\r\n</tr>\r\n<tr>\r\n	<td height=\\\"26\\\" colspan=\\\"2\\\"><strong>主演：</strong>[!--player--]</td>\r\n</tr>\r\n<tr>\r\n	<td height=\\\"26\\\" colspan=\\\"2\\\"><strong>类型：</strong>[!--movietype--]</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\\\"2\\\"><strong>影片简介：</strong>[!--moviesay--] [<a title=\\\"阅读全文\\\" href=\\\"[!--titleurl--]\\\">详细内容</a>]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>',1,5,'Y-m-d',0,0,0),(6,'默认商城列表模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td colspan=\\\"3\\\" class=\\\"shop_list\\\">\r\n[!--empirenews.listtemp--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"line_bottom\\\">\r\n	<tr>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var1--></td>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var2--></td>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var3--></td>\r\n		<td width=\\\"25%\\\" valign=\\\"top\\\"><!--list.var4--></td>\r\n	</tr>\r\n</table>\r\n[!--empirenews.listtemp--]\r\n</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n<tr>\r\n<td>[!--show.page--]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table></td>\r\n<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>商品推荐</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>最后更新</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[ecmsinfo]\\\'selfinfo\\\',10,44,0,0,2,0[/ecmsinfo]\r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>热门点击</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ol class=\\\"rank\\\">\r\n[ecmsinfo]\\\'selfinfo\\\',10,40,0,1,10,0[/ecmsinfo]\r\n</ol></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',60,0,'<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"4\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td align=\\\"center\\\" class=\\\"titlepic\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"[!--oldtitle--]\\\" width=\\\"120\\\" height=\\\"120\\\" border=\\\"0\\\" /></a></td>\r\n	</tr>\r\n	<tr>\r\n		<td align=\\\"center\\\"><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a></td>\r\n	</tr>\r\n	<tr>\r\n		<td align=\\\"center\\\" class=\\\"tprice\\\">市场价：<span>￥[!--tprice--]</span></td>\r\n	</tr>\r\n	<tr>\r\n		<td align=\\\"center\\\" class=\\\"price\\\">优惠价：<span>￥[!--price--]</span></td>\r\n	</tr>\r\n</table>',4,6,'Y-m-d',20,0,0),(7,'默认文章列表模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"news_list\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[!--empirenews.listtemp--]<ul>\r\n<!--list.var1-->\r\n<!--list.var2-->\r\n<!--list.var3-->\r\n<!--list.var4-->\r\n<!--list.var5--></ul>\r\n[!--empirenews.listtemp--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n<tr>\r\n<td>[!--show.page--]</td>\r\n</tr>\r\n</table>\r\n</td>\r\n</tr>\r\n</table></td>\r\n<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>推荐资讯</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic]\r\n</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>最后更新</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[ecmsinfo]\\\'selfinfo\\\',10,44,0,0,2,0[/ecmsinfo] \r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>热门点击</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ol class=\\\"rank\\\">\r\n[ecmsinfo]\\\'selfinfo\\\',10,40,0,1,10,0[/ecmsinfo] \r\n</ol></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',0,0,'<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a> <span>[!--newstime--]</span></li>',5,7,'Y-m-d',0,0,0),(8,'分类信息默认列表模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.dtheader--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td colspan=\\\"3\\\" class=\\\"info_list\\\">\r\n[!--empirenews.listtemp--]\r\n<!--list.var1-->\r\n[!--empirenews.listtemp--]\r\n</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n<tr>\r\n<td>[!--show.page--]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table></td>\r\n<td class=\\\"sider\\\">\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>地区导航</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[!--temp.infoarea--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>分类导航</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td>[!--temp.infoclassnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>推荐信息</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[!--self.goodnews--]\r\n</ul></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',120,0,'<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"8\\\" class=\\\"line_bottom\\\">\r\n<tr valign=\\\"top\\\">\r\n<td align=\\\"center\\\" class=\\\"titlepic\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"[!--oldtitle--]\\\" width=\\\"80\\\" height=\\\"60\\\" border=\\\"0\\\" /></a></td>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n<tr>\r\n<td width=\\\"70%\\\" height=\\\"26\\\" style=\\\"font-size:14px\\\"><strong>[<a href=\\\"[!--news.url--]e/action/ListInfo.php?classid=[!--classid--]&amp;ph=1&amp;myarea=[!--myarea--]\\\">[!--myarea--]</a>] <a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n</strong> 关注：<span class=\\\"click\\\">[!--onclick--]</span></td>\r\n<td width=\\\"30%\\\"><div align=\\\"right\\\">[!--newstime--]</div></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\\\"2\\\" class=\\\"smalltext\\\">[!--smalltext--] [<a title=\\\"阅读全文\\\" href=\\\"[!--titleurl--]\\\">详细内容</a>]</td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>',1,8,'Y-m-d',0,0,0),(10,'集团荣誉模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"D_case\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]		</ul>\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.footer--]',0,0,'<li><div class=\\\"D_case_img\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a></div>\r\n<div class=\\\"D_case_title\\\"><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></div></li>\r\n',1,1,'Y-m-d',0,0,0),(9,'图片列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"D_case\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]		</ul>\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.footer--]',0,0,'<li><div class=\\\"D_case_img\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a></div>\r\n<div class=\\\"D_case_title\\\"><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></div></li>\r\n',1,1,'Y-m-d',0,0,0),(11,'城中分校新闻列表模板','[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul><li class=\\\"c_info_bg c_info_bg_dtl\\\"></li>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"c_title detail_d_newstitle clear\\\">\r\n				<span>[!--class.name--]</span>\r\n			</div>\r\n			<div class=\\\"c_info detail_d_info\\\">\r\n				<ul>[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n				</ul>\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]',0,0,'<li><a href=\\\"[!--titleurl--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',1,1,'Y-m-d',0,0,0),(12,'爱德分校新闻列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1),(13,'爱德分校图片+信息列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0),(14,'太湖分校新闻列表模板','[!--temp.thheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]40,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1),(15,'太湖分校图片+信息列表模板','[!--temp.thheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]40,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0),(16,'苏州湾校区新闻列表模板','[!--temp.szwheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>\r\n[!--temp.szwfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1),(17,'苏州湾校区图片+信息列表模板','[!--temp.szwheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.szwfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0);

/*Table structure for table `phome_enewslisttempclass` */

DROP TABLE IF EXISTS `phome_enewslisttempclass`;

CREATE TABLE `phome_enewslisttempclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewslisttempclass` */

/*Table structure for table `phome_enewslog` */

DROP TABLE IF EXISTS `phome_enewslog`;

CREATE TABLE `phome_enewslog` (
  `loginid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `logintime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `loginip` varchar(20) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(30) NOT NULL DEFAULT '',
  `loginauth` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loginid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewslog` */

insert  into `phome_enewslog`(`loginid`,`username`,`logintime`,`loginip`,`status`,`password`,`loginauth`) values (1,'manage','2016-08-22 13:42:43','::1',1,'',0),(2,'manage','2016-08-25 13:08:30','::1',1,'',0),(3,'manage','2016-08-25 15:35:41','::1',0,'',0),(4,'manage','2016-08-25 15:35:46','::1',1,'',0),(5,'manage','2016-08-25 15:36:54','::1',1,'',0),(6,'manage','2016-08-25 17:13:39','::1',1,'',0),(7,'manage','2016-08-25 17:16:36','::1',1,'',0),(8,'manage','2016-08-25 17:21:26','::1',0,'',0),(9,'manage','2016-08-25 17:21:36','::1',1,'',0),(10,'manage','2016-08-26 09:03:14','::1',1,'',0),(11,'manage','2016-09-01 08:26:09','::1',1,'',0),(12,'manage','2016-09-01 08:43:32','::1',1,'',0),(13,'manage','2016-09-02 09:49:34','::1',1,'',0),(14,'manage','2016-09-02 12:39:00','::1',1,'',0),(15,'manage','2016-09-02 14:27:11','180.103.188.220',1,'',0),(16,'manage','2016-09-02 15:58:36','49.64.72.66',1,'',0),(17,'manage','2016-09-06 09:56:57','::1',1,'',0),(18,'admin','2016-09-07 12:59:17','58.210.80.187',1,'',0),(19,'admin','2016-09-07 13:00:57','58.210.80.187',1,'',0),(20,'admin','2016-09-07 16:17:14','58.210.80.187',1,'',0),(21,'admin','2016-09-08 16:06:45','58.210.80.187',1,'',0),(22,'manage','2016-09-08 21:34:39','111.74.215.99',1,'',0),(23,'manage','2016-09-08 21:35:18','111.74.215.79',1,'',0),(24,'manage','2016-09-09 08:17:18','117.80.139.75',1,'',0),(25,'admin','2016-09-09 08:19:26','58.210.80.187',1,'',0),(26,'dd','2016-09-09 08:27:13','117.80.139.75',1,'',0),(27,'dd','2016-09-09 08:52:41','117.80.139.75',1,'',0),(28,'dd','2016-09-09 08:52:51','117.80.139.75',0,'',1),(29,'manage','2016-09-09 08:56:32','117.80.139.75',1,'',0),(30,'dd','2016-09-09 08:57:07','117.80.139.75',1,'',0),(31,'manage','2016-09-09 11:29:49','58.210.80.187',0,'',1),(32,'manage','2016-09-09 11:30:13','58.210.80.187',0,'',1),(33,'manage','2016-09-09 11:30:47','58.210.80.187',0,'',1),(34,'admin','2016-09-09 11:33:50','58.210.80.187',0,'',1),(35,'manage','2016-09-09 13:26:26','58.210.80.187',1,'',0),(36,'爱德','2016-09-09 13:28:13','58.210.80.187',1,'',0),(37,'爱德','2016-09-09 16:57:27','58.210.80.187',1,'',0),(38,'爱德','2016-09-12 07:34:02','221.224.114.98',1,'',0),(39,'爱德','2016-09-12 07:46:59','221.224.114.98',1,'',0),(40,'manage','2016-09-12 09:34:31','58.210.80.187',1,'',0),(41,'爱德','2016-09-12 13:17:13','221.224.114.98',1,'',0),(42,'manage','2016-09-13 08:48:14','58.210.80.187',1,'',0),(43,'苏州湾','2016-09-13 08:51:02','58.210.80.187',1,'',0),(44,'manage','2016-09-13 08:51:40','58.210.80.187',1,'',0),(45,'苏州湾','2016-09-13 08:53:57','58.210.80.187',1,'',0),(46,'manage','2016-09-13 08:54:58','58.210.80.187',1,'',0),(47,'苏州湾','2016-09-13 08:57:06','58.210.80.187',1,'',0),(48,'manage','2016-09-13 09:40:03','58.210.80.187',1,'',0),(49,'manage','2016-09-13 12:59:06','58.210.80.187',1,'',0),(50,'城中','2016-09-13 13:00:48','58.210.80.187',1,'',0),(51,'城中','2016-09-13 16:11:31','218.4.59.173',1,'',0),(52,'城中','2016-09-13 16:14:19','218.4.59.173',1,'',0),(53,'manage','2016-09-14 01:40:13','222.93.20.136',1,'',0),(54,'manage','2016-09-14 01:45:20','222.93.20.136',1,'',0),(55,'manage','2016-09-14 11:02:58','58.210.80.187',1,'',0),(56,'爱德','2016-09-14 11:57:11','221.224.114.98',1,'',0),(57,'爱德','2016-09-14 17:02:17','221.224.114.98',1,'',0),(58,'manage','2016-09-18 08:25:23','58.210.80.187',1,'',0),(59,'manage','2016-09-19 13:00:31','121.224.137.241',1,'',0),(60,'manage','2016-09-19 14:23:25','121.224.137.241',1,'',0),(61,'manage','2016-09-20 16:56:26','49.75.189.3',1,'',0),(62,'manage','2016-09-21 09:06:23','58.210.80.187',1,'',0),(63,'管文锦','2016-09-21 10:03:11','58.210.80.187',1,'',0),(64,'管文锦','2016-09-21 12:40:42','58.210.80.187',1,'',0),(65,'管文锦','2016-09-26 08:44:09','58.210.80.187',1,'',0),(66,'manage','2016-09-26 11:32:27','58.210.80.187',1,'',0),(67,'manage','2016-09-27 11:05:35','58.210.80.187',1,'',0),(68,'manage','2016-09-27 16:17:55','58.210.80.187',1,'',0),(69,'manage','2016-09-28 17:51:18','58.208.126.174',1,'',0),(70,'管文锦','2016-09-30 12:52:09','58.210.80.187',1,'',0),(71,'管文锦','2016-09-30 12:55:51','58.210.80.187',1,'',0),(72,'管文锦','2016-09-30 12:56:48','58.210.80.187',1,'',0),(73,'管文锦','2016-09-30 13:47:09','58.210.80.187',1,'',0),(74,'管文锦','2016-09-30 13:57:35','58.210.80.187',1,'',0),(75,'manage','2016-10-01 00:41:12','114.217.76.217',1,'',0),(76,'苏州湾','2016-10-08 09:52:39','58.210.80.187',1,'',0),(77,'爱德','2016-10-08 15:48:24','221.224.114.98',1,'',0),(78,'爱德','2016-10-08 16:04:50','221.224.114.98',0,'',0),(79,'爱德','2016-10-08 16:06:07','221.224.114.98',1,'',0),(80,'爱德','2016-10-08 16:17:19','221.224.114.98',1,'',0),(81,'manage','2016-10-08 16:29:15','58.210.80.187',0,'',0),(82,'manage','2016-10-08 16:29:27','58.210.80.187',1,'',0),(83,'爱德','2016-10-08 16:35:33','221.224.114.98',1,'',0),(84,'爱德','2016-10-08 16:36:47','221.224.114.98',0,'',1),(85,'爱德','2016-10-08 16:38:03','221.224.114.98',1,'',0),(86,'manage','2016-10-09 16:43:17','58.211.212.154',0,'',0),(87,'admin','2016-10-09 16:43:28','58.211.212.154',1,'',0),(88,'admin','2016-10-09 16:44:25','58.211.212.154',1,'',0),(89,'manage','2016-10-10 10:10:48','58.210.80.187',0,'',0),(90,'manage','2016-10-10 10:10:56','58.210.80.187',1,'',0),(91,'城中','2016-10-11 08:29:18','218.4.59.173',0,'',0),(92,'城中','2016-10-11 08:29:42','218.4.59.173',0,'',0),(93,'城中','2016-10-11 08:30:20','218.4.59.173',1,'',0),(94,'城中','2016-10-11 08:58:54','218.4.59.173',1,'',0),(95,'manage','2016-10-11 09:02:51','58.210.80.187',0,'',0),(96,'manage','2016-10-11 09:03:00','58.210.80.187',1,'',0),(97,'城中','2016-10-11 15:05:23','218.4.59.173',1,'',0),(98,'manage','2016-10-11 16:42:48','49.75.150.46',0,'',0),(99,'manage','2016-10-11 16:42:57','49.75.150.46',0,'',0),(100,'manage','2016-10-12 09:01:46','58.210.80.187',1,'',0),(101,'manage','2016-10-12 09:02:32','58.210.80.187',1,'',0),(102,'管文锦','2016-10-12 09:23:31','58.210.80.187',0,'',0),(103,'管文锦','2016-10-12 09:24:02','58.210.80.187',1,'',0),(104,'manage','2016-10-12 10:08:54','49.75.150.46',1,'',0),(105,'manage','2016-10-12 18:13:36','127.0.0.1',1,'',0),(106,'manage','2016-10-16 20:53:10','127.0.0.1',1,'',0);

/*Table structure for table `phome_enewsloginfail` */

DROP TABLE IF EXISTS `phome_enewsloginfail`;

CREATE TABLE `phome_enewsloginfail` (
  `ip` varchar(20) NOT NULL DEFAULT '',
  `num` tinyint(4) NOT NULL DEFAULT '0',
  `lasttime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsloginfail` */

insert  into `phome_enewsloginfail`(`ip`,`num`,`lasttime`) values ('58.210.80.187',1,1476235411);

/*Table structure for table `phome_enewsmember` */

DROP TABLE IF EXISTS `phome_enewsmember`;

CREATE TABLE `phome_enewsmember` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `rnd` char(20) NOT NULL DEFAULT '',
  `email` char(50) NOT NULL DEFAULT '',
  `registertime` int(10) unsigned NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userdate` int(10) unsigned NOT NULL DEFAULT '0',
  `money` float(11,2) NOT NULL DEFAULT '0.00',
  `zgroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `havemsg` tinyint(1) NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `salt` char(8) NOT NULL DEFAULT '',
  `userkey` char(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`),
  KEY `groupid` (`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmember` */

/*Table structure for table `phome_enewsmember_connect` */

DROP TABLE IF EXISTS `phome_enewsmember_connect`;

CREATE TABLE `phome_enewsmember_connect` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `apptype` char(20) NOT NULL DEFAULT '',
  `bindkey` char(32) NOT NULL DEFAULT '',
  `bindtime` int(10) unsigned NOT NULL DEFAULT '0',
  `loginnum` int(10) unsigned NOT NULL DEFAULT '0',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `bindkey` (`bindkey`),
  KEY `apptype` (`apptype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmember_connect` */

/*Table structure for table `phome_enewsmember_connect_app` */

DROP TABLE IF EXISTS `phome_enewsmember_connect_app`;

CREATE TABLE `phome_enewsmember_connect_app` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `apptype` char(20) NOT NULL DEFAULT '',
  `appname` char(30) NOT NULL DEFAULT '',
  `appid` char(60) NOT NULL DEFAULT '',
  `appkey` char(120) NOT NULL DEFAULT '',
  `isclose` tinyint(1) NOT NULL DEFAULT '0',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `qappname` char(30) NOT NULL DEFAULT '',
  `appsay` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `apptype` (`apptype`),
  KEY `isclose` (`isclose`),
  KEY `myorder` (`myorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmember_connect_app` */

/*Table structure for table `phome_enewsmemberadd` */

DROP TABLE IF EXISTS `phome_enewsmemberadd`;

CREATE TABLE `phome_enewsmemberadd` (
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `truename` varchar(20) NOT NULL DEFAULT '',
  `oicq` varchar(25) NOT NULL DEFAULT '',
  `msn` varchar(120) NOT NULL DEFAULT '',
  `mycall` varchar(30) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `zip` varchar(8) NOT NULL DEFAULT '',
  `spacestyleid` smallint(6) NOT NULL DEFAULT '0',
  `homepage` varchar(200) NOT NULL DEFAULT '',
  `saytext` text NOT NULL,
  `company` varchar(255) NOT NULL DEFAULT '',
  `fax` varchar(30) NOT NULL DEFAULT '',
  `userpic` varchar(200) NOT NULL DEFAULT '',
  `spacename` varchar(255) NOT NULL DEFAULT '',
  `spacegg` text NOT NULL,
  `viewstats` int(11) NOT NULL DEFAULT '0',
  `regip` varchar(20) NOT NULL DEFAULT '',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(20) NOT NULL DEFAULT '',
  `loginnum` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmemberadd` */

/*Table structure for table `phome_enewsmemberf` */

DROP TABLE IF EXISTS `phome_enewsmemberf`;

CREATE TABLE `phome_enewsmemberf` (
  `fid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f` varchar(30) NOT NULL DEFAULT '',
  `fname` varchar(30) NOT NULL DEFAULT '',
  `fform` varchar(20) NOT NULL DEFAULT '',
  `fhtml` mediumtext NOT NULL,
  `fzs` varchar(255) NOT NULL DEFAULT '',
  `myorder` smallint(6) NOT NULL DEFAULT '0',
  `ftype` varchar(30) NOT NULL DEFAULT '',
  `flen` varchar(20) NOT NULL DEFAULT '',
  `fvalue` text NOT NULL,
  `fformsize` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmemberf` */

insert  into `phome_enewsmemberf`(`fid`,`f`,`fname`,`fform`,`fhtml`,`fzs`,`myorder`,`ftype`,`flen`,`fvalue`,`fformsize`) values (1,'truename','真实姓名','text','\r\n<input name=\"truename\" type=\"text\" id=\"truename\" value=\"<?=$ecmsfirstpost==1?\"\":htmlspecialchars(stripSlashes($addr[truename]))?>\">\r\n','',1,'VARCHAR','20','',''),(2,'oicq','QQ号码','text','<input name=\\\"oicq\\\" type=\\\"text\\\" id=\\\"oicq\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":htmlspecialchars(stripSlashes($addr[oicq]))?>\\\">\r\n','',5,'VARCHAR','25','',''),(3,'msn','MSN','text','<input name=\\\"msn\\\" type=\\\"text\\\" id=\\\"msn\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":htmlspecialchars(stripSlashes($addr[msn]))?>\\\">\r\n','',6,'VARCHAR','120','',''),(4,'mycall','联系电话','text','<input name=\\\"mycall\\\" type=\\\"text\\\" id=\\\"mycall\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":htmlspecialchars(stripSlashes($addr[mycall]))?>\\\">\r\n','',2,'VARCHAR','30','',''),(5,'phone','手机','text','<input name=\\\"phone\\\" type=\\\"text\\\" id=\\\"phone\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":htmlspecialchars(stripSlashes($addr[phone]))?>\\\">\r\n','',4,'VARCHAR','30','',''),(6,'address','联系地址','text','<input name=\\\"address\\\" type=\\\"text\\\" id=\\\"address\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":htmlspecialchars(stripSlashes($addr[address]))?>\\\" size=\\\"50\\\">\r\n','',9,'VARCHAR','255','',''),(7,'zip','邮编','text','<input name=\\\"zip\\\" type=\\\"text\\\" id=\\\"zip\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":htmlspecialchars(stripSlashes($addr[zip]))?>\\\" size=\\\"8\\\">\r\n','',10,'VARCHAR','8','',''),(9,'homepage','网址','text','\r\n<input name=\"homepage\" type=\"text\" id=\"homepage\" value=\"<?=$ecmsfirstpost==1?\"\":htmlspecialchars(stripSlashes($addr[homepage]))?>\">\r\n','',7,'VARCHAR','200','',''),(10,'saytext','简介','textarea','<textarea name=\\\"saytext\\\" cols=\\\"65\\\" rows=\\\"10\\\" id=\\\"saytext\\\"><?=$ecmsfirstpost==1?\\\"\\\":stripSlashes($addr[saytext])?></textarea>\r\n','',11,'TEXT','','',''),(11,'company','公司名称','text','<input name=\\\"company\\\" type=\\\"text\\\" id=\\\"company\\\" value=\\\"<?=$ecmsfirstpost==1?\\\"\\\":htmlspecialchars(stripSlashes($addr[company]))?>\\\" size=\\\"38\\\">\r\n','',0,'VARCHAR','255','',''),(12,'fax','传真','text','\r\n<input name=\"fax\" type=\"text\" id=\"fax\" value=\"<?=$ecmsfirstpost==1?\"\":htmlspecialchars(stripSlashes($addr[fax]))?>\">\r\n','',3,'VARCHAR','30','',''),(13,'userpic','会员头像','img','<input type=\\\"file\\\" name=\\\"userpicfile\\\">&nbsp;&nbsp;\r\n<?=empty($addr[userpic])?\\\"\\\":\\\"<img src=\\\'\\\".htmlspecialchars(stripSlashes($addr[userpic])).\\\"\\\' border=0>\\\"?>','',8,'VARCHAR','200','','');

/*Table structure for table `phome_enewsmemberfeedback` */

DROP TABLE IF EXISTS `phome_enewsmemberfeedback`;

CREATE TABLE `phome_enewsmemberfeedback` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL DEFAULT '',
  `company` varchar(80) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `fax` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `zip` varchar(8) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `ftext` text NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `uname` varchar(20) NOT NULL DEFAULT '',
  `addtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`fid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmemberfeedback` */

/*Table structure for table `phome_enewsmemberform` */

DROP TABLE IF EXISTS `phome_enewsmemberform`;

CREATE TABLE `phome_enewsmemberform` (
  `fid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(60) NOT NULL DEFAULT '',
  `ftemp` mediumtext NOT NULL,
  `fzs` varchar(255) NOT NULL DEFAULT '',
  `enter` text NOT NULL,
  `mustenter` text NOT NULL,
  `filef` varchar(255) NOT NULL DEFAULT '',
  `imgf` varchar(255) NOT NULL DEFAULT '0',
  `tobrf` text NOT NULL,
  `viewenter` text NOT NULL,
  `searchvar` varchar(255) NOT NULL DEFAULT '',
  `canaddf` text NOT NULL,
  `caneditf` text NOT NULL,
  `checkboxf` text NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmemberform` */

insert  into `phome_enewsmemberform`(`fid`,`fname`,`ftemp`,`fzs`,`enter`,`mustenter`,`filef`,`imgf`,`tobrf`,`viewenter`,`searchvar`,`canaddf`,`caneditf`,`checkboxf`) values (1,'个人注册表单','<table width=\\\'100%\\\' align=\\\'center\\\' cellpadding=3 cellspacing=1 bgcolor=\\\'#DBEAF5\\\'>\r\n<tr><td width=\\\'25%\\\' height=25 bgcolor=\\\'ffffff\\\'>真实姓名</td><td bgcolor=\\\'ffffff\\\'>[!--truename--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>QQ号码</td><td bgcolor=\\\'ffffff\\\'>[!--oicq--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>MSN</td><td bgcolor=\\\'ffffff\\\'>[!--msn--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>联系电话</td><td bgcolor=\\\'ffffff\\\'>[!--mycall--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>手机</td><td bgcolor=\\\'ffffff\\\'>[!--phone--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>网站地址</td><td bgcolor=\\\'ffffff\\\'>[!--homepage--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>会员头像</td><td bgcolor=\\\'ffffff\\\'>[!--userpic--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>联系地址</td><td bgcolor=\\\'ffffff\\\'>[!--address--]&nbsp;邮编: [!--zip--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>个人介绍</td><td bgcolor=\\\'ffffff\\\'>[!--saytext--]</td></tr>\r\n</table>','','真实姓名<!--field--->truename<!--record-->联系电话<!--field--->mycall<!--record-->手机<!--field--->phone<!--record-->QQ号码<!--field--->oicq<!--record-->MSN<!--field--->msn<!--record-->网站地址<!--field--->homepage<!--record-->会员头像<!--field--->userpic<!--record-->联系地址<!--field--->address<!--record-->邮编<!--field--->zip<!--record-->简介<!--field--->saytext<!--record-->','',',',',userpic,',',saytext,','真实姓名<!--field--->truename<!--record-->联系电话<!--field--->mycall<!--record-->手机<!--field--->phone<!--record-->QQ号码<!--field--->oicq<!--record-->MSN<!--field--->msn<!--record-->网站地址<!--field--->homepage<!--record-->会员头像<!--field--->userpic<!--record-->联系地址<!--field--->address<!--record-->邮编<!--field--->zip<!--record-->简介<!--field--->saytext<!--record-->','',',truename,mycall,phone,oicq,msn,homepage,userpic,address,zip,saytext,',',truename,mycall,phone,oicq,msn,homepage,userpic,address,zip,saytext,',','),(2,'企员注册表单','<table width=\\\'100%\\\' align=\\\'center\\\' cellpadding=3 cellspacing=1 bgcolor=\\\'#DBEAF5\\\'><tr><td width=\\\'25%\\\' height=25 bgcolor=\\\'ffffff\\\'>公司名称</td><td bgcolor=\\\'ffffff\\\'>[!--company--](*)</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>联系人</td><td bgcolor=\\\'ffffff\\\'>[!--truename--](*)</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>联系电话</td><td bgcolor=\\\'ffffff\\\'>[!--mycall--](*)</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>传真</td><td bgcolor=\\\'ffffff\\\'>[!--fax--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>手机</td><td bgcolor=\\\'ffffff\\\'>[!--phone--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>QQ号码</td><td bgcolor=\\\'ffffff\\\'>[!--oicq--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>MSN</td><td bgcolor=\\\'ffffff\\\'>[!--msn--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>网址</td><td bgcolor=\\\'ffffff\\\'>[!--homepage--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>会员头像</td><td bgcolor=\\\'ffffff\\\'>[!--userpic--]</td></tr>\r\n<tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>联系地址</td><td bgcolor=\\\'ffffff\\\'>[!--address--]&nbsp;邮编: [!--zip--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>公司介绍</td><td bgcolor=\\\'ffffff\\\'>[!--saytext--]</td></tr></table>','','公司名称<!--field--->company<!--record-->联系人<!--field--->truename<!--record-->联系电话<!--field--->mycall<!--record-->传真<!--field--->fax<!--record-->手机<!--field--->phone<!--record-->QQ号码<!--field--->oicq<!--record-->MSN<!--field--->msn<!--record-->网址<!--field--->homepage<!--record-->会员头像<!--field--->userpic<!--record-->联系地址<!--field--->address<!--record-->邮编<!--field--->zip<!--record-->公司介绍<!--field--->saytext<!--record-->',',company,truename,mycall,',',',',userpic,',',saytext,','公司名称<!--field--->company<!--record-->联系人<!--field--->truename<!--record-->联系电话<!--field--->mycall<!--record-->传真<!--field--->fax<!--record-->手机<!--field--->phone<!--record-->QQ号码<!--field--->oicq<!--record-->MSN<!--field--->msn<!--record-->网址<!--field--->homepage<!--record-->会员头像<!--field--->userpic<!--record-->联系地址<!--field--->address<!--record-->邮编<!--field--->zip<!--record-->公司介绍<!--field--->saytext<!--record-->',',company,',',company,truename,mycall,fax,phone,oicq,msn,homepage,userpic,address,zip,saytext,',',company,truename,mycall,fax,phone,oicq,msn,homepage,userpic,address,zip,saytext,',',');

/*Table structure for table `phome_enewsmembergbook` */

DROP TABLE IF EXISTS `phome_enewsmembergbook`;

CREATE TABLE `phome_enewsmembergbook` (
  `gid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `isprivate` tinyint(1) NOT NULL DEFAULT '0',
  `uid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `uname` varchar(20) NOT NULL DEFAULT '',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `addtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `gbtext` text NOT NULL,
  `retext` text NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmembergbook` */

/*Table structure for table `phome_enewsmembergroup` */

DROP TABLE IF EXISTS `phome_enewsmembergroup`;

CREATE TABLE `phome_enewsmembergroup` (
  `groupid` smallint(6) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(60) NOT NULL DEFAULT '',
  `level` smallint(6) NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `favanum` smallint(6) NOT NULL DEFAULT '0',
  `daydown` int(11) NOT NULL DEFAULT '0',
  `msglen` int(11) NOT NULL DEFAULT '0',
  `msgnum` int(11) NOT NULL DEFAULT '0',
  `canreg` tinyint(1) NOT NULL DEFAULT '0',
  `formid` smallint(6) NOT NULL DEFAULT '0',
  `regchecked` tinyint(1) NOT NULL DEFAULT '0',
  `spacestyleid` smallint(6) NOT NULL DEFAULT '0',
  `dayaddinfo` smallint(6) NOT NULL DEFAULT '0',
  `infochecked` tinyint(1) NOT NULL DEFAULT '0',
  `plchecked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmembergroup` */

insert  into `phome_enewsmembergroup`(`groupid`,`groupname`,`level`,`checked`,`favanum`,`daydown`,`msglen`,`msgnum`,`canreg`,`formid`,`regchecked`,`spacestyleid`,`dayaddinfo`,`infochecked`,`plchecked`) values (1,'普通会员',1,0,120,0,255,50,1,1,0,1,0,0,0),(2,'VIP会员',2,0,200,0,255,120,0,1,0,1,0,0,0),(3,'企业会员',1,0,120,0,255,50,1,2,0,2,0,0,0),(4,'企业VIP会员',2,0,200,0,255,120,0,2,0,2,0,0,0);

/*Table structure for table `phome_enewsmemberpub` */

DROP TABLE IF EXISTS `phome_enewsmemberpub`;

CREATE TABLE `phome_enewsmemberpub` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `todayinfodate` char(10) NOT NULL DEFAULT '',
  `todayaddinfo` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `todaydate` char(10) NOT NULL DEFAULT '',
  `todaydown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `authstr` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmemberpub` */

/*Table structure for table `phome_enewsmenu` */

DROP TABLE IF EXISTS `phome_enewsmenu`;

CREATE TABLE `phome_enewsmenu` (
  `menuid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuname` varchar(60) NOT NULL DEFAULT '',
  `menuurl` varchar(255) NOT NULL DEFAULT '',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuid`),
  KEY `myorder` (`myorder`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmenu` */

/*Table structure for table `phome_enewsmenuclass` */

DROP TABLE IF EXISTS `phome_enewsmenuclass`;

CREATE TABLE `phome_enewsmenuclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(60) NOT NULL DEFAULT '',
  `issys` tinyint(1) NOT NULL DEFAULT '0',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classtype` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classid`),
  KEY `myorder` (`myorder`),
  KEY `classtype` (`classtype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmenuclass` */

/*Table structure for table `phome_enewsmod` */

DROP TABLE IF EXISTS `phome_enewsmod`;

CREATE TABLE `phome_enewsmod` (
  `mid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `mname` varchar(100) NOT NULL DEFAULT '',
  `mtemp` mediumtext NOT NULL,
  `mzs` varchar(255) NOT NULL DEFAULT '',
  `cj` mediumtext NOT NULL,
  `enter` mediumtext NOT NULL,
  `tempvar` mediumtext NOT NULL,
  `sonclass` text NOT NULL,
  `searchvar` varchar(255) NOT NULL DEFAULT '',
  `tid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `qenter` mediumtext NOT NULL,
  `mustqenterf` text NOT NULL,
  `qmtemp` mediumtext NOT NULL,
  `listandf` text NOT NULL,
  `setandf` tinyint(1) NOT NULL DEFAULT '0',
  `listtempvar` mediumtext NOT NULL,
  `qmname` varchar(30) NOT NULL DEFAULT '',
  `canaddf` text NOT NULL,
  `caneditf` text NOT NULL,
  `definfovoteid` smallint(6) NOT NULL DEFAULT '0',
  `showmod` tinyint(1) NOT NULL DEFAULT '0',
  `usemod` tinyint(1) NOT NULL DEFAULT '0',
  `myorder` smallint(6) NOT NULL DEFAULT '0',
  `orderf` text NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `listfile` varchar(30) NOT NULL DEFAULT '',
  `printtempid` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`),
  KEY `tid` (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsmod` */

insert  into `phome_enewsmod`(`mid`,`mname`,`mtemp`,`mzs`,`cj`,`enter`,`tempvar`,`sonclass`,`searchvar`,`tid`,`tbname`,`qenter`,`mustqenterf`,`qmtemp`,`listandf`,`setandf`,`listtempvar`,`qmname`,`canaddf`,`caneditf`,`definfovoteid`,`showmod`,`usemod`,`myorder`,`orderf`,`isdefault`,`listfile`,`printtempid`) values (1,'新闻系统模型','<table width=\\\'100%\\\' align=center cellpadding=3 cellspacing=1 class=\\\"tableborder\\\"><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>标题(*)</td><td bgcolor=\\\'ffffff\\\'>[!--title--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>副标题</td><td bgcolor=\\\'ffffff\\\'>[!--ftitle--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>特殊属性</td><td bgcolor=\\\'ffffff\\\'>[!--special.field--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>发布时间</td><td bgcolor=\\\'ffffff\\\'>[!--newstime--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>标题图片</td><td bgcolor=\\\'ffffff\\\'>[!--titlepic--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>内容简介</td><td bgcolor=\\\'ffffff\\\'>[!--smalltext--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>作者</td><td bgcolor=\\\'ffffff\\\'>[!--writer--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>信息来源</td><td bgcolor=\\\'ffffff\\\'>[!--befrom--]</td></tr><tr><td height=25 colspan=2 bgcolor=\\\'ffffff\\\'><div align=left>新闻正文(*)</div></td></tr></table><div style=\\\"background-color:#D0D0D0\\\">[!--newstext--]</div>','新闻系统模型','标题<!--field--->title<!--record-->副标题<!--field--->ftitle<!--record-->发布时间<!--field--->newstime<!--record-->标题图片<!--field--->titlepic<!--record-->内容简介<!--field--->smalltext<!--record-->作者<!--field--->writer<!--record-->信息来源<!--field--->befrom<!--record-->新闻正文<!--field--->newstext<!--record-->','标题<!--field--->title<!--record-->副标题<!--field--->ftitle<!--record-->特殊属性<!--field--->special.field<!--record-->发布时间<!--field--->newstime<!--record-->标题图片<!--field--->titlepic<!--record-->内容简介<!--field--->smalltext<!--record-->作者<!--field--->writer<!--record-->信息来源<!--field--->befrom<!--record-->新闻正文<!--field--->newstext<!--record-->','标题<!--field--->title<!--record-->副标题<!--field--->ftitle<!--record-->发布时间<!--field--->newstime<!--record-->标题图片<!--field--->titlepic<!--record-->内容简介<!--field--->smalltext<!--record-->作者<!--field--->writer<!--record-->信息来源<!--field--->befrom<!--record-->新闻正文<!--field--->newstext<!--record-->顶<!--field--->diggtop<!--record-->','|2|3|4|5|6|7|9|10|11|12|13|16|17|19|20|21|23|24|26|27|28|30|31|32|34|35|36|37|38|39|42|43|44|45|46|47|48|49|51|52|53|54|55|56|',',title,smalltext,',1,'news','标题<!--field--->title<!--record-->副标题<!--field--->ftitle<!--record-->特殊属性<!--field--->special.field<!--record-->标题图片<!--field--->titlepic<!--record-->内容简介<!--field--->smalltext<!--record-->作者<!--field--->writer<!--record-->信息来源<!--field--->befrom<!--record-->新闻正文<!--field--->newstext<!--record-->',',title,newstext,','<table width=100% align=center cellpadding=3 cellspacing=1 class=\\\"tableborder\\\"><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>标题(*)</td><td bgcolor=\\\'ffffff\\\'>[!--title--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>副标题</td><td bgcolor=\\\'ffffff\\\'>[!--ftitle--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>关键字</td><td bgcolor=\\\'ffffff\\\'>[!--special.field--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>标题图片</td><td bgcolor=\\\'ffffff\\\'>[!--titlepic--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>内容简介</td><td bgcolor=\\\'ffffff\\\'>[!--smalltext--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>作者</td><td bgcolor=\\\'ffffff\\\'>[!--writer--]</td></tr><tr><td width=\\\'16%\\\' height=25 bgcolor=\\\'ffffff\\\'>信息来源</td><td bgcolor=\\\'ffffff\\\'>[!--befrom--]</td></tr><tr><td height=25 colspan=2 bgcolor=\\\'ffffff\\\'><div align=left>新闻正文(*)</div></td></tr></table><div style=\\\"background-color:#D0D0D0\\\">[!--newstext--]</div>','',0,'标题<!--field--->title<!--record-->副标题<!--field--->ftitle<!--record-->发布时间<!--field--->newstime<!--record-->标题图片<!--field--->titlepic<!--record-->内容简介<!--field--->smalltext<!--record-->顶<!--field--->diggtop<!--record-->','新闻',',title,ftitle,newstime,titlepic,smalltext,writer,befrom,newstext,',',title,ftitle,newstime,titlepic,smalltext,writer,befrom,newstext,',0,0,0,0,'',1,'',0);

/*Table structure for table `phome_enewsnewstemp` */

DROP TABLE IF EXISTS `phome_enewsnewstemp`;

CREATE TABLE `phome_enewsnewstemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `temptext` mediumtext NOT NULL,
  `showdate` varchar(50) NOT NULL DEFAULT '',
  `modid` smallint(6) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsnewstemp` */

insert  into `phome_enewsnewstemp`(`tempid`,`tempname`,`isdefault`,`temptext`,`showdate`,`modid`,`classid`) values (1,'关于内容模板',0,'[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]1,1,0,0[/showclasstemp]\r\n		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		[!--newstext--]\r\n	</div>[!--temp.footer--]','Y-m-d H:i:s',1,0),(11,'地理位置内容模板',0,'[!--temp.header--]<script type=\\\"text/javascript\\\" src=\\\"http://api.map.baidu.com/api?key=&amp;v=1.1&amp;services=true\\\"></script>\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]1,1,0,0[/showclasstemp]\r\n		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		<div id=\\\"dituContent\\\"></div>\r\n	</div>[!--temp.footer--]\r\n<script type=\\\"text/javascript\\\">\r\n    //创建和初始化地图函数：\r\n    function initMap(){\r\n        createMap();//创建地图\r\n        setMapEvent();//设置地图事件\r\n        addMapControl();//向地图添加控件\r\n        addMarker();//向地图中添加marker\r\n    }\r\n    \r\n    //创建地图函数：\r\n    function createMap(){\r\n        var map = new BMap.Map(\\\"dituContent\\\");//在百度地图容器中创建一个地图\r\n        var point = new BMap.Point(120.654933,31.157809);//定义一个中心点坐标\r\n        map.centerAndZoom(point,13);//设定地图的中心点和坐标并将地图显示在地图容器中\r\n        window.map = map;//将map变量存储在全局\r\n    }\r\n    \r\n    //地图事件设置函数：\r\n    function setMapEvent(){\r\n        map.enableDragging();//启用地图拖拽事件，默认启用(可不写)\r\n		\r\n		//map.disableDragging();     //禁止拖拽\r\n        map.enableScrollWheelZoom();//启用地图滚轮放大缩小\r\n        map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)\r\n        map.enableKeyboard();//启用键盘上下左右键移动地图\r\n    }\r\n    \r\n    //地图控件添加函数：\r\n    function addMapControl(){\r\n        //向地图中添加缩放控件\r\n	var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});\r\n	map.addControl(ctrl_nav);\r\n        //向地图中添加缩略图控件\r\n	var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});\r\n	map.addControl(ctrl_ove);\r\n        //向地图中添加比例尺控件\r\n	var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});\r\n	map.addControl(ctrl_sca);\r\n    }\r\n    \r\n    //标注点数组\r\n    var markerArr = [{title:\\\"太湖校区\\\",content:\\\"联系方式：63452079\\\",point:\\\"120.634488|31.145772\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"爱德校区\\\",content:\\\"联系方式：\\\",point:\\\"120.640524|31.155314\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"城中校区\\\",content:\\\"联系方式：\\\",point:\\\"120.647333|31.168872\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ];\r\n    //创建marker\r\n    function addMarker(){\r\n        for(var i=0;i<markerArr.length;i++){\r\n            var json = markerArr[i];\r\n            var p0 = json.point.split(\\\"|\\\")[0];\r\n            var p1 = json.point.split(\\\"|\\\")[1];\r\n            var point = new BMap.Point(p0,p1);\r\n			var iconImg = createIcon(json.icon);\r\n            var marker = new BMap.Marker(point,{icon:iconImg});\r\n			var iw = createInfoWindow(i);\r\n			var label = new BMap.Label(json.title,{\\\"offset\\\":new BMap.Size(json.icon.lb-json.icon.x+10,-20)});\r\n			marker.setLabel(label);\r\n            map.addOverlay(marker);\r\n            label.setStyle({\r\n                        borderColor:\\\"#808080\\\",\r\n                        color:\\\"#333\\\",\r\n                        cursor:\\\"pointer\\\"\r\n            });\r\n			\r\n			(function(){\r\n				var index = i;\r\n				var _iw = createInfoWindow(i);\r\n				var _marker = marker;\r\n				_marker.addEventListener(\\\"click\\\",function(){\r\n				    this.openInfoWindow(_iw);\r\n			    });\r\n			    _iw.addEventListener(\\\"open\\\",function(){\r\n				    _marker.getLabel().hide();\r\n			    })\r\n			    _iw.addEventListener(\\\"close\\\",function(){\r\n				    _marker.getLabel().show();\r\n			    })\r\n				label.addEventListener(\\\"click\\\",function(){\r\n				    _marker.openInfoWindow(_iw);\r\n			    })\r\n				if(!!json.isOpen){\r\n					label.hide();\r\n					_marker.openInfoWindow(_iw);\r\n				}\r\n			})()\r\n        }\r\n    }\r\n    //创建InfoWindow\r\n    function createInfoWindow(i){\r\n        var json = markerArr[i];\r\n        var iw = new BMap.InfoWindow(\\\"<b class=\\\'iw_poi_title\\\' title=\\\'\\\" + json.title + \\\"\\\'>\\\" + json.title + \\\"</b><div class=\\\'iw_poi_content\\\'>\\\"+json.content+\\\"</div>\\\");\r\n        return iw;\r\n    }\r\n    //创建一个Icon\r\n    function createIcon(json){\r\n        var icon = new BMap.Icon(\\\"http://app.baidu.com/map/images/us_mk_icon.png\\\", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowOffset:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})\r\n        return icon;\r\n    }\r\n    \r\n    initMap();//创建和初始化地图\r\n</script>','Y-m-d H:i:s',1,0),(10,'新闻内容模板',0,'[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_news_d clear\\\">\r\n		<div class=\\\"D_news_d_title\\\">\r\n			<h2>[!--title--]</h2>\r\n			<div class=\\\"time\\\">\r\n				<span>发布时间：<span>[!--newstime--]</span></span>\r\n				<span>点击量：<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script>次</span></span>\r\n			</div>\r\n		</div>\r\n		<p>[!--newstext--]\r\n		</p><div class=\\\"sx\\\">上一篇：[!--info.pre--]<br>\r\n下一篇：[!--info.next--]</div>\r\n	</div>[!--temp.footer--]','Y-m-d H:i:s',1,0),(2,'默认下载内容模板',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"Keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>您当前的位置：[!--newsnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"2\\\" bgcolor=\\\"#FFFFFF\\\">\r\n							<tbody>\r\n								<tr>\r\n									<td width=\\\"78\\\" align=\\\"center\\\" nowrap=\\\"nowrap\\\" bgcolor=\\\"#E1EFFB\\\"><b>软件名称：</b></td>\r\n									<td colspan=\\\"2\\\" bgcolor=\\\"#E1EFFB\\\"><b>[!--title--]</b></td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\"><strong>文件类型：</strong></td>\r\n									<td width=\\\"296\\\">[!--filetype--]</td>\r\n									<td width=\\\"180\\\" rowspan=\\\"9\\\" align=\\\"center\\\"><a href=\\\"[!--titlepic--]\\\" target=\\\"_blank\\\"><img src=\\\"[!--titlepic--]\\\" border=\\\"0\\\" width=\\\"128\\\" /></a></td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" bgcolor=\\\"#F4F9FD\\\"><strong>界面语言：</strong></td>\r\n									<td bgcolor=\\\"#F4F9FD\\\">[!--language--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\"><strong>软件类型：</strong></td>\r\n									<td>[!--softtype--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" bgcolor=\\\"#F4F9FD\\\"><strong>运行环境：</strong></td>\r\n									<td bgcolor=\\\"#F4F9FD\\\">[!--softfj--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\"><strong>授权方式：</strong></td>\r\n									<td>[!--softsq--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" bgcolor=\\\"#F4F9FD\\\"><strong>软件大小：</strong></td>\r\n									<td bgcolor=\\\"#F4F9FD\\\">[!--filesize--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\"><strong>软件等级：</strong></td>\r\n									<td><img src=\\\"[!--news.url--]e/data/images/[!--star--]star.gif\\\" border=\\\"0\\\" /></td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" bgcolor=\\\"#F4F9FD\\\"><strong>软件登陆：</strong></td>\r\n									<td bgcolor=\\\"#F4F9FD\\\">[!--username--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\"><strong>作 者 ：</strong></td>\r\n									<td>[!--softwriter--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" bgcolor=\\\"#F4F9FD\\\"><strong>官方网址：</strong></td>\r\n									<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\"><a href=\\\'[!--homepage--]\\\' target=\\\"_blank\\\">官方站</a></td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\"><strong>程序演示：</strong></td>\r\n									<td colspan=\\\"2\\\"><a href=\\\'[!--demo--]\\\' target=\\\"_blank\\\">演示</a></td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" bgcolor=\\\"#F4F9FD\\\"><strong>整理时间：</strong></td>\r\n									<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\">[!--newstime--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\"><strong>软件简介：</strong></td>\r\n									<td \r\n            colspan=\\\"2\\\"> [!--softsay--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td width=\\\"78\\\" align=\\\"center\\\" nowrap=\\\"nowrap\\\" bgcolor=\\\"#F4F9FD\\\"><b><font color=\\\"#008E00\\\">下载地址：</font></b></td>\r\n									<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\"> [!--downpath--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" nowrap=\\\"nowrap\\\"><strong>下载帮助：</strong></td>\r\n									<td colspan=\\\"2\\\"><img src=\\\"[!--news.url--]e/data/images/gonggao.gif\\\" border=\\\"0\\\" /><a href=\\\"[!--news.url--]e/pl/?classid=[!--classid--]&amp;id=[!--id--]\\\">发表评论</a> <img src=\\\"[!--news.url--]e/data/images/gonggao.gif\\\" border=\\\"0\\\" /><a href=\\\"[!--news.url--]e/member/fava/add/?classid=[!--classid--]&amp;id=[!--id--]\\\">加入收藏夹</a> <img src=\\\"[!--news.url--]e/data/images/gonggao.gif\\\" border=\\\"0\\\" /><a href=\\\"[!--news.url--]e/public/report/?classid=[!--classid--]&amp;id=[!--id--]\\\" target=\\\"_blank\\\">错误报告</a></td>\r\n								</tr>\r\n								<tr>\r\n									<td width=\\\"78\\\" align=\\\"center\\\" valign=\\\"top\\\" nowrap=\\\"nowrap\\\" bgcolor=\\\"#F4F9FD\\\"><b><font color=\\\"#008E0\\\">相关软件：</font></b></td>\r\n									<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\">[!--other.link--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\\\"center\\\" valign=\\\"center\\\" nowrap=\\\"nowrap\\\"><strong>下载说明：</strong></td>\r\n									<td colspan=\\\"2\\\">⊙推荐使用网际快车下载本站软件，使用 WinRAR v3.10 以上版本解压本站软件。<br />\r\n										⊙如果这个软件总是不能下载的请点击报告错误,谢谢合作!!<br />\r\n										⊙下载本站资源，如果服务器暂不能下载请过一段时间重试！<br />\r\n										⊙如果遇到什么问题，请到<a href=\\\"http://www.phome.net\\\" target=\\\"_blank\\\">本站论坛</a>去咨寻，我们将在那里提供更多\r\n										、更好的资源！<br />\r\n										⊙本站提供的一些商业软件是供学习研究之用，如用于商业用途，请购买正版。 </td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n				</tr>\r\n			</table>\r\n			[!--temp.pl--]</td>\r\n		<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n				<tr>\r\n					<td><strong>推荐下载</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_goodnews.js\'></script></ul></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n				<tr>\r\n					<td><strong>最后更新</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_newnews.js\'></script></ul></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n				<tr>\r\n					<td><strong>热门点击</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_hotnews.js\'></script></ul></td>\r\n				</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--] [!--page.stats--]\r\n</body>\r\n</html>','Y-m-d',2,0),(3,'默认图片内容模板(单图片)',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"Keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>您当前的位置：[!--newsnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"4\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#FFFFFF\\\">\r\n							<tr>\r\n								<td bgcolor=\\\"#E1EFFB\\\">&nbsp;&nbsp;<strong>[!--title--]</strong></td>\r\n							</tr>\r\n							<tr>\r\n								<td><table width=\\\"0\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n										<tr>\r\n											<td width=\\\"25\\\"><img src=\\\"[!--news.url--]e/data/images/mod/center_1.gif\\\" width=\\\"25\\\" height=\\\"24\\\" /></td>\r\n											<td background=\\\"[!--news.url--]e/data/images/mod/box_1.gif\\\">&nbsp;</td>\r\n											<td width=\\\"25\\\"><img src=\\\"[!--news.url--]e/data/images/mod/center_2.gif\\\" width=\\\"25\\\" height=\\\"24\\\" /></td>\r\n										</tr>\r\n										<tr>\r\n											<td background=\\\"[!--news.url--]e/data/images/mod/box_4.gif\\\">&nbsp;</td>\r\n											<td bgcolor=\\\"#F3F3F3\\\"><a href=\\\"[!--news.url--]e/ViewImg/index.html?url=[!--picurl--]\\\" target=\\\"_blank\\\"><img border=\\\"0\\\" src=\\\"[!--picurl--]\\\" class=\\\"photoresize\\\" /></a></td>\r\n											<td background=\\\"[!--news.url--]e/data/images/mod/box_2.gif\\\">&nbsp;</td>\r\n										</tr>\r\n										<tr>\r\n											<td><img src=\\\"[!--news.url--]e/data/images/mod/center_3.gif\\\" width=\\\"25\\\" height=\\\"24\\\" /></td>\r\n											<td background=\\\"[!--news.url--]e/data/images/mod/box_3.gif\\\">&nbsp;</td>\r\n											<td><img src=\\\"[!--news.url--]e/data/images/mod/center_4.gif\\\" width=\\\"25\\\" height=\\\"24\\\" /></td>\r\n										</tr>\r\n								</table></td>\r\n							</tr>\r\n							<tr>\r\n								<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"4\\\" cellspacing=\\\"1\\\">\r\n										<tr>\r\n											<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\">&nbsp;&nbsp;<strong>图片资料</strong></td>\r\n										</tr>\r\n										<tr>\r\n											<td width=\\\"80\\\">图片名称：</td>\r\n											<td width=\\\"462\\\"> [!--title--]</td>\r\n										</tr>\r\n										<tr>\r\n											<td bgcolor=\\\"#F8F8F8\\\">人气：</td>\r\n											<td bgcolor=\\\"#F8F8F8\\\"> [!--onclick--]</td>\r\n										</tr>\r\n										<tr>\r\n											<td>图片尺寸：</td>\r\n											<td> [!--picsize--]</td>\r\n										</tr>\r\n										<tr>\r\n											<td bgcolor=\\\"#F8F8F8\\\">图片大小：</td>\r\n											<td bgcolor=\\\"#F8F8F8\\\"> [!--filesize--]</td>\r\n										</tr>\r\n										<tr>\r\n											<td>编辑：</td>\r\n											<td> [!--username--]</td>\r\n										</tr>\r\n										<tr>\r\n											<td bgcolor=\\\"#F8F8F8\\\">创建日期：</td>\r\n											<td bgcolor=\\\"#F8F8F8\\\"> [!--newstime--]</td>\r\n										</tr>\r\n										<tr>\r\n											<td valign=\\\"top\\\">简介：</td>\r\n											<td valign=\\\"top\\\">[!--picsay--]</td>\r\n										</tr>\r\n								</table></td>\r\n							</tr>\r\n												</table>						</td>\r\n				</tr>\r\n			</table>\r\n			[!--temp.pl--]</td>\r\n		<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n				<tr>\r\n					<td><strong>图片推荐</strong></td>\r\n				</tr>\r\n			</table>\r\n				<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>最后更新</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_newnews.js\'></script> </ul></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>热门点击</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_hotnews.js\'></script></ul></td>\r\n					</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--] [!--page.stats--]\r\n</body>\r\n</html>','Y-m-d H:i:s',3,0),(4,'默认图片内容模板(图片集)',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"Keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>您当前的位置：[!--newsnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"4\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#FFFFFF\\\">\r\n						<tr>\r\n							<td align=\\\"center\\\" bgcolor=\\\"#E1EFFB\\\"><strong>[!--title--]</strong></td>\r\n						</tr>\r\n						<tr>\r\n							<td align=\\\"center\\\">[eshowphoto]11,0,0[/eshowphoto]</td>\r\n						</tr>\r\n						<tr>\r\n							<td>&nbsp;</td>\r\n						</tr>\r\n					</table></td>\r\n				</tr>\r\n			</table>\r\n			[!--temp.pl--]</td>\r\n		<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n				<tr>\r\n					<td><strong>图片推荐</strong></td>\r\n				</tr>\r\n			</table>\r\n				<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>最后更新</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_newnews.js\'></script> </ul></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>热门点击</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_hotnews.js\'></script></ul></td>\r\n					</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--] [!--page.stats--]\r\n</body>\r\n</html>','Y-m-d H:i:s',3,0),(5,'默认FLASH内容模板',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"Keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage news\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>您当前的位置：[!--newsnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"title_info\\\">\r\n							<tr>\r\n								<td><h1>[!--title--]</h1></td>\r\n							</tr>\r\n							<tr>\r\n								<td class=\\\"info_text\\\">时间：[!--newstime--]&nbsp;&nbsp;作者：[!--flashwriter--]&nbsp;&nbsp;大小：[!--filesize--]</td>\r\n							</tr>\r\n						</table>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\">\r\n							<tr>\r\n								<td id=\\\"text\\\"><p align=\\\"center\\\">\r\n										<object classid=\\\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\\\" codebase=\\\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0\\\" name=\\\"top10movie\\\" width=\\\"[!--width--]\\\" height=\\\"[!--height--]\\\" id=\\\"top10movie\\\">\r\n											<param name=\\\"movie\\\" value=\\\"[!--flashurl--]\\\" />\r\n											<param name=\\\"quality\\\" value=\\\"high\\\" />\r\n											<param name=\\\"menu\\\" value=\\\"false\\\" />\r\n											<embed src=\\\"[!--flashurl--]\\\" width=\\\"[!--width--]\\\" height=\\\"[!--height--]\\\" quality=\\\"high\\\" pluginspage=\\\"http://www.macromedia.com/go/getflashplayer\\\" type=\\\"application/x-shockwave-flash\\\" id=\\\"top10movie\\\" name=\\\"top10movie\\\" menu=\\\"false\\\"></embed>\r\n										</object>\r\n									</p>\r\n									<p><strong>作品简介：</strong>[!--flashsay--]</p></td>\r\n							</tr>\r\n						</table></td>\r\n				</tr>\r\n			</table>\r\n			[!--temp.pl--]</td>\r\n		<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n				<tr>\r\n					<td><strong>推荐下载</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n				<tr>\r\n					<td><strong>最后更新</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_newnews.js\'></script></ul></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n				<tr>\r\n					<td><strong>热门点击</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_hotnews.js\'></script></ul></td>\r\n				</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--] [!--page.stats--]\r\n</body>\r\n</html>','Y-m-d H:i:s',4,0),(6,'默认电影内容模板',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"Keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>您当前的位置：[!--newsnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"2\\\">\r\n						<tr>\r\n							<td width=\\\"16%\\\"><div align=\\\"center\\\">[影片名称]</div></td>\r\n							<td width=\\\"45%\\\"><strong>[!--title--]</strong></td>\r\n							<td width=\\\"39%\\\" rowspan=\\\"10\\\" align=\\\"center\\\" valign=\\\"top\\\" bgcolor=\\\"#F4F9FD\\\" class=\\\"photo\\\"><a href=\\\"[!--titlepic--]\\\" title=\\\"查看原图\\\" target=\\\"_blank\\\"><img src=\\\"[!--titlepic--]\\\" width=\\\"205\\\" height=\\\"278\\\" border=\\\"0\\\" align=\\\"top\\\" /></a></td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor=\\\"#F4F9FD\\\"><div align=\\\"center\\\">[领衔主演]</div></td>\r\n							<td bgcolor=\\\"#F4F9FD\\\">[!--player--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td><div align=\\\"center\\\">[影片类型]</div></td>\r\n							<td>[!--movietype--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor=\\\"#F4F9FD\\\"><div align=\\\"center\\\">[带宽要求]</div></td>\r\n							<td bgcolor=\\\"#F4F9FD\\\">[!--playdk--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td><div align=\\\"center\\\">\r\n									<p>[影片长度]</p>\r\n							</div></td>\r\n							<td>[!--playtime--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor=\\\"#F4F9FD\\\"><div align=\\\"center\\\">[文件大小]</div></td>\r\n							<td bgcolor=\\\"#F4F9FD\\\">[!--filesize--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td><div align=\\\"center\\\">[推荐级别]</div></td>\r\n							<td><img src=\\\"[!--news.url--]e/data/images/[!--star--]star.gif\\\" border=\\\"0\\\" /></td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor=\\\"#F4F9FD\\\"><div align=\\\"center\\\">[上传时间]</div></td>\r\n							<td bgcolor=\\\"#F4F9FD\\\">[!--movietime--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td><div align=\\\"center\\\">[下载次数]</div></td>\r\n							<td><script src=\\\"[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&amp;id=[!--id--]&amp;down=1\\\"></script></td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor=\\\"#F4F9FD\\\"><div align=\\\"center\\\">[文件格式]</div></td>\r\n							<td bgcolor=\\\"#F4F9FD\\\">[!--filetype--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td><div align=\\\"center\\\">[在线观看]</div></td>\r\n							<td colspan=\\\"2\\\">[!--onlinepath--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor=\\\"#F4F9FD\\\"><div align=\\\"center\\\">[下载影片]</div></td>\r\n							<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\">[!--downpath--]</td>\r\n						</tr>\r\n						<tr>\r\n							<td><div align=\\\"center\\\">[下载帮助]</div></td>\r\n							<td colspan=\\\"2\\\"><img src=\\\"[!--news.url--]e/data/images/gonggao.gif\\\" border=\\\"0\\\" /><a href=\\\"[!--news.url--]e/pl/?classid=[!--classid--]&amp;id=[!--id--]\\\">发表评论</a> <img src=\\\"[!--news.url--]e/data/images/gonggao.gif\\\" border=\\\"0\\\" /><a href=\\\"[!--news.url--]e/member/fava/add/?classid=[!--classid--]&amp;id=[!--id--]\\\">加入收藏夹</a> <img src=\\\"[!--news.url--]e/data/images/gonggao.gif\\\" border=\\\"0\\\" /><a href=\\\"[!--news.url--]e/public/report/?classid=[!--classid--]&amp;id=[!--id--]\\\" target=\\\"_blank\\\">错误报告</a></td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor=\\\"#F4F9FD\\\"><div align=\\\"center\\\">[影片介绍]</div></td>\r\n							<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\">&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td colspan=\\\"3\\\" valign=\\\"top\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"4\\\" style=\\\"table-layout:  fixed;  word-wrap:  break-word\\\">\r\n									<tr>\r\n										<td>[!--moviesay--]</td>\r\n									</tr>\r\n							</table></td>\r\n						</tr>\r\n					</table></td>\r\n				</tr>\r\n			</table>\r\n			[!--temp.pl--]</td>\r\n		<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n				<tr>\r\n					<td><strong>影视推荐</strong></td>\r\n				</tr>\r\n			</table>\r\n				<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>最后更新</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_newnews.js\'></script></ul></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>热门点击</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_hotnews.js\'></script></ul></td>\r\n					</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--] [!--page.stats--]\r\n</body>\r\n</html>','Y-m-d',5,0),(7,'默认商城内容模板',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"Keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>您当前的位置：[!--newsnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"2\\\">\r\n						<tr>\r\n							<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\">&nbsp;&nbsp;<strong>商品基本信息</strong></td>\r\n						</tr>\r\n						<tr>\r\n							<td width=\\\"30%\\\" align=\\\"center\\\" class=\\\"titlepic\\\"><a href=\\\"[!--productpic--]\\\" title=\\\"预览大图\\\" target=\\\"_blank\\\"><img height=\\\"128\\\" src=\\\"[!--titlepic--]\\\" width=\\\"128\\\" border=\\\"0\\\" /></a></td>\r\n							<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"2\\\">\r\n								<tr>\r\n									<td style=\\\"font-size: 14px;\\\"><strong>商品名称：[!--title--]</strong></td>\r\n								</tr>\r\n								<tr>\r\n									<td>市场价：<span class=\\\"tprice\\\">￥[!--tprice--]</span>&nbsp;优惠价：<span class=\\\"price\\\">￥[!--price--]</span>&nbsp; \r\n										点数: [!--buyfen--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td>商品编号:[!--productno--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td>商品品牌:[!--pbrand--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td>计量单位:[!--unit--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td>商品重量:[!--weight--]</td>\r\n								</tr>\r\n								<tr>\r\n									<td>[<a href=\\\"#ecms\\\" onclick=\\\"window.open(\\\'[!--news.url--]e/ShopSys/doaction.php?enews=AddBuycar&amp;classid=[!--classid--]&amp;id=[!--id--]\\\',\\\'\\\',\\\'width=680,height=500,scrollbars=yes,resizable=yes\\\');\\\">加入购物车</a>]&nbsp;&nbsp;&nbsp;&nbsp;[<a href=\\\"[!--news.url--]e/member/fava/add/?classid=[!--classid--]&amp;id=[!--id--]\\\" target=\\\"_blank\\\">放入收藏夹</a>]&nbsp;&nbsp;&nbsp;&nbsp;[<a href=\\\"[!--news.url--]e/pl/?classid=[!--classid--]&amp;id=[!--id--]\\\" target=\\\"_blank\\\">查看评论</a>]</td>\r\n								</tr>\r\n							</table></td>\r\n						</tr>\r\n						<tr>\r\n							<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\">&nbsp;&nbsp;<strong>商品介绍</strong></td>\r\n						</tr>\r\n						<tr>\r\n							<td colspan=\\\"2\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"8\\\">\r\n									<tr>\r\n										<td id=\\\"text\\\">[!--newstext--]</td>\r\n									</tr>\r\n							</table></td>\r\n						</tr>\r\n					</table></td>\r\n				</tr>\r\n			</table>\r\n			[!--temp.pl--]</td>\r\n		<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n				<tr>\r\n					<td><strong>商品推荐</strong></td>\r\n				</tr>\r\n			</table>\r\n				<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td>[phomenewspic]\\\'selfinfo\\\',2,4,128,90,1,20,2[/phomenewspic] </td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>最后更新</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_newnews.js\'></script></ul></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n					<tr>\r\n						<td><strong>热门点击</strong></td>\r\n					</tr>\r\n				</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n					<tr>\r\n						<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_hotnews.js\'></script></ul></td>\r\n					</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--] [!--page.stats--]\r\n</body>\r\n</html>','Y-m-d',6,0),(8,'默认文章内容模板',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]e/data/js/ajax.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage news\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>您当前的位置：[!--newsnav--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"title_info\\\">\r\n<tr>\r\n<td><h1>[!--title--]</h1></td>\r\n</tr>\r\n<tr>\r\n<td class=\\\"info_text\\\">时间：[!--newstime--]&nbsp;&nbsp;来源：[!--befrom--]&nbsp;&nbsp;作者：[!--writer--]</td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\">\r\n<tr>\r\n<td id=\\\"text\\\">[!--newstext--]\r\n<p align=\\\"center\\\" class=\\\"pageLink\\\">[!--page.url--]</p></td>\r\n</tr>\r\n</table>\r\n<table border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"0\\\" cellspacing=\\\"8\\\">\r\n<tr>\r\n<td><table border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"digg\\\">\r\n<tr>\r\n<td class=\\\"diggnum\\\" id=\\\"diggnum\\\"><strong><script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&down=5\\\"></script></strong></td>\r\n</tr>\r\n<tr>\r\n<td class=\\\"diggit\\\"><a href=\\\"JavaScript:makeRequest(\\\'[!--news.url--]e/public/digg/?classid=[!--classid--]&id=[!--id--]&dotop=1&doajax=1&ajaxarea=diggnum\\\',\\\'EchoReturnedText\\\',\\\'GET\\\',\\\'\\\');\\\">来顶一下</a></td>\r\n</tr>\r\n</table></td>\r\n<td><table border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"digg\\\">\r\n<tr>\r\n<td valign=\\\"middle\\\" class=\\\"diggnum\\\"><strong><a href=\\\"[!--news.url--]\\\"><img src=\\\"[!--news.url--]skin/default/images/back.gif\\\" alt=\\\"返回首页\\\" width=\\\"12\\\" height=\\\"13\\\" border=\\\"0\\\" align=\\\"absmiddle\\\" /></a></strong></td>\r\n</tr>\r\n<tr>\r\n<td class=\\\"diggit\\\"><a href=\\\"[!--news.url--]\\\">返回首页</a></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n</td>\r\n</tr>\r\n</table>\r\n[!--temp.pl--]\r\n</td>\r\n<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n<tr>\r\n<td><strong>推荐资讯</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"8\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n          <td>[phomenewspic]\\\'article\\\',2,4,128,90,1,20,20[/phomenewspic] </td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>相关文章</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n[!--other.link--]\r\n</ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>栏目更新</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_newnews.js\'></script></ul></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n<tr>\r\n<td><strong>栏目热门</strong></td>\r\n</tr>\r\n</table>\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n<tr>\r\n<td><ul>\r\n              <script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_hotnews.js\'></script></ul></td>\r\n</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n[!--page.stats--]\r\n</body>\r\n</html>','Y-m-d H:i:s',7,0),(9,'默认分类信息内容模板',0,'<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"Keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"showpage info\\\">\r\n[!--temp.header--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"main\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>您当前的位置：[!--newsnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" class=\\\"title_info\\\">\r\n							<tr>\r\n								<td colspan=\\\"2\\\"><h1>[!--title--]</h1></td>\r\n							</tr>\r\n							<tr>\r\n								<td class=\\\"info_text\\\">所在地区：<a href=\\\"[!--news.url--]e/action/ListInfo.php?classid=[!--classid--]&amp;ph=1&amp;myarea=[!--myarea--]\\\">[!--myarea--]</a>&nbsp;&nbsp;</td>\r\n								<td align=\\\"right\\\" class=\\\"info_text\\\">时间：[!--newstime--]&nbsp;&nbsp;【<a \r\nhref=\\\"[!--news.url--]e/member/fava/add/?classid=[!--classid--]&amp;id=[!--id--]\\\">加入收藏夹</a>】【<a href=\\\"[!--news.url--]e/public/report/?classid=[!--classid--]&amp;id=[!--id--]\\\" target=\\\"_blank\\\">举报</a>】【<a href=\\\"javascript:window.close()\\\">关闭</a>】</td>\r\n							</tr>\r\n						</table>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\">\r\n							<tr>\r\n								<td id=\\\"text\\\"><p><a href=\\\"[!--titlepic--]\\\" title=\\\"点击查看原图\\\" target=\\\"_blank\\\"><img src=\\\"[!--titlepic--]\\\" hspace=\\\"6\\\" border=\\\"0\\\" align=\\\"left\\\" /></a>[!--smalltext--]</p></td>\r\n							</tr>\r\n						</table>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"4\\\" cellspacing=\\\"0\\\" bgcolor=\\\"#FFFFFF\\\">\r\n							<tr>\r\n								<td bgcolor=\\\"#F4F9FD\\\">&nbsp;&nbsp;<strong>联系方式</strong></td>\r\n							</tr>\r\n						</table>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#F4F9FD\\\">\r\n							<tr>\r\n								<td width=\\\"13%\\\" bgcolor=\\\"#FFFFFF\\\"><div align=\\\"right\\\">发 布 者：</div></td>\r\n								\r\n                <td width=\\\"88%\\\" bgcolor=\\\"#FFFFFF\\\">[!--linkusername--]&nbsp;(<a href=\\\"[!--news.url--]e/member/msg/AddMsg/?enews=AddMsg&amp;re=1&amp;username=[!--username--]\\\" target=\\\"_blank\\\">发送站内信息</a>)</td>\r\n							</tr>\r\n							<tr>\r\n								<td bgcolor=\\\"#FFFFFF\\\"><div align=\\\"right\\\">联系邮箱：</div></td>\r\n								<td bgcolor=\\\"#FFFFFF\\\"><a href=\\\"mailto:[!--email--]\\\">[!--email--]</a></td>\r\n							</tr>\r\n							<tr>\r\n								<td bgcolor=\\\"#FFFFFF\\\"><div align=\\\"right\\\">联系方式：</div></td>\r\n								<td bgcolor=\\\"#FFFFFF\\\">[!--mycontact--]</td>\r\n							</tr>\r\n							<tr>\r\n								<td bgcolor=\\\"#FFFFFF\\\"><div align=\\\"right\\\">联系地址：</div></td>\r\n								<td bgcolor=\\\"#FFFFFF\\\">[!--address--]</td>\r\n							</tr>\r\n						</table>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"4\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#FFFFFF\\\">\r\n							<tr>\r\n								<td>&nbsp;</td>\r\n							</tr>\r\n							<tr>\r\n								<td bgcolor=\\\"#F8F8F8\\\">&nbsp;&nbsp;<strong>推荐给朋友</strong></td>\r\n							</tr>\r\n							<tr>\r\n								<td><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\">\r\n										<tr>\r\n											<td width=\\\"2%\\\">&nbsp;</td>\r\n											<td><script>\r\nfunction sendtof(url)\r\n{\r\nwindow.clipboardData.setData(\\\'Text\\\',url);\r\nalert(\\\'复制地址成功，粘贴给你好友一起分享。\\\');\r\n}\r\n  			</script>\r\n												<input name=\\\"textfield\\\" type=\\\"text\\\" value=\\\"http://域名[!--titleurl--]\\\" size=\\\"60\\\" onclick=\\\"sendtof(this.value);\\\" /></td>\r\n										</tr>\r\n									</table></td>\r\n							</tr>\r\n						</table>\r\n						\r\n          </td>\r\n				</tr>\r\n			</table>\r\n			[!--temp.pl--]</td>\r\n		<td class=\\\"sider\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title\\\">\r\n				<tr>\r\n					<td><strong>地区导航</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td>[!--temp.infoarea--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n				<tr>\r\n					<td><strong>分类导航</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td>[!--temp.infoclassnav--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"title margin_top\\\">\r\n				<tr>\r\n					<td><strong>推荐信息</strong></td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><ul>\r\n							<script src=\'[!--news.url--]d/js/class/class[!--self.classid--]_goodnews.js\'></script>\r\n						</ul></td>\r\n				</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--] [!--page.stats--]\r\n</body>\r\n</html>','Y年m月d日 H:i:s',8,0),(12,'城中分校内容模板',0,'[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n<div class=\\\"c_title detail_d_newstitle clear\\\">\r\n				<span>[!--class.name--]</span>\r\n			</div>\r\n			<div class=\\\"c_info detail_d_info\\\">\r\n				<ul>\r\n					<li class=\\\"c_info_bg c_info_bg_dtl\\\"></li></ul></div>\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--title--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span><p>[!--newstext--]</p> </span>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]','Y-m-d',1,0),(13,'爱德校区详细页',0,'[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]','Y-m-d',1,0),(14,'太湖校区详细页',0,'[!--temp.thheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]40,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]','Y-m-d',1,0),(15,'苏州湾校区详细页',0,'[!--temp.szwheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.szwfooter--]','Y-m-d',1,0);

/*Table structure for table `phome_enewsnewstempclass` */

DROP TABLE IF EXISTS `phome_enewsnewstempclass`;

CREATE TABLE `phome_enewsnewstempclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsnewstempclass` */

/*Table structure for table `phome_enewsnotcj` */

DROP TABLE IF EXISTS `phome_enewsnotcj`;

CREATE TABLE `phome_enewsnotcj` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `word` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsnotcj` */

insert  into `phome_enewsnotcj`(`id`,`word`) values (1,'<input type=hidden value=\\\'欢迎使用网站管理系统：www.phome.net\\\'>'),(2,'<phome 网站管理系统,phome.net>'),(3,'<!--CMS,phome.net-->'),(4,'<table style=display=none><tr><td>\r\nEmpire CMS,phome.net\r\n</td></tr></table>'),(5,'<div style=display=none>\r\n拥有一切，皆有可能。欢迎访问phome.net\r\n</div>');

/*Table structure for table `phome_enewsnotice` */

DROP TABLE IF EXISTS `phome_enewsnotice`;

CREATE TABLE `phome_enewsnotice` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL DEFAULT '',
  `msgtext` text NOT NULL,
  `haveread` tinyint(1) NOT NULL DEFAULT '0',
  `msgtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to_username` varchar(30) NOT NULL DEFAULT '',
  `from_userid` int(10) unsigned NOT NULL DEFAULT '0',
  `from_username` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`mid`),
  KEY `to_username` (`to_username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsnotice` */

/*Table structure for table `phome_enewspage` */

DROP TABLE IF EXISTS `phome_enewspage`;

CREATE TABLE `phome_enewspage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL DEFAULT '',
  `pagetext` mediumtext NOT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pagetitle` varchar(120) NOT NULL DEFAULT '',
  `pagekeywords` varchar(255) NOT NULL DEFAULT '',
  `pagedescription` varchar(255) NOT NULL DEFAULT '',
  `tempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspage` */

/*Table structure for table `phome_enewspageclass` */

DROP TABLE IF EXISTS `phome_enewspageclass`;

CREATE TABLE `phome_enewspageclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspageclass` */

/*Table structure for table `phome_enewspagetemp` */

DROP TABLE IF EXISTS `phome_enewspagetemp`;

CREATE TABLE `phome_enewspagetemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(30) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  PRIMARY KEY (`tempid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspagetemp` */

/*Table structure for table `phome_enewspayapi` */

DROP TABLE IF EXISTS `phome_enewspayapi`;

CREATE TABLE `phome_enewspayapi` (
  `payid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `paytype` varchar(20) NOT NULL DEFAULT '',
  `myorder` tinyint(4) NOT NULL DEFAULT '0',
  `payfee` varchar(10) NOT NULL DEFAULT '',
  `payuser` varchar(60) NOT NULL DEFAULT '',
  `partner` varchar(60) NOT NULL DEFAULT '',
  `paykey` varchar(120) NOT NULL DEFAULT '',
  `paylogo` varchar(200) NOT NULL DEFAULT '',
  `paysay` text NOT NULL,
  `payname` varchar(120) NOT NULL DEFAULT '',
  `isclose` tinyint(1) NOT NULL DEFAULT '0',
  `payemail` varchar(120) NOT NULL DEFAULT '',
  `paymethod` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payid`),
  UNIQUE KEY `paytype` (`paytype`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspayapi` */

insert  into `phome_enewspayapi`(`payid`,`paytype`,`myorder`,`payfee`,`payuser`,`partner`,`paykey`,`paylogo`,`paysay`,`payname`,`isclose`,`payemail`,`paymethod`) values (1,'tenpay',1,'0','','','','','财付通（www.tenpay.com） - 腾讯旗下在线支付平台，通过国家权威安全认证，支持各大银行网上支付。','财付通',0,'',0),(2,'chinabank',2,'0','','','','','网银在线与中国工商银行、招商银行、中国建设银行、农业银行、民生银行等数十家金融机构达成协议。全面支持全国19家银行的信用卡及借记卡实现网上支付。（网址：http://www.chinabank.com.cn）','网银在线',0,'',0),(3,'alipay',0,'0','','','','','支付宝网站(www.alipay.com) 是国内先进的网上支付平台。','支付宝接口',0,'',1);

/*Table structure for table `phome_enewspayrecord` */

DROP TABLE IF EXISTS `phome_enewspayrecord`;

CREATE TABLE `phome_enewspayrecord` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `orderid` varchar(50) NOT NULL DEFAULT '',
  `money` varchar(20) NOT NULL DEFAULT '',
  `posttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `paybz` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(12) NOT NULL DEFAULT '',
  `payip` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`,`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspayrecord` */

/*Table structure for table `phome_enewspic` */

DROP TABLE IF EXISTS `phome_enewspic`;

CREATE TABLE `phome_enewspic` (
  `picid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL DEFAULT '',
  `pic_url` varchar(200) NOT NULL DEFAULT '',
  `url` varchar(200) NOT NULL DEFAULT '',
  `pic_width` varchar(20) NOT NULL DEFAULT '',
  `pic_height` varchar(20) NOT NULL DEFAULT '',
  `open_pic` varchar(20) NOT NULL DEFAULT '',
  `border` tinyint(1) NOT NULL DEFAULT '0',
  `pictext` text NOT NULL,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`picid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspic` */

/*Table structure for table `phome_enewspicclass` */

DROP TABLE IF EXISTS `phome_enewspicclass`;

CREATE TABLE `phome_enewspicclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspicclass` */

insert  into `phome_enewspicclass`(`classid`,`classname`) values (1,'默认图片信息类别');

/*Table structure for table `phome_enewspl_1` */

DROP TABLE IF EXISTS `phome_enewspl_1`;

CREATE TABLE `phome_enewspl_1` (
  `plid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pubid` bigint(16) NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `sayip` varchar(20) NOT NULL DEFAULT '',
  `saytime` int(10) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `zcnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `fdnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `saytext` text NOT NULL,
  PRIMARY KEY (`plid`),
  KEY `id` (`id`),
  KEY `classid` (`classid`),
  KEY `pubid` (`pubid`,`checked`,`plid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspl_1` */

/*Table structure for table `phome_enewspl_set` */

DROP TABLE IF EXISTS `phome_enewspl_set`;

CREATE TABLE `phome_enewspl_set` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `pltime` smallint(5) unsigned NOT NULL DEFAULT '0',
  `plsize` smallint(5) unsigned NOT NULL DEFAULT '0',
  `plincludesize` smallint(5) unsigned NOT NULL DEFAULT '0',
  `plkey_ok` tinyint(1) NOT NULL DEFAULT '0',
  `plface` text NOT NULL,
  `plfacenum` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `plgroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `plclosewords` text NOT NULL,
  `plf` text NOT NULL,
  `plmustf` text NOT NULL,
  `pldatatbs` text NOT NULL,
  `defpltempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pl_num` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pldeftb` smallint(5) unsigned NOT NULL DEFAULT '0',
  `plurl` varchar(200) NOT NULL DEFAULT '',
  `plmaxfloor` smallint(5) unsigned NOT NULL DEFAULT '0',
  `plquotetemp` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspl_set` */

insert  into `phome_enewspl_set`(`id`,`pltime`,`plsize`,`plincludesize`,`plkey_ok`,`plface`,`plfacenum`,`plgroupid`,`plclosewords`,`plf`,`plmustf`,`pldatatbs`,`defpltempid`,`pl_num`,`pldeftb`,`plurl`,`plmaxfloor`,`plquotetemp`) values (1,20,500,0,1,'||[~e.jy~]##1.gif||[~e.kq~]##2.gif||[~e.se~]##3.gif||[~e.sq~]##4.gif||[~e.lh~]##5.gif||[~e.ka~]##6.gif||[~e.hh~]##7.gif||[~e.ys~]##8.gif||[~e.ng~]##9.gif||[~e.ot~]##10.gif||',10,0,'','','',',1,',1,12,1,'/e/pl/',0,'<div class=\\\"ecomment\\\">\r\n<span class=\\\"ecommentauthor\\\">网友 [!--username--] 的原文：</span>\r\n<p class=\\\"ecommenttext\\\">[!--pltext--]</p>\r\n</div>');

/*Table structure for table `phome_enewsplayer` */

DROP TABLE IF EXISTS `phome_enewsplayer`;

CREATE TABLE `phome_enewsplayer` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `player` varchar(30) NOT NULL DEFAULT '',
  `filename` varchar(20) NOT NULL DEFAULT '',
  `bz` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsplayer` */

insert  into `phome_enewsplayer`(`id`,`player`,`filename`,`bz`) values (1,'RealPlayer','realplayer.php','RealPlayer播放器'),(2,'MediaPlayer','mediaplayer.php','MediaPlayer播放器'),(3,'FLASH','flasher.php','FLASH播放器'),(4,'FLV播放器','flver.php','FLV播放器');

/*Table structure for table `phome_enewsplf` */

DROP TABLE IF EXISTS `phome_enewsplf`;

CREATE TABLE `phome_enewsplf` (
  `fid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f` varchar(30) NOT NULL DEFAULT '',
  `fname` varchar(30) NOT NULL DEFAULT '',
  `fzs` varchar(255) NOT NULL DEFAULT '',
  `ftype` varchar(30) NOT NULL DEFAULT '',
  `flen` varchar(20) NOT NULL DEFAULT '',
  `ismust` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsplf` */

/*Table structure for table `phome_enewspltemp` */

DROP TABLE IF EXISTS `phome_enewspltemp`;

CREATE TABLE `phome_enewspltemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspltemp` */

insert  into `phome_enewspltemp`(`tempid`,`tempname`,`temptext`,`isdefault`) values (1,'默认评论列表模板','<!DOCTYPE HTML PUBLIC \\\"-//W3C//DTD HTML 4.01 Transitional//EN\\\">\r\n<html>\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\">\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<style type=\\\"text/css\\\">\r\n<!--\r\nbody,Table{ color: #222; font-size: 12px; }\r\na { color: #222; text-decoration: none; }\r\na:hover { color: #f00; text-decoration: underline; }\r\nh1 { font-size:32px; font-weight: bold; }\r\nh2 { color: #1e3a9e; font-size: 25px; font-weight: bold;  }\r\n.you { color: #1f3a87; font-size: 14px; }\r\n.text { font-size: 14px; padding-left: 5px; padding-right: 5px; line-height: 20px}\r\n.re a { color: #1f3a87; }\r\n.name { color: #1f3a87; }\r\n.name a { color: #1f3a87; text-decoration: underline;}\r\n.retext { background-color: #f3f3f3; width: 100%; float: left; padding-top: 22px; padding-bottom: 22px; border-top: 1px solid #ccc; }\r\n.retext textarea { width: 535px; height: 130px; float: left; margin-left: 60px; border-top-style: inset; border-top-width: 2px; border-left-style: inset; border-left-width: 2px; }\r\n.hrLine{BORDER-BOTTOM: #807d76 1px dotted;}\r\n\r\n.ecomment {margin:0;padding:0;}\r\n.ecomment {margin-bottom:12px;overflow-x:hidden;overflow-y:hidden;padding-bottom:3px;padding-left:3px;padding-right:3px;padding-top:3px;background:#FFFFEE;padding:3px;border:solid 1px #999;}\r\n.ecommentauthor {float:left; color:#F96; font-weight:bold;}\r\n.ecommenttext {clear:left;margin:0;padding:0;}\r\n-->\r\n</style>\r\n<script src=\\\"[!--news.url--]e/data/js/ajax.js\\\"></script>\r\n</head>\r\n\r\n<body topmargin=\\\"0\\\">\r\n<table width=\\\"766\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\">\r\n  <tr>\r\n    <td width=\\\"210\\\"><a href=\\\"[!--news.url--]\\\"><img src=\\\"[!--news.url--]skin/default/images/logo.gif\\\" border=\\\"0\\\" /></a></td>\r\n    <td><h1>网友评论</h1></td>\r\n    <td><div align=\\\"right\\\"><a href=\\\"#tosaypl\\\"><strong><font color=\\\"#FF0000\\\">我也评两句</font></strong></a></div></td>\r\n  </tr>\r\n</table>\r\n<table width=\\\"766\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#222\\\">\r\n  <tr>\r\n    <td height=\\\"2\\\"></td>\r\n  </tr>\r\n</table>\r\n<table width=\\\"766\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\">\r\n  <tr> \r\n    <td height=\\\"42\\\"> \r\n      <h2>评论：<a href=\\\"[!--titleurl--]\\\" target=\\\"_blank\\\"><font color=\\\"#1e3a9e\\\">[!--title--]</font></a></h2></td>\r\n    <td><div align=\\\"right\\\"><a href=\\\"[!--titleurl--]\\\" target=\\\"_blank\\\">查看原文</a></div></td>\r\n  </tr>\r\n</table>\r\n<hr align=\\\"center\\\" width=\\\"766\\\" size=1 class=hrline>\r\n<table width=\\\"766\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#384EA3\\\">\r\n  <form action=\\\"../enews/index.php\\\" method=\\\"post\\\" name=\\\"infopfenform\\\">\r\n    <input type=\\\"hidden\\\" name=\\\"enews\\\" value=\\\"AddInfoPfen\\\" />\r\n    <input type=\\\"hidden\\\" name=\\\"classid\\\" value=\\\"[!--classid--]\\\" />\r\n    <input type=\\\"hidden\\\" name=\\\"id\\\" value=\\\"[!--id--]\\\" />\r\n    <tr> \r\n      <td width=\\\"50%\\\" height=\\\"27\\\" valign=\\\"middle\\\"><font color=\\\"#FFFFFF\\\">&nbsp;评分: \r\n        <input type=\\\"radio\\\" name=\\\"fen\\\" value=\\\"1\\\">\r\n        1分 \r\n        <input type=\\\"radio\\\" name=\\\"fen\\\" value=\\\"2\\\">\r\n        2分 \r\n        <input name=\\\"fen\\\" type=\\\"radio\\\" value=\\\"3\\\" checked>\r\n        3分 \r\n        <input type=\\\"radio\\\" name=\\\"fen\\\" value=\\\"4\\\">\r\n        4分 \r\n        <input type=\\\"radio\\\" name=\\\"fen\\\" value=\\\"5\\\">\r\n        5分 \r\n        <input type=\\\"submit\\\" name=\\\"Submit\\\" value=\\\"提交\\\">\r\n        </font></td>\r\n      <td width=\\\"50%\\\" valign=\\\"middle\\\"><div align=\\\"center\\\"><font color=\\\"#FFFFFF\\\">平均得分: \r\n          <strong><span id=\\\"pfendiv\\\">[!--pinfopfen--]</span></strong> 分，共有 <strong>[!--infopfennum--]</strong> \r\n          人参与评分</font></div></td>\r\n    </tr>\r\n  </form>\r\n</table>\r\n<table width=\\\"766\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#CCCCCC\\\">\r\n  <tr> \r\n    <td height=\\\"30\\\" bgcolor=\\\"#FFFFFF\\\"> \r\n      <table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\">\r\n        <tr> \r\n          <td width=\\\"17%\\\">&nbsp;&nbsp;&nbsp;网友评论</td>\r\n          <td width=\\\"83%\\\"><div align=\\\"right\\\">[!--listpage--]&nbsp;&nbsp;&nbsp;</div></td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n  <tr> \r\n    <td bgcolor=\\\"#f8fcff\\\"> [!--empirenews.listtemp--] \r\n      <table width=\\\"96%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" style=\\\"word-break:break-all; word-wrap:break-all;\\\">\r\n        <tr> \r\n          <td height=\\\"30\\\"><span class=\\\"name\\\">本站网友 [!--username--]</span> <font color=\\\"#666666\\\">ip:[!--plip--]</font></td>\r\n          <td><div align=\\\"right\\\"><font color=\\\"#666666\\\">[!--pltime--] 发表</font></div></td>\r\n        </tr>\r\n        <tr valign=\\\"top\\\"> \r\n          <td height=\\\"50\\\" colspan=\\\"2\\\" class=\\\"text\\\">[!--pltext--]</td>\r\n        </tr>\r\n        <tr> \r\n          <td height=\\\"30\\\">&nbsp;</td>\r\n          <td><div align=\\\"right\\\" class=\\\"re\\\"><a href=\\\"#tosaypl\\\"[!--includelink--]>回复</a>&nbsp; \r\n              <a href=\\\"JavaScript:makeRequest(\\\'../pl/doaction.php?enews=DoForPl&plid=[!--plid--]&classid=[!--classid--]&id=[!--id--]&dopl=1&doajax=1&ajaxarea=zcpldiv[!--plid--]\\\',\\\'EchoReturnedText\\\',\\\'GET\\\',\\\'\\\');\\\">支持</a>[<span id=\\\"zcpldiv[!--plid--]\\\">[!--zcnum--]</span>]&nbsp; \r\n              <a href=\\\"JavaScript:makeRequest(\\\'../pl/doaction.php?enews=DoForPl&plid=[!--plid--]&classid=[!--classid--]&id=[!--id--]&dopl=0&doajax=1&ajaxarea=fdpldiv[!--plid--]\\\',\\\'EchoReturnedText\\\',\\\'GET\\\',\\\'\\\');\\\">反对</a>[<span id=\\\"fdpldiv[!--plid--]\\\">[!--fdnum--]</span>]\r\n            </div></td>\r\n        </tr>\r\n      </table>\r\n      <table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\">\r\n        <tr>\r\n          <td background=\\\"[!--news.url--]skin/default/images/plhrbg.gif\\\"></td>\r\n        </tr>\r\n      </table>\r\n      [!--empirenews.listtemp--] \r\n      <div align=\\\"right\\\"><br />\r\n        [!--listpage--]&nbsp;&nbsp;&nbsp;<br />\r\n        <br />\r\n        <font color=\\\"#FF0000\\\">网友评论仅供网友表达个人看法，并不表明本站同意其观点或证实其描述&nbsp;&nbsp;&nbsp;</font><br><br> </div></td>\r\n  </tr>\r\n  <script>\r\n  function CheckPl(obj)\r\n  {\r\n  	if(obj.saytext.value==\\\"\\\")\r\n  	{\r\n  		alert(\\\"错误，评论不能为空\\\");\r\n  		obj.saytext.focus();\r\n  		return false;\r\n  	}\r\n  	return true;\r\n  }\r\n  </script>\r\n  <form action=\\\"../pl/doaction.php\\\" method=\\\"post\\\" name=\\\"saypl\\\" id=\\\"saypl\\\" onsubmit=\\\"return CheckPl(document.saypl)\\\">\r\n  <tr id=\\\"tosaypl\\\"> \r\n    <td bgcolor=\\\"#f8fcff\\\"> <table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\">\r\n        <tr> \r\n            <td width=\\\"13%\\\" height=\\\"28\\\">&nbsp;&nbsp;&nbsp;<span class=\\\"you\\\">我也评两句</span></td>\r\n            <td valign=\\\"middle\\\">用户名： \r\n              <input name=\\\"username\\\" type=\\\"text\\\" id=\\\"username\\\" size=\\\"12\\\" value=\\\"[!--lusername--]\\\" />\r\n            密码： \r\n            <input name=\\\"password\\\" type=\\\"password\\\" id=\\\"password\\\" size=\\\"12\\\" value=\\\"[!--lpassword--]\\\" />\r\n            验证码： \r\n            <input name=\\\"key\\\" type=\\\"text\\\" id=\\\"key\\\" size=\\\"6\\\" />\r\n              <img src=\\\"[!--key.url--]\\\" align=\\\"middle\\\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\\\"[!--news.url--]e/member/register/\\\" target=\\\"_blank\\\">还没有注册？</a></td>\r\n        </tr>\r\n      </table></td>\r\n  </tr>\r\n  <tr> \r\n    <td bgcolor=\\\"#f8fcff\\\"> <table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\" class=\\\"retext\\\">\r\n        <tr> \r\n          <td width=\\\"78%\\\"><div align=\\\"center\\\"> \r\n              <textarea name=\\\"saytext\\\" cols=\\\"58\\\" rows=\\\"6\\\" id=\\\"saytext\\\"></textarea>\r\n            </div></td>\r\n          <td width=\\\"22%\\\" rowspan=\\\"2\\\"> <div align=\\\"center\\\">\r\n              <input name=\\\"nomember\\\" type=\\\"checkbox\\\" id=\\\"nomember\\\" value=\\\"1\\\" checked=\\\"checked\\\" />\r\n                匿名发表<br>\r\n                <br />\r\n              <input name=\\\"imageField\\\" type=\\\"submit\\\" id=\\\"imageField\\\" value=\\\" 提 交 \\\" />\r\n            </div></td>\r\n        </tr>\r\n        <tr> \r\n          <td><div align=\\\"center\\\"> \r\n              <script src=\\\"[!--news.url--]d/js/js/plface.js\\\"></script>\r\n            </div></td>\r\n        </tr>\r\n      </table> </td>\r\n  </tr>\r\n  <input name=\\\"id\\\" type=\\\"hidden\\\" id=\\\"id\\\" value=\\\"[!--id--]\\\" />\r\n  <input name=\\\"classid\\\" type=\\\"hidden\\\" id=\\\"classid\\\" value=\\\"[!--classid--]\\\" />\r\n  <input name=\\\"enews\\\" type=\\\"hidden\\\" id=\\\"enews\\\" value=\\\"AddPl\\\" />\r\n  <input name=\\\"repid\\\" type=\\\"hidden\\\" id=\\\"repid\\\" value=\\\"0\\\" />\r\n  </form>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',1);

/*Table structure for table `phome_enewspostdata` */

DROP TABLE IF EXISTS `phome_enewspostdata`;

CREATE TABLE `phome_enewspostdata` (
  `postid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rnd` varchar(32) NOT NULL DEFAULT '',
  `postdata` varchar(255) NOT NULL DEFAULT '',
  `ispath` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`postid`),
  KEY `rnd` (`rnd`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspostdata` */

/*Table structure for table `phome_enewspostserver` */

DROP TABLE IF EXISTS `phome_enewspostserver`;

CREATE TABLE `phome_enewspostserver` (
  `pid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pname` varchar(60) NOT NULL DEFAULT '',
  `purl` varchar(255) NOT NULL DEFAULT '',
  `ptype` tinyint(1) NOT NULL DEFAULT '0',
  `ftphost` varchar(255) NOT NULL DEFAULT '',
  `ftpport` varchar(20) NOT NULL DEFAULT '',
  `ftpusername` varchar(120) NOT NULL DEFAULT '',
  `ftppassword` varchar(120) NOT NULL DEFAULT '',
  `ftppath` varchar(255) NOT NULL DEFAULT '',
  `ftpmode` tinyint(1) NOT NULL DEFAULT '0',
  `ftpssl` tinyint(1) NOT NULL DEFAULT '0',
  `ftppasv` tinyint(1) NOT NULL DEFAULT '0',
  `ftpouttime` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposttime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`),
  KEY `ptype` (`ptype`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspostserver` */

insert  into `phome_enewspostserver`(`pid`,`pname`,`purl`,`ptype`,`ftphost`,`ftpport`,`ftpusername`,`ftppassword`,`ftppath`,`ftpmode`,`ftpssl`,`ftppasv`,`ftpouttime`,`lastposttime`) values (1,'附件服务器','',1,'','','','','',0,0,0,0,0);

/*Table structure for table `phome_enewsprinttemp` */

DROP TABLE IF EXISTS `phome_enewsprinttemp`;

CREATE TABLE `phome_enewsprinttemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `showdate` varchar(50) NOT NULL DEFAULT '',
  `modid` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsprinttemp` */

insert  into `phome_enewsprinttemp`(`tempid`,`tempname`,`temptext`,`isdefault`,`showdate`,`modid`) values (1,'默认打印模板','<html>\r\n<head>\r\n<meta http-equiv=\\\"content-type\\\" content=\\\"text/html; charset=utf-8\\\">\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<style>\r\nbody{font-family:宋体}td,.f12{font-size:12px}.f24 {font-size:24px;}.f14 {font-size:14px;}.title14 {font-size:14px;line-height:130%}.l17 {line-height:170%;}\r\n</style>\r\n</head>\r\n<body bgcolor=\\\"#ffffff\\\" topmargin=5 leftmargin=5 marginheight=5 marginwidth=5 onLoad=\\\'window.print()\\\'>\r\n<center>\r\n<table width=650 border=0 cellspacing=0 cellpadding=0>\r\n<tr>\r\n<td height=65 width=180><A href=\\\"http://www.phome.net/\\\"><IMG src=\\\"../../skin/default/images/elogo.jpg\\\" alt=\\\"软件\\\" width=\\\"180\\\" height=\\\"65\\\" border=0></A></td> \r\n<td valign=\\\"bottom\\\">[!--url--]</td>\r\n<td width=\\\"83\\\" align=\\\"right\\\" valign=\\\"bottom\\\"><a href=\\\'javascript:history.back()\\\'>返回</a>　<a href=\\\'javascript:window.print()\\\'>打印</a></td>\r\n</tr>\r\n</table>\r\n<table width=650 border=0 cellpadding=0 cellspacing=20 bgcolor=\\\"#EDF0F5\\\">\r\n<tr>\r\n<td>\r\n<BR>\r\n<TABLE cellSpacing=0 cellPadding=0 width=\\\"100%\\\" border=0>\r\n<TBODY>\r\n<TR>\r\n<TH class=\\\"f24\\\"><FONT color=#05006c>[!--title--]</FONT></TH></TR>\r\n<TR>\r\n<TD>\r\n<HR SIZE=1 bgcolor=\\\"#d9d9d9\\\">\r\n</TD>\r\n</TR>\r\n<TR>\r\n<TD align=\\\"middle\\\" height=20><div align=\\\"center\\\">[!--writer--]&nbsp;&nbsp;[!--newstime--]&nbsp;&nbsp;[!--befrom--]</div></TD>\r\n</TR>\r\n<TR>\r\n<TD height=15></TD>\r\n</TR>\r\n<TR>\r\n<TD class=\\\"l17\\\">\r\n<FONT class=\\\"f14\\\" id=\\\"zoom\\\"> \r\n<P>[!--newstext--]<br>\r\n<BR clear=all>\r\n</P>\r\n</FONT>\r\n</TD>\r\n</TR>\r\n<TR height=10>\r\n<TD></TD>\r\n</TR>\r\n</TBODY>\r\n</TABLE>\r\n[!--titlelink--]\r\n</td>\r\n</tr>\r\n</table>\r\n</center>\r\n</body>\r\n</html>',1,'Y-m-d H:i:s',1);

/*Table structure for table `phome_enewspublic` */

DROP TABLE IF EXISTS `phome_enewspublic`;

CREATE TABLE `phome_enewspublic` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `newsurl` varchar(120) NOT NULL DEFAULT '',
  `sitename` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `filetype` text NOT NULL,
  `filesize` int(11) NOT NULL DEFAULT '0',
  `hotnum` tinyint(4) NOT NULL DEFAULT '0',
  `newnum` tinyint(4) NOT NULL DEFAULT '0',
  `relistnum` int(11) NOT NULL DEFAULT '0',
  `renewsnum` int(11) NOT NULL DEFAULT '0',
  `min_keyboard` tinyint(4) NOT NULL DEFAULT '0',
  `max_keyboard` tinyint(4) NOT NULL DEFAULT '0',
  `search_num` tinyint(4) NOT NULL DEFAULT '0',
  `search_pagenum` tinyint(4) NOT NULL DEFAULT '0',
  `newslink` tinyint(4) NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `searchtime` int(11) NOT NULL DEFAULT '0',
  `loginnum` smallint(6) NOT NULL DEFAULT '0',
  `logintime` int(11) NOT NULL DEFAULT '0',
  `addnews_ok` tinyint(1) NOT NULL DEFAULT '0',
  `register_ok` tinyint(1) NOT NULL DEFAULT '0',
  `indextype` varchar(10) NOT NULL DEFAULT '',
  `goodlencord` tinyint(4) NOT NULL DEFAULT '0',
  `goodtype` varchar(10) NOT NULL DEFAULT '',
  `goodnum` tinyint(4) NOT NULL DEFAULT '0',
  `searchtype` varchar(10) NOT NULL DEFAULT '',
  `exittime` smallint(6) NOT NULL DEFAULT '0',
  `smalltextlen` smallint(6) NOT NULL DEFAULT '0',
  `defaultgroupid` smallint(6) NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `phpmode` tinyint(1) NOT NULL DEFAULT '0',
  `ftphost` varchar(255) NOT NULL DEFAULT '',
  `ftpport` varchar(20) NOT NULL DEFAULT '21',
  `ftpusername` varchar(120) NOT NULL DEFAULT '',
  `ftppassword` varchar(120) NOT NULL DEFAULT '',
  `ftppath` varchar(255) NOT NULL DEFAULT '',
  `ftpmode` tinyint(1) NOT NULL DEFAULT '0',
  `install` tinyint(1) NOT NULL DEFAULT '0',
  `hotplnum` tinyint(4) NOT NULL DEFAULT '0',
  `softversion` varchar(30) NOT NULL DEFAULT '0',
  `lctime` int(11) NOT NULL DEFAULT '0',
  `dorepnum` smallint(6) NOT NULL DEFAULT '0',
  `loadtempnum` smallint(6) NOT NULL DEFAULT '0',
  `firstnum` tinyint(4) NOT NULL DEFAULT '0',
  `bakdbpath` varchar(50) NOT NULL DEFAULT '',
  `bakdbzip` varchar(50) NOT NULL DEFAULT '',
  `downpass` varchar(32) NOT NULL DEFAULT '',
  `min_userlen` tinyint(4) NOT NULL DEFAULT '0',
  `max_userlen` tinyint(4) NOT NULL DEFAULT '0',
  `min_passlen` tinyint(4) NOT NULL DEFAULT '0',
  `max_passlen` tinyint(4) NOT NULL DEFAULT '0',
  `filechmod` tinyint(1) NOT NULL DEFAULT '0',
  `tid` smallint(6) NOT NULL DEFAULT '0',
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `loginkey_ok` tinyint(1) NOT NULL DEFAULT '0',
  `limittype` tinyint(1) NOT NULL DEFAULT '0',
  `redodown` smallint(6) NOT NULL DEFAULT '0',
  `candocode` tinyint(1) NOT NULL DEFAULT '0',
  `opennotcj` tinyint(1) NOT NULL DEFAULT '0',
  `reuserpagenum` int(11) NOT NULL DEFAULT '0',
  `revotejsnum` int(11) NOT NULL DEFAULT '0',
  `readjsnum` int(11) NOT NULL DEFAULT '0',
  `qaddtran` tinyint(1) NOT NULL DEFAULT '0',
  `qaddtransize` int(11) NOT NULL DEFAULT '0',
  `ebakthisdb` tinyint(1) NOT NULL DEFAULT '0',
  `delnewsnum` int(11) NOT NULL DEFAULT '0',
  `markpos` tinyint(4) NOT NULL DEFAULT '0',
  `markimg` varchar(80) NOT NULL DEFAULT '',
  `marktext` varchar(80) NOT NULL DEFAULT '',
  `markfontsize` varchar(12) NOT NULL DEFAULT '',
  `markfontcolor` varchar(12) NOT NULL DEFAULT '',
  `markfont` varchar(80) NOT NULL DEFAULT '',
  `adminloginkey` tinyint(1) NOT NULL DEFAULT '0',
  `php_outtime` int(11) NOT NULL DEFAULT '0',
  `listpagefun` varchar(36) NOT NULL DEFAULT '',
  `textpagefun` varchar(36) NOT NULL DEFAULT '',
  `adfile` varchar(30) NOT NULL DEFAULT '',
  `notsaveurl` text NOT NULL,
  `jstempid` smallint(6) NOT NULL DEFAULT '0',
  `rssnum` smallint(6) NOT NULL DEFAULT '0',
  `rsssub` smallint(6) NOT NULL DEFAULT '0',
  `savetxtf` text NOT NULL,
  `dorepdlevelnum` int(11) NOT NULL DEFAULT '0',
  `listpagelistfun` varchar(36) NOT NULL DEFAULT '',
  `listpagelistnum` smallint(6) NOT NULL DEFAULT '0',
  `infolinknum` int(11) NOT NULL DEFAULT '0',
  `searchgroupid` smallint(6) NOT NULL DEFAULT '0',
  `opencopytext` tinyint(1) NOT NULL DEFAULT '0',
  `reuserjsnum` int(11) NOT NULL DEFAULT '0',
  `reuserlistnum` int(11) NOT NULL DEFAULT '0',
  `opentitleurl` tinyint(1) NOT NULL DEFAULT '0',
  `qaddtranimgtype` text NOT NULL,
  `qaddtranfile` tinyint(1) NOT NULL DEFAULT '0',
  `qaddtranfilesize` int(11) NOT NULL DEFAULT '0',
  `qaddtranfiletype` text NOT NULL,
  `sendmailtype` tinyint(1) NOT NULL DEFAULT '0',
  `smtphost` varchar(255) NOT NULL DEFAULT '',
  `fromemail` varchar(120) NOT NULL DEFAULT '',
  `loginemail` tinyint(1) NOT NULL DEFAULT '0',
  `emailusername` varchar(60) NOT NULL DEFAULT '',
  `emailpassword` varchar(60) NOT NULL DEFAULT '',
  `smtpport` varchar(20) NOT NULL DEFAULT '',
  `emailname` varchar(120) NOT NULL DEFAULT '',
  `deftempid` smallint(6) NOT NULL DEFAULT '0',
  `feedbacktfile` tinyint(1) NOT NULL DEFAULT '0',
  `feedbackfilesize` int(11) NOT NULL DEFAULT '0',
  `feedbackfiletype` text NOT NULL,
  `searchtempvar` tinyint(1) NOT NULL DEFAULT '0',
  `showinfolevel` smallint(6) NOT NULL DEFAULT '0',
  `navfh` varchar(20) NOT NULL DEFAULT '',
  `spicwidth` smallint(6) NOT NULL DEFAULT '0',
  `spicheight` smallint(6) NOT NULL DEFAULT '0',
  `spickill` tinyint(1) NOT NULL DEFAULT '0',
  `jpgquality` tinyint(4) NOT NULL DEFAULT '0',
  `markpct` tinyint(4) NOT NULL DEFAULT '0',
  `redoview` smallint(6) NOT NULL DEFAULT '0',
  `reggetfen` smallint(6) NOT NULL DEFAULT '0',
  `regbooktime` smallint(6) NOT NULL DEFAULT '0',
  `revotetime` smallint(6) NOT NULL DEFAULT '0',
  `nreclass` text NOT NULL,
  `nreinfo` text NOT NULL,
  `nrejs` text NOT NULL,
  `fpath` tinyint(1) NOT NULL DEFAULT '0',
  `filepath` varchar(30) NOT NULL DEFAULT '',
  `openmembertranimg` tinyint(1) NOT NULL DEFAULT '0',
  `memberimgsize` int(11) NOT NULL DEFAULT '0',
  `memberimgtype` text NOT NULL,
  `openmembertranfile` tinyint(1) NOT NULL DEFAULT '0',
  `memberfilesize` int(11) NOT NULL DEFAULT '0',
  `memberfiletype` text NOT NULL,
  `nottobq` text NOT NULL,
  `defspacestyleid` smallint(6) NOT NULL DEFAULT '0',
  `canposturl` text NOT NULL,
  `openspace` tinyint(1) NOT NULL DEFAULT '0',
  `defadminstyle` smallint(6) NOT NULL DEFAULT '0',
  `realltime` smallint(6) NOT NULL DEFAULT '0',
  `closeip` text NOT NULL,
  `openip` text NOT NULL,
  `hopenip` text NOT NULL,
  `closewords` text NOT NULL,
  `closewordsf` text NOT NULL,
  `textpagelistnum` smallint(6) NOT NULL DEFAULT '0',
  `memberlistlevel` smallint(6) NOT NULL DEFAULT '0',
  `wapopen` tinyint(1) NOT NULL DEFAULT '0',
  `wapdefstyle` smallint(6) NOT NULL DEFAULT '0',
  `wapshowmid` varchar(255) NOT NULL DEFAULT '',
  `waplistnum` tinyint(4) NOT NULL DEFAULT '0',
  `wapsubtitle` tinyint(4) NOT NULL DEFAULT '0',
  `wapshowdate` varchar(50) NOT NULL DEFAULT '',
  `wapchar` tinyint(1) NOT NULL DEFAULT '0',
  `ebakcanlistdb` tinyint(1) NOT NULL DEFAULT '0',
  `paymoneytofen` int(11) NOT NULL DEFAULT '0',
  `payminmoney` int(11) NOT NULL DEFAULT '0',
  `keytog` tinyint(1) NOT NULL DEFAULT '0',
  `keyrnd` varchar(60) NOT NULL DEFAULT '',
  `keytime` int(11) NOT NULL DEFAULT '0',
  `regkey_ok` tinyint(1) NOT NULL DEFAULT '0',
  `opengetdown` tinyint(1) NOT NULL DEFAULT '0',
  `gbkey_ok` tinyint(1) NOT NULL DEFAULT '0',
  `fbkey_ok` tinyint(1) NOT NULL DEFAULT '0',
  `newaddinfotime` smallint(6) NOT NULL DEFAULT '0',
  `classnavline` smallint(6) NOT NULL DEFAULT '0',
  `classnavfh` varchar(120) NOT NULL DEFAULT '',
  `adminstyle` text NOT NULL,
  `sitekey` varchar(255) NOT NULL DEFAULT '',
  `siteintro` text NOT NULL,
  `docnewsnum` int(11) NOT NULL DEFAULT '0',
  `openschall` tinyint(1) NOT NULL DEFAULT '0',
  `schallfield` tinyint(1) NOT NULL DEFAULT '0',
  `schallminlen` tinyint(4) NOT NULL DEFAULT '0',
  `schallmaxlen` tinyint(4) NOT NULL DEFAULT '0',
  `schallnotcid` text NOT NULL,
  `schallnum` smallint(6) NOT NULL DEFAULT '0',
  `schallpagenum` smallint(6) NOT NULL DEFAULT '0',
  `dtcanbq` tinyint(1) NOT NULL DEFAULT '0',
  `dtcachetime` int(11) NOT NULL DEFAULT '0',
  `regretime` smallint(6) NOT NULL DEFAULT '0',
  `regclosewords` text NOT NULL,
  `regemailonly` tinyint(1) NOT NULL DEFAULT '0',
  `repkeynum` smallint(6) NOT NULL DEFAULT '0',
  `getpasstime` int(11) NOT NULL DEFAULT '0',
  `acttime` int(11) NOT NULL DEFAULT '0',
  `regacttype` tinyint(1) NOT NULL DEFAULT '0',
  `acttext` text NOT NULL,
  `getpasstext` text NOT NULL,
  `acttitle` varchar(120) NOT NULL DEFAULT '',
  `getpasstitle` varchar(120) NOT NULL DEFAULT '',
  `opengetpass` tinyint(1) NOT NULL DEFAULT '0',
  `hlistinfonum` int(11) NOT NULL DEFAULT '0',
  `qlistinfonum` int(11) NOT NULL DEFAULT '0',
  `dtncanbq` tinyint(1) NOT NULL DEFAULT '0',
  `dtncachetime` int(11) NOT NULL DEFAULT '0',
  `readdinfotime` smallint(6) NOT NULL DEFAULT '0',
  `qeditinfotime` int(11) NOT NULL DEFAULT '0',
  `ftpssl` tinyint(1) NOT NULL DEFAULT '0',
  `ftppasv` tinyint(1) NOT NULL DEFAULT '0',
  `ftpouttime` smallint(6) NOT NULL DEFAULT '0',
  `onclicktype` tinyint(1) NOT NULL DEFAULT '0',
  `onclickfilesize` int(11) NOT NULL DEFAULT '0',
  `onclickfiletime` int(11) NOT NULL DEFAULT '0',
  `schalltime` smallint(6) NOT NULL DEFAULT '0',
  `defprinttempid` smallint(6) NOT NULL DEFAULT '0',
  `opentags` tinyint(1) NOT NULL DEFAULT '0',
  `tagstempid` smallint(6) NOT NULL DEFAULT '0',
  `usetags` text NOT NULL,
  `chtags` text NOT NULL,
  `tagslistnum` smallint(6) NOT NULL DEFAULT '0',
  `closeqdt` tinyint(1) NOT NULL DEFAULT '0',
  `settop` tinyint(1) NOT NULL DEFAULT '0',
  `qlistinfomod` tinyint(1) NOT NULL DEFAULT '0',
  `gb_num` tinyint(4) NOT NULL DEFAULT '0',
  `member_num` tinyint(4) NOT NULL DEFAULT '0',
  `space_num` tinyint(4) NOT NULL DEFAULT '0',
  `opendoip` text NOT NULL,
  `closedoip` text NOT NULL,
  `doiptype` varchar(255) NOT NULL DEFAULT '',
  `filelday` int(11) NOT NULL DEFAULT '0',
  `infolday` int(11) NOT NULL DEFAULT '0',
  `baktempnum` tinyint(4) NOT NULL DEFAULT '0',
  `dorepkey` tinyint(1) NOT NULL DEFAULT '0',
  `dorepword` tinyint(1) NOT NULL DEFAULT '0',
  `onclickrnd` varchar(20) NOT NULL DEFAULT '',
  `indexpagedt` tinyint(1) NOT NULL DEFAULT '0',
  `keybgcolor` varchar(8) NOT NULL DEFAULT '',
  `keyfontcolor` varchar(8) NOT NULL DEFAULT '',
  `keydistcolor` varchar(8) NOT NULL DEFAULT '',
  `indexpageid` smallint(6) NOT NULL DEFAULT '0',
  `closeqdtmsg` varchar(255) NOT NULL DEFAULT '',
  `openfileserver` tinyint(1) NOT NULL DEFAULT '0',
  `closemods` varchar(255) NOT NULL DEFAULT '',
  `fieldandtop` tinyint(1) NOT NULL DEFAULT '0',
  `fieldandclosetb` text NOT NULL,
  `filedatatbs` text NOT NULL,
  `filedeftb` smallint(5) unsigned NOT NULL DEFAULT '0',
  `firsttitlename` varchar(255) NOT NULL DEFAULT '',
  `isgoodname` varchar(255) NOT NULL DEFAULT '',
  `closelisttemp` varchar(255) NOT NULL DEFAULT '',
  `chclasscolor` varchar(8) NOT NULL DEFAULT '',
  `timeclose` varchar(255) NOT NULL DEFAULT '',
  `timeclosedo` varchar(255) NOT NULL DEFAULT '',
  `ipaddinfonum` int(10) unsigned NOT NULL DEFAULT '0',
  `ipaddinfotime` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rewriteinfo` varchar(120) NOT NULL DEFAULT '',
  `rewriteclass` varchar(120) NOT NULL DEFAULT '',
  `rewriteinfotype` varchar(120) NOT NULL DEFAULT '',
  `rewritetags` varchar(120) NOT NULL DEFAULT '',
  `closehmenu` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspublic` */

insert  into `phome_enewspublic`(`id`,`newsurl`,`sitename`,`email`,`filetype`,`filesize`,`hotnum`,`newnum`,`relistnum`,`renewsnum`,`min_keyboard`,`max_keyboard`,`search_num`,`search_pagenum`,`newslink`,`checked`,`searchtime`,`loginnum`,`logintime`,`addnews_ok`,`register_ok`,`indextype`,`goodlencord`,`goodtype`,`goodnum`,`searchtype`,`exittime`,`smalltextlen`,`defaultgroupid`,`fileurl`,`phpmode`,`ftphost`,`ftpport`,`ftpusername`,`ftppassword`,`ftppath`,`ftpmode`,`install`,`hotplnum`,`softversion`,`lctime`,`dorepnum`,`loadtempnum`,`firstnum`,`bakdbpath`,`bakdbzip`,`downpass`,`min_userlen`,`max_userlen`,`min_passlen`,`max_passlen`,`filechmod`,`tid`,`tbname`,`loginkey_ok`,`limittype`,`redodown`,`candocode`,`opennotcj`,`reuserpagenum`,`revotejsnum`,`readjsnum`,`qaddtran`,`qaddtransize`,`ebakthisdb`,`delnewsnum`,`markpos`,`markimg`,`marktext`,`markfontsize`,`markfontcolor`,`markfont`,`adminloginkey`,`php_outtime`,`listpagefun`,`textpagefun`,`adfile`,`notsaveurl`,`jstempid`,`rssnum`,`rsssub`,`savetxtf`,`dorepdlevelnum`,`listpagelistfun`,`listpagelistnum`,`infolinknum`,`searchgroupid`,`opencopytext`,`reuserjsnum`,`reuserlistnum`,`opentitleurl`,`qaddtranimgtype`,`qaddtranfile`,`qaddtranfilesize`,`qaddtranfiletype`,`sendmailtype`,`smtphost`,`fromemail`,`loginemail`,`emailusername`,`emailpassword`,`smtpport`,`emailname`,`deftempid`,`feedbacktfile`,`feedbackfilesize`,`feedbackfiletype`,`searchtempvar`,`showinfolevel`,`navfh`,`spicwidth`,`spicheight`,`spickill`,`jpgquality`,`markpct`,`redoview`,`reggetfen`,`regbooktime`,`revotetime`,`nreclass`,`nreinfo`,`nrejs`,`fpath`,`filepath`,`openmembertranimg`,`memberimgsize`,`memberimgtype`,`openmembertranfile`,`memberfilesize`,`memberfiletype`,`nottobq`,`defspacestyleid`,`canposturl`,`openspace`,`defadminstyle`,`realltime`,`closeip`,`openip`,`hopenip`,`closewords`,`closewordsf`,`textpagelistnum`,`memberlistlevel`,`wapopen`,`wapdefstyle`,`wapshowmid`,`waplistnum`,`wapsubtitle`,`wapshowdate`,`wapchar`,`ebakcanlistdb`,`paymoneytofen`,`payminmoney`,`keytog`,`keyrnd`,`keytime`,`regkey_ok`,`opengetdown`,`gbkey_ok`,`fbkey_ok`,`newaddinfotime`,`classnavline`,`classnavfh`,`adminstyle`,`sitekey`,`siteintro`,`docnewsnum`,`openschall`,`schallfield`,`schallminlen`,`schallmaxlen`,`schallnotcid`,`schallnum`,`schallpagenum`,`dtcanbq`,`dtcachetime`,`regretime`,`regclosewords`,`regemailonly`,`repkeynum`,`getpasstime`,`acttime`,`regacttype`,`acttext`,`getpasstext`,`acttitle`,`getpasstitle`,`opengetpass`,`hlistinfonum`,`qlistinfonum`,`dtncanbq`,`dtncachetime`,`readdinfotime`,`qeditinfotime`,`ftpssl`,`ftppasv`,`ftpouttime`,`onclicktype`,`onclickfilesize`,`onclickfiletime`,`schalltime`,`defprinttempid`,`opentags`,`tagstempid`,`usetags`,`chtags`,`tagslistnum`,`closeqdt`,`settop`,`qlistinfomod`,`gb_num`,`member_num`,`space_num`,`opendoip`,`closedoip`,`doiptype`,`filelday`,`infolday`,`baktempnum`,`dorepkey`,`dorepword`,`onclickrnd`,`indexpagedt`,`keybgcolor`,`keyfontcolor`,`keydistcolor`,`indexpageid`,`closeqdtmsg`,`openfileserver`,`closemods`,`fieldandtop`,`fieldandclosetb`,`filedatatbs`,`filedeftb`,`firsttitlename`,`isgoodname`,`closelisttemp`,`chclasscolor`,`timeclose`,`timeclosedo`,`ipaddinfonum`,`ipaddinfotime`,`rewriteinfo`,`rewriteclass`,`rewriteinfotype`,`rewritetags`,`closehmenu`) values (1,'/','吴江实验小学','admin@phome.net','|.gif|.jpg|.png|.swf|.rar|.zip|.mp3|.wmv|.txt|.doc|',8048,10,10,8,100,2,20,20,16,0,0,30,5,5,0,1,'.html',0,'',10,'.html',40,160,1,'/d/file/',0,'','21','','','',0,0,10,'7.0,1357574410',1222406370,300,50,10,'bdata','zip','TTTTTTTTTTTTTTTTTTTT',3,20,6,20,1,1,'news',0,0,1,1,0,50,100,100,1,50,1,300,5,'../data/mark/maskdef.gif','','5','','../data/mark/cour.ttf',1,0,'sys_ShowListPage','sys_ShowTextPage','thea','',1,50,300,',',300,'sys_ShowListMorePage',12,100,0,0,100,8,1,'|.gif|.jpg|',1,500,'|.zip|.rar|',1,'smtp.163.com','ecms@163.com',1,'ecms','ecms','25','CMS',0,1,500,'|.zip|.rar|',1,0,'>',105,118,1,80,65,24,0,30,30,',2,3,4,6,7,',',',',',0,'Y-m-d',1,50,'|.gif|.jpg|',1,500,'|.zip|.rar|',',',1,'',0,1,0,'','','','','',6,0,1,1,'',10,30,'m-d',0,0,10,1,2,'2222222222222222222222TTTTTTTT',30,0,0,0,0,0,20,'&nbsp;|&nbsp;',',1,2,','','吴江实验小学',300,0,1,3,20,',,',20,16,1,43200,0,'',1,0,72,720,0,'[!--username--] ，\r\n这封信是由 [!--sitename--] 发送的。\r\n\r\n您收到这封邮件，是因为在我们网站的新用户注册 Email 使用\r\n了您的地址。如果您并没有访问过我们的网站，或没有进行上述操作，请忽\r\n略这封邮件。您不需要退订或进行其他进一步的操作。\r\n\r\n----------------------------------------------------------------------\r\n帐号激活说明\r\n----------------------------------------------------------------------\r\n\r\n您是我们网站的新用户，注册 Email 时使用了本地址，我们需\r\n要对您的地址有效性进行验证以避免垃圾邮件或地址被滥用。\r\n\r\n您只需点击下面的链接即可激活您的帐号：\r\n\r\n[!--pageurl--]\r\n\r\n(如果上面不是链接形式，请将地址手工粘贴到浏览器地址栏再访问)\r\n\r\n感谢您的访问，祝您使用愉快！\r\n\r\n\r\n\r\n此致\r\n\r\n[!--sitename--] 管理团队.\r\n[!--news.url--]','[!--username--] ，\r\n这封信是由 [!--sitename--] 发送的。\r\n\r\n您收到这封邮件，是因为在我们的网站上这个邮箱地址被登记为用户邮箱，\r\n且该用户请求使用 Email 密码重置功能所致。\r\n\r\n----------------------------------------------------------------------\r\n重要！\r\n----------------------------------------------------------------------\r\n\r\n如果您没有提交密码重置的请求或不是我们网站的注册用户，请立即忽略\r\n并删除这封邮件。只在您确认需要重置密码的情况下，才继续阅读下面的\r\n内容。\r\n\r\n----------------------------------------------------------------------\r\n密码重置说明\r\n----------------------------------------------------------------------\r\n\r\n您只需在提交请求后的三天之内，通过点击下面的链接重置您的密码：\r\n\r\n[!--pageurl--]\r\n\r\n(如果上面不是链接形式，请将地址手工粘贴到浏览器地址栏再访问)\r\n\r\n上面的页面打开后，输入新的密码后提交，之后您即可使用新的密码登录\r\n网站了。您可以在用户控制面板中随时修改您的密码。\r\n\r\n\r\n\r\n此致\r\n\r\n[!--sitename--] 管理团队.\r\n[!--news.url--]','[[!--sitename--]] Email 地址验证','[[!--sitename--]] 取回密码说明',0,30,25,1,43200,0,0,0,0,0,0,10,60,0,1,1,1,',1,2,3,4,5,6,7,8,','',25,0,0,0,20,20,25,'','','',0,0,3,0,0,'',0,'','','',0,'',0,'',0,'',',1,',1,'一级头条|二级头条|三级头条|四级头条|五级头条|六级头条|七级头条|八级头条|九级头条','一级推荐|二级推荐|三级推荐|四级推荐|五级推荐|六级推荐|七级推荐|八级推荐|九级推荐','','#99C4E3','','',0,0,'','','','','');

/*Table structure for table `phome_enewspublic_update` */

DROP TABLE IF EXISTS `phome_enewspublic_update`;

CREATE TABLE `phome_enewspublic_update` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `lasttimeinfo` int(10) unsigned NOT NULL DEFAULT '0',
  `lasttimepl` int(10) unsigned NOT NULL DEFAULT '0',
  `lastnuminfo` int(10) unsigned NOT NULL DEFAULT '0',
  `lastnumpl` int(10) unsigned NOT NULL DEFAULT '0',
  `lastnuminfotb` text NOT NULL,
  `lastnumpltb` text NOT NULL,
  `todaytimeinfo` int(10) unsigned NOT NULL DEFAULT '0',
  `todaytimepl` int(10) unsigned NOT NULL DEFAULT '0',
  `todaynuminfo` int(10) unsigned NOT NULL DEFAULT '0',
  `yesterdaynuminfo` int(10) unsigned NOT NULL DEFAULT '0',
  `todaynumpl` int(10) unsigned NOT NULL DEFAULT '0',
  `yesterdaynumpl` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspublic_update` */

insert  into `phome_enewspublic_update`(`id`,`lasttimeinfo`,`lasttimepl`,`lastnuminfo`,`lastnumpl`,`lastnuminfotb`,`lastnumpltb`,`todaytimeinfo`,`todaytimepl`,`todaynuminfo`,`yesterdaynuminfo`,`todaynumpl`,`yesterdaynumpl`) values (1,1355124469,1355124476,36,0,'|1,36|','',1476147877,1476147877,2,0,0,0);

/*Table structure for table `phome_enewspubtemp` */

DROP TABLE IF EXISTS `phome_enewspubtemp`;

CREATE TABLE `phome_enewspubtemp` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `indextemp` mediumtext NOT NULL,
  `cptemp` mediumtext NOT NULL,
  `searchtemp` mediumtext NOT NULL,
  `searchjstemp` mediumtext NOT NULL,
  `searchjstemp1` mediumtext NOT NULL,
  `otherlinktemp` mediumtext NOT NULL,
  `downsofttemp` text NOT NULL,
  `onlinemovietemp` text NOT NULL,
  `listpagetemp` text NOT NULL,
  `gbooktemp` mediumtext NOT NULL,
  `loginiframe` mediumtext NOT NULL,
  `otherlinktempsub` tinyint(4) NOT NULL DEFAULT '0',
  `otherlinktempdate` varchar(20) NOT NULL DEFAULT '',
  `loginjstemp` mediumtext NOT NULL,
  `downpagetemp` mediumtext NOT NULL,
  `pljstemp` mediumtext NOT NULL,
  `schalltemp` mediumtext NOT NULL,
  `schallsubnum` smallint(6) NOT NULL DEFAULT '0',
  `schalldate` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspubtemp` */

insert  into `phome_enewspubtemp`(`id`,`indextemp`,`cptemp`,`searchtemp`,`searchjstemp`,`searchjstemp1`,`otherlinktemp`,`downsofttemp`,`onlinemovietemp`,`listpagetemp`,`gbooktemp`,`loginiframe`,`otherlinktempsub`,`otherlinktempdate`,`loginjstemp`,`downpagetemp`,`pljstemp`,`schalltemp`,`schallsubnum`,`schalldate`) values (1,'[!--temp.header--]\r\n<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>新闻动态</span>\r\n				<a href=\\\"/news/hnews/\\\">更多...</a>\r\n			</div>\r\n			[ecmsinfo]9,3,32,0,0,2,0[/ecmsinfo]\r\n		</div>\r\n		<div class=\\\"D_news_right clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>集团故事</span>\r\n				<a href=\\\"/news/jtgs/\\\">更多...</a>\r\n			</div>\r\n			[ecmsinfo]10,4,32,0,0,14,0[/ecmsinfo]		</div>\r\n	</div>\r\n[!--temp.footer--]','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--pagetitle--] - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.dtheader--] \r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>现在的位置：[!--newsnav--]\r\n</td>\r\n</tr>\r\n</table>\r\n      <table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n        <tr> \r\n          <td width=\\\"300\\\" valign=\\\"top\\\"> \r\n		  <?php\r\n		  $lguserid=intval(getcvar(\\\'mluserid\\\'));//登陆用户ID\r\n		  $lgusername=RepPostVar(getcvar(\\\'mlusername\\\'));//登陆用户\r\n		  $lggroupid=intval(getcvar(\\\'mlgroupid\\\'));//会员组ID\r\n		  if($lggroupid)	//登陆会员显示菜单\r\n		  {\r\n		  ?>\r\n            <table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" class=\\\"tableborder\\\">\r\n              <tr class=\\\"header\\\"> \r\n                <td height=\\\"20\\\" bgcolor=\\\"#FFFFFF\\\"> <div align=\\\"center\\\"><strong><a href=\\\"[!--news.url--]e/member/cp/\\\">功能菜单</a></strong></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/EditInfo/\\\">修改资料</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/my/\\\">帐号状态</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/msg/\\\">站内信息</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/mspace/SetSpace.php\\\">空间设置</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/DoInfo/\\\">管理信息</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/fava/\\\">收藏夹</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/payapi/\\\">在线支付</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/friend/\\\">我的好友</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/buybak/\\\">消费记录</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/buygroup/\\\">在线充值</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/card/\\\">点卡充值</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"#ecms\\\" onclick=\\\"window.open(\\\'[!--news.url--]e/ShopSys/buycar/\\\',\\\'\\\',\\\'width=680,height=500,scrollbars=yes,resizable=yes\\\');\\\">我的购物车</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/ShopSys/ListDd/\\\">我的订单</a></div></td>\r\n              </tr>\r\n			  <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/login/\\\">重新登陆</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/doaction.php?enews=exit\\\" onclick=\\\"return confirm(\\\'确认要退出?\\\');\\\">退出登陆</a></div></td>\r\n              </tr>\r\n            </table>\r\n			<?php\r\n			}\r\n			else	//游客显示菜单\r\n			{\r\n			?>  \r\n            <table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" class=\\\"tableborder\\\">\r\n              <tr class=\\\"header\\\"> \r\n                <td height=\\\"20\\\" bgcolor=\\\"#FFFFFF\\\"> <div align=\\\"center\\\"><strong><a href=\\\"[!--news.url--]e/member/cp/\\\">功能菜单</a></strong></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/login/\\\">会员登陆</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/member/register/\\\">注册帐号</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"[!--news.url--]e/DoInfo/\\\">发布投稿</a></div></td>\r\n              </tr>\r\n              <tr> \r\n                <td height=\\\"25\\\" bgcolor=\\\"#FFFFFF\\\" onmouseout=\\\"this.style.backgroundColor=\\\'#ffffff\\\'\\\" onmouseover=\\\"this.style.backgroundColor=\\\'#EBF3FC\\\'\\\"><div align=\\\"center\\\"><a href=\\\"#ecms\\\" onclick=\\\"window.open(\\\'[!--news.url--]e/ShopSys/buycar/\\\',\\\'\\\',\\\'width=680,height=500,scrollbars=yes,resizable=yes\\\');\\\">我的购物车</a></div></td>\r\n              </tr>\r\n            </table>\r\n			<?php\r\n			}\r\n			?>\r\n			</td>\r\n          <td width=\\\"85%\\\" valign=\\\"top\\\">[!--empirenews.template--]</td>\r\n        </tr>\r\n      </table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>高级搜索 - Powered by EmpireCMS</title>\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n<script src=\\\"[!--news.url--]e/data/images/setday.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.dtheader--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>现在的位置：[!--url--]</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n						<tr>\r\n							<td><form action=\\\'[!--news.url--]e/search/index.php\\\' method=\\\"post\\\" name=\\\"search_news\\\" id=\\\"search_news\\\">\r\n									<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\">\r\n										<tr>\r\n											<td width=\\\"54%\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"6\\\" cellspacing=\\\"0\\\">\r\n													<tr>\r\n														<td bgcolor=\\\"#F4F4F4\\\"><strong>搜索范围</strong></td>\r\n													</tr>\r\n											</table></td>\r\n											<td width=\\\"46%\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"6\\\" cellspacing=\\\"0\\\">\r\n													<tr>\r\n														<td bgcolor=\\\"#F4F4F4\\\"><strong>搜索栏目</strong></td>\r\n													</tr>\r\n											</table></td>\r\n										</tr>\r\n										<tr>\r\n											<td valign=\\\"top\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"6\\\" cellspacing=\\\"0\\\">\r\n													<tr>\r\n														<td><input type=\\\"radio\\\" name=\\\"show\\\" value=\\\"title\\\" checked=\\\"checked\\\" />\r\n															标题\r\n															<input type=\\\"radio\\\" name=\\\"show\\\" value=\\\"smalltext\\\" />\r\n															简介\r\n															<input type=\\\"radio\\\" name=\\\"show\\\" value=\\\"newstext\\\" />\r\n															内容\r\n															<input type=\\\"radio\\\" name=\\\"show\\\" value=\\\"writer\\\" />\r\n															作者\r\n															<input name=\\\"show\\\" type=\\\"radio\\\" value=\\\"title,smalltext,newstext,writer\\\" />\r\n															不限</td>\r\n													</tr>\r\n												</table>\r\n													<br />\r\n													<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"6\\\" cellspacing=\\\"0\\\">\r\n														<tr bgcolor=\\\"#F4F4F4\\\">\r\n															<td><strong>时间限制</strong><font color=\\\"#666666\\\">(0000-00-00为不限制)</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td height=\\\"30\\\" valign=\\\"top\\\">从\r\n																<input name=\\\"starttime\\\" type=\\\"text\\\" value=\\\"0000-00-00\\\" size=\\\"12\\\" onclick=\\\"setday(this)\\\" />\r\n																到\r\n																<input name=\\\"endtime\\\" type=\\\"text\\\" value=\\\"0000-00-00\\\" size=\\\"12\\\" onclick=\\\"setday(this)\\\" />\r\n																之间的数据</td>\r\n														</tr>\r\n														<tr>\r\n															<td bgcolor=\\\"#F4F4F4\\\"><strong>价格限制</strong><font color=\\\"#666666\\\">(商城模型中有效,0为不限制)</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>从\r\n																<input name=\\\"startprice\\\" type=\\\"text\\\" id=\\\"startprice\\\" value=\\\"0\\\" size=\\\"6\\\" />\r\n																到\r\n																<input name=\\\"endprice\\\" type=\\\"text\\\" id=\\\"endprice\\\" value=\\\"0\\\" size=\\\"6\\\" />\r\n																元之间</td>\r\n														</tr>\r\n												</table></td>\r\n											<td valign=\\\"top\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"6\\\" cellpadding=\\\"0\\\">\r\n													<tr>\r\n														<td><select name=\\\"classid\\\" size=\\\"8\\\" id=\\\"select2\\\" style=\\\"width: 100%;height:112px\\\">\r\n																<option value=\\\"0\\\" selected=\\\"selected\\\">所有栏目</option>\r\n															[!--class--]\r\n          \r\n														</select></td>\r\n													</tr>\r\n												</table>\r\n													<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"6\\\" cellspacing=\\\"0\\\">\r\n														<tr>\r\n															<td bgcolor=\\\"#F4F4F4\\\"><strong>结果显示</strong></td>\r\n														</tr>\r\n														<tr>\r\n															<td height=\\\"30\\\"><select name=\\\"orderby\\\" id=\\\"select4\\\">\r\n																	<option value=\\\"\\\">按发布日期</option>\r\n																	<option value=\\\"id\\\">信息ID</option>\r\n																	<option value=\\\"plnum\\\">评论数</option>\r\n																	<option value=\\\"onclick\\\">人气</option>\r\n																	<option value=\\\"totaldown\\\">下载数</option>\r\n																</select>\r\n																	<select name=\\\"myorder\\\" id=\\\"select5\\\">\r\n																		<option value=\\\"0\\\">倒序排列</option>\r\n																		<option value=\\\"1\\\">顺序排列</option>\r\n																	</select>\r\n															</td>\r\n														</tr>\r\n												</table></td>\r\n										</tr>\r\n									</table>\r\n								<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"4\\\" cellspacing=\\\"0\\\">\r\n										<tr>\r\n											<td>关键字：\r\n												<input name=\\\"keyboard\\\" type=\\\"text\\\" id=\\\"keyboard2\\\" size=\\\"60\\\" />\r\n													<input type=\\\"submit\\\" name=\\\"Submit22\\\" value=\\\"搜索\\\" />\r\n											</td>\r\n										</tr>\r\n									</table>\r\n							</form></td>\r\n						</tr>\r\n					</table></td>\r\n				</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>','<table border=0 cellpadding=3 cellspacing=1><form name=search_js1 method=post action=\'[!--news.url--]e/search/index.php\' onsubmit=\'return search_check(document.search_js1);\'><tr><td><div align=center>搜索: <select name=show><option value=title>标题</option><option value=smalltext>简介</option><option value=newstext>内容</option><option value=writer>作者</option><option value=title,smalltext,newstext,writer>搜索全部</option></select><select name=classid><option value=0>所有栏目</option>[!--class--]</select><input name=keyboard type=text size=13><input type=submit name=Submit value=搜索></div></td></tr></form></table>','<table width=99% border=0 cellpadding=3 cellspacing=1><form name=search_js2 method=post action=\'[!--news.url--]e/search/index.php\' onsubmit=\'return search_check(document.search_js2);\'><tr><td height=25><div align=center>关键字: <input name=keyboard type=text size=13></div></td></tr><tr><td><div align=center>范围: <select name=show><option value=title>标题</option><option value=smalltext>简介</option><option value=newstext>内容</option><option value=writer>作者</option><option value=title,smalltext,newstext,writer>搜索全部</option></select></div></td></tr><tr><td><div align=center>栏目:<select name=classid><option value=0>所有栏目</option>[!--class--]</select></div></td></tr><tr><td><div align=center><input type=submit name=Submit value=搜索></div></td></tr></form></table>','[!--empirenews.listtemp--]<li><a href=\"[!--titleurl--]\" title=\"[!--oldtitle--]\">[!--title--]</a></li>[!--empirenews.listtemp--]','[ <a href=\\\"#ecms\\\" onclick=\\\"window.open(\\\'[!--down.url--]\\\',\\\'\\\',\\\'width=300,height=300,resizable=yes\\\');\\\">[!--down.name--]</a> ]','[ <a href=\\\"#ecms\\\" onclick=\\\"window.open(\\\'[!--down.url--]\\\',\\\'\\\',\\\'width=300,height=300,resizable=yes\\\');\\\">[!--down.name--]</a> ]','页次：[!--thispage--]/[!--pagenum--]&nbsp;每页[!--lencord--]&nbsp;总数[!--num--]&nbsp;&nbsp;&nbsp;&nbsp;[!--pagelink--]&nbsp;&nbsp;&nbsp;&nbsp;转到:[!--options--]','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>留言板 - Powered by EmpireCMS</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--bname--]\\\" />\r\n<meta name=\\\"description\\\" content=\\\"[!--bname--]\\\" />\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.dtheader--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n<tr valign=\\\"top\\\">\r\n<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n<tr>\r\n<td>现在的位置：<a href=../../../>首页</a>&nbsp;>&nbsp;[!--bname--]\r\n</td>\r\n</tr>\r\n</table><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n	<tr>\r\n		<td><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"2\\\">\r\n			<tr>\r\n				<td align=\\\"center\\\" bgcolor=\\\"#E1EFFB\\\"><strong>[!--bname--]</strong></td>\r\n			</tr>\r\n			<tr>\r\n				<td align=\\\"left\\\" valign=\\\"top\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"4\\\" cellspacing=\\\"0\\\" bgcolor=\\\"#FFFFFF\\\">\r\n						<tr>\r\n							<td height=\\\"100%\\\" valign=\\\"top\\\" bgcolor=\\\"#FFFFFF\\\"> [!--empirenews.listtemp--]\r\n								<table width=\\\"92%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"4\\\" cellspacing=\\\"1\\\" bgcolor=\\\"#F4F9FD\\\" class=\\\"tableborder\\\">\r\n										<tr class=\\\"header\\\">\r\n											<td width=\\\"55%\\\" height=\\\"23\\\">发布者: [!--name--] </td>\r\n											<td width=\\\"45%\\\">发布时间: [!--lytime--] </td>\r\n										</tr>\r\n										<tr bgcolor=\\\"#FFFFFF\\\">\r\n											<td height=\\\"23\\\" colspan=\\\"2\\\"><table border=\\\"0\\\" width=\\\"100%\\\" cellspacing=\\\"1\\\" cellpadding=\\\"8\\\" bgcolor=\\\'#cccccc\\\'>\r\n													<tr>\r\n														<td width=\\\'100%\\\' bgcolor=\\\'#FFFFFF\\\' style=\\\'word-break:break-all\\\'> [!--lytext--] </td>\r\n													</tr>\r\n												</table>\r\n												[!--start.regbook--]\r\n												<table width=\\\"100%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\">\r\n													<tr>\r\n														<td><img src=\\\"../../data/images/regb.gif\\\" width=\\\"18\\\" height=\\\"18\\\" /><strong><font color=\\\"#FF0000\\\">回复:</font></strong> [!--retext--] </td>\r\n													</tr>\r\n												</table>\r\n												[!--end.regbook--] </td>\r\n										</tr>\r\n									</table>\r\n								<br />\r\n								[!--empirenews.listtemp--]\r\n								<table width=\\\"92%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"4\\\" cellspacing=\\\"1\\\">\r\n									<tr>\r\n										<td>分页: [!--listpage--]</td>\r\n									</tr>\r\n								</table>\r\n								<form action=\\\"../../enews/index.php\\\" method=\\\"post\\\" name=\\\"form1\\\" id=\\\"form1\\\">\r\n									<table width=\\\"92%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"4\\\" cellspacing=\\\"1\\\"class=\\\"tableborder\\\">\r\n										<tr class=\\\"header\\\">\r\n											<td colspan=\\\"2\\\" bgcolor=\\\"#F4F9FD\\\"><strong>请您留言:</strong></td>\r\n										</tr>\r\n										<tr bgcolor=\\\"#FFFFFF\\\">\r\n											<td width=\\\"20%\\\">姓名:</td>\r\n											<td width=\\\"722\\\" height=\\\"23\\\"><input name=\\\"name\\\" type=\\\"text\\\" id=\\\"name\\\" />\r\n												*</td>\r\n										</tr>\r\n										<tr bgcolor=\\\"#FFFFFF\\\">\r\n											<td>联系邮箱:</td>\r\n											<td height=\\\"23\\\"><input name=\\\"email\\\" type=\\\"text\\\" id=\\\"email\\\" />\r\n												*</td>\r\n										</tr>\r\n										<tr bgcolor=\\\"#FFFFFF\\\">\r\n											<td>联系电话:</td>\r\n											<td height=\\\"23\\\"><input name=\\\"mycall\\\" type=\\\"text\\\" id=\\\"mycall\\\" /></td>\r\n										</tr>\r\n										<tr bgcolor=\\\"#FFFFFF\\\">\r\n											<td>留言内容(*):</td>\r\n											<td height=\\\"23\\\"><textarea name=\\\"lytext\\\" cols=\\\"60\\\" rows=\\\"12\\\" id=\\\"lytext\\\"></textarea></td>\r\n										</tr>\r\n										<tr bgcolor=\\\"#FFFFFF\\\">\r\n											<td height=\\\"23\\\">&nbsp;</td>\r\n											<td height=\\\"23\\\"><input type=\\\"submit\\\" name=\\\"Submit3\\\" value=\\\"提交\\\" />\r\n											<input type=\\\"reset\\\" name=\\\"Submit22\\\" value=\\\"重置\\\" />\r\n											<input name=\\\"enews\\\" type=\\\"hidden\\\" id=\\\"enews\\\" value=\\\"AddGbook\\\" /></td>\r\n										</tr>\r\n									</table>\r\n								</form></td>\r\n						</tr>\r\n				</table></td>\r\n			</tr>\r\n		</table></td>\r\n	</tr>\r\n</table></td>\r\n</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>','<html>\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\">\r\n<title>登陆</title>\r\n<LINK href=\\\"../../data/images/qcss.css\\\" rel=stylesheet>\r\n</head>\r\n<body bgcolor=\\\"#ededed\\\" topmargin=\\\"0\\\">\r\n<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\">\r\n  <form name=login method=post action=\\\"../../member/doaction.php\\\">\r\n    <input type=hidden name=enews value=login>\r\n    <input type=hidden name=prtype value=1>\r\n    <tr> \r\n      <td height=\\\"23\\\" align=\\\"center\\\">\r\n      <div align=\\\"left\\\">\r\n      用户名：<input name=\\\"username\\\" type=\\\"text\\\" size=\\\"8\\\">&nbsp;\r\n      密码：<input name=\\\"password\\\" type=\\\"password\\\" size=\\\"8\\\">\r\n      <select name=\\\"lifetime\\\" id=\\\"lifetime\\\">\r\n         <option value=\\\"0\\\">不保存</option>\r\n         <option value=\\\"3600\\\">一小时</option>\r\n         <option value=\\\"86400\\\">一天</option>\r\n         <option value=\\\"2592000\\\">一个月</option>\r\n         <option value=\\\"315360000\\\">永久</option>\r\n      </select>&nbsp;\r\n      <input type=\\\"submit\\\" name=\\\"Submit\\\" value=\\\"登陆\\\">&nbsp;\r\n      <input type=\\\"button\\\" name=\\\"Submit2\\\" value=\\\"注册\\\" onclick=\\\"window.open(\\\'../register/\\\');\\\">\r\n      </div>\r\n      </td>\r\n    </tr>\r\n  </form>\r\n</table>\r\n</body>\r\n</html>\r\n[!--empirenews.template--]\r\n<html>\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\">\r\n<title>登陆</title>\r\n<LINK href=\\\"../../data/images/qcss.css\\\" rel=stylesheet>\r\n</head>\r\n<body bgcolor=\\\"#ededed\\\" topmargin=\\\"0\\\">\r\n<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\">\r\n    <tr>\r\n	<td height=\\\"23\\\" align=\\\"center\\\">\r\n	<div align=\\\"left\\\">\r\n		&raquo;&nbsp;<font color=red><b>[!--username--]</b></font>&nbsp;&nbsp;<a href=\\\"../my/\\\" target=\\\"_parent\\\">[!--groupname--]</a>&nbsp;[!--havemsg--]&nbsp;<a href=\\\"[!--news.url--]e/space/?userid=[!--userid--]\\\" target=_blank>我的空间</a>&nbsp;&nbsp;<a href=\\\"../msg/\\\" target=_blank>短信息</a>&nbsp;&nbsp;<a href=\\\"../fava/\\\" target=_blank>收藏夹</a>&nbsp;&nbsp;<a href=\\\"../cp/\\\" target=\\\"_parent\\\">控制面板</a>&nbsp;&nbsp;<a href=\\\"../../member/doaction.php?enews=exit&prtype=9\\\" onclick=\\\"return confirm(\\\'确认要退出?\\\');\\\">退出</a> \r\n	</div>\r\n	</td>\r\n    </tr>\r\n</table>\r\n</body>\r\n</html>',50,'Y-m-d H:i:s','<form name=login method=post action=\\\"[!--news.url--]e/member/doaction.php\\\">\r\n    <input type=hidden name=enews value=login>\r\n    <input type=hidden name=ecmsfrom value=9>\r\n    用户名：<input name=\\\"username\\\" type=\\\"text\\\" class=\\\"inputText\\\" size=\\\"16\\\" />&nbsp;\r\n    密码：<input name=\\\"password\\\" type=\\\"password\\\" class=\\\"inputText\\\" size=\\\"16\\\" />&nbsp;\r\n    <input type=\\\"submit\\\" name=\\\"Submit\\\" value=\\\"登陆\\\" class=\\\"inputSub\\\" />&nbsp;\r\n    <input type=\\\"button\\\" name=\\\"Submit2\\\" value=\\\"注册\\\" class=\\\"inputSub\\\" onclick=\\\"window.open(\\\'[!--news.url--]e/member/register/\\\');\\\" />\r\n</form>\r\n[!--empirenews.template--]\r\n&raquo;&nbsp;<font color=red><b>[!--username--]</b></font>&nbsp;&nbsp;<a href=\\\"[!--news.url--]e/member/my/\\\" target=\\\"_parent\\\">[!--groupname--]</a>&nbsp;[!--havemsg--]&nbsp;<a href=\\\"[!--news.url--]e/space/?userid=[!--userid--]\\\" target=_blank>我的空间</a>&nbsp;&nbsp;<a href=\\\"[!--news.url--]e/member/msg/\\\" target=_blank>短信息</a>&nbsp;&nbsp;<a href=\\\"[!--news.url--]e/member/fava/\\\" target=_blank>收藏夹</a>&nbsp;&nbsp;<a href=\\\"[!--news.url--]e/member/cp/\\\" target=\\\"_parent\\\">控制面板</a>&nbsp;&nbsp;<a href=\\\"[!--news.url--]e/member/doaction.php?enews=exit&ecmsfrom=9\\\" onclick=\\\"return confirm(\\\'确认要退出?\\\');\\\">退出</a>','<html>\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\">\r\n<title>[!--pagetitle--]</title>\r\n<meta name=\\\"keywords\\\" content=\\\"[!--pagekey--]\\\">\r\n<meta name=\\\"description\\\" content=\\\"[!--pagedes--]\\\">\r\n<link href=\\\"../../data/images/qcss.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\">\r\n</head>\r\n<body>\r\n<br>\r\n<br>\r\n<br>\r\n<br>\r\n<table align=\\\"center\\\" width=\\\"100%\\\">\r\n  <tr> \r\n    <td height=\\\"32\\\" align=center>\r\n	<a href=\\\"[!--down.url--]\\\" title=\\\"[!--title--] －[!--down.name--]\\\">\r\n	<img src=\\\"../../data/images/download.jpg\\\" border=0>\r\n	</a>\r\n	</td>\r\n  </tr>\r\n  <tr> \r\n    <td align=center>(点击下载)</td>\r\n  </tr>\r\n</table>\r\n<br>\r\n</body>\r\n</html>','[!--empirenews.listtemp--] \r\n      <table width=\\\"100%\\\" border=\\\"0\\\" align=\\\"center\\\" cellpadding=\\\"3\\\" cellspacing=\\\"1\\\" style=\\\"word-break:break-all; word-wrap:break-all;\\\">\r\n        <tr> \r\n          <td height=\\\"30\\\"><span class=\\\"name\\\">本站网友 [!--username--]</span> <font color=\\\"#666666\\\">ip:[!--plip--]</font></td>\r\n          <td><div align=\\\"right\\\"><font color=\\\"#666666\\\">[!--pltime--] 发表</font></div></td>\r\n        </tr>\r\n        <tr valign=\\\"top\\\"> \r\n          <td height=\\\"50\\\" colspan=\\\"2\\\" class=\\\"text\\\">[!--pltext--]</td>\r\n        </tr>\r\n        <tr> \r\n          <td height=\\\"30\\\">&nbsp;</td>\r\n          <td><div align=\\\"right\\\" class=\\\"re\\\"> \r\n              <a href=\\\"JavaScript:makeRequest(\\\'[!--news.url--]e/pl/doaction.php?enews=DoForPl&plid=[!--plid--]&classid=[!--classid--]&id=[!--id--]&dopl=1&doajax=1&ajaxarea=zcpldiv[!--plid--]\\\',\\\'EchoReturnedText\\\',\\\'GET\\\',\\\'\\\');\\\">支持</a>[<span id=\\\"zcpldiv[!--plid--]\\\">[!--zcnum--]</span>]&nbsp; \r\n              <a href=\\\"JavaScript:makeRequest(\\\'[!--news.url--]e/pl/doaction.php?enews=DoForPl&plid=[!--plid--]&classid=[!--classid--]&id=[!--id--]&dopl=0&doajax=1&ajaxarea=fdpldiv[!--plid--]\\\',\\\'EchoReturnedText\\\',\\\'GET\\\',\\\'\\\');\\\">反对</a>[<span id=\\\"fdpldiv[!--plid--]\\\">[!--fdnum--]</span>]\r\n            </div></td>\r\n        </tr>\r\n      </table>\r\n      <table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"1\\\" cellpadding=\\\"3\\\">\r\n        <tr>\r\n          <td background=\\\"[!--news.url--]skin/default/images/plhrbg.gif\\\"></td>\r\n        </tr>\r\n      </table>\r\n[!--empirenews.listtemp--]','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>搜索 - Powered by EmpireCMS</title>\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n<style type=\\\"text/css\\\">\r\n<!--\r\n.r {\r\ndisplay:inline;\r\nfont-weight:normal;\r\nmargin:0;\r\nfont-size:16px;\r\nmargin-top:10px;\r\n}\r\n.a{color:green}\r\n.fl{color:#77c}\r\n-->\r\n</style>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.dtheader--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>现在的位置：<a href=\\\"[!--news.url--]\\\">首页</a>&nbsp;>&nbsp;搜索</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><form action=\\\'index.php\\\' method=\\\"GET\\\" name=\\\"search_news\\\" id=\\\"search_news\\\">\r\n							<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"6\\\" cellpadding=\\\"0\\\">\r\n								<tr>\r\n									<td height=\\\"32\\\">关键字：\r\n										<input name=\\\"keyboard\\\" type=\\\"text\\\" id=\\\"keyboard\\\" value=\\\"[!--keyboard--]\\\" size=\\\"42\\\" />\r\n                    <select name=\\\"field\\\" id=\\\"field\\\">\r\n                      <option value=\\\"1\\\">全文</option>\r\n                      <option value=\\\"2\\\">标题</option>\r\n                      <option value=\\\"3\\\">内容</option>\r\n                    </select> \r\n                    <input type=\\\"submit\\\" name=\\\"Submit22\\\" value=\\\"搜索\\\" />\r\n                    <font color=\\\"#666666\\\">(多个关键字请用&quot;空格&quot;隔开)</font> </td>\r\n								</tr>\r\n							</table>\r\n						</form>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"6\\\">\r\n							<tr>\r\n								<td>系统搜索到约有<strong>[!--num--]</strong>项符合<strong>[!--keyboard--]</strong>的查询结果</td>\r\n							</tr>\r\n						</table>\r\n						[!--empirenews.listtemp--]\r\n						<h2 class=\\\"r\\\"><span>[!--no.num--].</span> <a class=\\\"l\\\" href=\\\"[!--titleurl--]\\\" target=\\\"_blank\\\">[!--title--]</a></h2>\r\n						<table width=\\\"80%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n							<tbody>\r\n							<tr>\r\n								<td>[!--smalltext--]</td>\r\n							</tr>\r\n							<tr>\r\n								<td><span class=\\\"a\\\">[!--titleurl--] - [!--newstime--]</span></td>\r\n							</tr>\r\n							<tr>\r\n								<td>&nbsp;</td>\r\n							</tr>\r\n							</tbody>\r\n						</table>\r\n						[!--empirenews.listtemp--]\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n							<tr>\r\n								<td>[!--listpage--]</td>\r\n							</tr>\r\n						</table></td>\r\n				</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',120,'Y-m-d H:i:s');

/*Table structure for table `phome_enewspubvar` */

DROP TABLE IF EXISTS `phome_enewspubvar`;

CREATE TABLE `phome_enewspubvar` (
  `varid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `myvar` varchar(60) NOT NULL DEFAULT '',
  `varname` varchar(20) NOT NULL DEFAULT '',
  `varvalue` text NOT NULL,
  `varsay` varchar(255) NOT NULL DEFAULT '',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tocache` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`varid`),
  UNIQUE KEY `varname` (`varname`),
  KEY `classid` (`classid`),
  KEY `tocache` (`tocache`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspubvar` */

/*Table structure for table `phome_enewspubvarclass` */

DROP TABLE IF EXISTS `phome_enewspubvarclass`;

CREATE TABLE `phome_enewspubvarclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  `classsay` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewspubvarclass` */

/*Table structure for table `phome_enewsqmsg` */

DROP TABLE IF EXISTS `phome_enewsqmsg`;

CREATE TABLE `phome_enewsqmsg` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL DEFAULT '',
  `msgtext` text NOT NULL,
  `haveread` tinyint(1) NOT NULL DEFAULT '0',
  `msgtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to_username` varchar(30) NOT NULL DEFAULT '',
  `from_userid` int(10) unsigned NOT NULL DEFAULT '0',
  `from_username` varchar(30) NOT NULL DEFAULT '',
  `isadmin` tinyint(1) NOT NULL DEFAULT '0',
  `issys` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`),
  KEY `to_username` (`to_username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsqmsg` */

/*Table structure for table `phome_enewssearch` */

DROP TABLE IF EXISTS `phome_enewssearch`;

CREATE TABLE `phome_enewssearch` (
  `searchid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `keyboard` varchar(255) NOT NULL DEFAULT '',
  `searchtime` int(10) unsigned NOT NULL DEFAULT '0',
  `searchclass` varchar(255) NOT NULL DEFAULT '',
  `result_num` int(10) unsigned NOT NULL DEFAULT '0',
  `searchip` varchar(20) NOT NULL DEFAULT '',
  `classid` varchar(255) NOT NULL DEFAULT '',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `orderby` varchar(30) NOT NULL DEFAULT '0',
  `myorder` tinyint(1) NOT NULL DEFAULT '0',
  `checkpass` varchar(32) NOT NULL DEFAULT '',
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `tempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `iskey` tinyint(1) NOT NULL DEFAULT '0',
  `andsql` text NOT NULL,
  `trueclassid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`searchid`),
  KEY `checkpass` (`checkpass`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssearch` */

/*Table structure for table `phome_enewssearchall` */

DROP TABLE IF EXISTS `phome_enewssearchall`;

CREATE TABLE `phome_enewssearchall` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `infotime` int(10) unsigned NOT NULL DEFAULT '0',
  `infotext` mediumtext NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `id` (`id`,`classid`,`infotime`),
  FULLTEXT KEY `title` (`title`,`infotext`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssearchall` */

/*Table structure for table `phome_enewssearchall_load` */

DROP TABLE IF EXISTS `phome_enewssearchall_load`;

CREATE TABLE `phome_enewssearchall_load` (
  `lid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `titlefield` varchar(30) NOT NULL DEFAULT '',
  `infotextfield` varchar(30) NOT NULL DEFAULT '',
  `smalltextfield` varchar(30) NOT NULL DEFAULT '',
  `loadnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssearchall_load` */

/*Table structure for table `phome_enewssearchtemp` */

DROP TABLE IF EXISTS `phome_enewssearchtemp`;

CREATE TABLE `phome_enewssearchtemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  `subnews` smallint(6) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `listvar` text NOT NULL,
  `rownum` smallint(6) NOT NULL DEFAULT '0',
  `modid` smallint(6) NOT NULL DEFAULT '0',
  `showdate` varchar(50) NOT NULL DEFAULT '',
  `subtitle` smallint(6) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `docode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssearchtemp` */

insert  into `phome_enewssearchtemp`(`tempid`,`tempname`,`temptext`,`subnews`,`isdefault`,`listvar`,`rownum`,`modid`,`showdate`,`subtitle`,`classid`,`docode`) values (1,'默认搜索模板','<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\r\n<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\">\r\n<head>\r\n<meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" />\r\n<title>[!--keyboard--] 搜索结果 - Powered by EmpireCMS</title>\r\n<link href=\\\"[!--news.url--]skin/default/css/style.css\\\" rel=\\\"stylesheet\\\" type=\\\"text/css\\\" />\r\n<script type=\\\"text/javascript\\\" src=\\\"[!--news.url--]skin/default/js/tabs.js\\\"></script>\r\n<style type=\\\"text/css\\\">\r\n<!--\r\n.r {\r\ndisplay:inline;\r\nfont-weight:normal;\r\nmargin:0;\r\nfont-size:16px;\r\nmargin-top:10px;\r\n}\r\n.a{color:green}\r\n.fl{color:#77c}\r\n-->\r\n</style>\r\n</head>\r\n<body class=\\\"listpage\\\">\r\n[!--temp.dtheader--]\r\n<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"10\\\" cellpadding=\\\"0\\\">\r\n	<tr valign=\\\"top\\\">\r\n		<td class=\\\"list_content\\\"><table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"position\\\">\r\n				<tr>\r\n					<td>现在的位置：<a href=\\\"[!--news.url--]\\\" class=\\\"classlinkclass\\\">首页</a>&nbsp;>&nbsp;<a href=\\\"[!--news.url--]search/\\\" class=\\\"classlinkclass\\\">高级搜索</a>&nbsp;>&nbsp;搜索结果</td>\r\n				</tr>\r\n			</table>\r\n			<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"box\\\">\r\n				<tr>\r\n					<td><form action=\\\'../../search/index.php\\\' method=\\\"post\\\" name=\\\"search_news\\\" id=\\\"search_news\\\">\r\n							<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"6\\\" cellpadding=\\\"0\\\">\r\n								<input type=\\\"hidden\\\" name=\\\"show\\\" value=\\\"title\\\" />\r\n								<tr>\r\n									<td height=\\\"32\\\">关键字：\r\n										<input name=\\\"keyboard\\\" type=\\\"text\\\" id=\\\"keyboard\\\" value=\\\"[!--keyboard--]\\\" size=\\\"42\\\" />\r\n										<input type=\\\"submit\\\" name=\\\"Submit22\\\" value=\\\"搜索\\\" />\r\n										&nbsp;\r\n										<input type=\\\"button\\\" name=\\\"Submit\\\" value=\\\"高级搜索\\\" onclick=\\\"self.location.href=\\\'../../../search/\\\'\\\" />\r\n										(多个关键字请用&quot;空格&quot;隔开) </td>\r\n								</tr>\r\n							</table>\r\n						</form>\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"6\\\">\r\n							<tr>\r\n								<td>系统搜索到约有<strong>[!--ecms.num--]</strong>项符合<strong>[!--keyboard--]</strong>的查询结果</td>\r\n							</tr>\r\n						</table>\r\n						[!--empirenews.listtemp--]\r\n						<!--list.var1-->\r\n						[!--empirenews.listtemp--]\r\n						<table width=\\\"100%\\\" border=\\\"0\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" class=\\\"list_page\\\">\r\n							<tr>\r\n								<td>[!--show.page--]</td>\r\n							</tr>\r\n						</table></td>\r\n				</tr>\r\n			</table></td>\r\n	</tr>\r\n</table>\r\n[!--temp.footer--]\r\n</body>\r\n</html>',180,1,'<h2 class=\\\"r\\\"><span>[!--no.num--].</span> <a class=\\\"l\\\" href=\\\"[!--titleurl--]\\\" target=\\\"_blank\\\">[!--title--]</a></h2>\r\n<table width=\\\"80%\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\">\r\n	<tbody>\r\n		<tr>\r\n			<td>[!--smalltext--]</td>\r\n		</tr>\r\n		<tr>\r\n			<td><span class=\\\"a\\\">[!--titleurl--] - [!--newstime--]</span> - <a class=\\\"fl\\\" href=\\\"[!--this.classlink--]\\\" target=\\\"_blank\\\">[!--this.classname--]</a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',1,1,'Y-m-d',0,0,0);

/*Table structure for table `phome_enewssearchtempclass` */

DROP TABLE IF EXISTS `phome_enewssearchtempclass`;

CREATE TABLE `phome_enewssearchtempclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssearchtempclass` */

/*Table structure for table `phome_enewsshop_address` */

DROP TABLE IF EXISTS `phome_enewsshop_address`;

CREATE TABLE `phome_enewsshop_address` (
  `addressid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `addressname` char(50) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `truename` char(20) NOT NULL DEFAULT '',
  `oicq` char(20) NOT NULL DEFAULT '',
  `msn` char(60) NOT NULL DEFAULT '',
  `email` char(60) NOT NULL DEFAULT '',
  `address` char(255) NOT NULL DEFAULT '',
  `zip` char(8) NOT NULL DEFAULT '',
  `mycall` char(30) NOT NULL DEFAULT '',
  `phone` char(30) NOT NULL DEFAULT '',
  `signbuild` char(100) NOT NULL DEFAULT '',
  `besttime` char(120) NOT NULL DEFAULT '',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`addressid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshop_address` */

/*Table structure for table `phome_enewsshop_ddlog` */

DROP TABLE IF EXISTS `phome_enewsshop_ddlog`;

CREATE TABLE `phome_enewsshop_ddlog` (
  `logid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ddid` int(10) unsigned NOT NULL DEFAULT '0',
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ecms` varchar(30) NOT NULL DEFAULT '',
  `bz` varchar(255) NOT NULL DEFAULT '',
  `addbz` varchar(255) NOT NULL DEFAULT '',
  `logtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`logid`),
  KEY `ddid` (`ddid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshop_ddlog` */

/*Table structure for table `phome_enewsshop_precode` */

DROP TABLE IF EXISTS `phome_enewsshop_precode`;

CREATE TABLE `phome_enewsshop_precode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prename` varchar(30) NOT NULL DEFAULT '',
  `precode` varchar(36) NOT NULL DEFAULT '',
  `premoney` int(10) unsigned NOT NULL DEFAULT '0',
  `pretype` tinyint(1) NOT NULL DEFAULT '0',
  `reuse` tinyint(1) NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `groupid` varchar(255) NOT NULL DEFAULT '',
  `classid` text NOT NULL,
  `musttotal` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `precode` (`precode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshop_precode` */

/*Table structure for table `phome_enewsshop_set` */

DROP TABLE IF EXISTS `phome_enewsshop_set`;

CREATE TABLE `phome_enewsshop_set` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `shopddgroupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `buycarnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `havefp` tinyint(1) NOT NULL DEFAULT '0',
  `fpnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `fpname` text NOT NULL,
  `ddmust` text NOT NULL,
  `haveatt` tinyint(1) NOT NULL DEFAULT '0',
  `shoptbs` varchar(255) NOT NULL DEFAULT '',
  `buystep` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `shoppsmust` tinyint(1) NOT NULL DEFAULT '0',
  `shoppayfsmust` tinyint(1) NOT NULL DEFAULT '0',
  `dddeltime` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cutnumtype` tinyint(1) NOT NULL DEFAULT '0',
  `cutnumtime` int(10) unsigned NOT NULL DEFAULT '0',
  `freepstotal` int(10) unsigned NOT NULL DEFAULT '0',
  `singlenum` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshop_set` */

insert  into `phome_enewsshop_set`(`id`,`shopddgroupid`,`buycarnum`,`havefp`,`fpnum`,`fpname`,`ddmust`,`haveatt`,`shoptbs`,`buystep`,`shoppsmust`,`shoppayfsmust`,`dddeltime`,`cutnumtype`,`cutnumtime`,`freepstotal`,`singlenum`) values (1,0,0,0,10,'商品名称\r\n办公用品\r\n日用品',',truename,mycall,address,',0,',shop,',0,1,1,0,0,120,0,0);

/*Table structure for table `phome_enewsshopdd` */

DROP TABLE IF EXISTS `phome_enewsshopdd`;

CREATE TABLE `phome_enewsshopdd` (
  `ddid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ddno` varchar(30) NOT NULL DEFAULT '',
  `ddtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `outproduct` tinyint(1) NOT NULL DEFAULT '0',
  `haveprice` tinyint(1) NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `truename` varchar(20) NOT NULL DEFAULT '',
  `oicq` varchar(25) NOT NULL DEFAULT '',
  `msn` varchar(120) NOT NULL DEFAULT '',
  `email` varchar(120) NOT NULL DEFAULT '',
  `mycall` varchar(30) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `zip` varchar(8) NOT NULL DEFAULT '',
  `psid` smallint(6) NOT NULL DEFAULT '0',
  `psname` varchar(60) NOT NULL DEFAULT '',
  `pstotal` float(11,2) NOT NULL DEFAULT '0.00',
  `alltotal` float(11,2) NOT NULL DEFAULT '0.00',
  `payfsid` smallint(6) NOT NULL DEFAULT '0',
  `payfsname` varchar(60) NOT NULL DEFAULT '',
  `payby` tinyint(4) NOT NULL DEFAULT '0',
  `alltotalfen` float(11,2) NOT NULL DEFAULT '0.00',
  `fp` tinyint(1) NOT NULL DEFAULT '0',
  `fptt` varchar(255) NOT NULL DEFAULT '',
  `fptotal` float(11,2) NOT NULL DEFAULT '0.00',
  `fpname` varchar(50) NOT NULL DEFAULT '',
  `userip` varchar(20) NOT NULL DEFAULT '',
  `signbuild` varchar(100) NOT NULL DEFAULT '',
  `besttime` varchar(120) NOT NULL DEFAULT '',
  `pretotal` float(11,2) NOT NULL DEFAULT '0.00',
  `ddtruetime` int(10) unsigned NOT NULL DEFAULT '0',
  `havecutnum` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ddid`),
  KEY `ddno` (`ddno`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshopdd` */

/*Table structure for table `phome_enewsshopdd_add` */

DROP TABLE IF EXISTS `phome_enewsshopdd_add`;

CREATE TABLE `phome_enewsshopdd_add` (
  `ddid` int(10) unsigned NOT NULL DEFAULT '0',
  `buycar` mediumtext NOT NULL,
  `bz` text NOT NULL,
  `retext` text NOT NULL,
  PRIMARY KEY (`ddid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshopdd_add` */

/*Table structure for table `phome_enewsshoppayfs` */

DROP TABLE IF EXISTS `phome_enewsshoppayfs`;

CREATE TABLE `phome_enewsshoppayfs` (
  `payid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `payname` varchar(60) NOT NULL DEFAULT '',
  `payurl` varchar(255) NOT NULL DEFAULT '',
  `paysay` text NOT NULL,
  `userpay` tinyint(1) NOT NULL DEFAULT '0',
  `userfen` tinyint(1) NOT NULL DEFAULT '0',
  `isclose` tinyint(1) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshoppayfs` */

insert  into `phome_enewsshoppayfs`(`payid`,`payname`,`payurl`,`paysay`,`userpay`,`userfen`,`isclose`,`isdefault`) values (1,'邮政汇款','','邮政汇款地址:******',0,0,0,0),(2,'银行转帐','','银行转帐帐号:******',0,0,0,0),(3,'网银支付','/e/payapi/ShopPay.php?paytype=alipay','<p>网银支付</p>',0,0,0,1),(4,'预付款支付','','预付款支付',1,0,0,0),(5,'货到付款(或上门收款)','','货到付款(或上门收款)说明',0,0,0,0),(6,'点数购买','','点数购买',0,1,0,0);

/*Table structure for table `phome_enewsshopps` */

DROP TABLE IF EXISTS `phome_enewsshopps`;

CREATE TABLE `phome_enewsshopps` (
  `pid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pname` varchar(60) NOT NULL DEFAULT '',
  `price` float(11,2) NOT NULL DEFAULT '0.00',
  `otherprice` text NOT NULL,
  `psay` text NOT NULL,
  `isclose` tinyint(1) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsshopps` */

insert  into `phome_enewsshopps`(`pid`,`pname`,`price`,`otherprice`,`psay`,`isclose`,`isdefault`) values (1,'送货上门',10.00,'','送货上门',0,0),(2,'特快专递（EMS）',25.00,'','特快专递（EMS）',0,0),(3,'普通邮递',5.00,'','普通邮递',0,1),(4,'邮局快邮',12.00,'','邮局快邮',0,0);

/*Table structure for table `phome_enewssp` */

DROP TABLE IF EXISTS `phome_enewssp`;

CREATE TABLE `phome_enewssp` (
  `spid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spname` varchar(30) NOT NULL DEFAULT '',
  `varname` varchar(30) NOT NULL DEFAULT '',
  `sppic` varchar(255) NOT NULL DEFAULT '',
  `spsay` varchar(255) NOT NULL DEFAULT '',
  `sptype` tinyint(1) NOT NULL DEFAULT '0',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `maxnum` int(11) NOT NULL DEFAULT '0',
  `sptime` int(10) unsigned NOT NULL DEFAULT '0',
  `groupid` text NOT NULL,
  `userclass` text NOT NULL,
  `username` text NOT NULL,
  `isclose` tinyint(1) NOT NULL DEFAULT '0',
  `cladd` tinyint(1) NOT NULL DEFAULT '0',
  `refile` tinyint(1) NOT NULL DEFAULT '0',
  `spfile` varchar(255) NOT NULL DEFAULT '',
  `spfileline` smallint(6) NOT NULL DEFAULT '0',
  `spfilesub` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`spid`),
  UNIQUE KEY `varname` (`varname`),
  KEY `refile` (`refile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssp` */

/*Table structure for table `phome_enewssp_1` */

DROP TABLE IF EXISTS `phome_enewssp_1`;

CREATE TABLE `phome_enewssp_1` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spid` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(200) NOT NULL DEFAULT '',
  `titlepic` varchar(200) NOT NULL DEFAULT '',
  `bigpic` varchar(200) NOT NULL DEFAULT '',
  `titleurl` varchar(200) NOT NULL DEFAULT '',
  `smalltext` varchar(255) NOT NULL DEFAULT '',
  `titlefont` varchar(20) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepre` varchar(30) NOT NULL DEFAULT '',
  `titlenext` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`sid`),
  KEY `spid` (`spid`),
  KEY `newstime` (`newstime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssp_1` */

/*Table structure for table `phome_enewssp_2` */

DROP TABLE IF EXISTS `phome_enewssp_2`;

CREATE TABLE `phome_enewssp_2` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spid` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sid`),
  KEY `spid` (`spid`),
  KEY `newstime` (`newstime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssp_2` */

/*Table structure for table `phome_enewssp_3` */

DROP TABLE IF EXISTS `phome_enewssp_3`;

CREATE TABLE `phome_enewssp_3` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spid` int(10) unsigned NOT NULL DEFAULT '0',
  `sptext` mediumtext NOT NULL,
  PRIMARY KEY (`sid`),
  UNIQUE KEY `spid` (`spid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssp_3` */

/*Table structure for table `phome_enewssp_3_bak` */

DROP TABLE IF EXISTS `phome_enewssp_3_bak`;

CREATE TABLE `phome_enewssp_3_bak` (
  `bid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sid` int(10) unsigned NOT NULL DEFAULT '0',
  `spid` int(10) unsigned NOT NULL DEFAULT '0',
  `sptext` mediumtext NOT NULL,
  `lastuser` varchar(30) NOT NULL DEFAULT '',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bid`),
  KEY `sid` (`sid`),
  KEY `spid` (`spid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssp_3_bak` */

/*Table structure for table `phome_enewsspacestyle` */

DROP TABLE IF EXISTS `phome_enewsspacestyle`;

CREATE TABLE `phome_enewsspacestyle` (
  `styleid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `stylename` varchar(30) NOT NULL DEFAULT '',
  `stylepic` varchar(255) NOT NULL DEFAULT '',
  `stylesay` varchar(255) NOT NULL DEFAULT '',
  `stylepath` varchar(30) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `membergroup` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`styleid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsspacestyle` */

insert  into `phome_enewsspacestyle`(`styleid`,`stylename`,`stylepic`,`stylesay`,`stylepath`,`isdefault`,`membergroup`) values (1,'默认个人空间模板','','默认个人空间模板','default',1,',1,2,'),(2,'默认企业空间模板','','默认企业空间模板','comdefault',0,',3,4,');

/*Table structure for table `phome_enewsspclass` */

DROP TABLE IF EXISTS `phome_enewsspclass`;

CREATE TABLE `phome_enewsspclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  `classsay` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsspclass` */

/*Table structure for table `phome_enewssql` */

DROP TABLE IF EXISTS `phome_enewssql`;

CREATE TABLE `phome_enewssql` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `sqlname` varchar(60) NOT NULL DEFAULT '',
  `sqltext` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewssql` */

/*Table structure for table `phome_enewstable` */

DROP TABLE IF EXISTS `phome_enewstable`;

CREATE TABLE `phome_enewstable` (
  `tid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tbname` varchar(60) NOT NULL DEFAULT '',
  `tname` varchar(60) NOT NULL DEFAULT '',
  `tsay` text NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `datatbs` text NOT NULL,
  `deftb` varchar(6) NOT NULL DEFAULT '',
  `yhid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `mid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `intb` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstable` */

insert  into `phome_enewstable`(`tid`,`tbname`,`tname`,`tsay`,`isdefault`,`datatbs`,`deftb`,`yhid`,`mid`,`intb`) values (1,'news','新闻系统数据表','新闻系统数据表',1,',1,','1',0,1,0);

/*Table structure for table `phome_enewstags` */

DROP TABLE IF EXISTS `phome_enewstags`;

CREATE TABLE `phome_enewstags` (
  `tagid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tagname` char(20) NOT NULL DEFAULT '',
  `num` int(10) unsigned NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tagid`),
  UNIQUE KEY `tagname` (`tagname`),
  KEY `isgood` (`isgood`),
  KEY `cid` (`cid`),
  KEY `num` (`num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstags` */

/*Table structure for table `phome_enewstagsclass` */

DROP TABLE IF EXISTS `phome_enewstagsclass`;

CREATE TABLE `phome_enewstagsclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstagsclass` */

/*Table structure for table `phome_enewstagsdata` */

DROP TABLE IF EXISTS `phome_enewstagsdata`;

CREATE TABLE `phome_enewstagsdata` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tagid` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `mid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tid`),
  KEY `tagid` (`tagid`),
  KEY `classid` (`classid`),
  KEY `id` (`id`),
  KEY `newstime` (`newstime`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstagsdata` */

/*Table structure for table `phome_enewstask` */

DROP TABLE IF EXISTS `phome_enewstask`;

CREATE TABLE `phome_enewstask` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `taskname` varchar(60) NOT NULL DEFAULT '',
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `isopen` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(60) NOT NULL DEFAULT '',
  `lastdo` int(10) unsigned NOT NULL DEFAULT '0',
  `doweek` char(1) NOT NULL DEFAULT '0',
  `doday` char(2) NOT NULL DEFAULT '0',
  `dohour` char(2) NOT NULL DEFAULT '0',
  `dominute` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstask` */

/*Table structure for table `phome_enewstempbak` */

DROP TABLE IF EXISTS `phome_enewstempbak`;

CREATE TABLE `phome_enewstempbak` (
  `bid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  `subnews` smallint(6) NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `listvar` text NOT NULL,
  `rownum` smallint(6) NOT NULL DEFAULT '0',
  `modid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `showdate` varchar(50) NOT NULL DEFAULT '',
  `subtitle` smallint(6) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `docode` tinyint(1) NOT NULL DEFAULT '0',
  `baktime` int(10) unsigned NOT NULL DEFAULT '0',
  `temptype` varchar(30) NOT NULL DEFAULT '',
  `gid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastuser` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`bid`),
  KEY `tempid` (`tempid`),
  KEY `temptype` (`temptype`),
  KEY `gid` (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstempbak` */

insert  into `phome_enewstempbak`(`bid`,`tempid`,`tempname`,`temptext`,`subnews`,`isdefault`,`listvar`,`rownum`,`modid`,`showdate`,`subtitle`,`classid`,`docode`,`baktime`,`temptype`,`gid`,`lastuser`) values (134,1,'header','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/js/id.js\\\"></script>\r\n<title>吴江实验小学</title>\r\n</head>\r\n<body>\r\n<div class=\\\"D\\\">\r\n	<!--顶部-->\r\n	<div class=\\\"D_bg top\\\">\r\n		<div class=\\\"D_top clear\\\">\r\n			<ul>\r\n				<li>\r\n					<a href=\\\"index.htm/l\\\">返回首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">速递邮局</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">办公OA</a>\r\n				</li>\r\n			</ul>\r\n			<div>\r\n				<a class=\\\"\\\" href=\\\"###\\\">常用信息</a>\r\n				<div>\r\n					<input class=\\\"D_top_text\\\" type=\\\"text\\\" value=\\\"\\\" />\r\n					<input class=\\\"D_top_btn\\\" type=\\\"button\\\" value=\\\"\\\" />\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<!--logo部分,导航部分-->\r\n	<div class=\\\"D_head clear\\\">\r\n		<div class=\\\"D_head_logo\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/images/logo.jpg\\\" alt=\\\"\\\">\r\n			</a>\r\n		</div>\r\n		<div class=\\\"D_head_nav\\\">\r\n			<img  class=\\\"D_head_nav_img\\\" src=\\\"/skin/default/images/nav1.png\\\" alt=\\\"\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li  class=\\\"onnav\\\">\r\n					<a href=\\\"/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">关于我们</a>\r\n					<dl>[showclasstemp]1,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">集团组成</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"/chengzhong/\\\" target=\\\"_blank\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/aide/\\\" target=\\\"_blank\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/taihu/\\\" target=\\\"_blank\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/szw/\\\" target=\\\"_blank\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">新闻动态</a>\r\n					<dl>[showclasstemp]8,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">教师频道</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">管理平台</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">资源中心</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',1,0,'页面头部',0,0,'',0,0,0,1473381838,'tempvar',1,'dd'),(21,2,'首页新闻调用','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"[!--titleurl--]\\\">\r\n						<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">[!--smalltext--]...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>',1,1,'Y-m-d H:i:s',0,0,0,1471847789,'bqtemp',1,'manage'),(95,2,'footer','<div class=\\\"D_bg about\\\">\r\n		<div class=\\\"D_about\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li>\r\n					<dl>\r\n						<dt>关于我们</dt>\r\n						[showclasstemp]1,13,0,0[/showclasstemp]\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>集团组成</dt>\r\n						<dd>\r\n							<a href=\\\"/chengzhong/\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/aide/\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>新闻动态</dt>\r\n						[showclasstemp]8,13,0,0[/showclasstemp]					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>教师频道</dt>\r\n						<dd>\r\n							<a  href=\\\"index.html\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>管理平台</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>资源中心</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li class=\\\"li_dis\\\">\r\n					<dl>\r\n						<dt>联系我们</dt>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<span>E-mail：edu@wjsx.com</span>\r\n						</dd>\r\n						<dd>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n		<div class=\\\"D_bg bottom\\\">\r\n		<div class=\\\"D_bottom\\\">\r\n			<span>Copyright 2015 吴江实验小学 保留所有版权 苏ICP备10057875号</span>\r\n			<div class=\\\"D_bottom_links\\\">\r\n				<span class=\\\"D_bottom_links_btn\\\">相关连接</span>\r\n				<ul>[ecmsinfo]13,10,32,0,0,3,0[/ecmsinfo]</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'页面尾部',0,0,'',0,0,0,1472785204,'tempvar',1,'manage'),(24,1,'','[!--temp.header--]\r\n<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>新闻动态</span>\r\n				<a href=\\\"/news/hnews/\\\">更多...</a>\r\n			</div>\r\n			[ecmsinfo]9,3,32,0,0,2,0[/ecmsinfo]\r\n		</div>\r\n		<div class=\\\"D_news_right clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>集团故事</span>\r\n				<a href=\\\"/news/jtgs/\\\">更多...</a>\r\n			</div>\r\n			<[ecmsinfo]10,4,32,0,0,14,0[/ecmsinfo]		</div>\r\n	</div>\r\n[!--temp.footer--]',0,0,'',0,0,'',0,0,0,1471847980,'pubindextemp',1,'manage'),(4,1,'关于内容模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li>\r\n				<a href=\\\"about.html\\\">集团概况</a>\r\n			</li>\r\n			<li class=\\\"onwebnav\\\">\r\n				<a href=\\\"###\\\">总校长寄语</a>\r\n			</li>\r\n			<li>\r\n				<a href=\\\"ab2.html#\\\">集团战略</a>\r\n			</li>\r\n			<li>\r\n				<a href=\\\"ab3.html\\\">集团荣誉</a>\r\n			</li>\r\n			<li>\r\n				<a href=\\\"ab4.html\\\">地理位置</a>\r\n			</li>\r\n			<li>\r\n				<a href=\\\"ab5.html\\\">联系我们</a>\r\n			</li>\r\n		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		<a href=\\\"###\\\">首页</a>\r\n		&gt;\r\n		<a href=\\\"###\\\">关于我们</a>\r\n		&gt;\r\n		<span>总校长寄语</span>\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		<img src=\\\"/skin/default/images/speech.jpg\\\"  style=\\\"max-width:427px; w\\\" alt=\\\"\\\"/>\r\n	</div>[!--temp.footer--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1471845056,'newstemp',1,'manage'),(5,1,'默认新闻列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onwebnav\\\">\r\n				<a href=\\\"###\\\">新闻资讯</a>\r\n			</li>\r\n			<li>\r\n				<a href=\\\"ns1.html\\\">集团故事</a>\r\n			</li>\r\n			<li>\r\n				<a href=\\\"case.html\\\">图片掠影</a>\r\n			</li>\r\n			<li>\r\n				<a href=\\\"news.html\\\">视频关注</a>\r\n			</li>\r\n		</ul>\r\n	</div>\r\n	\r\n	<div class=\\\"webnav\\\">\r\n		<a href=\\\"###\\\">首页</a>\r\n		&gt;\r\n		<a href=\\\"###\\\">新闻动态</a>\r\n		&gt;\r\n		<span>新闻资讯</span>\r\n	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left D_news_conter clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>新闻动态</span>\r\n				<a href=\\\"###\\\">更多...</a>\r\n			</div>\r\n			<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"news_d.html\\\">\r\n						<img src=\\\"/skin/default/images/20160302.jpg\\\" alt=\\\"\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"news_d.html\\\">建构“和融向上”行政管理团队</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">2月18日下午，在本部东区南会议室隆重举行吴江实验小学校区管理交流大会，由校党支部迮美娟书记主持。...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"news_d.html\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"###\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"news_d.html\\\">建构“和融向上”行政管理团队</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">2月18日下午，在本部东区南会议室隆重举行吴江实验小学校区管理交流大会，由校党支部迮美娟书记主持。...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"news_d.html\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"news_d.html\\\">\r\n						<img src=\\\"/skin/default/images/20160302.jpg\\\" alt=\\\"\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"news_d.html\\\">建构“和融向上”行政管理团队</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">2月18日下午，在本部东区南会议室隆重举行吴江实验小学校区管理交流大会，由校党支部迮美娟书记主持。...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"news_d.html\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.footer--]',0,0,'<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a> <span>[!--newstime--]</span></li>',0,1,'Y-m-d',0,0,0,1471845203,'listtemp',1,'manage'),(88,9,'图片列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"D_case\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]		</ul>\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.footer--]',0,0,'<li><div class=\\\"D_case_img\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a></div>\r\n<div class=\\\"D_case_title\\\"><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></div></li>\r\n',1,1,'Y-m-d',0,0,0,1472784452,'listtemp',1,'manage'),(89,1,'默认新闻列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left D_news_conter clear\\\">\r\n						[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>[!--temp.footer--]',0,0,'<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"[!--titleurl--]\\\">\r\n						<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">[!--smalltext--]...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>\r\n',1,1,'Y-m-d',0,0,0,1472784469,'listtemp',1,'manage'),(32,10,'新闻内容模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_case_d\\\">\r\n		<div class=\\\"D_case_d_title\\\">\r\n			<h3>[!--title--]</h3>\r\n		</div>\r\n		<p>[!--newstext--]\r\n		<img src=\\\"/skin/default/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0003.jpg\\\"  alt=\\\"\\\"/></p>\r\n	</div>[!--temp.footer--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1471849132,'newstemp',1,'manage'),(33,10,'集团荣誉模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"D_case\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]		</ul>\r\n	</div>\r\n[!--temp.footer--]',0,0,'<li><div class=\\\"D_case_img\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a></div>\r\n<div class=\\\"D_case_title\\\"><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></div></li>\r\n',1,1,'Y-m-d',0,0,0,1471849257,'listtemp',1,'manage'),(8,1,'子栏目导航标签模板','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li><a href=\\\"[!--classurl--]\\\">[!--classname--]</a></li>',1,1,'Y-m-d H:i:s',0,0,0,1471845736,'bqtemp',1,'manage'),(9,1,'关于内容模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]1,1,0,0[/showclasstemp]\r\n		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		[!--newstext--]\r\n	</div>[!--temp.footer--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1471845818,'newstemp',1,'manage'),(10,11,'地理位置内容模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]1,1,0,0[/showclasstemp]\r\n		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		<div id=\\\"dituContent\\\"></div>\r\n	</div>[!--temp.footer--]\r\n<script type=\\\"text/javascript\\\">\r\n    //创建和初始化地图函数：\r\n    function initMap(){\r\n        createMap();//创建地图\r\n        setMapEvent();//设置地图事件\r\n        addMapControl();//向地图添加控件\r\n        addMarker();//向地图中添加marker\r\n    }\r\n    \r\n    //创建地图函数：\r\n    function createMap(){\r\n        var map = new BMap.Map(\\\"dituContent\\\");//在百度地图容器中创建一个地图\r\n        var point = new BMap.Point(120.654933,31.157809);//定义一个中心点坐标\r\n        map.centerAndZoom(point,13);//设定地图的中心点和坐标并将地图显示在地图容器中\r\n        window.map = map;//将map变量存储在全局\r\n    }\r\n    \r\n    //地图事件设置函数：\r\n    function setMapEvent(){\r\n        map.enableDragging();//启用地图拖拽事件，默认启用(可不写)\r\n		\r\n		//map.disableDragging();     //禁止拖拽\r\n        map.enableScrollWheelZoom();//启用地图滚轮放大缩小\r\n        map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)\r\n        map.enableKeyboard();//启用键盘上下左右键移动地图\r\n    }\r\n    \r\n    //地图控件添加函数：\r\n    function addMapControl(){\r\n        //向地图中添加缩放控件\r\n	var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});\r\n	map.addControl(ctrl_nav);\r\n        //向地图中添加缩略图控件\r\n	var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});\r\n	map.addControl(ctrl_ove);\r\n        //向地图中添加比例尺控件\r\n	var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});\r\n	map.addControl(ctrl_sca);\r\n    }\r\n    \r\n    //标注点数组\r\n    var markerArr = [{title:\\\"太湖校区\\\",content:\\\"联系方式：63452079\\\",point:\\\"120.634488|31.145772\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"爱德校区\\\",content:\\\"联系方式：\\\",point:\\\"120.640524|31.155314\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"城中校区\\\",content:\\\"联系方式：\\\",point:\\\"120.647333|31.168872\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ];\r\n    //创建marker\r\n    function addMarker(){\r\n        for(var i=0;i<markerArr.length;i++){\r\n            var json = markerArr[i];\r\n            var p0 = json.point.split(\\\"|\\\")[0];\r\n            var p1 = json.point.split(\\\"|\\\")[1];\r\n            var point = new BMap.Point(p0,p1);\r\n			var iconImg = createIcon(json.icon);\r\n            var marker = new BMap.Marker(point,{icon:iconImg});\r\n			var iw = createInfoWindow(i);\r\n			var label = new BMap.Label(json.title,{\\\"offset\\\":new BMap.Size(json.icon.lb-json.icon.x+10,-20)});\r\n			marker.setLabel(label);\r\n            map.addOverlay(marker);\r\n            label.setStyle({\r\n                        borderColor:\\\"#808080\\\",\r\n                        color:\\\"#333\\\",\r\n                        cursor:\\\"pointer\\\"\r\n            });\r\n			\r\n			(function(){\r\n				var index = i;\r\n				var _iw = createInfoWindow(i);\r\n				var _marker = marker;\r\n				_marker.addEventListener(\\\"click\\\",function(){\r\n				    this.openInfoWindow(_iw);\r\n			    });\r\n			    _iw.addEventListener(\\\"open\\\",function(){\r\n				    _marker.getLabel().hide();\r\n			    })\r\n			    _iw.addEventListener(\\\"close\\\",function(){\r\n				    _marker.getLabel().show();\r\n			    })\r\n				label.addEventListener(\\\"click\\\",function(){\r\n				    _marker.openInfoWindow(_iw);\r\n			    })\r\n				if(!!json.isOpen){\r\n					label.hide();\r\n					_marker.openInfoWindow(_iw);\r\n				}\r\n			})()\r\n        }\r\n    }\r\n    //创建InfoWindow\r\n    function createInfoWindow(i){\r\n        var json = markerArr[i];\r\n        var iw = new BMap.InfoWindow(\\\"<b class=\\\'iw_poi_title\\\' title=\\\'\\\" + json.title + \\\"\\\'>\\\" + json.title + \\\"</b><div class=\\\'iw_poi_content\\\'>\\\"+json.content+\\\"</div>\\\");\r\n        return iw;\r\n    }\r\n    //创建一个Icon\r\n    function createIcon(json){\r\n        var icon = new BMap.Icon(\\\"http://app.baidu.com/map/images/us_mk_icon.png\\\", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowOffset:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})\r\n        return icon;\r\n    }\r\n    \r\n    initMap();//创建和初始化地图\r\n</script>',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1471846236,'newstemp',1,'manage'),(11,11,'地理位置内容模板','[!--temp.header--]<script type=\\\"text/javascript\\\" src=\\\"http://api.map.baidu.com/api?key=&amp;v=1.1&amp;services=true\\\"></script>\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]1,1,0,0[/showclasstemp]\r\n		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		<div id=\\\"dituContent\\\"></div>\r\n	</div>[!--temp.footer--]\r\n<script type=\\\"text/javascript\\\">\r\n    //创建和初始化地图函数：\r\n    function initMap(){\r\n        createMap();//创建地图\r\n        setMapEvent();//设置地图事件\r\n        addMapControl();//向地图添加控件\r\n        addMarker();//向地图中添加marker\r\n    }\r\n    \r\n    //创建地图函数：\r\n    function createMap(){\r\n        var map = new BMap.Map(\\\"dituContent\\\");//在百度地图容器中创建一个地图\r\n        var point = new BMap.Point(120.654933,31.157809);//定义一个中心点坐标\r\n        map.centerAndZoom(point,13);//设定地图的中心点和坐标并将地图显示在地图容器中\r\n        window.map = map;//将map变量存储在全局\r\n    }\r\n    \r\n    //地图事件设置函数：\r\n    function setMapEvent(){\r\n        map.enableDragging();//启用地图拖拽事件，默认启用(可不写)\r\n		\r\n		//map.disableDragging();     //禁止拖拽\r\n        map.enableScrollWheelZoom();//启用地图滚轮放大缩小\r\n        map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)\r\n        map.enableKeyboard();//启用键盘上下左右键移动地图\r\n    }\r\n    \r\n    //地图控件添加函数：\r\n    function addMapControl(){\r\n        //向地图中添加缩放控件\r\n	var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});\r\n	map.addControl(ctrl_nav);\r\n        //向地图中添加缩略图控件\r\n	var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});\r\n	map.addControl(ctrl_ove);\r\n        //向地图中添加比例尺控件\r\n	var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});\r\n	map.addControl(ctrl_sca);\r\n    }\r\n    \r\n    //标注点数组\r\n    var markerArr = [{title:\\\"太湖校区\\\",content:\\\"联系方式：63452079\\\",point:\\\"120.634488|31.145772\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"爱德校区\\\",content:\\\"联系方式：\\\",point:\\\"120.640524|31.155314\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ,{title:\\\"城中校区\\\",content:\\\"联系方式：\\\",point:\\\"120.647333|31.168872\\\",isOpen:0,icon:{w:21,h:21,l:0,t:0,x:6,lb:5}}\r\n		 ];\r\n    //创建marker\r\n    function addMarker(){\r\n        for(var i=0;i<markerArr.length;i++){\r\n            var json = markerArr[i];\r\n            var p0 = json.point.split(\\\"|\\\")[0];\r\n            var p1 = json.point.split(\\\"|\\\")[1];\r\n            var point = new BMap.Point(p0,p1);\r\n			var iconImg = createIcon(json.icon);\r\n            var marker = new BMap.Marker(point,{icon:iconImg});\r\n			var iw = createInfoWindow(i);\r\n			var label = new BMap.Label(json.title,{\\\"offset\\\":new BMap.Size(json.icon.lb-json.icon.x+10,-20)});\r\n			marker.setLabel(label);\r\n            map.addOverlay(marker);\r\n            label.setStyle({\r\n                        borderColor:\\\"#808080\\\",\r\n                        color:\\\"#333\\\",\r\n                        cursor:\\\"pointer\\\"\r\n            });\r\n			\r\n			(function(){\r\n				var index = i;\r\n				var _iw = createInfoWindow(i);\r\n				var _marker = marker;\r\n				_marker.addEventListener(\\\"click\\\",function(){\r\n				    this.openInfoWindow(_iw);\r\n			    });\r\n			    _iw.addEventListener(\\\"open\\\",function(){\r\n				    _marker.getLabel().hide();\r\n			    })\r\n			    _iw.addEventListener(\\\"close\\\",function(){\r\n				    _marker.getLabel().show();\r\n			    })\r\n				label.addEventListener(\\\"click\\\",function(){\r\n				    _marker.openInfoWindow(_iw);\r\n			    })\r\n				if(!!json.isOpen){\r\n					label.hide();\r\n					_marker.openInfoWindow(_iw);\r\n				}\r\n			})()\r\n        }\r\n    }\r\n    //创建InfoWindow\r\n    function createInfoWindow(i){\r\n        var json = markerArr[i];\r\n        var iw = new BMap.InfoWindow(\\\"<b class=\\\'iw_poi_title\\\' title=\\\'\\\" + json.title + \\\"\\\'>\\\" + json.title + \\\"</b><div class=\\\'iw_poi_content\\\'>\\\"+json.content+\\\"</div>\\\");\r\n        return iw;\r\n    }\r\n    //创建一个Icon\r\n    function createIcon(json){\r\n        var icon = new BMap.Icon(\\\"http://app.baidu.com/map/images/us_mk_icon.png\\\", new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowOffset:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})\r\n        return icon;\r\n    }\r\n    \r\n    initMap();//创建和初始化地图\r\n</script>',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1471846392,'newstemp',1,'manage'),(25,1,'','[!--temp.header--]\r\n<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>新闻动态</span>\r\n				<a href=\\\"/news/hnews/\\\">更多...</a>\r\n			</div>\r\n			[ecmsinfo]9,3,32,0,0,2,0[/ecmsinfo]\r\n		</div>\r\n		<div class=\\\"D_news_right clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>集团故事</span>\r\n				<a href=\\\"/news/jtgs/\\\">更多...</a>\r\n			</div>\r\n			[ecmsinfo]10,4,32,0,0,14,0[/ecmsinfo]		</div>\r\n	</div>\r\n[!--temp.footer--]',0,0,'',0,0,'',0,0,0,1471848354,'pubindextemp',1,'manage'),(119,2,'footer','<div class=\\\"D_bg about\\\">\r\n		<div class=\\\"D_about\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li>\r\n					<dl>\r\n						<dt>关于我们</dt>\r\n						[showclasstemp]1,13,0,0[/showclasstemp]\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>集团组成</dt>\r\n						<dd>\r\n							<a href=\\\"/chengzhong/\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/aide/\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/taihu/\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>新闻动态</dt>\r\n						[showclasstemp]8,13,0,0[/showclasstemp]					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>教师频道</dt>\r\n						<dd>\r\n							<a  href=\\\"index.html\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>管理平台</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>资源中心</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li class=\\\"li_dis\\\">\r\n					<dl>\r\n						<dt>联系我们</dt>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<span>E-mail：edu@wjsx.com</span>\r\n						</dd>\r\n						<dd>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n		<div class=\\\"D_bg bottom\\\">\r\n		<div class=\\\"D_bottom\\\">\r\n			<span>Copyright 2015 吴江实验小学 保留所有版权 苏ICP备10057875号</span>\r\n			<div class=\\\"D_bottom_links\\\">\r\n				<span class=\\\"D_bottom_links_btn\\\">相关连接</span>\r\n				<ul>[ecmsinfo]13,10,32,0,0,3,0[/ecmsinfo]</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'页面尾部',0,0,'',0,0,0,1473149727,'tempvar',1,'manage'),(27,3,'标题+简介','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',60,0,'<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\" target=\\\"_blank\\\">[!--title--]</a></li>',1,1,'m-d',0,0,0,1471848716,'bqtemp',1,'manage'),(28,3,'友情链接','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',60,0,'<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\" target=\\\"_blank\\\">[!--title--]</a></li>',1,1,'m-d',0,0,0,1471848783,'bqtemp',1,'manage'),(29,2,'footer','<div class=\\\"D_bg about\\\">\r\n		<div class=\\\"D_about\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li>\r\n					<dl>\r\n						<dt>关于我们</dt>\r\n						[showclasstemp]1,13,0,0[/showclasstemp]\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>集团组成</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>新闻动态</dt>\r\n						[showclasstemp]8,13,0,0[/showclasstemp]					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>教师频道</dt>\r\n						<dd>\r\n							<a  href=\\\"index.html\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>管理平台</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>资源中心</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li class=\\\"li_dis\\\">\r\n					<dl>\r\n						<dt>联系我们</dt>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<span>E-mail：edu@wjsx.com</span>\r\n						</dd>\r\n						<dd>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n		<div class=\\\"D_bg bottom\\\">\r\n		<div class=\\\"D_bottom\\\">\r\n			<span>Copyright 2015 吴江实验小学 保留所有版权 苏ICP备10057875号</span>\r\n			<div class=\\\"D_bottom_links\\\">\r\n				<span class=\\\"D_bottom_links_btn\\\">相关连接</span>\r\n				<ul>[ecmsinfo]13,10,32,0,0,3,0[/ecmsinfo]</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'页面尾部',0,0,'',0,0,0,1471848833,'tempvar',1,'manage'),(151,1,'header','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/js/id.js\\\"></script>\r\n<title>吴江实验小学</title>\r\n</head>\r\n<body>\r\n<div class=\\\"D\\\">\r\n	<!--顶部-->\r\n	<div class=\\\"D_bg top\\\">\r\n		<div class=\\\"D_top clear\\\">\r\n			<ul>\r\n				<li>\r\n					<a href=\\\"index.htm/l\\\">返回首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">速递邮局</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">办公OA</a>\r\n				</li>\r\n			</ul>\r\n			<div>\r\n				<a class=\\\"\\\" href=\\\"###\\\">常用信息</a>\r\n				<div>\r\n					<input class=\\\"D_top_text\\\" type=\\\"text\\\" value=\\\"\\\" />\r\n					<input class=\\\"D_top_btn\\\" type=\\\"button\\\" value=\\\"\\\" />\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<!--logo部分,导航部分-->\r\n	<div class=\\\"D_head clear\\\">\r\n		<div class=\\\"D_head_logo\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/images/logo.jpg\\\" alt=\\\"\\\">\r\n			</a>\r\n		</div>\r\n		<div class=\\\"D_head_nav\\\">\r\n			<img  class=\\\"D_head_nav_img\\\" src=\\\"/skin/default/images/nav1.png\\\" alt=\\\"\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li  class=\\\"onnav\\\">\r\n					<a href=\\\"/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">关于我们</a>\r\n					<dl>[showclasstemp]1,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/group/\\\">集团组成</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"/chengzhong/\\\" target=\\\"_blank\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/aide/\\\" target=\\\"_blank\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/taihu/\\\" target=\\\"_blank\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/szw/\\\" target=\\\"_blank\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">新闻动态</a>\r\n					<dl>[showclasstemp]8,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">教师频道</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">管理平台</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">资源中心</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',1,0,'页面头部',0,0,'',0,0,0,1474363905,'tempvar',1,'manage'),(124,10,'szwheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/szw/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/szw/js/index.js\\\"></script>\r\n<title>苏州湾校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/szw/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/szw/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/dlad/\\\">点亮爱德</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"/szw/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"/szw/dlad/\\\">点亮爱德</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/szw/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'苏州湾校区头部',0,0,'',0,0,0,1473380442,'tempvar',1,'manage'),(35,10,'新闻内容模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_news_d clear\\\">\r\n		<div class=\\\"D_news_d_title\\\">\r\n			<h2>[!--title--]</h2>\r\n			<div class=\\\"time\\\">\r\n				<span>发布时间：<span>[!--newstime--]</span></span>\r\n				<span>点击量：<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script>次</span></span>\r\n			</div>\r\n		</div>\r\n		<p>[!--newstext--]\r\n		</p><div class=\\\"sx\\\">上一篇：[!--info.pre--]<br>\r\n下一篇：[!--info.next--]</div>\r\n	</div>[!--temp.footer--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1472182836,'newstemp',1,'manage'),(14,13,'导航二级','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<dd><a href=\\\"[!--classurl--]\\\">[!--classname--]</a></dd>',1,1,'Y-m-d H:i:s',0,0,0,1471846544,'bqtemp',1,'manage'),(132,1,'header','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/js/id.js\\\"></script>\r\n<title>吴江实验小学</title>\r\n</head>\r\n<body>\r\n<div class=\\\"D\\\">\r\n	<!--顶部-->\r\n	<div class=\\\"D_bg top\\\">\r\n		<div class=\\\"D_top clear\\\">\r\n			<ul>\r\n				<li>\r\n					<a href=\\\"index.htm/l\\\">返回首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">速递邮局</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">办公OA</a>\r\n				</li>\r\n			</ul>\r\n			<div>\r\n				<a class=\\\"\\\" href=\\\"###\\\">常用信息</a>\r\n				<div>\r\n					<input class=\\\"D_top_text\\\" type=\\\"text\\\" value=\\\"\\\" />\r\n					<input class=\\\"D_top_btn\\\" type=\\\"button\\\" value=\\\"\\\" />\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<!--logo部分,导航部分-->\r\n	<div class=\\\"D_head clear\\\">\r\n		<div class=\\\"D_head_logo\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/images/logo.jpg\\\" alt=\\\"\\\">\r\n			</a>\r\n		</div>\r\n		<div class=\\\"D_head_nav\\\">\r\n			<img  class=\\\"D_head_nav_img\\\" src=\\\"/skin/default/images/nav1.png\\\" alt=\\\"\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li  class=\\\"onnav\\\">\r\n					<a href=\\\"/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">关于我们</a>\r\n					<dl>[showclasstemp]1,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">集团组成</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"/chengzhong/\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/aide/\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/taihu/\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/szw/\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">新闻动态</a>\r\n					<dl>[showclasstemp]8,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">教师频道</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">管理平台</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">资源中心</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',1,0,'页面头部',0,0,'',0,0,0,1473381270,'tempvar',1,'dd'),(52,4,'标题+时间','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',1,1,'Y-m-d',0,0,0,1472706827,'bqtemp',1,'manage'),(16,9,'图片列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">[showclasstemp]8,1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"D_case\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]		</ul>\r\n	</div>\r\n[!--temp.footer--]',0,0,'<li><div class=\\\"D_case_img\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a></div>\r\n<div class=\\\"D_case_title\\\"><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></div></li>\r\n',1,1,'Y-m-d',0,0,0,1471847052,'listtemp',1,'manage'),(17,1,'默认新闻列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left D_news_conter clear\\\">\r\n						[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</div>\r\n	</div>[!--temp.footer--]',0,0,'<div class=\\\"D_news_left_body clear\\\">\r\n				<div class=\\\"D_news_left_img\\\">\r\n					<a href=\\\"[!--titleurl--]\\\">\r\n						<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n					</a>\r\n				</div>\r\n				<div class=\\\"D_news_left_text\\\">\r\n					<span class=\\\"span1\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n					</span>\r\n					<span class=\\\"span2\\\">[!--smalltext--]...</span>\r\n					<span class=\\\"span3\\\">\r\n						<a href=\\\"[!--titleurl--]\\\">查看详情</a>\r\n					</span>\r\n				</div>\r\n			</div>\r\n',1,1,'Y-m-d',0,0,0,1471847234,'listtemp',1,'manage'),(18,9,'图片列表模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"D_case\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]		</ul>\r\n	</div>\r\n[!--temp.footer--]',0,0,'<li><div class=\\\"D_case_img\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a></div>\r\n<div class=\\\"D_case_title\\\"><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></div></li>\r\n',1,1,'Y-m-d',0,0,0,1471847248,'listtemp',1,'manage'),(34,10,'新闻内容模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent D_adcontent_4\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]		</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]	</div>\r\n	<!--新闻news-->\r\n	<div class=\\\"D_case_d\\\">\r\n		<div class=\\\"D_case_d_title\\\">\r\n			<h3>[!--title--]</h3>\r\n		</div>\r\n		<p>[!--newstext--]\r\n		</p>\r\n	</div>[!--temp.footer--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1472174618,'newstemp',1,'manage'),(22,1,'','[!--temp.header--]\r\n<!--新闻news-->\r\n	<div class=\\\"D_news clear\\\">\r\n		<div class=\\\"D_news_top\\\">\r\n			<span>NEWS</span>\r\n			<span>\r\n				<span>承“爱德求真”　办“生动大气”</span>\r\n			</span>\r\n		</div>\r\n		<div class=\\\"D_news_left clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>新闻动态</span>\r\n				<a href=\\\"/news/hnews/\\\">更多...</a>\r\n			</div>\r\n			[ecmsinfo]9,3,32,0,0,2,0[/ecmsinfo]\r\n		</div>\r\n		<div class=\\\"D_news_right clear\\\">\r\n			<div class=\\\"D_news_head clear\\\">\r\n				<span>集团故事</span>\r\n				<a href=\\\"/news/jtgs/\\\">更多...</a>\r\n			</div>\r\n			<div class=\\\"D_news_right_body clear\\\">\r\n				<span class=\\\"span1\\\">\r\n					<a href=\\\"news_d.html\\\">建构“和融向上”行政管理团队</a>\r\n				</span>\r\n				<span class=\\\"span2\\\">11月16日下午，吴江实验小学“2015金秋课堂论坛（科学）”活动在本部校区西区一楼阶梯教室隆重召开。活动分为“大市科学带头人吴韦萍示范课”、“课堂论坛”、“专家评点与讲座”等三大版块。</span>\r\n			</div>\r\n			<div class=\\\"D_news_right_body clear\\\">\r\n				<span class=\\\"span1\\\">\r\n					<a href=\\\"###\\\">建构“和融向上”行政管理团队</a>\r\n				</span>\r\n				<span class=\\\"span2\\\">11月16日下午，吴江实验小学“2015金秋课堂论坛（科学）”活动在本部校区西区一楼阶梯教室隆重召开。活动分为“大市科学带头人吴韦萍示范课”、“课堂论坛”、“专家评点与讲座”等三大版块。</span>\r\n			</div>\r\n			<div class=\\\"D_news_right_body clear\\\">\r\n				<span class=\\\"span1\\\">\r\n					<a href=\\\"news_d.html\\\">建构“和融向上”行政管理团队</a>\r\n				</span>\r\n				<span class=\\\"span2\\\">11月16日下午，吴江实验小学“2015金秋课堂论坛（科学）”活动在本部校区西区一楼阶梯教室隆重召开。活动分为“大市科学带头人吴韦萍示范课”、“课堂论坛”、“专家评点与讲座”等三大版块。</span>\r\n			</div>\r\n			<div class=\\\"D_news_right_body clear\\\">\r\n				<span class=\\\"span1\\\">\r\n					<a href=\\\"news_d.html\\\">建构“和融向上”行政管理团队</a>\r\n				</span>\r\n				<span class=\\\"span2\\\">11月16日下午，吴江实验小学“2015金秋课堂论坛（科学）”活动在本部校区西区一楼阶梯教室隆重召开。活动分为“大市科学带头人吴韦萍示范课”、“课堂论坛”、“专家评点与讲座”等三大版块。</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.footer--]',0,0,'',0,0,'',0,0,0,1471847897,'pubindextemp',1,'manage'),(23,14,'首页集团故事','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<div class=\\\"D_news_right_body clear\\\">\r\n				<span class=\\\"span1\\\">\r\n					<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				</span>\r\n				<span class=\\\"span2\\\">[!--smalltext--]</span>\r\n			</div>\r\n',1,1,'Y-m-d H:i:s',0,0,0,1471847949,'bqtemp',1,'manage'),(142,1,'城中分校','[!--temp.czheader--]	\r\n	<div class=\\\"news_2 clear\\\">\r\n		<div>\r\n			<div class=\\\"\\\">\r\n				<span>校园新闻</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]16,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n		<div>\r\n			<div >\r\n				<span>公告栏</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]17,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"nav_2\\\">\r\n		<div class=\\\"div1\\\">\r\n			<div>\r\n				<span>阅读:</span>\r\n			</div>\r\n			<ul>[showclasstemp]18,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div2\\\">\r\n			<div>\r\n				<span>班级:</span>\r\n			</div>\r\n			<ul>[showclasstemp]22,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div3\\\">\r\n			<div>\r\n				<span>科技:</span>\r\n			</div>\r\n			<ul>[showclasstemp]29,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div4\\\">\r\n			<div>\r\n				<span>教师:</span>\r\n			</div>\r\n			<ul>[showclasstemp]25,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n	</div>\r\n[!--temp.czfooter--]',0,0,'',0,0,'',0,0,0,1474262339,'classtemp',1,'manage'),(49,11,'城中分校新闻列表模板','[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul><li class=\\\"c_info_bg c_info_bg_dtl\\\"></li>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"c_title detail_d_newstitle clear\\\">\r\n				<span>[!--class.name--]</span>\r\n			</div>\r\n			<div class=\\\"c_info detail_d_info\\\">\r\n				<ul>[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]',0,0,'<li><a href=\\\"[!--titleurl--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',1,1,'Y-m-d',0,0,0,1472706524,'listtemp',1,'manage'),(50,12,'城中分校内容模板','[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n<div class=\\\"c_title detail_d_newstitle clear\\\">\r\n				<span>[!--class.name--]</span>\r\n			</div>\r\n			<div class=\\\"c_info detail_d_info\\\">\r\n				<ul>\r\n					<li class=\\\"c_info_bg c_info_bg_dtl\\\"></li></ul></div>\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--title--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span><p>[!--newstext--]</p> </span>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1472706669,'newstemp',1,'manage'),(146,12,'城中分校内容模板','[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n<div class=\\\"c_title detail_d_newstitle clear\\\">\r\n				<span>[!--class.name--]</span>\r\n			</div>\r\n			<div class=\\\"c_info detail_d_info\\\">\r\n				<ul>\r\n					<li class=\\\"c_info_bg c_info_bg_dtl\\\"></li></ul></div>\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--title--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span><p>[!--newstext--]</p> </span>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]',0,0,'',0,1,'Y-m-d',0,0,0,1474262690,'newstemp',1,'manage'),(143,1,'城中分校','[!--temp.czheader--]	\r\n	<div class=\\\"news_2 clear\\\">\r\n		<div>\r\n			<div class=\\\"\\\">\r\n				<span>校园新闻</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]16,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n		<div>\r\n			<div >\r\n				<span>公告栏</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]17,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"nav_2\\\">\r\n		<div class=\\\"div1\\\">\r\n			<div>\r\n				<span>阅读:</span>\r\n			</div>\r\n			<ul>[showclasstemp]18,1,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div2\\\">\r\n			<div>\r\n				<span>班级:</span>\r\n			</div>\r\n			<ul>[showclasstemp]22,1,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div3\\\">\r\n			<div>\r\n				<span>科技:</span>\r\n			</div>\r\n			<ul>[showclasstemp]29,1,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div4\\\">\r\n			<div>\r\n				<span>教师:</span>\r\n			</div>\r\n			<ul>[showclasstemp]25,1,0,0[/showclasstemp]</ul>\r\n		</div>\r\n	</div>\r\n[!--temp.czfooter--]',0,0,'',0,0,'',0,0,0,1474262460,'classtemp',1,'manage'),(144,16,'城中分校首页类别调用','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li><a href=\\\"[!--classurl--]\\\" target=\\\"_blank\\\">[!--classname--]</a></li>\r\n',1,1,'Y-m-d H:i:s',0,0,0,1474262536,'bqtemp',1,'manage'),(54,16,'城中分校首页类别调用','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<p><a href=\\\"[!--classurl--]\\\">[!--classname--]</a></p>\r\n',1,1,'Y-m-d H:i:s',0,0,0,1472707453,'bqtemp',1,'manage'),(139,7,'adfooter','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学校信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-63970028</span>\r\n			<span>地址：江苏省吴江区太湖新城吴模路1915号</span>\r\n			<span>邮编：215200</span><br>\r\n			<span>Copy(c)吴江市实验小学2015-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'爱德底部',0,0,'',0,0,0,1473827911,'tempvar',1,'manage'),(72,1,'子栏目导航标签模板','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li classid=\\\"[!--classid--]\\\"><a href=\\\"[!--classurl--]\\\">[!--classname--]</a></li>',1,1,'Y-m-d H:i:s',0,0,0,1472783180,'bqtemp',1,'manage'),(73,12,'爱德分校新闻列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,0,1472783273,'listtemp',1,'manage'),(100,3,'czheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/czxq/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/czxq/js/index.js\\\"></script>\r\n<title>城中分校</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header clear\\\">\r\n		<a href=\\\"index.html\\\">\r\n			<img src=\\\"/skin/default/czxq/images/logo.png\\\" alt=\\\"\\\"/>\r\n		</a>\r\n		<div class=\\\"nav_btn\\\">\r\n			<img src=\\\"/skin/default/czxq/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n			<ul class=\\\"nav\\\">\r\n				<li class=\\\"icon_index\\\">\r\n					<span>\r\n						<a href=\\\"/chengzhong/\\\">网站首页</a>\r\n					</span>\r\n				</li>\r\n				<li class=\\\"icon_kj\\\">\r\n					<span>\r\n						<a>阅读中心</a>\r\n						<img src=\\\"/skin/default/czxq/images/bot_icon.png\\\" alt=\\\"\\\"/>\r\n					</span>\r\n					<ul>\r\n						[showclasstemp]18,1,0,0[/showclasstemp]\r\n					</ul>\r\n				</li>\r\n				<li class=\\\"icon_dt\\\">\r\n					<span>\r\n						<a>班级</a>\r\n						<img src=\\\"/skin/default/czxq/images/bot_icon.png\\\" alt=\\\"\\\"/>\r\n					</span>\r\n					<ul>\r\n						[showclasstemp]22,1,0,0[/showclasstemp]\r\n					</ul>\r\n				</li>\r\n				<li class=\\\"icon_wh\\\">\r\n					<span>\r\n						<a>教师</a>\r\n						<img src=\\\"/skin/default/czxq/images/bot_icon.png\\\" alt=\\\"\\\"/>\r\n					</span>\r\n					<ul>\r\n						[showclasstemp]25,1,0,0[/showclasstemp]\r\n					</ul>\r\n				</li>\r\n				<li class=\\\"icon_ms\\\">\r\n					<span>\r\n						<a>科技</a>\r\n						<img src=\\\"/skin/default/czxq/images/bot_icon.png\\\" alt=\\\"\\\"/>\r\n					</span>\r\n					<ul>\r\n						[showclasstemp]29,1,0,0[/showclasstemp]\r\n					</ul>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/czxq/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/czxq/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/czxq/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/czxq/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/czxq/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'城中分校头部',0,0,'',0,0,0,1472797692,'tempvar',1,'manage'),(145,1,'城中分校','[!--temp.czheader--]	\r\n	<div class=\\\"news_2 clear\\\">\r\n		<div>\r\n			<div class=\\\"\\\">\r\n				<span>校园新闻</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]16,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n		<div>\r\n			<div >\r\n				<span>公告栏</span>\r\n			</div>\r\n			<ul>\r\n				[ecmsinfo]17,6,48,0,0,4,0[/ecmsinfo]\r\n			</ul>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"nav_2\\\">\r\n		<div class=\\\"div1\\\">\r\n			<div>\r\n				<span>阅读:</span>\r\n			</div>\r\n			<ul>[showclasstemp]18,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div2\\\">\r\n			<div>\r\n				<span>班级:</span>\r\n			</div>\r\n			<ul>[showclasstemp]22,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div3\\\">\r\n			<div>\r\n				<span>科技:</span>\r\n			</div>\r\n			<ul>[showclasstemp]29,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n		<div class=\\\"div4\\\">\r\n			<div>\r\n				<span>教师:</span>\r\n			</div>\r\n			<ul>[showclasstemp]25,16,0,0[/showclasstemp]</ul>\r\n		</div>\r\n	</div>\r\n[!--temp.czfooter--]',0,0,'',0,0,'',0,0,0,1474262549,'classtemp',1,'manage'),(150,5,'集团组成','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		<style>\r\nul{\r\n	padding: 0;margin:0 ;\r\n}\r\nli{\r\n	list-style:none;\r\n}\r\n.img_box{\r\n	width:1200px;\r\n	margin:0 auto;\r\n}\r\n.img_box>ul{\r\n	width:100%;\r\n	overflow:hidden;\r\n}\r\n.img_box>ul>li{\r\n	float:left;overflow:hidden;\r\n}\r\n.text{\r\n	width:59px;\r\n	background:#ccc;\r\n	height:360px;\r\n	float:left;\r\n	position:relative;\r\n	cursor:pointer;\r\n	border-right:1px #fff solid;\r\n}\r\n.text>div{\r\n	position:absolute;\r\n	bottom:160px;\r\n	left:0;\r\n	right:0;\r\n	width:16px;\r\n	margin:0 auto;\r\n}\r\n.img1{\r\n	width:760px;\r\n	height:360px;\r\n	float:right;\r\n	display:none;\r\n}\r\n.img1>a img{\r\n	width:100%;\r\n	height:100%;\r\n}\r\n</style>\r\n<div class=\\\"img_box\\\">\r\n	<ul>\r\n    	<li><div class=\\\"text\\\"><div>城中校区</div></div><div class=\\\"img1\\\" style=\\\"display:block;\\\"><a href=\\\"/chengzhong/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/d1.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>爱德校区</div></div><div class=\\\"img1\\\"><a href=\\\"/aide/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/2.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>太湖校区</div></div><div class=\\\"img1\\\"><a href=\\\"/taihu/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/3.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>苏州湾校区</div></div><div class=\\\"img1\\\"><a href=\\\"/szw/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/4.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n		<li><div class=\\\"text\\\"><div>幼儿园</div></div><div class=\\\"img1\\\"><a href=\\\"#\\\"><img src=\\\"/skin/default/images/fx/5.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n		<li><div class=\\\"text\\\"><div>德音书院</div></div><div class=\\\"img1\\\"><a href=\\\"#\\\"><img src=\\\"/skin/default/images/fx/6.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n    </ul>\r\n</div>\r\n<script>\r\n$(\\\".text\\\").click(function(){\r\n	$(\\\".img1\\\").hide();\r\n	$(this).next(\\\".img1\\\").show();\r\n})\r\n</script>\r\n	</div>[!--temp.footer--]',0,0,'',0,0,'',0,0,0,1474363872,'classtemp',1,'manage'),(137,5,'adheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/aide/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/aide/js/index.js\\\"></script>\r\n<title>爱德校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/aide/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/aide/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/dlad/\\\">点亮爱德</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/bj/\\\">班级GARDEN</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"/aide/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"/aide/dlad/\\\">点亮爱德</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"/aide/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"/aide/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/ssgz/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/aide/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'爱德分校头部',0,0,'',0,0,0,1473644412,'tempvar',1,'manage'),(138,7,'adfooter','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学校信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-634970028</span>\r\n			<span>地址：江苏省吴江区太湖新城吴模路1915号</span>\r\n			<span>邮编：215200</span><br>\r\n			<span>Copy(c)吴江市实验小学2015-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'爱德底部',0,0,'',0,0,0,1473827877,'tempvar',1,'manage'),(40,4,'czfooter','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<p>学院信箱：service@wjsx.com\r\n				校长信箱：wjsx@wjsx.com\r\n				联系电话：0512-6345079</p>\r\n			<p>地址：江苏省吴江市松陵镇永康路84号  邮编：215200</p>\r\n			<p>Copy(c) 吴江市实验小学2000-2016\r\n				All Rights Reserved 苏ICP备10057875号</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>\r\n',0,0,'城中分校底部',0,0,'',0,0,0,1472697447,'tempvar',1,'manage'),(44,15,'城中分校左侧分类','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li><a href=\\\"[!--classurl--]\\\"><span>[!--classname--]</span></a></li>\r\n',1,1,'Y-m-d H:i:s',0,0,0,1472698234,'bqtemp',1,'manage'),(84,11,'城中分校新闻列表模板','[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul><li class=\\\"c_info_bg c_info_bg_dtl\\\"></li>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"c_title detail_d_newstitle clear\\\">\r\n				<span>[!--class.name--]</span>\r\n			</div>\r\n			<div class=\\\"c_info detail_d_info\\\">\r\n				<ul>[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n				</ul>\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]',0,0,'<li><a href=\\\"[!--titleurl--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',1,1,'Y-m-d',0,0,0,1472784405,'listtemp',1,'manage'),(46,11,'城中分校新闻列表模板','[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"c_title detail_d_newstitle clear\\\">\r\n				<span>[!--class.name--]</span>\r\n			</div>\r\n			<div class=\\\"c_info detail_d_info\\\">\r\n				<ul>[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]',0,0,'<li><a href=\\\"[!--titleurl--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',1,1,'Y-m-d',0,0,0,1472702240,'listtemp',1,'manage'),(47,12,'城中分校内容模板','[!--temp.czheader--]\r\n<div class=\\\"content  clear\\\">\r\n		<div class=\\\"meun\\\">\r\n			<div class=\\\"meun_title\\\"><script language=\\\"javascript\\\">var txt=\\\"[!--bclass.name--]\\\"; if (txt==\\\"\\\") {txt=\\\"[!--class.name--]\\\";}; document.write(txt);</script></div>\r\n			<ul>\r\n				[showclasstemp]\\\'selfinfo\\\',15,0,0[/showclasstemp]\r\n			</ul>\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span><p>[!--newstext--]</p> </span>\r\n			</div>\r\n		</div>\r\n	</div>[!--temp.czfooter--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1472702329,'newstemp',1,'manage'),(140,3,'czheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\">\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/style.css\\\">\r\n<script src=\\\"/skin/default/czxq/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/czxq/js/index_2.js\\\"></script>\r\n<title>城中分校</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"dc\\\">\r\n	<div class=\\\"header clear\\\">\r\n		<a href=\\\"/chengzhong/\\\">\r\n			<img src=\\\"/skin/default/czxq/images/logo.png\\\" alt=\\\"\\\"/>\r\n		</a>\r\n	</div>\r\n	<div class=\\\"banner clear\\\">\r\n		<img src=\\\"/skin/default/czxq/images/0001.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0002.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0003.jpg\\\" alt=\\\"\\\" />\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'城中分校头部',0,0,'',0,0,0,1474262222,'tempvar',1,'manage'),(141,4,'czfooter','<div class=\\\"footer\\\">\r\n		<p>地址：江苏省吴江市松陵镇永康路84号  邮编：215200</p>\r\n		<p>Copy(c) 吴江市实验小学2000-2016</p>\r\n		<p>All Rights Reserved 苏ICP备10057875号</p>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>\r\n',0,0,'城中分校底部',0,0,'',0,0,0,1474262238,'tempvar',1,'manage'),(136,2,'爱德分校','[!--temp.adheader--]\r\n<div class=\\\"content\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_ssgz\\\">\r\n				<div class=\\\"c_ssgz_title clear\\\">\r\n					<span>时时关注</span>\r\n					<a href=\\\"/aide/ssgz/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<div class=\\\"c_ssgz_info clear\\\">\r\n					[ecmsinfo]37,1,32,0,0,18,0,\\\'isgood=1\\\'[/ecmsinfo]\r\n					<ul>\r\n						[ecmsinfo]37,7,32,0,0,17,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n					</ul>\r\n				</div>\r\n			</li>\r\n			<li class=\\\"c_dlad\\\">\r\n				<div class=\\\"c_dlad_title clear\\\">\r\n					<span>点亮德育</span>\r\n					<a href=\\\"/aide/dlad/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<ul>\r\n					[ecmsinfo]34,2,32,0,0,20,0[/ecmsinfo]\r\n				</ul>\r\n			</li>\r\n			<li class=\\\"c_diy c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>课程DIY</span>\r\n				</div>\r\n				[ecmsinfo]35,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_post c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>教师POST</span>\r\n				</div>\r\n				[ecmsinfo]36,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_gardend c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>班级GARDEN</span>\r\n				</div>\r\n				[ecmsinfo]38,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_spa c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>心理SPA</span>\r\n				</div>\r\n				[ecmsinfo]39,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n		</ul>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'',0,0,'',0,0,0,1473644178,'classtemp',1,'manage'),(125,5,'adheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/aide/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/aide/js/index.js\\\"></script>\r\n<title>爱德校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/aide/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/aide/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/dlad/\\\">点亮爱德</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/bj/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"/aide/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"/aide/dlad/\\\">点亮爱德</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"/aide/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"/aide/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/ssgz/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/aide/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'爱德分校头部',0,0,'',0,0,0,1473380504,'tempvar',1,'manage'),(82,2,'爱德分校','[!--temp.adheader--]\r\n<div class=\\\"content\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_ssgz\\\">\r\n				<div class=\\\"c_ssgz_title clear\\\">\r\n					<span>时时关注</span>\r\n					<a href=\\\"/aide/ssgz/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<div class=\\\"c_ssgz_info clear\\\">\r\n					[ecmsinfo]37,1,32,0,0,18,0,\\\'isgood=1\\\'[/ecmsinfo]\r\n					<ul>\r\n						[ecmsinfo]37,7,32,0,0,17,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n					</ul>\r\n				</div>\r\n			</li>\r\n			<li class=\\\"c_dlad\\\">\r\n				<div class=\\\"c_dlad_title clear\\\">\r\n					<span>点亮德育</span>\r\n					<a href=\\\"/aide/dlad/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<ul>\r\n					[ecmsinfo]34,2,32,0,0,17,0[/ecmsinfo]\r\n				</ul>\r\n			</li>\r\n			<li class=\\\"c_diy c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>课程DIY</span>\r\n				</div>\r\n				[ecmsinfo]35,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_post c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>教师POST</span>\r\n				</div>\r\n				[ecmsinfo]36,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_gardend c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>班级GARDEND</span>\r\n				</div>\r\n				[ecmsinfo]38,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_spa c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>心理SPA</span>\r\n				</div>\r\n				[ecmsinfo]39,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n		</ul>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'',0,0,'',0,0,0,1472784115,'classtemp',1,'manage'),(87,10,'集团荣誉模板','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		<ul class=\\\"clear\\\">[showclasstemp]\\\'selfinfo\\\',1,0,0[/showclasstemp]</ul>\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"D_case\\\">\r\n		<ul class=\\\"clear\\\">\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]		</ul>\r\n<div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.footer--]',0,0,'<li><div class=\\\"D_case_img\\\"><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a></div>\r\n<div class=\\\"D_case_title\\\"><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></div></li>\r\n',1,1,'Y-m-d',0,0,0,1472784441,'listtemp',1,'manage'),(90,17,'爱德实时关注','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li class=\\\"clear\\\"><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--span--]</span></li>',1,1,'Y-m-d',0,0,0,1472784631,'bqtemp',1,'manage'),(60,7,'adfooter','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学院信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-6345079</span>\r\n			<span>地址：江苏省吴江市松陵镇永康路84号</span>\r\n			<span>邮编：215200</span><br>\r\n			<span>Copy(c)吴江市实验小学2000-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'爱德底部',0,0,'',0,0,0,1472781474,'tempvar',1,'manage'),(101,5,'adheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/aide/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/aide/js/index.js\\\"></script>\r\n<title>爱德校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/aide/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/aide/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/dlad/\\\">点亮爱德</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/bj/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"index.html\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"dlad.html\\\">点亮爱德</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"diy.html\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"post.html\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"ssgz.html\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"gardend.html\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"spa.html\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/aide/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'爱德分校头部',0,0,'',0,0,0,1472797700,'tempvar',1,'manage'),(79,12,'爱德分校新闻列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1,1472783543,'listtemp',1,'manage'),(85,12,'爱德分校新闻列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1,1472784417,'listtemp',1,'manage'),(86,13,'爱德分校图片+信息列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0,1472784430,'listtemp',1,'manage'),(64,13,'爱德分校图片+信息列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onmeun\\\">\r\n					<a href=\\\"dlad.html\\\">点亮爱德</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"diy.html\\\">课程DIY</a>\r\n				</li>\r\n				<li class=\\\"\\\">\r\n					<a href=\\\"post.html\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"ssgz.html\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"gardend.html\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"spa.html\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0,1472782098,'listtemp',1,'manage'),(65,13,'爱德校区详细页','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onmeun\\\">\r\n					<a href=\\\"dlad.html\\\">点亮爱德</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"diy.html\\\">课程DIY</a>\r\n				</li>\r\n				<li class=\\\"\\\">\r\n					<a href=\\\"post.html\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"ssgz.html\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"gardend.html\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"spa.html\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1472782223,'newstemp',1,'manage'),(93,17,'爱德实时关注','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li class=\\\"clear\\\"><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--newstime--]</span></li>',1,1,'Y-m-d',0,0,0,1472784752,'bqtemp',1,'manage'),(67,18,'爱德实时关注推荐','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<div><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a>\r\n						<div>\r\n							<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n							<span>[!--smalltext--]...</span>\r\n						</div>\r\n					</div>',1,1,'Y-m-d',0,0,0,1472782690,'bqtemp',1,'manage'),(92,2,'爱德分校','[!--temp.adheader--]\r\n<div class=\\\"content\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_ssgz\\\">\r\n				<div class=\\\"c_ssgz_title clear\\\">\r\n					<span>时时关注</span>\r\n					<a href=\\\"/aide/ssgz/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<div class=\\\"c_ssgz_info clear\\\">\r\n					[ecmsinfo]37,1,32,0,0,18,0,\\\'isgood=1\\\'[/ecmsinfo]\r\n					<ul>\r\n						[ecmsinfo]37,7,32,0,0,17,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n					</ul>\r\n				</div>\r\n			</li>\r\n			<li class=\\\"c_dlad\\\">\r\n				<div class=\\\"c_dlad_title clear\\\">\r\n					<span>点亮德育</span>\r\n					<a href=\\\"/aide/dlad/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<ul>\r\n					[ecmsinfo]34,2,32,0,0,20,0[/ecmsinfo]\r\n				</ul>\r\n			</li>\r\n			<li class=\\\"c_diy c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>课程DIY</span>\r\n				</div>\r\n				[ecmsinfo]35,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_post c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>教师POST</span>\r\n				</div>\r\n				[ecmsinfo]36,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_gardend c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>班级GARDEND</span>\r\n				</div>\r\n				[ecmsinfo]38,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_spa c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>心理SPA</span>\r\n				</div>\r\n				[ecmsinfo]39,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n		</ul>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'',0,0,'',0,0,0,1472784690,'classtemp',1,'manage'),(83,13,'爱德校区详细页','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'',0,1,'Y-m-d',0,0,0,1472784318,'newstemp',1,'manage'),(69,19,'爱德首页图片+简介','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n<p>[!--smalltext--]</p>',1,1,'Y-m-d',0,0,0,1472782918,'bqtemp',1,'manage'),(74,13,'爱德分校图片+信息列表模板','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0,1472783295,'listtemp',1,'manage'),(75,13,'爱德校区详细页','[!--temp.adheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.adfooter--]',0,0,'',0,1,'Y-m-d H:i:s',0,0,0,1472783310,'newstemp',1,'manage'),(77,17,'爱德实时关注','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li class=\\\"clear\\\"><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--smalltext--]</span></li>',1,1,'Y-m-d',0,0,0,1472783428,'bqtemp',1,'manage'),(91,20,'爱德首页点亮德育','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<li><a href=\\\"[!--titleurl--]\\\" title=\\\"[!--oldtitle--]\\\">[!--title--]</a><span>[!--smalltext--]</span></li>',1,1,'Y-m-d',0,0,0,1472784661,'bqtemp',1,'manage'),(94,18,'爱德实时关注推荐','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<div><a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" width=\\\"310\\\" height=\\\"145\\\"></a>\r\n						<div>\r\n							<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n							<span>[!--smalltext--]...</span>\r\n						</div>\r\n					</div>',1,1,'Y-m-d',0,0,0,1472784844,'bqtemp',1,'manage'),(147,3,'czheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\">\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/style.css\\\">\r\n<script src=\\\"/skin/default/czxq/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/czxq/js/index_2.js\\\"></script>\r\n<title>城中分校</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"dc\\\">\r\n	<div class=\\\"header clear\\\">\r\n		<a href=\\\"/chengzhong/\\\">\r\n			<img src=\\\"/skin/default/czxq/images/logo.png\\\" alt=\\\"\\\"/>\r\n		</a>\r\n	</div>\r\n	<div class=\\\"banner clear\\\">\r\n		<img src=\\\"/skin/default/czxq/images/0001.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0002.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0003.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0004.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0005.jpg\\\" alt=\\\"\\\" />\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n                        <li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'城中分校头部',0,0,'',0,0,0,1474266255,'tempvar',1,'manage'),(149,5,'集团组成','[!--temp.header--]\r\n<div class=\\\"D_adcontent\\\">\r\n		\r\n	</div>\r\n	<div class=\\\"webnav\\\">\r\n		[!--newsnav--]\r\n	</div>\r\n	<div class=\\\"adbx\\\">\r\n		<style>\r\nul{\r\n	padding: 0;margin:0 ;\r\n}\r\nli{\r\n	list-style:none;\r\n}\r\n.img_box{\r\n	width:1200px;\r\n	margin:0 auto;\r\n}\r\n.img_box>ul{\r\n	width:100%;\r\n	overflow:hidden;\r\n}\r\n.img_box>ul>li{\r\n	float:left;overflow:hidden;\r\n}\r\n.text{\r\n	width:59px;\r\n	background:#ccc;\r\n	height:360px;\r\n	float:left;\r\n	position:relative;\r\n	cursor:pointer;\r\n	border-right:1px #fff solid;\r\n}\r\n.text>div{\r\n	position:absolute;\r\n	bottom:160px;\r\n	left:0;\r\n	right:0;\r\n	width:16px;\r\n	margin:0 auto;\r\n}\r\n.img1{\r\n	width:760px;\r\n	height:360px;\r\n	float:right;\r\n	display:none;\r\n}\r\n.img1>a img{\r\n	width:100%;\r\n	height:100%;\r\n}\r\n</style>\r\n<div class=\\\"img_box\\\">\r\n	<ul>\r\n    	<li><div class=\\\"text\\\"><div>城中校区</div></div><div class=\\\"img1\\\" style=\\\"display:block;\\\"><a href=\\\"/chengzhong/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/d1.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>爱德校区</div></div><div class=\\\"img1\\\"><a href=\\\"/aide/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/2.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>太湖校区</div></div><div class=\\\"img1\\\"><a href=\\\"/taihu/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/3.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n        <li><div class=\\\"text\\\"><div>苏州湾校区</div></div><div class=\\\"img1\\\"><a href=\\\"/szw/\\\" target=\\\"_blank\\\"><img src=\\\"/skin/default/images/fx/4.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n		<li><div class=\\\"text\\\"><div>幼儿园</div></div><div class=\\\"img1\\\"><a href=\\\"#\\\"><img src=\\\"5.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n		<li><div class=\\\"text\\\"><div>德音书院</div></div><div class=\\\"img1\\\"><a href=\\\"#\\\"><img src=\\\"6.jpg\\\" alt=\\\"\\\"/></a></div></li>\r\n    </ul>\r\n</div>\r\n<script>\r\n$(\\\".text\\\").click(function(){\r\n	$(\\\".img1\\\").hide();\r\n	$(this).next(\\\".img1\\\").show();\r\n})\r\n</script>\r\n	</div>[!--temp.footer--]',0,0,'',0,0,'',0,0,0,1474363770,'classtemp',1,'manage'),(123,8,'thheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\">\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<title>太湖校区</title>\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style_mld.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/taihu/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/taihu/js/index.js\\\"></script>\r\n</head>\r\n<body>\r\n<div class=\\\"dc\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n			<img src=\\\"/skin/default/taihu/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/taihu/>首 页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/taihu/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"banner clear\\\">\r\n		<img src=\\\"/skin/default/taihu/images/0002.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0001.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0003.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0004.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0005.jpg\\\" alt=\\\"\\\" />\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'太湖校区',0,0,'',0,0,0,1473154085,'tempvar',1,'manage'),(103,9,'thfooter','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学院信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-6345079</span>\r\n			<span>地址：江苏省吴江市松陵镇永康路84号</span>\r\n			<span>邮编：215200</span>\r\n			<span>Copy(c)吴江市实验小学2000-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'太湖校区底部',0,0,'',0,0,0,1473127308,'tempvar',1,'manage'),(115,3,'太湖分校','[!--temp.thheader--]\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"xyfw\\\">\r\n			<div class=\\\"xyfw_title c_title clear\\\">\r\n				<div>\r\n					<span>校园服务</span>\r\n				</div>\r\n				<span>\r\n				<a href=\\\"###\\\">MORE <span>+</span>\r\n				</a>\r\n				</span>\r\n			</div>\r\n			<div class=\\\"xyfw_c clear\\\">\r\n				<ul class=\\\"clear\\\">\r\n					\r\n[e:loop={\\\'select * from phome_enewsclass where classid in (46,47,48,49) order by classid\\\',20,24,0}]<li><div class=\\\"xyfw_c_b\\\">\r\n							<img src=\\\"<?=$bqr[classimg]?>\\\" alt=\\\"\\\">\r\n							<a href=\\\"<?=$bqr[classurl]?>\\\"><?=$bqr[classname]?></a>\r\n						</div></li>[/e:loop] \r\n				</ul>\r\n			</div>\r\n		</div>\r\n		<div class=\\\"xyzx\\\">\r\n			<div class=\\\"xyzx_title c_title clear\\\">\r\n				<div>\r\n					<span>新闻资讯</span>\r\n				</div>\r\n				</div>\r\n			<div class=\\\"xyzx_c clear\\\">\r\n				<div class=\\\"xyzx_c_l\\\">\r\n					<div class=\\\"xyzx_c_l_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>新闻动态</span>\r\n						</div>\r\n						<div>\r\n							<span>媒体报道</span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle0 clear\\\">\r\n						<div>\r\n							<a href=\\\"###\\\">\r\n							<img src=\\\"/skin/default/taihu/images/xyzx_info_1.jpg\\\" alt=\\\"\\\"/>\r\n							</a>\r\n							<div>吴江实验小学位于吴江市松陵镇永康路84号，源自创建于清光绪三十年（1904）的“私立爱德女子两等小学堂”和创建于清光绪三十四（1908年）的“吴江县立城区初等小学堂”。 </div>\r\n						</div>\r\n						<ul>[ecmsinfo]42,8,32,0,0,4,0[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle1 clear\\\">\r\n						<div>\r\n							<a href=\\\"###\\\">\r\n							<img src=\\\"/skin/default/taihu/images/xyzx_info_1.jpg\\\" alt=\\\"\\\"/>\r\n							</a>\r\n							<div>吴江实2验小学位于吴江市松陵镇永康路84号，源自创建于清光绪三十年（1904）的“私立爱德女子两等小学堂”和创建于清光绪三十四（1908年）的“吴江县立城区初等小学堂”。 </div>\r\n						</div>\r\n						<ul>[ecmsinfo]43,8,32,0,0,4,0[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n				</div>\r\n				<div class=\\\"xyzx_c_r\\\">\r\n					<div class=\\\"xyzx_c_r_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>最新公告 </span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/notice/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<ul class=\\\"xyzx_c_r_c\\\">\r\n						[ecmsinfo]44,4,32,0,0,12,0[/ecmsinfo]\r\n					</ul>\r\n					\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'',0,0,'',0,0,0,1473149288,'classtemp',1,'manage'),(116,11,'太湖分校首页头条','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'<a href=\\\"[!--titleurl--]\\\"><img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\"/></a>\r\n<div>[!--smalltext--]</div>',1,1,'Y-m-d H:i:s',0,0,0,1473149568,'bqtemp',1,'manage'),(126,11,'szwfooter','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学院信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-6345079</span>\r\n			<span>地址：江苏省吴江市松陵镇永康路84号</span>\r\n			<span>邮编：215200</span><br>\r\n			<span>Copy(c)吴江市实验小学2000-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'苏州湾校区底部',0,0,'',0,0,0,1473380537,'tempvar',1,'manage'),(112,12,'太湖分校首页公告','[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]',0,0,'$listtemp=\\\'<li>\r\n							<div>\r\n								<span>\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n								<span>\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n							</div>\r\n							<a href=\\\"[!--titleurl--]\\\" class=\\\"xyzx_c_r_c_fa\\\">[!--title--]</a>\r\n							<p>[!--smalltext--]</p>\r\n							<a href=\\\"[!--titleurl--]\\\">详细内容&gt;&gt;</a>\r\n						</li>\\\';',1,1,'Y-m-d H:i:s',0,0,0,1473148841,'bqtemp',1,'manage'),(133,10,'szwheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/szw/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/szw/js/index.js\\\"></script>\r\n<title>苏州湾校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/szw/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/szw/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/dlad/\\\">点亮苏州湾</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"/szw/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"/szw/dlad/\\\">点亮爱德</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/szw/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'苏州湾校区头部',0,0,'',0,0,0,1473381798,'tempvar',1,'dd'),(135,10,'szwheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/szw/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/szw/js/index.js\\\"></script>\r\n<title>苏州湾校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/szw/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/szw/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/dlad/\\\">点亮苏州湾</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"/szw/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"/szw/dlad/\\\">点亮苏州湾</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/szw/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'苏州湾校区头部',0,0,'',0,0,0,1473381862,'tempvar',1,'dd'),(105,14,'太湖分校新闻列表模板','[!--temp.thheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1,1473132896,'listtemp',1,'manage'),(106,14,'太湖分校新闻列表模板','[!--temp.thheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]40,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1,1473132916,'listtemp',1,'manage'),(107,15,'太湖分校图片+信息列表模板','[!--temp.thheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]40,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0,1473132943,'listtemp',1,'manage'),(108,14,'太湖校区详细页','[!--temp.thheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]40,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'',0,1,'Y-m-d',0,0,0,1473132994,'newstemp',1,'manage'),(117,3,'太湖分校','[!--temp.thheader--]\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"xyfw\\\">\r\n			<div class=\\\"xyfw_title c_title clear\\\">\r\n				<div>\r\n					<span>校园服务</span>\r\n				</div>\r\n				<span>\r\n				<a href=\\\"###\\\">MORE <span>+</span>\r\n				</a>\r\n				</span>\r\n			</div>\r\n			<div class=\\\"xyfw_c clear\\\">\r\n				<ul class=\\\"clear\\\">\r\n					\r\n[e:loop={\\\'select * from phome_enewsclass where classid in (46,47,48,49) order by classid\\\',20,24,0}]<li><div class=\\\"xyfw_c_b\\\">\r\n							<img src=\\\"<?=$bqr[classimg]?>\\\" alt=\\\"\\\">\r\n							<a href=\\\"<?=$bqr[classurl]?>\\\"><?=$bqr[classname]?></a>\r\n						</div></li>[/e:loop] \r\n				</ul>\r\n			</div>\r\n		</div>\r\n		<div class=\\\"xyzx\\\">\r\n			<div class=\\\"xyzx_title c_title clear\\\">\r\n				<div>\r\n					<span>新闻资讯</span>\r\n				</div>\r\n				</div>\r\n			<div class=\\\"xyzx_c clear\\\">\r\n				<div class=\\\"xyzx_c_l\\\">\r\n					<div class=\\\"xyzx_c_l_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>新闻动态</span>\r\n						</div>\r\n						<div>\r\n							<span>媒体报道</span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle0 clear\\\">\r\n						<div>[ecmsinfo]42,1,32,0,0,11,0,\\\'isgood=1\\\'[/ecmsinfo]</div>\r\n						<ul>[ecmsinfo]42,8,32,0,0,4,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle1 clear\\\">\r\n						<div>[ecmsinfo]43,1,32,0,0,11,0,\\\'isgood=1\\\'[/ecmsinfo]</div>\r\n						<ul>[ecmsinfo]43,8,32,0,0,4,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n				</div>\r\n				<div class=\\\"xyzx_c_r\\\">\r\n					<div class=\\\"xyzx_c_r_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>最新公告 </span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/notice/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<ul class=\\\"xyzx_c_r_c\\\">\r\n						[ecmsinfo]44,4,32,0,0,12,0[/ecmsinfo]\r\n					</ul>\r\n					\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'',0,0,'',0,0,0,1473149642,'classtemp',1,'manage'),(148,8,'thheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\">\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<title>太湖校区</title>\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style_mld.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/taihu/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/taihu/js/index.js\\\"></script>\r\n</head>\r\n<body>\r\n<div class=\\\"dc\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n			<img src=\\\"/skin/default/taihu/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/taihu/\\\">首 页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/taihu/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"banner clear\\\">\r\n		<img src=\\\"/skin/default/taihu/images/0002.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0001.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0003.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0004.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0005.jpg\\\" alt=\\\"\\\" />\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'太湖校区',0,0,'',0,0,0,1474361894,'tempvar',1,'manage'),(153,8,'thheader','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\">\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<title>太湖校区</title>\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style_mld.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/taihu/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/taihu/js/index.js\\\"></script>\r\n</head>\r\n<body>\r\n<div class=\\\"dc\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n			<img src=\\\"/skin/default/taihu/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/taihu/\\\">首 页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/kcdiy/\\\">德育天地</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/jspost/\\\">教育科研</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">教学之窗</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">精致校园</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/taihu/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/kcdiy/\\\">德育天地</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/jspost/\\\">教育科研</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/bj/\\\">教学之窗</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/xlspa/\\\">精致校园</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"banner clear\\\">\r\n		<img src=\\\"/skin/default/taihu/images/0002.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0001.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0003.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0004.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0005.jpg\\\" alt=\\\"\\\" />\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,'太湖校区',0,0,'',0,0,0,1474420637,'tempvar',1,'manage'),(152,3,'太湖分校','[!--temp.thheader--]\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"xyfw\\\">\r\n			<div class=\\\"xyfw_title c_title clear\\\">\r\n				<div>\r\n					<span>校园服务</span>\r\n				</div>\r\n				<span>\r\n				<a href=\\\"###\\\">MORE <span>+</span>\r\n				</a>\r\n				</span>\r\n			</div>\r\n			<div class=\\\"xyfw_c clear\\\">\r\n				<ul class=\\\"clear\\\">\r\n					\r\n[e:loop={\\\'select * from phome_enewsclass where classid in (46,47,48,49) order by classid\\\',20,24,0}]<li><div class=\\\"xyfw_c_b\\\">\r\n							<img src=\\\"<?=$bqr[classimg]?>\\\" alt=\\\"\\\">\r\n							<a href=\\\"<?=$bqr[classurl]?>\\\"><?=$bqr[classname]?></a>\r\n						</div></li>[/e:loop] \r\n				</ul>\r\n			</div>\r\n		</div>\r\n		<div class=\\\"xyzx\\\">\r\n			<div class=\\\"xyzx_title c_title clear\\\">\r\n				<div>\r\n					<span>新闻资讯</span>\r\n				</div>\r\n				</div>\r\n			<div class=\\\"xyzx_c clear\\\">\r\n				<div class=\\\"xyzx_c_l\\\">\r\n					<div class=\\\"xyzx_c_l_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>动感校园</span>\r\n						</div>\r\n						<div>\r\n							<span>媒体报道</span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle0 clear\\\">\r\n						<div>[ecmsinfo]42,1,32,0,0,11,0,\\\'isgood=1\\\'[/ecmsinfo]</div>\r\n						<ul>[ecmsinfo]42,8,32,0,0,4,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n					<div class=\\\"xyzx_c_l_c ontitle1 clear\\\">\r\n						<div>[ecmsinfo]43,1,32,0,0,11,0,\\\'isgood=1\\\'[/ecmsinfo]</div>\r\n						<ul>[ecmsinfo]43,8,32,0,0,4,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n						</ul>\r\n					</div>\r\n				</div>\r\n				<div class=\\\"xyzx_c_r\\\">\r\n					<div class=\\\"xyzx_c_r_title xyzx_c_title clear\\\">\r\n						<div class=\\\"ontitle\\\">\r\n							<span>通知公告 </span>\r\n						</div>\r\n						<span>\r\n						<a href=\\\"/taihu/news/notice/\\\">MORE <span>+</span>\r\n						</a>\r\n						</span>\r\n					</div>\r\n					<ul class=\\\"xyzx_c_r_c\\\">\r\n						[ecmsinfo]44,4,32,0,0,12,0[/ecmsinfo]\r\n					</ul>\r\n					\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.thfooter--]',0,0,'',0,0,'',0,0,0,1474420414,'classtemp',1,'manage'),(154,9,'thfooter','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学院信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-6345079</span>\r\n			<span>地址：江苏省苏州市吴江区开平路1666号</span>\r\n			<span>邮编：215200</span>\r\n			<span>Copy(c)吴江市实验小学2015-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,'太湖校区底部',0,0,'',0,0,0,1474420698,'tempvar',1,'manage'),(127,16,'苏州湾校区新闻列表模板','[!--temp.szwheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"dlad\\\">\r\n			<ul>\r\n				[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n			</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n		</div>\r\n	</div>\r\n[!--temp.szwfooter--]',0,0,'$listtemp=\\\'<li class=\\\"clear\\\">\r\n					<div class=\\\"datetime clear\\\">\r\n						<span class=\\\"dt_d\\\">\\\'.date(\\\"d\\\",$r[newstime]).\\\'</span>\r\n						<span class=\\\"dt_ym\\\">\\\'.date(\\\"Y-m\\\",$r[newstime]).\\\'</span>\r\n					</div>\r\n					<div class=\\\"dtle\\\">\r\n						<span><a href=\\\"[!--titleurl--]\\\">[!--title--]</a></span>\r\n						<p>[!--smalltext--] </p>\r\n					</div>\r\n				</li> \\\';',1,1,'Y-m-d',0,0,1,1473380570,'listtemp',1,'manage'),(128,17,'苏州湾校区图片+信息列表模板','[!--temp.szwheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_diy c_ulli c_ulli_f clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>[!--class.name--]</span>\r\n				</div>\r\n			</li>\r\n			[!--empirenews.listtemp--]<!--list.var1-->[!--empirenews.listtemp--]\r\n		</ul><div class=\\\"pages\\\">[!--show.listpage--]</div>\r\n	</div>\r\n[!--temp.szwfooter--]',0,0,'<li class=\\\"c_ulli clear\\\">\r\n				<img src=\\\"[!--titlepic--]\\\" alt=\\\"\\\">\r\n				<a href=\\\"[!--titleurl--]\\\">[!--title--]</a>\r\n				<p>[!--smalltext--]</p>\r\n			</li>',1,1,'Y-m-d',0,0,0,1473380651,'listtemp',1,'manage'),(129,4,'苏州湾校区','[!--temp.szwheader--]\r\n<div class=\\\"content\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_ssgz\\\">\r\n				<div class=\\\"c_ssgz_title clear\\\">\r\n					<span>时时关注</span>\r\n					<a href=\\\"/aide/ssgz/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<div class=\\\"c_ssgz_info clear\\\">\r\n					[ecmsinfo]37,1,32,0,0,18,0,\\\'isgood=1\\\'[/ecmsinfo]\r\n					<ul>\r\n						[ecmsinfo]37,7,32,0,0,17,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n					</ul>\r\n				</div>\r\n			</li>\r\n			<li class=\\\"c_dlad\\\">\r\n				<div class=\\\"c_dlad_title clear\\\">\r\n					<span>点亮德育</span>\r\n					<a href=\\\"/aide/dlad/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<ul>\r\n					[ecmsinfo]34,2,32,0,0,20,0[/ecmsinfo]\r\n				</ul>\r\n			</li>\r\n			<li class=\\\"c_diy c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>课程DIY</span>\r\n				</div>\r\n				[ecmsinfo]35,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_post c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>教师POST</span>\r\n				</div>\r\n				[ecmsinfo]36,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_gardend c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>班级GARDEND</span>\r\n				</div>\r\n				[ecmsinfo]38,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_spa c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>心理SPA</span>\r\n				</div>\r\n				[ecmsinfo]39,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n		</ul>\r\n	</div>\r\n[!--temp.szwfooter--]',0,0,'',0,0,'',0,0,0,1473380674,'classtemp',1,'manage'),(130,15,'苏州湾校区详细页','[!--temp.szwheader--]\r\n<div class=\\\"meun_bg\\\">\r\n		<div class=\\\"meun\\\">\r\n			<ul class=\\\"clear\\\" id=\\\"leftNav\\\">\r\n				[showclasstemp]33,1,0,0[/showclasstemp]\r\n			</ul><script language=\\\"javascript\\\">\r\n$(document).ready(\r\nfunction() {\r\n$(\\\"#leftNav li\\\").each(\r\n  function() {\r\nif($(this).attr(\\\"classid\\\")==\\\"[!--self.classid--]\\\") { $(this).addClass(\\\"onmeun\\\");}\r\n}\r\n);\r\n}\r\n);\r\n</script>\r\n		</div>\r\n	</div>\r\n<div class=\\\"content\\\">\r\n		<div class=\\\"webnav\\\">\r\n			[!--newsnav--]\r\n		</div>\r\n		<div class=\\\"detail_d\\\">\r\n			<div class=\\\"detail_d_title\\\">\r\n				<h2>[!--class.name--]</h2>\r\n				<div>\r\n					<span>发布时间:\r\n						<span>[!--newstime--]</span>\r\n					</span>\r\n					<span>点击量:\r\n						<span><script src=[!--news.url--]e/public/ViewClick/?classid=[!--classid--]&id=[!--id--]&addclick=1></script></span>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\\\"detail_d_p\\\">\r\n				<span>[!--newstext--]</span>\r\n			</div>\r\n		</div>\r\n	</div>\r\n[!--temp.szwfooter--]',0,0,'',0,1,'Y-m-d',0,0,0,1473380875,'newstemp',1,'dd'),(131,4,'苏州湾校区','[!--temp.szwheader--]\r\n<div class=\\\"content\\\">\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"c_ssgz\\\">\r\n				<div class=\\\"c_ssgz_title clear\\\">\r\n					<span>时时关注</span>\r\n					<a href=\\\"/aide/ssgz/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<div class=\\\"c_ssgz_info clear\\\">\r\n					[ecmsinfo]54,1,32,0,0,18,0,\\\'isgood=1\\\'[/ecmsinfo]\r\n					<ul>\r\n						[ecmsinfo]54,7,32,0,0,17,0,\\\'isgood<>1\\\'[/ecmsinfo]\r\n					</ul>\r\n				</div>\r\n			</li>\r\n			<li class=\\\"c_dlad\\\">\r\n				<div class=\\\"c_dlad_title clear\\\">\r\n					<span>点亮苏州湾</span>\r\n					<a href=\\\"/szw/dlad/\\\">更多&gt;&gt;</a>\r\n				</div>\r\n				<ul>\r\n					[ecmsinfo]51,2,32,0,0,20,0[/ecmsinfo]\r\n				</ul>\r\n			</li>\r\n			<li class=\\\"c_diy c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>课程DIY</span>\r\n				</div>\r\n				[ecmsinfo]52,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_post c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>教师POST</span>\r\n				</div>\r\n				[ecmsinfo]53,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_gardend c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>班级GARDEND</span>\r\n				</div>\r\n				[ecmsinfo]55,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n			<li class=\\\"c_spa c_ulli clear\\\">\r\n				<div class=\\\"clear\\\">\r\n					<span>心理SPA</span>\r\n				</div>\r\n				[ecmsinfo]56,1,32,0,0,19,0[/ecmsinfo]\r\n			</li>\r\n		</ul>\r\n	</div>\r\n[!--temp.szwfooter--]',0,0,'',0,0,'',0,0,0,1473381229,'classtemp',1,'dd');

/*Table structure for table `phome_enewstempdt` */

DROP TABLE IF EXISTS `phome_enewstempdt`;

CREATE TABLE `phome_enewstempdt` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempvar` char(30) NOT NULL DEFAULT '',
  `tempname` char(30) NOT NULL DEFAULT '',
  `tempsay` char(255) NOT NULL DEFAULT '',
  `tempfile` char(200) NOT NULL DEFAULT '',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `temptype` char(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`tempid`),
  UNIQUE KEY `tempvar` (`tempvar`),
  KEY `temptype` (`temptype`),
  KEY `myorder` (`myorder`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstempdt` */

insert  into `phome_enewstempdt`(`tempid`,`tempvar`,`tempname`,`tempsay`,`tempfile`,`myorder`,`temptype`) values (1,'header','主界面头部','主界面头部','e/template/incfile/header.php',0,'incfile'),(2,'footer','主界面尾部','主界面尾部','e/template/incfile/footer.php',0,'incfile'),(3,'qDoInfo','管理投稿首页','管理投稿首页','e/template/DoInfo/DoInfo.php',0,'doinfo'),(4,'qChangeClass','投稿选择栏目页','投稿选择栏目页','e/template/DoInfo/ChangeClass.php',0,'doinfo'),(5,'qDoInfoTran','发布投稿上传附件页面','发布投稿上传附件页面','e/template/DoInfo/tran.php',0,'doinfo'),(6,'qAddInfo','发布投稿页','发布投稿页','e/template/DoInfo/AddInfo.php',0,'doinfo'),(7,'qListInfo','默认管理投稿列表页','默认管理投稿列表页','e/data/html/list/qlistinfo.php',0,'doinfo'),(8,'report','提交错误报告页面','提交错误报告页面','e/template/public/report.php',0,'pubtemp'),(9,'payapi','在线支付页面','在线支付页面','e/template/payapi/payapi.php',0,'pubtemp'),(10,'infovote','信息投票页面','信息投票页面','e/template/public/vote.php',0,'pubtemp'),(11,'vote','投票插件页面','投票插件页面','e/template/tool/vote.php',0,'pubtemp'),(12,'ShopBurcar','购物车页面','购物车页面','e/template/ShopSys/buycar.php',0,'shopsys'),(13,'ShopOrder','提交订单页面','提交订单页面','e/template/ShopSys/order.php',0,'shopsys'),(14,'ShopSubmitOrder','确认提交订单页面','确认提交订单页面','e/template/ShopSys/SubmitOrder.php',0,'shopsys'),(15,'ShopListDd','订单列表页面','订单列表页面','e/template/ShopSys/ListDd.php',0,'shopsys'),(16,'ShopShowDd','订单详细页面','订单详细页面','e/template/ShopSys/ShowDd.php',0,'shopsys'),(17,'ShopBurcarForm','购物车-加入表单模板','购物车-加入表单模板','e/template/ShopSys/buycar/buycar_form.php',0,'shopsys'),(18,'ShopBurcarOrder','购物车-确认订单模板','购物车-确认订单模板','e/template/ShopSys/buycar/buycar_order.php',0,'shopsys'),(19,'ShopBurcarShowdd','购物车-显示订单模板','购物车-显示订单模板','e/template/ShopSys/buycar/buycar_showdd.php',0,'shopsys'),(20,'ShopAddAddress','增加配送地址页面','增加配送地址页面','e/template/ShopSys/AddAddress.php',0,'shopsys'),(21,'ShopListAddress','管理配送地址页面','管理配送地址页面','e/template/ShopSys/ListAddress.php',0,'shopsys'),(22,'MemberReg','会员注册页面','会员注册页面','e/template/member/register.php',0,'member'),(23,'MemberChangeReg','选择注册类型页面','选择注册类型页面','e/template/member/ChangeRegister.php',0,'member'),(24,'MemberRegsend','重发注册激活邮件页面','重发注册激活邮件页面','e/template/member/regsend.php',0,'member'),(25,'MemberLogin','会员登录页面','会员登录页面','e/template/member/login.php',0,'member'),(26,'MemberLoginopen','会员登录弹出页面','会员登录弹出页面','e/template/member/loginopen.php',0,'member'),(27,'MemberEditinfo','修改会员信息页面','修改会员信息页面','e/template/member/EditInfo.php',0,'member'),(28,'MemberEditsafeinfo','修改会员安全信息页面','修改会员安全信息页面','e/template/member/EditSafeInfo.php',0,'member'),(29,'MemberGetPassword','取回密码页面','取回密码页面','e/template/member/GetPassword.php',0,'member'),(30,'MemberGetResetPass','取回密码重置页面','取回密码重置页面','e/template/member/getpass.php',0,'member'),(31,'MemberCp','会员中心首页','会员中心首页','e/template/member/cp.php',0,'member'),(32,'MemberMy','会员状态页面','会员状态页面','e/template/member/my.php',0,'member'),(33,'MemberShowInfo','查看会员信息页面','查看会员信息页面','e/template/member/ShowInfo.php',0,'member'),(34,'MemberList1','默认会员列表页面','默认会员列表页面','e/template/member/memberlist/1.php',0,'member'),(35,'MemberAddMsg','发送站内消息页面','发送站内消息页面','e/template/member/AddMsg.php',0,'membermsg'),(36,'MemberMsg','站内消息列表页面','站内消息列表页面','e/template/member/msg.php',0,'membermsg'),(37,'MemberViewMsg','查看站内消息页面','查看站内消息页面','e/template/member/ViewMsg.php',0,'membermsg'),(38,'MemberAddFriend','增加好友页面','增加好友页面','e/template/member/AddFriend.php',0,'memberfriend'),(39,'MemberFriend','好友列表页面','好友列表页面','e/template/member/friend.php',0,'memberfriend'),(40,'MemberFriendClass','好友分类页面','好友分类页面','e/template/member/FriendClass.php',0,'memberfriend'),(41,'MemberChangeFriend','选择好友页面','选择好友页面','e/template/member/ChangeFriend.php',0,'memberfriend'),(42,'MemberAddFava','增加收藏信息页面','增加收藏信息页面','e/template/member/AddFava.php',0,'memberfav'),(43,'MemberFava','管理收藏页面','管理收藏页面','e/template/member/fava.php',0,'memberfav'),(44,'MemberFavaClass','管理收藏分类页面','管理收藏分类页面','e/template/member/FavaClass.php',0,'memberfav'),(45,'MemberBuybak','充值记录页面','充值记录页面','e/template/member/buybak.php',0,'memberother'),(46,'MemberDownbak','下载记录页面','下载记录页面','e/template/member/downbak.php',0,'memberother'),(47,'MemberBuygroup','购买会员类型页面','购买会员类型页面','e/template/member/buygroup.php',0,'memberother'),(48,'MemberCard','点卡充值页面','点卡充值页面','e/template/member/card.php',0,'memberother'),(49,'MemberChangeStyle','选择会员空间风格页面','选择会员空间风格页面','e/template/member/mspace/ChangeStyle.php',0,'memberspace'),(50,'MemberSetSpace','设置会员空间页面','设置会员空间页面','e/template/member/mspace/SetSpace.php',0,'memberspace'),(51,'MemberFeedback','管理会员空间反馈页面','管理会员空间反馈页面','e/template/member/mspace/feedback.php',0,'memberspace'),(52,'MemberShowFeedback','查看会员空间反馈页面','查看会员空间反馈页面','e/template/member/mspace/ShowFeedback.php',0,'memberspace'),(53,'MemberGbook','管理会员空间留言页面','管理会员空间留言页面','e/template/member/mspace/gbook.php',0,'memberspace'),(54,'MemberReGbook','回复会员空间留言页面','回复会员空间留言页面','e/template/member/mspace/ReGbook.php',0,'memberspace'),(55,'MemberConnectListBind','登录绑定管理页面','登录绑定管理页面','e/template/memberconnect/ListBind.php',0,'memberconnect'),(56,'MemberConnectTobind','绑定登录帐号页面','绑定登录帐号页面','e/template/memberconnect/tobind.php',0,'memberconnect');

/*Table structure for table `phome_enewstempgroup` */

DROP TABLE IF EXISTS `phome_enewstempgroup`;

CREATE TABLE `phome_enewstempgroup` (
  `gid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `gname` varchar(60) NOT NULL DEFAULT '',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstempgroup` */

insert  into `phome_enewstempgroup`(`gid`,`gname`,`isdefault`) values (1,'默认模板组',1);

/*Table structure for table `phome_enewstempvar` */

DROP TABLE IF EXISTS `phome_enewstempvar`;

CREATE TABLE `phome_enewstempvar` (
  `varid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `myvar` varchar(60) NOT NULL DEFAULT '',
  `varname` varchar(60) NOT NULL DEFAULT '',
  `varvalue` mediumtext NOT NULL,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `isclose` tinyint(1) NOT NULL DEFAULT '0',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`varid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstempvar` */

insert  into `phome_enewstempvar`(`varid`,`myvar`,`varname`,`varvalue`,`classid`,`isclose`,`myorder`) values (1,'header','页面头部','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/js/id.js\\\"></script>\r\n<title>吴江实验小学</title>\r\n</head>\r\n<body>\r\n<div class=\\\"D\\\">\r\n	<!--顶部-->\r\n	<div class=\\\"D_bg top\\\">\r\n		<div class=\\\"D_top clear\\\">\r\n			<ul>\r\n				<li>\r\n					<a href=\\\"index.htm/l\\\">返回首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">速递邮局</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"###\\\">办公OA</a>\r\n				</li>\r\n			</ul>\r\n			<div>\r\n				<a class=\\\"\\\" href=\\\"###\\\">常用信息</a>\r\n				<div>\r\n					<input class=\\\"D_top_text\\\" type=\\\"text\\\" value=\\\"\\\" />\r\n					<input class=\\\"D_top_btn\\\" type=\\\"button\\\" value=\\\"\\\" />\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<!--logo部分,导航部分-->\r\n	<div class=\\\"D_head clear\\\">\r\n		<div class=\\\"D_head_logo\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/images/logo.jpg\\\" alt=\\\"\\\">\r\n			</a>\r\n		</div>\r\n		<div class=\\\"D_head_nav\\\">\r\n			<img  class=\\\"D_head_nav_img\\\" src=\\\"/skin/default/images/nav1.png\\\" alt=\\\"\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li  class=\\\"onnav\\\">\r\n					<a href=\\\"/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">关于我们</a>\r\n					<dl>[showclasstemp]1,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/group/\\\">集团组成</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"/chengzhong/\\\" target=\\\"_blank\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/aide/\\\" target=\\\"_blank\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/taihu/\\\" target=\\\"_blank\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/szw/\\\" target=\\\"_blank\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">新闻动态</a>\r\n					<dl>[showclasstemp]8,13,0,0[/showclasstemp]</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">教师频道</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">管理平台</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"javascript:;\\\">资源中心</a>\r\n					<dl>\r\n						<dd>\r\n							<a href=\\\"#\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"#\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,1),(2,'footer','页面尾部','<div class=\\\"D_bg about\\\">\r\n		<div class=\\\"D_about\\\">\r\n			<ul class=\\\"clear\\\">\r\n				<li>\r\n					<dl>\r\n						<dt>关于我们</dt>\r\n						[showclasstemp]1,13,0,0[/showclasstemp]\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>集团组成</dt>\r\n						<dd>\r\n							<a href=\\\"/chengzhong/\\\">城中校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/aide/\\\">爱德校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"/taihu/\\\">太湖校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">苏州湾校区</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">幼儿园</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音书院</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>新闻动态</dt>\r\n						[showclasstemp]8,13,0,0[/showclasstemp]					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>教师频道</dt>\r\n						<dd>\r\n							<a  href=\\\"index.html\\\">学术委员会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">教育工会</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">德音讲坛</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">名师工作室</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">青年联盟</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>管理平台</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">文件阅办系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">校务管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">学生管理系统</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">课题管理系统</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li>\r\n					<dl>\r\n						<dt>资源中心</dt>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">网上图书馆</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">图视中心</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">求真半月刊</a>\r\n						</dd>\r\n						<dd>\r\n							<a href=\\\"index.html\\\">统计年鉴</a>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n				<li class=\\\"li_dis\\\">\r\n					<dl>\r\n						<dt>联系我们</dt>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<h2>0512-63970028</h2>\r\n						</dd>\r\n						<dd>\r\n							<span>E-mail：edu@wjsx.com</span>\r\n						</dd>\r\n						<dd>\r\n						</dd>\r\n					</dl>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n	</div>\r\n		<div class=\\\"D_bg bottom\\\">\r\n		<div class=\\\"D_bottom\\\">\r\n			<span>Copyright 2015 吴江实验小学 保留所有版权 苏ICP备10057875号</span>\r\n			<div class=\\\"D_bottom_links\\\">\r\n				<span class=\\\"D_bottom_links_btn\\\">相关连接</span>\r\n				<ul>[ecmsinfo]13,10,32,0,0,3,0[/ecmsinfo]</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,0),(3,'czheader','城中分校头部','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\">\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/czxq/css/style.css\\\">\r\n<script src=\\\"/skin/default/czxq/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/czxq/js/index_2.js\\\"></script>\r\n<title>城中分校</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"dc\\\">\r\n	<div class=\\\"header clear\\\">\r\n		<a href=\\\"/chengzhong/\\\">\r\n			<img src=\\\"/skin/default/czxq/images/logo.png\\\" alt=\\\"\\\"/>\r\n		</a>\r\n	</div>\r\n	<div class=\\\"banner clear\\\">\r\n		<img src=\\\"/skin/default/czxq/images/0001.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0002.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0003.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0004.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/czxq/images/0005.jpg\\\" alt=\\\"\\\" />\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n                        <li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,0),(4,'czfooter','城中分校底部','<div class=\\\"footer\\\">\r\n		<p>地址：江苏省吴江市松陵镇永康路84号  邮编：215200</p>\r\n		<p>Copy(c) 吴江市实验小学2000-2016</p>\r\n		<p>All Rights Reserved 苏ICP备10057875号</p>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>\r\n',0,0,0),(5,'adheader','爱德分校头部','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/aide/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/aide/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/aide/js/index.js\\\"></script>\r\n<title>爱德校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/aide/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/aide/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/dlad/\\\">点亮爱德</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/bj/\\\">班级GARDEN</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/aide/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"/aide/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"/aide/dlad/\\\">点亮爱德</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"/aide/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"/aide/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/ssgz/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/aide/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/aide/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/aide/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,0),(8,'thheader','太湖校区','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\">\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<title>太湖校区</title>\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/style_mld.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/taihu/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/taihu/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/taihu/js/index.js\\\"></script>\r\n</head>\r\n<body>\r\n<div class=\\\"dc\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n			<img src=\\\"/skin/default/taihu/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/taihu/\\\">首 页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/kcdiy/\\\">德育天地</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/jspost/\\\">教育科研</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">教学之窗</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/taihu/xlspa/\\\">精致校园</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/taihu/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/about/\\\">关于我们</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/kcdiy/\\\">德育天地</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/jspost/\\\">教育科研</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/news/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/bj/\\\">教学之窗</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"\\\">\r\n						<span>\r\n						<a href=\\\"/taihu/xlspa/\\\">精致校园</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class=\\\"banner clear\\\">\r\n		<img src=\\\"/skin/default/taihu/images/0002.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0001.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0003.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0004.jpg\\\" alt=\\\"\\\" />\r\n		<img src=\\\"/skin/default/taihu/images/0005.jpg\\\" alt=\\\"\\\" />\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,0),(7,'adfooter','爱德底部','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学校信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-63970028</span>\r\n			<span>地址：江苏省吴江区太湖新城吴模路1915号</span>\r\n			<span>邮编：215200</span><br>\r\n			<span>Copy(c)吴江市实验小学2015-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,0),(9,'thfooter','太湖校区底部','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学院信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-6345079</span>\r\n			<span>地址：江苏省苏州市吴江区开平路1666号</span>\r\n			<span>邮编：215200</span>\r\n			<span>Copy(c)吴江市实验小学2015-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,0),(10,'szwheader','苏州湾校区头部','<!doctype html>\r\n<html>\r\n<head>\r\n<meta charset=\\\"utf-8\\\"/>\r\n<meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/reset.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/style.css\\\">\r\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/skin/default/szw/css/mediacss.css\\\">\r\n<script src=\\\"/skin/default/szw/js/jq.js\\\"></script>\r\n<script src=\\\"/skin/default/szw/js/index.js\\\"></script>\r\n<title>苏州湾校区</title>\r\n</head>\r\n\r\n<body>\r\n<div class=\\\"d_c\\\">\r\n	<div class=\\\"header_bg\\\">\r\n		<div class=\\\"header clear\\\">\r\n			<a href=\\\"index.html\\\">\r\n				<img src=\\\"/skin/default/szw/images/logo.png\\\" alt=\\\"\\\"/>\r\n			</a>\r\n			<ul class=\\\"clear\\\">\r\n				<li class=\\\"onnav\\\">\r\n					<a href=\\\"/szw/\\\">首页</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/dlad/\\\">点亮苏州湾</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n				</li>\r\n				<li>\r\n					<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n				</li>\r\n			</ul>\r\n			<div class=\\\"nav_btn\\\">\r\n				<img src=\\\"/skin/default/aide/images/nav_btn.png\\\" alt=\\\"\\\"/>\r\n				<ul class=\\\"nav\\\">\r\n					<li class=\\\"icon_index\\\">\r\n						<span>\r\n							<a href=\\\"/szw/\\\">首页</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_kj\\\">\r\n						<span>\r\n							<a href=\\\"/szw/dlad/\\\">点亮苏州湾</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_dt\\\">\r\n						<span>\r\n							<a href=\\\"/szw/kcdiy/\\\">课程DIY</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_wh\\\">\r\n						<span>\r\n							<a href=\\\"/szw/jspost/\\\">教师POST</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/ssgz/\\\">时时关注</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/bj/\\\">班级GARDEND</a>\r\n						</span>\r\n					</li>\r\n					<li class=\\\"icon_ms\\\">\r\n						<span>\r\n							<a href=\\\"/szw/xlspa/\\\">心理SPA</a>\r\n						</span>\r\n					</li>\r\n				</ul>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<!--banner-->\r\n	<div class=\\\"D_banner clear\\\">\r\n		<img src=\\\"/skin/default/szw/images/0001.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0002.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0003.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0004.jpg\\\"  alt=\\\"\\\"/>\r\n		<img src=\\\"/skin/default/szw/images/0005.jpg\\\"  alt=\\\"\\\"/>\r\n		<ul class=\\\"clear\\\">\r\n			<li class=\\\"onli\\\"></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n			<li></li>\r\n		</ul>\r\n	</div>',0,0,0),(11,'szwfooter','苏州湾校区底部','<div class=\\\"footer_bg\\\">\r\n		<div class=\\\"footer\\\">\r\n			<span>学院信箱：service@wjsx.com</span>\r\n			<span>校长信箱：wjsx@wjsx.com</span>\r\n			<span>联系电话：0512-6345079</span>\r\n			<span>地址：江苏省吴江市松陵镇永康路84号</span>\r\n			<span>邮编：215200</span><br>\r\n			<span>Copy(c)吴江市实验小学2000-2016</span>\r\n			<span>All Rights Reserved 苏ICP备10057875号</span>\r\n		</div>\r\n	</div>\r\n</div>\r\n</body>\r\n</html>',0,0,0);

/*Table structure for table `phome_enewstempvarclass` */

DROP TABLE IF EXISTS `phome_enewstempvarclass`;

CREATE TABLE `phome_enewstempvarclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstempvarclass` */

/*Table structure for table `phome_enewstogzts` */

DROP TABLE IF EXISTS `phome_enewstogzts`;

CREATE TABLE `phome_enewstogzts` (
  `togid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyboard` varchar(255) NOT NULL DEFAULT '',
  `searchf` varchar(255) NOT NULL DEFAULT '',
  `query` text NOT NULL,
  `specialsearch` varchar(255) NOT NULL DEFAULT '',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `retype` tinyint(1) NOT NULL DEFAULT '0',
  `startday` varchar(20) NOT NULL DEFAULT '',
  `endday` varchar(20) NOT NULL DEFAULT '',
  `startid` int(10) unsigned NOT NULL DEFAULT '0',
  `endid` int(10) unsigned NOT NULL DEFAULT '0',
  `pline` int(11) NOT NULL DEFAULT '0',
  `doecmszt` tinyint(1) NOT NULL DEFAULT '0',
  `togztname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`togid`),
  KEY `togztname` (`togztname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewstogzts` */

/*Table structure for table `phome_enewsuser` */

DROP TABLE IF EXISTS `phome_enewsuser`;

CREATE TABLE `phome_enewsuser` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `rnd` varchar(20) NOT NULL DEFAULT '',
  `adminclass` mediumtext NOT NULL,
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `styleid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `filelevel` tinyint(1) NOT NULL DEFAULT '0',
  `salt` varchar(8) NOT NULL DEFAULT '',
  `loginnum` int(10) unsigned NOT NULL DEFAULT '0',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(20) NOT NULL DEFAULT '',
  `truename` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(120) NOT NULL DEFAULT '',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pretime` int(10) unsigned NOT NULL DEFAULT '0',
  `preip` varchar(20) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `addip` varchar(20) NOT NULL DEFAULT '',
  `userprikey` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuser` */

insert  into `phome_enewsuser`(`userid`,`username`,`password`,`rnd`,`adminclass`,`groupid`,`checked`,`styleid`,`filelevel`,`salt`,`loginnum`,`lasttime`,`lastip`,`truename`,`email`,`classid`,`pretime`,`preip`,`addtime`,`addip`,`userprikey`) values (1,'manage','d2cdacff1fe69551d62ed61365df062b','MNRmUGJsSWMZNtZLwjVG','',1,0,1,0,'BS3UYdTQ',47,1476622389,'127.0.0.1','','',0,1476267216,'127.0.0.1',1471844557,'::1','vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvhhhhhhh'),(2,'admin','b0d631f7592b60f5fc6719e123b581ce','7VpALKk465ZE2u5yBKLL','|',1,0,2,0,'NDm9kQvd',7,1476002665,'58.211.212.154','','',0,1476002608,'58.211.212.154',1472174636,'::1','eCqRUbtcrdBE2GdkV7ifffm7PkKytVA9XeuBBgRRA2zgALRC'),(6,'城中','9c8253827f8d190faaf500c3bff09d1f','vnWHz2hUEbNqQsa8nT5i','|16|17|19|20|21|23|24|26|27|28|30|31|32|',2,0,1,0,'EnaQJEvY',6,1476169523,'218.4.59.173','','',0,1476147534,'218.4.59.173',1473742819,'58.210.80.187','iB4zkHhA6xYfKHeiqG76A5SH8DbQs9KbN6jjSEsZf4t8hQPX'),(4,'爱德','57c5d52f10f6a99838686fdb3fb3e80a','esgJjPvCFXyWcLgK8Mmh','|34|35|36|37|38|39|',2,0,1,1,'jkr4suxM',12,1475915883,'221.224.114.98','','',0,1475915733,'221.224.114.98',1473398853,'58.210.80.187','K96pVktQuMpKAUyHxRfRsrd6ak5HuXrdmWv3b3pcm8MHds95'),(5,'苏州湾','d11f75891946027bfb36c8fead1e6a31','pZTGe3ZiWmtgEiZJYdab','|51|52|53|54|55|56|',2,0,1,1,'thWDfnSi',4,1475891559,'58.210.80.187','沈业凡','',0,1473728226,'58.210.80.187',1473727843,'58.210.80.187','XqPeasc49UtWCzQfsK9hh2vLfaT8PSAtH6JFAF7VMCdmEYZt'),(7,'管文锦','f4e993b8869430463c7a37fe2cfa807e','CErRrUeVza5Ki8LZ5aML','|9|10|',2,0,1,0,'brHQ7TBT',9,1476235442,'58.210.80.187','','',0,1475215055,'58.210.80.187',1474420053,'58.210.80.187','dPm8kRJkZ7WYWgegLxRLqfbegv8sGGrDQJdbQXq2A6BukN6Z');

/*Table structure for table `phome_enewsuseradd` */

DROP TABLE IF EXISTS `phome_enewsuseradd`;

CREATE TABLE `phome_enewsuseradd` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `equestion` tinyint(4) NOT NULL DEFAULT '0',
  `eanswer` varchar(32) NOT NULL DEFAULT '',
  `openip` text NOT NULL,
  `certkey` varchar(60) NOT NULL DEFAULT '',
  `certtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuseradd` */

insert  into `phome_enewsuseradd`(`userid`,`equestion`,`eanswer`,`openip`,`certkey`,`certtime`) values (1,0,'','','',0),(2,0,'','','',0),(6,0,'','','',0),(4,0,'','','',0),(5,0,'','','',0),(7,0,'','','',0);

/*Table structure for table `phome_enewsuserclass` */

DROP TABLE IF EXISTS `phome_enewsuserclass`;

CREATE TABLE `phome_enewsuserclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuserclass` */

/*Table structure for table `phome_enewsuserjs` */

DROP TABLE IF EXISTS `phome_enewsuserjs`;

CREATE TABLE `phome_enewsuserjs` (
  `jsid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `jsname` varchar(60) NOT NULL DEFAULT '',
  `jssql` text NOT NULL,
  `jstempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `jsfilename` varchar(120) NOT NULL DEFAULT '',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`jsid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuserjs` */

/*Table structure for table `phome_enewsuserjsclass` */

DROP TABLE IF EXISTS `phome_enewsuserjsclass`;

CREATE TABLE `phome_enewsuserjsclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuserjsclass` */

/*Table structure for table `phome_enewsuserlist` */

DROP TABLE IF EXISTS `phome_enewsuserlist`;

CREATE TABLE `phome_enewsuserlist` (
  `listid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `listname` varchar(60) NOT NULL DEFAULT '',
  `pagetitle` varchar(120) NOT NULL DEFAULT '',
  `filepath` varchar(120) NOT NULL DEFAULT '',
  `filetype` varchar(12) NOT NULL DEFAULT '',
  `totalsql` text NOT NULL,
  `listsql` text NOT NULL,
  `maxnum` int(11) NOT NULL DEFAULT '0',
  `lencord` int(11) NOT NULL DEFAULT '0',
  `listtempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pagekeywords` varchar(255) NOT NULL DEFAULT '',
  `pagedescription` varchar(255) NOT NULL DEFAULT '',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`listid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuserlist` */

/*Table structure for table `phome_enewsuserlistclass` */

DROP TABLE IF EXISTS `phome_enewsuserlistclass`;

CREATE TABLE `phome_enewsuserlistclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuserlistclass` */

/*Table structure for table `phome_enewsuserloginck` */

DROP TABLE IF EXISTS `phome_enewsuserloginck`;

CREATE TABLE `phome_enewsuserloginck` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `andauth` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsuserloginck` */

insert  into `phome_enewsuserloginck`(`userid`,`andauth`) values (3,'f8be0f7ffd8eb5cabe3dc52f98b4de17'),(4,'abc86c8c456d3503bdd4bea618731c4f'),(6,'1d851bf2c1d954f89ac93b4cb5f52e21'),(7,'05a9f698511d01765aa07adea70b973f'),(1,'989ce61e9663e86e0a8419072b081de1'),(5,'d5877a62dd1cf69106c27a82b6519cbe'),(2,'c7c69ee24c622f0053d92ab8d5ac8495');

/*Table structure for table `phome_enewsvote` */

DROP TABLE IF EXISTS `phome_enewsvote`;

CREATE TABLE `phome_enewsvote` (
  `voteid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL DEFAULT '',
  `votenum` int(10) unsigned NOT NULL DEFAULT '0',
  `voteip` mediumtext NOT NULL,
  `votetext` text NOT NULL,
  `voteclass` tinyint(1) NOT NULL DEFAULT '0',
  `doip` tinyint(1) NOT NULL DEFAULT '0',
  `votetime` int(10) unsigned NOT NULL DEFAULT '0',
  `dotime` date NOT NULL DEFAULT '0000-00-00',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `addtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`voteid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsvote` */

/*Table structure for table `phome_enewsvotemod` */

DROP TABLE IF EXISTS `phome_enewsvotemod`;

CREATE TABLE `phome_enewsvotemod` (
  `voteid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL DEFAULT '',
  `votetext` text NOT NULL,
  `voteclass` tinyint(1) NOT NULL DEFAULT '0',
  `doip` tinyint(1) NOT NULL DEFAULT '0',
  `dotime` date NOT NULL DEFAULT '0000-00-00',
  `tempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `votenum` int(10) unsigned NOT NULL DEFAULT '0',
  `ysvotename` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`voteid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsvotemod` */

/*Table structure for table `phome_enewsvotetemp` */

DROP TABLE IF EXISTS `phome_enewsvotetemp`;

CREATE TABLE `phome_enewsvotetemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `temptext` mediumtext NOT NULL,
  PRIMARY KEY (`tempid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsvotetemp` */

insert  into `phome_enewsvotetemp`(`tempid`,`tempname`,`temptext`) values (1,'默认投票模板','<table width=100% border=0 align=center cellpadding=3 cellspacing=0><form name=enewsvote method=POST action=\'[!--vote.action--]\' target=_blank><tr><td><strong>[!--title--]</strong></td></tr><input type=hidden name=voteid value=\'[!--voteid--]\'><input type=hidden name=enews value=AddVote>\r\n[!--empirenews.listtemp--]\r\n<tr><td>[!--vote.box--]&nbsp;[!--vote.name--]</td></tr>\r\n[!--empirenews.listtemp--]\r\n<tr><td align=center><input type=submit name=submit value=投票>&nbsp;&nbsp;<input type=button name=button value=查看结果 onclick=javascript:window.open(\'[!--vote.view--]\',\'\',\'width=[!--width--],height=[!--height--],scrollbars=yes\');></td></tr></form></table>'),(2,'默认信息投票模板','<table width=\\\'100%\\\' border=0 align=center cellpadding=3 cellspacing=0><form name=enewsvote method=POST action=\\\'[!--news.url--]e/enews/index.php\\\' target=_blank><tr><td><strong>[!--title--]</strong></td></tr><input type=hidden name=id value=\\\'[!--id--]\\\'><input type=hidden name=classid value=\\\'[!--classid--]\\\'><input type=hidden name=enews value=AddInfoVote>\r\n[!--empirenews.listtemp--]\r\n<tr><td>[!--vote.box--]&nbsp;[!--vote.name--]</td></tr>\r\n[!--empirenews.listtemp--]\r\n<tr><td align=center><input type=submit name=submit value=投票>&nbsp;&nbsp;<input type=button name=button value=查看结果 onclick=javascript:window.open(\\\'[!--news.url--]e/public/vote/?classid=[!--classid--]&id=[!--id--]\\\',\\\'\\\',\\\'width=[!--width--],height=[!--height--],scrollbars=yes\\\');></td></tr></form></table>');

/*Table structure for table `phome_enewswapstyle` */

DROP TABLE IF EXISTS `phome_enewswapstyle`;

CREATE TABLE `phome_enewswapstyle` (
  `styleid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `stylename` varchar(60) NOT NULL DEFAULT '',
  `path` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`styleid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewswapstyle` */

insert  into `phome_enewswapstyle`(`styleid`,`stylename`,`path`) values (1,'新闻模板',1),(2,'分类信息模板',2);

/*Table structure for table `phome_enewswfinfo` */

DROP TABLE IF EXISTS `phome_enewswfinfo`;

CREATE TABLE `phome_enewswfinfo` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `wfid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tid` int(10) unsigned NOT NULL DEFAULT '0',
  `groupid` text NOT NULL,
  `userclass` text NOT NULL,
  `username` text NOT NULL,
  `checknum` tinyint(4) NOT NULL DEFAULT '0',
  `tstatus` varchar(30) NOT NULL DEFAULT '0',
  `checktno` tinyint(4) NOT NULL DEFAULT '0',
  KEY `id` (`id`,`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewswfinfo` */

/*Table structure for table `phome_enewswfinfolog` */

DROP TABLE IF EXISTS `phome_enewswfinfolog`;

CREATE TABLE `phome_enewswfinfolog` (
  `logid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `wfid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tid` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `checktime` int(10) unsigned NOT NULL DEFAULT '0',
  `checktext` text NOT NULL,
  `checknum` tinyint(4) NOT NULL DEFAULT '0',
  `checktype` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`logid`),
  KEY `id` (`id`,`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewswfinfolog` */

/*Table structure for table `phome_enewswords` */

DROP TABLE IF EXISTS `phome_enewswords`;

CREATE TABLE `phome_enewswords` (
  `wordid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `oldword` varchar(255) NOT NULL DEFAULT '',
  `newword` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`wordid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewswords` */

/*Table structure for table `phome_enewsworkflow` */

DROP TABLE IF EXISTS `phome_enewsworkflow`;

CREATE TABLE `phome_enewsworkflow` (
  `wfid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `wfname` varchar(60) NOT NULL DEFAULT '',
  `wftext` varchar(255) NOT NULL DEFAULT '',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `adduser` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`wfid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsworkflow` */

/*Table structure for table `phome_enewsworkflowitem` */

DROP TABLE IF EXISTS `phome_enewsworkflowitem`;

CREATE TABLE `phome_enewsworkflowitem` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wfid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `tname` varchar(60) NOT NULL DEFAULT '',
  `tno` int(11) NOT NULL DEFAULT '0',
  `ttext` varchar(255) NOT NULL DEFAULT '',
  `groupid` text NOT NULL,
  `userclass` text NOT NULL,
  `username` text NOT NULL,
  `lztype` tinyint(1) NOT NULL DEFAULT '0',
  `tbdo` int(10) unsigned NOT NULL DEFAULT '0',
  `tddo` int(10) unsigned NOT NULL DEFAULT '0',
  `tstatus` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`tid`),
  KEY `wfid` (`wfid`),
  KEY `tno` (`tno`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsworkflowitem` */

/*Table structure for table `phome_enewswriter` */

DROP TABLE IF EXISTS `phome_enewswriter`;

CREATE TABLE `phome_enewswriter` (
  `wid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `writer` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(120) NOT NULL DEFAULT '',
  PRIMARY KEY (`wid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewswriter` */

insert  into `phome_enewswriter`(`wid`,`writer`,`email`) values (1,'申晓峰','mailto:sxf@wjsx.com');

/*Table structure for table `phome_enewsyh` */

DROP TABLE IF EXISTS `phome_enewsyh`;

CREATE TABLE `phome_enewsyh` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `yhname` varchar(30) NOT NULL DEFAULT '',
  `yhtext` varchar(255) NOT NULL DEFAULT '',
  `hlist` int(11) NOT NULL DEFAULT '0',
  `qlist` int(11) NOT NULL DEFAULT '0',
  `bqnew` int(11) NOT NULL DEFAULT '0',
  `bqhot` int(11) NOT NULL DEFAULT '0',
  `bqpl` int(11) NOT NULL DEFAULT '0',
  `bqgood` int(11) NOT NULL DEFAULT '0',
  `bqfirst` int(11) NOT NULL DEFAULT '0',
  `bqdown` int(11) NOT NULL DEFAULT '0',
  `otherlink` int(11) NOT NULL DEFAULT '0',
  `qmlist` int(11) NOT NULL DEFAULT '0',
  `dobq` tinyint(1) NOT NULL DEFAULT '0',
  `dojs` tinyint(1) NOT NULL DEFAULT '0',
  `dosbq` tinyint(1) NOT NULL DEFAULT '0',
  `rehtml` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsyh` */

/*Table structure for table `phome_enewszt` */

DROP TABLE IF EXISTS `phome_enewszt`;

CREATE TABLE `phome_enewszt` (
  `ztid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `ztname` varchar(60) NOT NULL DEFAULT '',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `ztnum` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `listtempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ztpath` varchar(255) NOT NULL DEFAULT '',
  `zttype` varchar(10) NOT NULL DEFAULT '',
  `zturl` varchar(200) NOT NULL DEFAULT '',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `islist` tinyint(1) NOT NULL DEFAULT '0',
  `maxnum` int(11) NOT NULL DEFAULT '0',
  `reorder` varchar(50) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `ztimg` varchar(255) NOT NULL DEFAULT '',
  `zcid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `showzt` tinyint(1) NOT NULL DEFAULT '0',
  `ztpagekey` varchar(255) NOT NULL DEFAULT '',
  `classtempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usezt` tinyint(1) NOT NULL DEFAULT '0',
  `yhid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  `closepl` tinyint(1) NOT NULL DEFAULT '0',
  `checkpl` tinyint(1) NOT NULL DEFAULT '0',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `usernames` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `pltempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ztid`),
  KEY `classid` (`classid`),
  KEY `zcid` (`zcid`),
  KEY `usezt` (`usezt`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewszt` */

/*Table structure for table `phome_enewsztadd` */

DROP TABLE IF EXISTS `phome_enewsztadd`;

CREATE TABLE `phome_enewsztadd` (
  `ztid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classtext` mediumtext NOT NULL,
  PRIMARY KEY (`ztid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsztadd` */

/*Table structure for table `phome_enewsztclass` */

DROP TABLE IF EXISTS `phome_enewsztclass`;

CREATE TABLE `phome_enewsztclass` (
  `classid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsztclass` */

/*Table structure for table `phome_enewsztf` */

DROP TABLE IF EXISTS `phome_enewsztf`;

CREATE TABLE `phome_enewsztf` (
  `fid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f` varchar(30) NOT NULL DEFAULT '',
  `fname` varchar(30) NOT NULL DEFAULT '',
  `fform` varchar(20) NOT NULL DEFAULT '',
  `fhtml` mediumtext NOT NULL,
  `fzs` varchar(255) NOT NULL DEFAULT '',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ftype` varchar(30) NOT NULL DEFAULT '',
  `flen` varchar(20) NOT NULL DEFAULT '',
  `fvalue` text NOT NULL,
  `fformsize` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsztf` */

/*Table structure for table `phome_enewsztinfo` */

DROP TABLE IF EXISTS `phome_enewsztinfo`;

CREATE TABLE `phome_enewsztinfo` (
  `zid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ztid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `mid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `isgood` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`zid`),
  KEY `ztid` (`ztid`),
  KEY `cid` (`cid`),
  KEY `classid` (`classid`),
  KEY `id` (`id`),
  KEY `newstime` (`newstime`),
  KEY `mid` (`mid`),
  KEY `isgood` (`isgood`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewsztinfo` */

/*Table structure for table `phome_enewszttype` */

DROP TABLE IF EXISTS `phome_enewszttype`;

CREATE TABLE `phome_enewszttype` (
  `cid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ztid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cname` varchar(20) NOT NULL DEFAULT '',
  `myorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `islist` tinyint(1) NOT NULL DEFAULT '0',
  `listtempid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `maxnum` int(10) unsigned NOT NULL DEFAULT '0',
  `tnum` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `reorder` varchar(50) NOT NULL DEFAULT '',
  `ttype` varchar(10) NOT NULL DEFAULT '',
  `endtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  KEY `ztid` (`ztid`),
  KEY `myorder` (`myorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewszttype` */

/*Table structure for table `phome_enewszttypeadd` */

DROP TABLE IF EXISTS `phome_enewszttypeadd`;

CREATE TABLE `phome_enewszttypeadd` (
  `cid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `classtext` mediumtext NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phome_enewszttypeadd` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
